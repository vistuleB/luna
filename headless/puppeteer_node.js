const puppeteer = require('puppeteer');
const PCR = require("puppeteer-chromium-resolver");

async function loadPage(url, chromiumExecutablePath) {
    const args = (chromiumExecutablePath != null) ? { headless: true, executablePath: chromiumExecutablePath } : { headless: true };
    const browser = await puppeteer.launch(args);
    const page = await browser.newPage();
    await page.goto(url, { waitUntil: 'networkidle0' });
    html = await page.content();
    await browser.close();
    return html;
}

async function start() {
    const html = await loadPage(url, (!stats) ? null : stats.executablePath);
    console.log(html);
}

const url = (process.argv.length >= 3) ? "file://" + process.argv[2] : undefined;
if (url === undefined) throw Error("puppeteer_node missing file name");
const stats = PCR.getStats({});

start();
