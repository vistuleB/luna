import { parse } from "https://deno.land/std@0.83.0/flags/mod.ts";
import puppeteer from "https://deno.land/x/puppeteer@14.1.1/mod.ts";
import "https://deno.land/x/dotenv@v3.2.0/load.ts";

async function getHtmlFromUrl(url : string) {
  const browser = await puppeteer.launch({
    executablePath: '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    args: ["--no-sandbox", "--disable-setuid-sandbox"],
    headless: true
  });
  let html = "";
  try {
    const page = await browser.newPage();
    await page.goto(url, { waitUntil: 'networkidle0' });
    html = await page.content();
  } catch (e) {
    console.log(e);
  } finally {
    await browser.close();
  }
  return html;
}

async function run() {
  const args = parse(Deno.args);
  const url = args._[0] as string;
  const html = await getHtmlFromUrl(url)
  console.log(html)
}

run();
