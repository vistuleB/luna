from lib_lUnAtIc import lUnAtIc


def an_inline_svg():
    root = lUnAtIc('svg.preserveAspectRatio=none')
    return root


def an_external_svg():
    root = an_inline_svg()
    root['xmlns'] = "http://www.w3.org/2000/svg"
    root['xmlns:xlink'] = "http://www.w3.org/1999/xlink"
    return root


def an_inkscape_svg(narrow=False):
    return an_external_svg().add_inkscape_stuff(narrow=narrow)


def an_svg(string='inline'):
    if string == 'inline':
        return an_inline_svg()
    if string == 'external':
        return an_external_svg()
    if string == 'inkscape':
        return an_inkscape_svg(narrow=False)
    if string == 'narrow-inkscape':
        return an_inkscape_svg(narrow=True)
    assert False


def an_ordinary_layer(name):
    assert '.' not in name
    return lUnAtIc('g#' + name)


def an_inkscape_layer(name):
    assert '.' not in name
    return lUnAtIc('g',
                   {'inkscape:groupmode': 'layer',
                    'inkscape:label': name,
                    'id': name})
