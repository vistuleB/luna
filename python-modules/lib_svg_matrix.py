import numpy as np
import re

from svgpathsio import Line, parse_transform, generate_transform, real_numbers_in
from math import sin, cos, sqrt, floor, radians, tan, atan, asin, ceil, acos, atan2, pow
from numbers import Real, Complex
from copy import copy
from itertools import product
from random import gauss, randint


tau = 6.283185307179586
eta = tau / 4


def randsign():
    return 2 * randint(0, 1) - 1


def randint_signed(a, b):
    return randsign() * randint(min(a, b), max(a, b))


def sign(x):
    if x > 0:
        return 1

    if x < 0:
        return -1

    assert False


def sign_with_zero(x):
    if x > 0:
        return 1

    if x < 0:
        return -1

    if x == 0:
        return 0

    assert False


def truncate_abs_val(x, a):
    assert a > 0
    if x > a:
        return a
    if x < -a:
        return -a
    return x


def round_up_to_odd_multiple(x, n):
    assert x > 0
    z = ceil(x / n)
    if z % 2 == 0:
        z += 1
    return n * z, z


def soft_int_else_float(z):
    if isinstance(z, int):
        return z
    return float(z)


def int_else_float(z):
    z = float(z)
    return int(z) if int(z) == z else z


def string_to_int_if_int_else_float_else_string(string):
    try:
        z = int(string)
        return z

    except ValueError:
        pass

    try:
        z = float(string)
        return z

    except ValueError:
        pass

    return string


def isclose(thing1, thing2):
    if isinstance(thing1, Point) and \
       isinstance(thing2, Point):
        return thing1.is_close_to(thing2)
    return np.isclose(thing1, thing2)


def solve_quadratic(a, b, c):
    assert not np.isclose(a, 0)

    disc = b**2 - 4 * a * c

    if disc < 0:
        raise ValueError('negative discriminant')

    s = -b / (2 * a)
    r = sqrt(disc) / (2 * a)

    return (s - r, s + r) if r > 0 else (s + r, s - r)


def point_dir_center_radius_intersections(p, dir, c, r):
    assert isinstance(p, Point)
    assert isinstance(c, Point)
    assert isinstance(dir, Point)
    assert isinstance(r, Real)
    assert r >= 0 and not np.isclose(r, 0)

    # (p.x + t * dir.x - c.x)^2 + (p.y + t * dir.y - c.y)^2 == r^2
    # (a + t * b)^2 + (c + t * d)^2 == r^2
    a = p.x - c.x
    c = p.y - c.y
    b = dir.x
    d = dir.y
    A = b * b + d * d
    B = 2 * (a * b + c * d)
    C = a * a + c * c - r * r
    t1, t2 = solve_quadratic(A, B, C)
    return p + t1 * dir, p + t2 * dir


def point_dir_center_radius_aperture_intersections(p, dir, c, r, a):
    dir_angle = dir.arg_in_degrees()
    dir1 = Point('polardeg', 1, dir_angle - a / 2)
    dir2 = Point('polardeg', 1, dir_angle + a / 2)
    a, _ = point_dir_center_radius_intersections(p, dir1, c, r)
    b, _ = point_dir_center_radius_intersections(p, dir2, c, r)
    return a, b


def center_r_chord_intersections(c, r, chord):
    p = c + chord
    dir = chord.rotated(90)
    return point_dir_center_radius_intersections(p, dir, c, r)


def gauss_2d(sigma):
    return Point(gauss(0, sigma), gauss(0, sigma))


def circle_tokens(r, cx=0, cy=0, counterclockwise=False):
    dir = -1 if counterclockwise else 1
    sweep = 0 if counterclockwise else 1
    return [
        'M', 
        Point(cx, cy - r),
        'A', r, r, 0, 0, sweep,
        Point(cx + dir * r, cy),
        'A', r, r, 0, 0, sweep,
        Point(cx, cy + r),
        'A', r, r, 0, 0, sweep,
        Point(cx - dir * r, cy),
        'A', r, r, 0, 0, sweep,
        Point(cx, cy - r),
        'Z'
    ]


def interpolate_along_segment(p1, p2, x):
    assert isinstance(p1, Point)
    assert isinstance(p2, Point)
    c1 = (p2.x - x) / (p2.x - p1.x)
    c2 = (x - p1.x) / (p2.x - p1.x)
    return c1 * p1 + c2 * p2


##################################
# modular_floor, modular_ceiling #
##################################


def modular_ceiling(value, modulus):
    return (value - 1) - ((value - 1) % modulus) + modulus


def modular_floor(value, modulus):
    return value - (value % modulus)


######################
# uses of np.arctan2 #
######################


def radian_angle(x, y):
    return np.arctan2(y, x)


def degrees(a):
    return a * 90 / eta


def degree_angle(x, y):
    if y == 0:
        return 0 if x >= 0 else 180

    if x == 0:
        return 90 if y > 0 else 270

    if abs(x) == abs(y):
        if x > 0 and y > 0:
            return 45

        if x < 0 and y > 0:
            return 45 + 90

        if x < 0 and y < 0:
            return 45 + 180

        if x > 0 and y < 0:
            return 45 + 270

        assert False

    return np.arctan2(y, x) * 90 / eta


########################
# other trig functions #
########################


def csc(a):
    return 1 / sin(a)


def sec(a):
    return 1 / cos(a)


def cot(a):
    return 1 / tan(a)


def sin_deg(a):
    z = a % 360
    if z == 180 or z == 0:
        return 0
    elif z == 90:
        return 1
    elif z == 270:
        return -1
    return sin(radians(a))


def cos_deg(a):
    z = a % 360
    if z == 180:
        return -1
    elif z == 90 or z == 270:
        return 0
    elif z == 0:
        return 1
    return cos(radians(a))


def sec_deg(a):
    return sec(radians(a))


def csc_deg(a):
    return csc(radians(a))


def cot_deg(a):
    return cot(radians(a))


def tan_deg(a):
    return tan(radians(a))


def arctan_deg(a):
    return atan(a) * 90 / eta


def arcsin_deg(a):
    return asin(a) * 90 / eta


def arccos_deg(a):
    return acos(a) * 90 / eta


def atan2_deg(y, x=None):
    if x is None:
        x, y = real_numbers_in(y)
    return atan2(y, x) * 90 / eta


##############
# path_point #
##############


def ze_2f(value, decimals=2):
    # it seems that the threshold at which python
    # chooses to print large floats representing
    # integers in scientific notation is 10^16, so
    # we'll use the same here (i.e., avoiding converting
    # something like 10^16 to an int, otherwise it will
    # print as 10000000000000000 instead of as 10^16)
    if int(value) == value and abs(value) < 1e16:
        return str(int(value))

    if decimals is None:
        return str(value)

    string = f'{value:.{decimals}f}'
    if decimals > 0:
        assert '.' in string

        while string.endswith('0'):
            string = string[:-1]

        if string.endswith('.'):
            string = string[:-1]

        assert len(string) > 0

    else:
        assert decimals == 0

    return string


def extract_point(*args):
    if len(args) == 1 and isinstance(args[0], Point):
        return args[0]

    if len(args) == 2 and all(isinstance(t, Real) for t in args):
        return Point(args[0], args[1])

    assert False


def is_identity_matrix(a):
    assert isinstance(a, np.ndarray)
    assert a.shape == (3, 3)
    return \
        a[0,0] == 1 and a[0,1] == 0 and a[0,2] == 0 and \
        a[1,0] == 0 and a[1,1] == 1 and a[1,2] == 0 and \
        a[2,0] == 0 and a[2,1] == 0 and a[2,2] == 1


def convert_to_33_matrix(v1, v2, v3):
    assert isinstance(v1, Point3)
    assert isinstance(v2, Point3)
    assert isinstance(v3, Point3)
    return np.array(
        [[v1.x, v2.x, v3.x], 
         [v1.y, v2.y, v3.y], 
         [v1.z, v2.z, v3.z]]
    )


class Point3:
    def __init__(self, *args):
        if len(args) == 3:
            x = args[0]
            y = args[1]
            z = args[2]
            assert isinstance(x, Real)
            assert isinstance(y, Real)
            assert isinstance(z, Real)
            self.x = x
            self.y = y
            self.z = z

        else:
            assert len(args) == 1
            a = args[0]
            assert isinstance(a, np.ndarray)
            self.x = a[0, 0]
            self.y = a[1, 0]
            self.z = a[2, 0]

    def cross(self, other):
        return Point3(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x, 
        )

    def dot(self, other):
        return self.x * other.x + self.y * other.y + self.z * other.z

    def norm(self):
        return sqrt(self.dot(self))

    def norm_squared(self):
        return self.dot(self)

    def angle_with(self, other):
        dot = self.dot(other)
        ze_cos = dot / (self.norm() * other.norm())
        return arccos_deg(ze_cos)
    
    def component_along(self, other):
        return self.dot(other) / other.norm()

    def __str__(self, decimals=None):
        x_part = ze_2f(self.x, decimals)
        y_part = ze_2f(self.y, decimals)
        z_part = ze_2f(self.z, decimals)
        return x_part + ',' + y_part + ',' + z_part

    def ze_2f(self, decimals=None):
        return self.__str__(decimals=decimals)

    def get_frame(self):
        v1 = Point3(1, 0, 0)
        v2 = Point3(0, 1, 0)
        f = self.normalized()
        q = f.cross(v1)
        r = f.cross(v2)
        second = q if q.norm_squared() > r.norm_squared() else r
        third = f.cross(second)
        s = second.normalized()
        t = third.normalized()
        return (f, s, t)

    def normalize(self, length=1):
        n = self.norm()
        assert not np.isclose(n, 0)
        self.x *= (length / n)
        self.y *= (length / n)
        self.z *= (length / n)
        return self

    def to_numpy_vector(self):
        return np.array([[self.x],
                         [self.y],
                         [self.z]])

    def normalized(self, length=1):
        return Point3(self.x, self.y, self.z).normalize(length=length)

    def __sub__(self, other):
        assert isinstance(other, Point3)
        return Point3(self.x - other.x, self.y - other.y, self.z - other.z)

    def __add__(self, other):
        assert isinstance(other, Point3)
        return Point3(self.x + other.x, self.y + other.y, self.z + other.z)

    def __mul__(self, other):
        assert isinstance(other, Real)
        return Point3(self.x * other, self.y * other, self.z * other)

    def __truediv__(self, other):
        assert isinstance(other, Real)
        assert not np.isclose(other, 0)
        return Point3(self.x / other, self.y / other, self.z / other)

    def __rmul__(self, other):
        assert isinstance(other, Real)
        return Point3(self.x * other, self.y * other, self.z * other)

    def left_multiply_by_matrix(self, other):
        assert isinstance(other, np.ndarray)
        assert other.shape == (3, 3)
        return Point3(other.dot(self.to_numpy_vector()))

    def __neg__(self):
        return Point3(-self.x, -self.y, -self.z)



class Point:
    def __init__(self, *args, **kwargs):
        if len(args) == 1 or (len(args) == 2 and args[1] is None):
            a = args[0]

            if isinstance(a, Complex):
                self.x = a.real
                self.y = a.imag

            elif isinstance(a, Point):
                self.x = a.x
                self.y = a.y

            elif isinstance(a, str):
                l = a.replace(',', ' ').split()
                assert len(l) == 2
                self.x = float(l[0])
                self.y = float(l[1])

            elif isinstance(a, list) or isinstance(a, tuple):
                assert len(a) == 2
                self.x = float(a[0])
                self.y = float(a[1])

            elif isinstance(a, np.ndarray):
                assert a.shape == (3, 1)
                assert a[2, 0] == 1 or a[2, 0] == 0
                self.x = a[0, 0]
                self.y = a[1, 0]

            else:
                assert False

        elif len(args) == 2:
            if isinstance(args[0], str):
                if args[0] in ['x equals y', 'xequalsy']:
                    self.x = soft_int_else_float(args[1])
                    self.y = soft_int_else_float(args[1])

                elif args[0] == 'unitdeg':
                    self.x = cos_deg(args[1])
                    self.y = sin_deg(args[1])

                else:
                    self.x = soft_int_else_float(args[0])
                    self.y = soft_int_else_float(args[1])

            else:
                self.x = soft_int_else_float(args[0])
                self.y = soft_int_else_float(args[1])

        elif len(args) == 3:
            if args[0].startswith('polar'):
                r = args[1]
                theta = args[2]

                if args[0] in ['polar deg', 'polardeg']:
                    self.x = r * cos_deg(theta)
                    self.y = r * sin_deg(theta)

                elif args[0] in ['polar rad', 'polarrad']:
                    self.x = r * cos(theta)
                    self.y = r * sin(theta)

                else:
                    assert False

            elif args[0] == 'min of':
                self.x = min(args[1].x, args[2].x)
                self.y = min(args[1].y, args[2].y)

            elif args[0] == 'max of':
                self.x = max(args[1].x, args[2].x)
                self.y = max(args[1].y, args[2].y)

            else:
                assert False

        else:
            assert False

    def __str__(self):
        return self.to_string()

    def __repr__(self):
        return self.to_string()

    def __eq__(self, other):
        assert isinstance(other, Point)
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not self.__eq__(other)

    def det(self, other):
        return np.linalg.det(np.array([[self.x, other.x], [self.y, other.y]]))

    def dot(self, other):
        assert isinstance(other, Point)
        return self.x * other.x + self.y * other.y

    def is_vertical(self):
        return self.x == 0

    def is_horizontal(self):
        return self.y == 0

    def maybe_copy(self, flag):
        assert type(flag) == bool
        return self if not flag else copy(self)

    def to_string(self, decimals=None, x_only=False, y_only=False):
        x_part = ze_2f(self.x, decimals)
        y_part = ze_2f(self.y, decimals)
        z_part = ze_2f(self.y, decimals)
        return x_part + ',' + y_part

    def is_close_to(self, other):
        return self.isclose(other)

    def isclose(self, other):
        return np.isclose((self - other).norm(), 0)

    def abs(self):
        return Point(abs(self.x), abs(self.y))

    def to_int(self, assertive=True):
        if assertive:
            assert self.x == int(self.x)
            assert self.y == int(self.y)
        return Point(int(self.x), int(self.y))

    def _2f(self, decimals=None):
        return f'{ze_2f(self.x, decimals=decimals)},{ze_2f(self.y, decimals=decimals)}'

    def c_2f(self, decimals=None):
        return f'cx={ze_2f(self.x, decimals=decimals)}.cy={ze_2f(self.y, decimals=decimals)}'

    def xy_2f(self, decimals=2):
        return f'x={ze_2f(self.x, decimals=decimals)}.y={ze_2f(self.y, decimals=decimals)}'

    def x1y1_2f(self, decimals=2):
        return f'x1={ze_2f(self.x, decimals=decimals)}.y1={ze_2f(self.y, decimals=decimals)}'

    def x2y2_2f(self, decimals=2):
        return f'x2={ze_2f(self.x, decimals=decimals)}.y2={ze_2f(self.y, decimals=decimals)}'

    def x1y1x2y2_2f(self, decimals=2):
        return Point(0, 0).x1y1_2f(decimals=decimals) + '.' + self.x2y2_2f(decimals=decimals)

    def wh_2f(self):
        assert self.x > 0 and self.y > 0
        return f'width={ze_2f(self.x)}.height={ze_2f(self.y)}'

    def translate_2f(self):
        return f'translate({ze_2f(self.x)},{ze_2f(self.y)})'

    def transformed(self, tf, repeat=1):
        tf = parse_transform(tf)
        result = self.to_projective_vector()
        while repeat > 0:
            result = tf.dot(result)
            repeat -= 1
        return Point(result)

    def transform_by(self, tf, repeat=1):
        pt = self.transformed(tf, repeat)
        self.x = pt.x
        self.y = pt.y
        return self

    # def transform(self, tf, repeat=1):
    #     return self.transform_by(tf, repeat=repeat)

    def flip_x(self):
        self.x *= -1
        return self

    def flip_y(self):
        self.y *= -1
        return self

    def flipped_x(self):
        return Point(-self.x, self.y)

    def flipped_y(self):
        return Point(self.x, -self.y)

    def flip_about(self, other):
        assert other.norm() > 0.01
        angle = other.arg_in_degrees()
        self.rotate_by(-angle)
        self.flip_x()
        self.rotate_by(angle)
        return self

    def rotate_by(self, angle):
        return self.transform_by(['rotate', angle])

    def rotate(self, angle):
        return self.transform(['rotate', angle])

    def rotated(self, angle):
        return self.transformed(['rotate', angle])

    def scale_by(self, sx, sy=None):
        if sy is None:
            sy = sx
        assert isinstance(sx, Real)
        assert isinstance(sy, Real)
        self.x *= sx
        self.y *= sy
        return self

    def scale(self, sx, sy=None):
        return self.scale_by(sx, sy)

    def scaled_by(self, sx, sy=None):
        return Point(self).scale_by(sx, sy)

    def scaled(self, sx, sy=None):
        return Point(self).scale_by(sx, sy)

    def polar_translated(self, how_much, angle):
        return self + Point('polar deg', how_much, angle)

    def projected_onto(self, other):
        return (other * self.dot(other)) / other.norm_squared()

    def projected_onto_segment(self, A, B, allow_point_projections=False):
        if A == B:
            assert allow_point_projections
            return A
        return A + (self - A).projected_onto(B - A)

    def translate_by(self, x, y=0):
        if isinstance(x, Point):
            self.x += x.x
            self.y += x.y

        elif isinstance(x, Complex):
            x += 1j * y
            self.x += x.real
            self.y += x.imag

        elif isinstance(x, Real):
            self.x += x
            self.y += y

        else:
            assert False

        return self

    def translated(self, x=0, y=0):
        if isinstance(x, Point):
            assert y == 0
            return Point(self.x + x.x, self.y + x.y)

        elif isinstance(x, Real):
            return Point(self.x + x, self.y + y)

        elif isinstance(x, Complex):
            assert y == 0
            return Point(self.x + x.real, self.y + x.imag)

        else:
            assert False

    def txed(self, amount):
        return Point(self.x + amount, self.y)

    def tyed(self, amount):
        return Point(self.x, self.y + amount)

    def to_projective_vector(self):
        return np.array([[self.x],
                         [self.y],
                         [1]])

    def to_unprojective_vector(self):
        return np.array([[self.x],
                         [self.y],
                         [0]])

    def to_ordinary_vector(self):
        return np.ndarray([[self.x], [self.y]])

    def norm_squared(self):
        return self.x * self.x + self.y * self.y

    def norm(self):
        if self.x == 0:
            return abs(self.y)
        if self.y == 0:
            return abs(self.x)
        return sqrt(self.norm_squared())

    def __abs__(self):
        return self.norm()

    def normalized(self, length=1):
        return Point(self.x, self.y).normalize(length=length)

    def normalize(self, length=1):
        if self.y == 0:
            self.x = sign(self.x) * length

        elif self.x == 0:
            self.y = sign(self.y) * length

        else:
            z = self.norm()
            assert not np.isclose(z, 0)
            self.x *= (length / z)
            self.y *= (length / z)

        return self

    def is_cardinal(self):
        return (self.x == 0 and self.y != 0) or (self.x != 0 and self.y == 0)

    def arg_in_radians(self):
        return radian_angle(self.x, self.y)

    def arg_in_degrees(self):
        return degree_angle(self.x, self.y)

    def scale_to_length(self, l):
        t = self.norm()
        if t < 1e-10:
            raise ValueError
        self *= l / t
        return self

    def inside_trbl(self, t, r, b, l):
        T = max(t, b)
        B = min(t, b)
        return l <= self.x <= r and B <= self.y <= T

    def project(self, axis):
        if axis == 'x':
            return self.x
        if axis == 'y':
            return self.y
        assert False

    def cleanup(self):
        if int(self.x) == self.x:
            self.x = int(self.x)
        if int(self.y) == self.y:
            self.y = int(self.y)
        return self

    def bisecting_angle(self, A, B):
        angle1 = (A - self).arg_in_degrees()
        angle2 = (B - self).arg_in_degrees()
        return (angle1 + angle2) / 2

    def left_multiply_by(self, m):
        assert isinstance(m, np.ndarray)
        assert m.shape == (2, 2)
        result = m.dot(np.array([[self.x], [self.y]]))
        return Point(result[0, 0], result[1, 0])

    def gaussian_blur(self, standard_deviation):
        p = gauss_2d(standard_deviation)
        self.x += p.x
        self.y += p.y
        return self

    def pointwise_min(self, other):
        return Point(min(self.x, other.x), min(self.y, other.y))

    def pointwise_max(self, other):
        return Point(max(self.x, other.x), max(self.y, other.y))

    def __add__(self, other):
        if isinstance(other, Point):
            return Point(self.x + other.x, self.y + other.y)

        elif isinstance(other, Complex):
            return Point(self.x + other.real, self.y + other.imag)

        assert False

    def __sub__(self, other):
        assert isinstance(other, Point)
        return Point(self.x - other.x, self.y - other.y)

    def __mul__(self, other):
        assert isinstance(other, Real)
        return Point(self.x * other, self.y * other)

    def __truediv__(self, other):
        assert isinstance(other, Real)
        assert not np.isclose(other, 0)
        return Point(self.x / other, self.y / other)

    def __rmul__(self, other):
        if isinstance(other, Real):
            return Point(self.x * other, self.y * other)

        if isinstance(other, np.ndarray):
            print("u R doing this multiplication....")
            assert other.shape == (3, 3)
            return Point(other.dot(self.to_projective_vector()))

        assert False

    def __neg__(self):
        return Point(-self.x, -self.y)

    def angle_with(self, other):
        dot = self.dot(other)
        ze_cos = dot / (self.norm() * other.norm())
        return arccos_deg(ze_cos)
    
    def determinant_with(self, other):
        assert isinstance(other, Point)
        return self.x * other.y - other.x * self.y

    def rounded(self):
        return Point(round(self.x), round(self.y))


def abc_angle(a, b, c):
    u = a - b
    v = c - b
    return u.angle_with(v)


def line_line_intersection(l1A, l1B, l2A, l2B, determinant_tolerance=None):
    return four_way_intersection(l1A, l1B - l1A, l2A, l2B - l2A, determinant_tolerance=determinant_tolerance)


def four_way_intersection(p1, dir1, p2, dir2, with_parameters=False, determinant_tolerance=None):
    if isinstance(dir1, Real):
        dir1 = Point('polar deg', 1, dir1)

    if isinstance(dir2, Real):
        dir2 = Point('polar deg', 1, dir2)

    assert all(isinstance(x, Point) for x in (p1, dir1, p2, dir2))

    b = p2 - p1
    a = np.array([[dir1.x, -dir2.x],
                  [dir1.y, -dir2.y]])
    B = np.array([[b.x], [b.y]])

    det = np.linalg.det(a);

    if determinant_tolerance is None:
        if np.isclose(0, det):
            raise ValueError("np.isclose(0) determinant error")

    else:
        assert determinant_tolerance > 0
        if abs(det) < determinant_tolerance:
            raise ValueError(f"np.linalg.det(a) = {det} < determinant_tolerance = {determinant_tolerance}")

    a_inv = np.linalg.inv(a)
    L = a_inv.dot(B)

    l1 = L[0, 0]
    l2 = L[1, 0]
    q1 = p1 + l1 * dir1
    q2 = p2 + l2 * dir2

    assert np.isclose(0, (q1 - q2).norm())

    if dir2.is_vertical():
        q1.x = p2.x

    elif dir2.is_horizontal():
        q1.y = p2.y

    assert np.isclose(0, (q1 - q2).norm())

    if dir1.is_vertical():
        assert q1.x == p1.x

    elif dir1.is_horizontal():
        assert q1.y == p1.y

    if with_parameters:
        return q1, l1, l2

    return q1


def point_dir_point_dir_intersection(p1, dir1, p2, dir2, **kwargs):
    return four_way_intersection(p1, dir1, p2, dir2, **kwargs)


def point_angle_point_angle_intersection(p1, a1, p2, a2, **kwargs):
    dir1 = Point('polardeg', 1, a1)
    dir2 = Point('polardeg', 1, a2)
    return four_way_intersection(p1, dir1, p2, dir2, **kwargs)


######################################################
# some pacman picture needed this (should be moved): #
######################################################


def merge_lines(l1, l2):
    if l1.is_vertical() and \
       l2.is_vertical() and \
       l1.x() == l2.x():
        x = l1.x()
        ymin = min(l1.ymin, l2.ymin)
        ymax = max(l1.ymax, l2.ymax)
        return Line(start=x + 1j * ymin, end=x + 1j * ymax)

    if l1.is_horizontal() and \
       l2.is_horizontal() and \
       l1.y() == l2.y():
        y = l1.y()
        xmin = min(l1.xmin, l2.xmin)
        xmax = max(l1.xmax, l2.xmax)
        return Line(start=xmin + 1j * y, end=xmax + 1j * y)

    return None


###################
# number interval #
###################


nonneg_integer = r'(?:[0-9]+)'
nonneg_decimal = r'(?:[0-9]*\.[0-9]+)'

signed_integer = r'(?:[-+]?)' + nonneg_integer
signed_decimal = r'(?:[-+]?)' + nonneg_decimal

nonneg = nonneg_integer + '|' + nonneg_decimal
signed = signed_integer + '|' + signed_decimal

at_plus_number_re = re.compile(r'@(' + nonneg + r')\+(' + signed + r')')


class Interval():
    def __init__(self, *args):
        if len(args) == 1:
            assert len(args[0]) == 2
            self.lo = args[0][0]
            self.hi = args[0][1]

        elif len(args) == 2:
            self.lo = args[0]
            self.hi = args[1]

        assert isinstance(self.lo, Real)
        assert isinstance(self.hi, Real)
        assert self.lo <= self.hi

    def length(self):
        return self.hi - self.lo

    def contains(self, x):
        if isinstance(x, Real):
            return self.lo <= x <= self.hi

        elif isinstance(x, Interval):
            return self.lo <= x.lo <= x.hi <= self.hi

        assert False

    def contains_any(self, *args):
        return any(self.contains(y) for y in args)

    def distance_to(self, x):
        if x > self.hi:
            return x - self.hi
        if x < self.lo:
            return self.lo - x
        return 0

    def multiples_in_range(self, *args):
        def process_args(*args):
            if len(args) == 1 and isinstance(args[0], str):
                string = args[0]
                assert '@' in string
                if '+' not in string:
                    string = string + '+0'
                groups = at_plus_number_re.match(string).groups()
                assert len(groups) == 2
                return float(groups[0]), float(groups[1])

            elif len(args) == 1 and isinstance(args[0], Real):
                assert args[0] > 0
                return args[0], 0

            elif len(args) == 2:
                assert isinstance(args[0], Real) and args[0] > 0
                assert isinstance(args[1], Real)
                return args[0], args[1]

            else:
                assert False

        multiple, offset = process_args(*args)
        assert isinstance(multiple, Real)
        assert isinstance(offset, Real)
        assert multiple > 0
        assert multiple > 1e-7

        lo, hi = self.lo, self.hi
        start = floor((lo - offset) / multiple) * multiple
        if start + offset < lo:
            start += multiple
        assert lo <= start + offset
        assert start + offset - multiple < lo
        things = []
        t = start + offset
        while t <= hi:
            assert abs(t) < 1e10
            things.append(round(t * 1e6) / 1e6)
            if int(things[-1]) == things[-1] and int(offset) == offset:
                things[-1] = int(things[-1])
            t += multiple
        return things

    def __eq__(self, other):
        assert isinstance(other, Interval)
        return self.lo == other.lo and self.hi == other.hi

    def __str__(self, precision=5):
        return f'{self.lo}--{self.hi}'


##############
# TRBLObject #
##############


class TRBLObject:
    def __init__(self, top, right, bottom, left):
        assert isinstance(top, Real)
        assert isinstance(right, Real)
        assert isinstance(bottom, Real)
        assert isinstance(left, Real)
        self.top = top
        self.bottom = bottom
        self.right = right
        self.left = left

    def scale(self, scale):
        self.top *= scale
        self.right *= scale
        self.bottom *= scale
        self.left *= scale
        return self

    def scaled_by(self, sx, sy=None):
        if sy is None:
            sy = sx
        return TRBLObject(
            sy * self.top,
            sx * self.right,
            sy * self.bottom,
            sx * self.left
        )

    def nonnegative(self):
        return \
            self.top >= 0 and \
            self.right >= 0 and \
            self.bottom >= 0 and \
            self.left >= 0

    def __iter__(self):
        return [
            self.top,
            self.right,
            self.bottom,
            self.left
        ].__iter__()

    def __add__(self, other):
        assert isinstance(other, TRBLObject)
        return TRBLObject(
            self.top + other.top,
            self.right + other.right,
            self.bottom + other.bottom,
            self.left + other.left
        )


################
# bounding box #
################


class BoundingBox():
    @staticmethod
    def none_max(a, b):
        if a is None:
            return b
        if b is None:
            return a
        return max(a, b)

    @staticmethod
    def none_min(a, b):
        if a is None:
            return b
        if b is None:
            return a
        return min(a, b)

    @staticmethod
    def none_leq(a, b):
        if a is None and b is None:
            return True
        if a is None or b is None:
            assert False
        return a <= b

    def __init__(self, *args):
        if len(args) == 0:
            args = [None, None, None, None]

        if isinstance(args[0], str) and args[0] == 'xywidthheight':  # Interval will complain if 'isinstance(args[0], str)' is removed
            if len(args) == 2:
                assert isinstance(args[1], list)
                assert len(args[1]) == 4 and all(
                    isinstance(x, Real) for x in args[1])
                x, y, width, height = args[1][0], args[1][1], args[1][2], args[1][3]

            else:
                assert len(args) == 5
                assert all(isinstance(x, Real) for x in args[1:])
                x, y, width, height = args[1], args[2], args[3], args[4]

            assert width >= 0 and height >= 0

            self.xmin = x
            self.ymin = y
            self.xmax = x + width
            self.ymax = y + height

        elif isinstance(args[0], BoundingBox):
            assert len(args) == 1
            guy = args[0]
            self.xmin = guy.xmin
            self.xmax = guy.xmax
            self.ymin = guy.ymin
            self.ymax = guy.ymax

        elif len(args) == 2:
            assert all(isinstance(a, Interval) for a in args)
            x_interval = args[0]
            y_interval = args[1]
            self.xmin = x_interval.lo
            self.xmax = x_interval.hi
            self.ymin = y_interval.lo
            self.ymax = y_interval.hi

        elif isinstance(args[0], tuple):
            assert len(args) == 1
            guy = args[0]
            self.xmin = guy[0]
            self.xmax = guy[1]
            self.ymin = guy[2]
            self.ymax = guy[3]

        else:
            assert len(args) == 4
            assert all(isinstance(x, Real) for x in args) or all(x is None for x in args)
            self.xmin = args[0]
            self.xmax = args[1]
            self.ymin = args[2]
            self.ymax = args[3]

        assert self.none_leq(self.xmin, self.xmax)
        assert self.none_leq(self.ymin, self.ymax)

    def init(self, xmin, xmax, ymin, ymax):
        assert self.is_empty()
        self.xmin = xmin
        self.xmax = xmax
        self.ymin = ymin
        self.ymax = ymax
        self.check_integrity()
        return self

    def init_by_viewbox(self, *args):
        def finish_job_from_list_of_reals(L):
            assert len(L) == 4
            assert all(isinstance(x, Real) for x in L)
            self.xmin = L[0]
            self.ymin = L[1]
            self.xmax = self.xmin + L[2]
            self.ymax = self.ymin + L[3]

        assert self.is_empty()
        if len(args) == 4:
            finish_job_from_list_of_reals(args)

        elif len(args) == 1:
            if isinstance(args[0], list):
                finish_job_from_list_of_reals(args[0])

            elif isinstance(args[0], str):
                finish_job_from_list_of_reals(
                    [float(x) for x in args[0].split()])

            else:
                assert False

        else:
            assert False

        self.check_integrity()
        return self

    def css_style_corner(self, string):
        if string[0] == 'b':
            y = self.ymax
        elif string[0] == 't':
            y = self.ymin
        elif string[0] == 'm' or string[0] == 'c':
            y = (self.ymin + self.ymax) * 0.5
        else:
            assert False

        if string[1] == 'l':
            x = self.xmin
        elif string[1] == 'r':
            x = self.xmax
        elif string[1] == 'm' or string[1] == 'c':
            x = (self.xmin + self.xmax) * 0.5
        else:
            assert False

        return Point(x, y)

    def agglomerate(self, p_or):
        if isinstance(p_or, Point):
            self.xmin = self.none_min(self.xmin, p_or.x)
            self.xmax = self.none_max(self.xmax, p_or.x)
            self.ymin = self.none_min(self.ymin, p_or.y)
            self.ymax = self.none_max(self.ymax, p_or.y)

        elif isinstance(p_or, BoundingBox):
            self.xmin = self.none_min(self.xmin, p_or.xmin)
            self.xmax = self.none_max(self.xmax, p_or.xmax)
            self.ymin = self.none_min(self.ymin, p_or.ymin)
            self.ymax = self.none_max(self.ymax, p_or.ymax)

        else:
            assert False

        self.check_integrity()

        return self

    def agglomerate_x1x2y1y2(self, x1, x2, y1, y2):
        self.xmin = self.none_min(self.xmin, x1)
        self.xmax = self.none_max(self.xmax, x2)
        self.ymin = self.none_min(self.ymin, y1)
        self.ymax = self.none_max(self.ymax, y2)

        self.check_integrity()

    def agglomerate_all(self, *args):
        if len(args) == 1:
            assert isinstance(args[0], list)
            for p in args[0]:
                self.agglomerate(p)

        else:
            for a in args:
                self.agglomerate(a)

        return self

    def expand_by_trbl(self, *args):
        if len(args) == 1 and isinstance(args[0], TRBLObject):
            trbl = args[0]

        elif len(args) == 1 and isinstance(args[0], list):
            ze_list = args[0]
            assert len(ze_list) == 4
            trbl = TRBLObject(ze_list[0], ze_list[1], ze_list[2], ze_list[3])

        elif len(args) == 1 and isinstance(args[0], Real):
            r = args[0]
            trbl = TRBLObject(r, r, r, r)

        else:
            assert len(args) == 4
            trbl = TRBLObject(args[0], args[1], args[2], args[3])

        assert trbl.nonnegative()

        self.xmin -= trbl.left
        self.xmax += trbl.right
        self.ymin -= trbl.top
        self.ymax += trbl.bottom
        self.check_integrity()
        
        return self

    def expanded_by_trbl(self, *args):
        return BoundingBox(self).expand_by_trbl(*args)

    def cut_this_fucking_line_up(self, line):
        assert isinstance(line, Line)
        assert self.is_nonempty() and self.check_integrity()

        if line.is_horizontal():
            if line.y() > self.ymax or line.y() < self.ymin:
                return [line]

            line_xmax = line.xmax
            line_xmin = line.xmin

            assert line_xmin < line_xmax

            if line_xmax <= self.xmin or line_xmin >= self.xmax:
                return [line]

            leftovers = []
            y = line.y()

            if line_xmin < self.xmin:
                leftovers.append(Line(line_xmin + 1j * y,
                                      self.xmin + 1j * y))

            if line_xmax > self.xmax:
                leftovers.append(Line(self.xmax + 1j * y,
                                      line_xmax + 1j * y))

            return leftovers

        elif line.is_vertical():
            if line.x() > self.xmax or line.x() < self.xmin:
                return [line]

            line_ymin = line.ymin
            line_ymax = line.ymax

            assert line_ymin < line_ymax

            if line_ymax <= self.ymin or line_ymin >= self.ymax:
                return [line]

            leftovers = []
            x = line.x()

            if line_ymin < self.ymin:
                leftovers.append(Line(x + 1j * line_ymin,
                                      x + 1j * self.ymin))

            if line_ymax > self.ymax:
                leftovers.append(Line(x + 1j * self.ymax,
                                      x + 1j * line_ymax))

            return leftovers

        else:
            assert False, "sorry not prepared for sloped line"

    def project(self, axis):
        if axis == 'x':
            return Interval(self.xmin, self.xmax)

        if axis == 'y':
            return Interval(self.ymin, self.ymax)

        assert False

    def check_integrity(self):
        if self.xmin is None:
            assert self.xmax is None
            assert self.ymin is None
            assert self.ymax is None

        else:
            assert isinstance(self.xmin, Real)
            assert isinstance(self.xmax, Real)
            assert isinstance(self.ymin, Real)
            assert isinstance(self.ymax, Real)
            assert self.xmin <= self.xmax
            assert self.ymin <= self.ymax

    def doesnt_overlap_with(self, other):
        assert isinstance(other, BoundingBox)
        return \
            other.xmin > self.xmax or \
            other.xmax < self.xmin or \
            other.ymin > self.ymax or \
            other.ymax < self.ymin

    def is_nonempty(self):
        self.check_integrity()
        return self.xmin is not None

    def is_empty(self):
        self.check_integrity()
        return self.xmin is None

    def has_nonzero_height(self):
        if self.ymin is None:
            return False
        assert self.ymax >= self.ymin
        return self.ymax > self.ymin

    def width(self):
        if self.xmin is None:
            return None
        return self.xmax - self.xmin

    def height(self):
        if self.ymin is None:
            return None
        return self.ymax - self.ymin

    def translate_by(self, x, y=0):
        self.xmin += x
        self.xmax += x
        self.ymin += y
        self.ymax += y

    def translated_by(self, x, y):
        return BoundingBox(self.xmin + x,
                           self.xmax + x,
                           self.ymin + y,
                           self.ymax + y)

    def transform_by(self, M):
        if self.xmin is not None:
            corners = [Point(c[0], c[1]) for c in product(
                [self.xmin, self.xmax], [self.ymin, self.ymax])]
            for z in corners:
                z.transform_by(M)
            self.xmin = min([z.x for z in corners])
            self.xmax = max([z.x for z in corners])
            self.ymin = min([z.y for z in corners])
            self.ymax = max([z.y for z in corners])

        else:
            self.check_integrity()

    def min_min_corner(self):
        return Point(self.xmin, self.ymin)

    def min_max_corner(self):
        return Point(self.xmin, self.ymax)

    def max_max_corner(self):
        return Point(self.xmax, self.ymax)

    def max_min_corner(self):
        return Point(self.xmax, self.ymin)

    def mid_x(self):
        return 0.5 * (self.xmin + self.xmax)

    def mid_y(self):
        return 0.5 * (self.ymin + self.ymax)

    def contains(self, *args):
        if len(args) == 2:
            assert isinstance(args[0], Real)
            assert isinstance(args[1], Real)

            return \
                self.xmin <= args[0] <= self.xmax and \
                self.ymin <= args[1] <= self.ymax

        if len(args) == 1:
            assert isinstance(args[0], Point)

            return \
                self.xmin <= args[0].x <= self.xmax and \
                self.ymin <= args[0].y <= self.ymax

        assert False

    def min_max_along_axis(self, axis):
        if axis == 'x':
            return [self.xmin, self.xmax]

        if axis == 'y':
            return [self.ymin, self.ymax]

        assert False

    def d(self, precision=None):
        return f"M {self.xmin}, {self.ymin} V {self.ymax} H {self.xmax} V {self.ymin} Z"
    
    def path_tokens(self):
        corners = self.clockwise_four_corners()
        return ['M', corners[0], corners[1], corners[2], corners[3], 'Z']

    def rect(self, fill="#f0f", opacity=1, decimals=None):
        if len(fill) > 7 and fill.startswith('#'):
            print("\nInkscape does not deal with #XXXXXXXX color format, unfortunately; use 'opacity' argument instead\n")
            assert False
        return f"rect.x={ze_2f(self.xmin, decimals)}.y={ze_2f(self.ymin, decimals)}.width={ze_2f(self.xmax - self.xmin, decimals)}.height={ze_2f(self.ymax - self.ymin, decimals)}.fill:{fill}.opacity={opacity}"

    def viewbox_list(self):
        return [self.xmin, self.ymin, self.xmax - self.xmin, self.ymax - self.ymin]
    
    def clockwise_four_corners(self):
        return [
            Point(self.xmin, self.ymin),
            Point(self.xmin, self.ymax),
            Point(self.xmax, self.ymax),
            Point(self.xmax, self.ymin)
        ]

    def convert_by(self, function):
        new_corners = [function(z) for z in self.clockwise_four_corners()]
        assert all(isinstance(q, Point) for q in new_corners)
        to_return = BoundingBox()
        to_return.agglomerate_all(new_corners)
        return to_return

    def viewbox_string(self, decimals=None):
        return ' '.join(ze_2f(t, decimals=decimals) for t in self.viewbox_list())

    def xywh_2f(self, decimals=None):
        return (
            f'x={ze_2f(self.xmin, decimals=decimals)}.y={ze_2f(self.ymin, decimals=decimals)}.' +
            f'w={ze_2f((self.xmax - self.xmin), decimals=decimals)}.h={ze_2f((self.ymax - self.ymin), decimals=decimals)}'
        )

    def centering_translation(self, where=None, list_form=True):
        if where is None:
            to_return = ['translate', -self.mid_x(), -self.mid_y()]

        elif where == 'south':
            to_return = ['translate', -self.mid_x(), -self.ymax]

        elif where == 'vertical':
            to_return = ['translate', 0, -self.mid_y()]

        elif where == 'horizontal':
            to_return = ['translate', -self.mid_x()]

        elif where == 'll':
            to_return = ['translate', -self.xmin, -self.ymax]

        else:
            assert False

        return to_return if list_form else generate_transform(to_return)

    def __str__(self):
        return "bounding box: " + str(self.xmin) + " " + str(self.xmax) + " " + str(self.ymin) + " " + str(self.ymax)

    def __repr__(self):
        return "bounding box: " + str(self.xmin) + " " + str(self.xmax) + " " + str(self.ymin) + " " + str(self.ymax)


##########################
# numpy matrix utilities #
##########################


def viewbox_to_viewbox_matrix(x1, y1, w1, h1,
                              x2, y2, w2, h2):
    return parse_transform(
        'translate', -x1, -y1,
        'scale', w2 / w1, h2 / h1,
        'translate', x2, y2
    )


def bounding_box_to_bounding_box_matrix(b1, b2):
    return parse_transform(
        'translate', -b1.xmin, -b1.ymin,
        'scale', b2.width() / b1.width(), b2.height() / b2.height(),
        'translate', b2.xmin, b2.ymin
    )


######################
# helping np.ndarray #
######################


def is_scaling_and_translation(matrix):
    assert isinstance(matrix, np.ndarray)
    assert matrix.shape == (3, 3)
    return \
        matrix[0, 1] == 0 and \
        matrix[1, 0] == 0


def is_translation(matrix):
    assert isinstance(matrix, np.ndarray)
    assert matrix.shape == (3, 3)
    return \
        matrix[0, 1] == 0 and \
        matrix[1, 0] == 0 and \
        matrix[0, 0] == 1 and \
        matrix[1, 1] == 1


print(sin_deg(68) * csc_deg(71) * sin_deg(59) * csc_deg(60))
print(pow(sin_deg(68) * csc_deg(71) * sin_deg(59) * csc_deg(60), 36))
