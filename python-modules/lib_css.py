import re
import os


from itertools import chain
from numbers import Real
from tinycss2 import parse_rule_list, parse_declaration_list
from tinycss2 import parse_one_declaration
from tinycss2.ast import (
    NumberToken,
    DimensionToken,
    Declaration,
    WhitespaceToken,
    IdentToken,
)
from tinycss2.ast import (
    StringToken,
    PercentageToken,
    URLToken,
    FunctionBlock,
    LiteralToken,
    HashToken,
)
from tinycss2.ast import SquareBracketsBlock, Comment, ParseError
from copy import deepcopy
from lib_svg_matrix import ze_2f


HTML_COLORS = {
    "aliceblue": "#F0F8FF",
    "antiquewhite": "#FAEBD7",
    "aqua": "#00FFFF",
    "aquamarine": "#7FFFD4",
    "azure": "#F0FFFF",
    "beige": "#F5F5DC",
    "bisque": "#FFE4C4",
    "black": "#000000",
    "blanchedalmond": "#FFEBCD",
    "blue": "#0000FF",
    "blueviolet": "#8A2BE2",
    "brown": "#A52A2A",
    "burlywood": "#DEB887",
    "cadetblue": "#5F9EA0",
    "chartreuse": "#7FFF00",
    "chocolate": "#D2691E",
    "coral": "#FF7F50",
    "cornflowerblue": "#6495ED",
    "cornsilk": "#FFF8DC",
    "crimson": "#DC143C",
    "cyan": "#00FFFF",
    "darkblue": "#00008B",
    "darkcyan": "#008B8B",
    "darkgoldenrod": "#B8860B",
    "darkgray": "#A9A9A9",
    "darkgrey": "#A9A9A9",
    "darkgreen": "#006400",
    "darkkhaki": "#BDB76B",
    "darkmagenta": "#8B008B",
    "darkolivegreen": "#556B2F",
    "darkorange": "#FF8C00",
    "darkorchid": "#9932CC",
    "darkred": "#8B0000",
    "darksalmon": "#E9967A",
    "darkseagreen": "#8FBC8F",
    "darkslateblue": "#483D8B",
    "darkslategray": "#2F4F4F",
    "darkslategrey": "#2F4F4F",
    "darkturquoise": "#00CED1",
    "darkviolet": "#9400D3",
    "deeppink": "#FF1493",
    "deepskyblue": "#00BFFF",
    "dimgray": "#696969",
    "dimgrey": "#696969",
    "dodgerblue": "#1E90FF",
    "firebrick": "#B22222",
    "floralwhite": "#FFFAF0",
    "forestgreen": "#228B22",
    "fuchsia": "#FF00FF",
    "gainsboro": "#DCDCDC",
    "ghostwhite": "#F8F8FF",
    "gold": "#FFD700",
    "goldenrod": "#DAA520",
    "gray": "#808080",
    "grey": "#808080",
    "green": "#008000",
    "greenyellow": "#ADFF2F",
    "honeydew": "#F0FFF0",
    "hotpink": "#FF69B4",
    "indianred": "#CD5C5C",
    "indigo": "#4B0082",
    "ivory": "#FFFFF0",
    "khaki": "#F0E68C",
    "lavender": "#E6E6FA",
    "lavenderblush": "#FFF0F5",
    "lawngreen": "#7CFC00",
    "lemonchiffon": "#FFFACD",
    "lightblue": "#ADD8E6",
    "lightcoral": "#F08080",
    "lightcyan": "#E0FFFF",
    "lightgoldenrodyellow": "#FAFAD2",
    "lightgray": "#D3D3D3",
    "lightgrey": "#D3D3D3",
    "lightgreen": "#90EE90",
    "lightpink": "#FFB6C1",
    "lightsalmon": "#FFA07A",
    "lightseagreen": "#20B2AA",
    "lightskyblue": "#87CEFA",
    "lightslategray": "#778899",
    "lightslategrey": "#778899",
    "lightsteelblue": "#B0C4DE",
    "lightyellow": "#FFFFE0",
    "lime": "#00FF00",
    "limegreen": "#32CD32",
    "linen": "#FAF0E6",
    "magenta": "#FF00FF",
    "maroon": "#800000",
    "mediumaquamarine": "#66CDAA",
    "mediumblue": "#0000CD",
    "mediumorchid": "#BA55D3",
    "mediumpurple": "#9370DB",
    "mediumseagreen": "#3CB371",
    "mediumslateblue": "#7B68EE",
    "mediumspringgreen": "#00FA9A",
    "mediumturquoise": "#48D1CC",
    "mediumvioletred": "#C71585",
    "midnightblue": "#191970",
    "mintcream": "#F5FFFA",
    "mistyrose": "#FFE4E1",
    "moccasin": "#FFE4B5",
    "navajowhite": "#FFDEAD",
    "navy": "#000080",
    "oldlace": "#FDF5E6",
    "olive": "#808000",
    "olivedrab": "#6B8E23",
    "orange": "#FFA500",
    "orangered": "#FF4500",
    "orchid": "#DA70D6",
    "palegoldenrod": "#EEE8AA",
    "palegreen": "#98FB98",
    "paleturquoise": "#AFEEEE",
    "palevioletred": "#DB7093",
    "papayawhip": "#FFEFD5",
    "peachpuff": "#FFDAB9",
    "peru": "#CD853F",
    "pink": "#FFC0CB",
    "plum": "#DDA0DD",
    "powderblue": "#B0E0E6",
    "purple": "#800080",
    "rebeccapurple": "#663399",
    "red": "#FF0000",
    "rosybrown": "#BC8F8F",
    "royalblue": "#4169E1",
    "saddlebrown": "#8B4513",
    "salmon": "#FA8072",
    "sandybrown": "#F4A460",
    "seagreen": "#2E8B57",
    "seashell": "#FFF5EE",
    "sienna": "#A0522D",
    "silver": "#C0C0C0",
    "skyblue": "#87CEEB",
    "slateblue": "#6A5ACD",
    "slategray": "#708090",
    "slategrey": "#708090",
    "snow": "#FFFAFA",
    "springgreen": "#00FF7F",
    "steelblue": "#4682B4",
    "tan": "#D2B48C",
    "teal": "#008080",
    "thistle": "#D8BFD8",
    "tomato": "#FF6347",
    "turquoise": "#40E0D0",
    "violet": "#EE82EE",
    "wheat": "#F5DEB3",
    "white": "#FFFFFF",
    "whitesmoke": "#F5F5F5",
    "yellow": "#FFFF00",
    "yellowgreen": "#9ACD32",
}


html_color_re = re.compile("#(([0-9a-fA-F]{3,4})|([0-9a-fA-F]{6})|([0-9a-fA-F]{8}))")
css_dimension_re = re.compile(r"((?:-)?(?:(?:[0-9]+(?:\.[0-9]*)?)|(?:\.[0-9]+)))(px|pt|cm|em|ex)?")


def encodes_html_color(string):
    assert len(string) >= 4
    if string[0] == "#":
        return html_color_re.fullmatch(string) is not None

    else:
        return string.lower() in HTML_COLORS


class Css_Color:
    def __init__(self, *args):
        if len(args) == 1:
            assert isinstance(args[0], str)
            if args[0] in HTML_COLORS:
                self.value = args[0]

            else:
                assert html_color_re.fullmatch(args[0]) is not None
                self.value = args[0]

    def __str__(self, decimals=None):
        # return self.value + '[Col]'
        return self.value


class Css_Number:
    def __init__(self, blurb):
        assert isinstance(blurb, NumberToken)
        self.value = blurb.value
        if self.value == int(self.value):
            self.value = int(self.value)

    def multiply_by(self, number):
        self.value *= number

    def __str__(self, decimals=None):
        return ze_2f(self.value, decimals)


class Css_Dimension:
    def __init__(self, *args):
        if len(args) == 1:
            if isinstance(args[0], DimensionToken):
                self.value = args[0].value
                self.units = args[0].unit

            elif isinstance(args[0], NumberToken):
                if args[0].value != 0:
                    assert False, str(args[0].value)
                self.value = args[0].value
                # assert self.value == 0
                self.units = ""

            else:
                assert False
        else:
            assert False

        assert isinstance(self.value, Real)

    def set_to(self, *args):
        if len(args) == 1:
            if isinstance(args[0], Real):
                assert args[0] == 0
                self.value = 0
            elif isinstance(args[0], str):
                match = css_dimension_re.fullmatch(args[0])
                self.value = float(match.group(1))
                if self.value == int(self.value):
                    self.value = int(self.value)
                self.units = match.group(2) if match.group(2) is not None else ""
                assert self.units != "" or self.value == 0
            else:
                assert False
        assert isinstance(self.value, Real)
        assert isinstance(self.units, str)

    def multiply_by(self, number):
        self.value *= number

    def __eq__(self, other):
        if isinstance(other, Real):
            if other == 0 and self.value == 0:
                return True
            return False
        if not isinstance(other, Css_Dimension):
            return False
        if other.value == 0 and self.value == 0:
            return True
        return self.__dict__ == other.__dict__

    def __str__(self, decimals=None):
        assert isinstance(self.value, Real)
        assert decimals is None or isinstance(decimals, int)
        number = ze_2f(self.value, decimals)
        return number + self.units


def convert_Css_Declaration_dict_to_ordinary_dict(d):
    newd = {}
    for key in d:
        assert isinstance(key, str)
        newd[key] = d[key].val_str()
    return newd


class Css_Declaration:
    def __init__(self, thing):
        self.tokens = []
        self.dimensions = []
        self.numbers = []
        self.color = None

        if isinstance(thing, Declaration):
            decl = thing

        elif isinstance(thing, str):
            decl = parse_one_declaration(thing)

        else:
            assert False

        self.key = decl.name
        self.important = decl.important

        for component in decl.value:
            self.__feed_blurb(component)

        self.consolidate_tokens()
        if len(self.tokens) == 0:
            if self.key == "vertical-align":
                self.tokens.append("inherit")

            else:
                print("self.key:", self.key)
                assert False

        self.final_checks()

    def last_token(self):
        if len(self.tokens) > 0:
            return self.tokens[-1]
        return None

    def conditional_append_token_space(self, chars):
        if len(self.tokens) > 0 and self.tokens[-1] not in chars:
            self.tokens.append(" ")

    def __feed_blurb(self, blurb):
        if isinstance(blurb, WhitespaceToken):
            self.conditional_append_token_space([" "])
            return

        if isinstance(blurb, DimensionToken):
            self.conditional_append_token_space([" "])
            dimen = Css_Dimension(blurb)
            self.tokens.append(dimen)
            self.dimensions.append(dimen)
            return

        if isinstance(blurb, NumberToken):
            if self.key in [
                "z-index",
                "stroke-width",
                "opacity",
                "stroke-opacity",
                "fill-opacity",
                "font-size",  # happens with svg text
                "stroke-miterlimit",
                "stroke-dashoffset",
                "stroke-dasharray",
                "font-weight",
                "line-height",
                "shape-padding",
                "solid-opacity",
                "font",
                "stop-opacity",
            ]:
                self.conditional_append_token_space([" ", "="])
                num = Css_Number(blurb)
                self.tokens.append(num)
                self.numbers.append(num)

            else:
                if self.key not in [
                    "border",
                    "margin",
                    "padding",
                    "text-indent",
                    "min-width",
                    "max-width",
                    "min-height",
                    "max-height",
                    "top",
                    "left",
                    "bottom",
                    "right",
                    "border-width"
                ]:
                    print("self.key, blurb:", self.key, blurb)
                    assert False
                self.conditional_append_token_space([" "])
                dimen = Css_Dimension(blurb)
                self.tokens.append(dimen)
                self.dimensions.append(dimen)
            return

        if isinstance(blurb, IdentToken):
            if blurb.lower_value in HTML_COLORS:
                assert self.color is None
                self.color = Css_Color(blurb.lower_value)
                self.tokens.append(self.color)

            else:
                assert '"' not in blurb.value
                self.tokens.append(blurb.value)

            return

        if isinstance(blurb, HashToken):
            assert encodes_html_color("#" + blurb.value)
            assert self.color is None
            self.color = Css_Color("#" + blurb.value)
            self.tokens.append(self.color)
            return

        if isinstance(blurb, LiteralToken):
            assert blurb.value != "%"
            assert '"' not in blurb.value
            self.tokens.append(blurb.value)
            return

        if isinstance(blurb, FunctionBlock):
            self.tokens.append(blurb.name)
            # commas show up as tokens, so no need to do ",".join:
            self.tokens.append(
                "(" + "".join([c.serialize() for c in blurb.arguments]) + ")"
            )
            return

        if isinstance(blurb, URLToken):
            self.tokens.append("url(" + blurb.value + ")")
            return

        if isinstance(blurb, PercentageToken):
            self.tokens.append(str(blurb.value) + "%")
            return

        if isinstance(blurb, StringToken):
            assert "'" not in blurb.value
            self.tokens.append("'" + blurb.value + "'")
            if self.key == 'stroke-miterlimit':
                assert False
            return

        print("unsupported blurb type:", blurb.type)
        print("the blurb itself:", blurb)
        assert False

    def consolidate_tokens(self):
        new_tokens = []
        last = None
        for c in self.tokens + [42]:
            if all([
                isinstance(c, str),
                c != " ",
                isinstance(last, str),
                last != " ",
            ]):
                last += c
            else:
                if last != " " and last is not None:
                    new_tokens.append(last)
                last = c
        assert len(new_tokens) > 0 or len(self.tokens) == 0
        self.tokens = new_tokens

    def final_checks(self):
        assert isinstance(self.important, bool)
        count1 = len([c for c in self.tokens if isinstance(c, Css_Dimension)])
        count2 = len(self.dimensions)
        assert count1 == count2
        count1 = len([c for c in self.tokens if isinstance(c, Css_Color)])
        count2 = 0 if self.color is None else 1
        assert count1 == count2

        # if self.key  == 'stroke-miterlimit':
        #     print("")
        #     for z in self.tokens:
        #         if isinstance(z, str):
        #             print("string token:",  z)

        #         else:
        #             print("type(z), z.__str__():", type(z), z.__str__())
        #     print("")

        #     assert False

        if self.key in ["margin", "padding"]:
            if 'auto' not in self.tokens or len(self.tokens) != 1:
                assert len(self.dimensions) == len(self.tokens)
                assert len(self.dimensions) in [1, 2, 3, 4]

    def value(self):
        if len(self.dimensions) == 1 and len(self.numbers) == 0:
            return self.dimensions[0].value

        if len(self.dimensions) == 0 and len(self.numbers) == 1:
            return self.numbers[0].value

        assert False

    def multiply_by(self, number):
        assert isinstance(number, Real)
        assert len(self.dimensions) + len(self.numbers) > 0
        for x in chain(self.dimensions, self.numbers):
            x.multiply_by(number)

    def __setitem__(self, key, item):
        if isinstance(key, int):
            assert 0 <= key < len(self.dimensions)
            self.dimensions.set_to(item)

        elif key in ["t", "r", "b", "l"]:
            assert self.key in ["margin", "padding"]
            assert len(self.dimensions) in [1, 2, 4]
            if len(self.dimensions) == 1:
                for _ in range(3):
                    a = deepcopy(self.dimensions[0])
                    self.dimensions.append(a)
                    self.tokens.append(a)
            if len(self.dimensions) == 2:
                a = deepcopy(self.dimensions[0])
                self.dimensions.append(a)
                self.tokens.append(a)
                b = deepcopy(self.dimensions[1])
                self.dimensions.append(b)
                self.tokens.append(b)
            assert len(self.dimensions) == len(self.tokens) == 4
            if key == "t":
                self.dimensions[0].set_to(item)
            elif key == "r":
                self.dimensions[1].set_to(item)
            elif key == "b":
                self.dimensions[2].set_to(item)
            elif key == "l":
                self.dimensions[3].set_to(item)
            else:
                assert False

        elif key in ["tb", "lr", "bt", "rl"]:
            if len(self.dimensions) == 1:
                a = deepcopy(self.dimensions[0])
                self.dimensions.append(a)
                self.tokens.append(a)
            if len(self.dimensions) == 2:
                if key in ["tb", "bt"]:
                    self.dimensions[0].set_to(item)
                elif key in ["lr", "rl"]:
                    self.dimensions[1].set_to(item)
                else:
                    assert False
            else:
                assert len(self.dimensions) == 4
                if key in ["tb", "bt"]:
                    self.dimensions[0].set_to(item)
                    self.dimensions[2].set_to(item)
                elif key in ["rl", "lr"]:
                    self.dimensions[1].set_to(item)
                    self.dimensions[3].set_to(item)
                else:
                    assert False
            if ([
                self.dimensions[0] == self.dimensions[2],
                self.dimensions[1] == self.dimensions[3],
            ]):
                self.dimensions = self.dimensions[:2]
                self.tokens = self.tokens[:2]

        elif key in ["all", "trbl"]:
            self.dimensions[0].set_to(item)
            self.dimensions = self.dimensions[:1]
            self.tokens = self.tokens[:1]

        else:
            assert False

    def val_str(self, decimals=None):
        to_return = ""
        for z in self.tokens:
            if isinstance(z, str):
                # if decimals is not None:
                #     try:
                #         to_add = " " + ze_2f(float(z), decimals)
                #     except:
                #         to_add = " " + z
                # else:
                #     to_add = " " + z
                to_return += " " + z

            else:
                to_return += " " + z.__str__(decimals=decimals)

        if self.important:
            to_return += "!important"

        return to_return[1:]

    def key_str(self):
        return self.key + ":"

    def __str__(self, decimals=None, space=True):
        return self.key_str() + (" " if space else "") + self.val_str(decimals=decimals)

    def __repr__(self):
        return self.val_str()

    def __eq__(self, other):
        if isinstance(other, Css_Declaration):
            return self.key_str() == other.key_str() and self.val_str() == other.val_str()

        elif isinstance(other, str):
            return self.val_str() == other

        else:
            assert False

    def __mul__(self, other):
        assert isinstance(other, Real)
        assert len(self.numbers) + len(self.dimensions) == 1
        to_return = deepcopy(self)
        for z in to_return.tokens:
            z.multiply_by(other)
        for z in to_return.numbers:
            z.multiply_by(other)
        for z in to_return.dimensions:
            z.multiply_by(other)
        return to_return


def target_list_from_prelude(prelude):
    targets = []
    new_target = ""
    assert isinstance(prelude, list)
    for c in prelude + [","]:
        if isinstance(c, HashToken):
            new_target += "#" + c.value

        elif isinstance(c, LiteralToken) and c.value != ",":
            new_target += c.value

        elif isinstance(c, IdentToken):
            new_target += c.value

        elif isinstance(c, WhitespaceToken):
            if len(new_target) > 0 and new_target[-1] != " ":
                new_target += " "

        elif isinstance(c, SquareBracketsBlock):
            assert new_target != ""
            new_target += c.serialize()

        elif isinstance(c, FunctionBlock):
            new_target += c.serialize()

        elif isinstance(c, LiteralToken) or c == ",":
            assert c == "," or c.value == ","
            assert "," not in new_target
            targets.append(new_target.strip())
            new_target = ""

        else:
            print("c:", c, c.type)
            assert False

    assert all(x != "" for x in targets)
    return targets


def style_parse(thing):
    if isinstance(thing, dict):
        if all(isinstance(x, Css_Declaration) for key, x in thing.items()):
            return thing
        assert all(isinstance(key, str) for key in thing)
        # assert all(isinstance(val, str) for key, val in thing.items())
        string = ""
        for key, val in thing.items():
            val_str = str(val)
            assert ":" not in key and ";" not in key
            assert ":" not in val_str and ";" not in val_str
            string += key + ":" + val_str + ";"

        thing = string

    assert isinstance(thing, str) or isinstance(thing, list)

    to_return = {}
    
    decls = parse_declaration_list(thing, skip_whitespace=True)
    for decl in decls:
        if isinstance(decl, Comment):
            continue

        if isinstance(decl, ParseError):
            print("thing:", thing)
            print("ParseError decl:")
            print(decl)
            assert False

        if decl.name in to_return:
            print(
                f"~~~ warning: ~~~ style_parse overwriting field '{decl.name}' in some css block"
            )

        to_return[decl.name] = Css_Declaration(decl)
        
    for key in to_return:
        assert isinstance(to_return[key], Css_Declaration)

    return to_return


def generate_compact_style(thing, separator=";"):
    if isinstance(thing, str):
        # thing = style_parse(thing)
        return thing

    blurbs = []
    for _, decl in thing.items():
        blurbs.append(decl.__str__(space=False))

    return separator.join(blurbs)


def check_css_selector_health(selector):
    assert isinstance(selector, str)
    assert selector == selector.strip()
    assert len(selector) > 0

    pieces = [x for x in re.split(r"[ >]", selector) if x]

    for p in pieces:
        assert not p.endswith(":")
        assert not p.startswith(":")
        if all([
            not p.startswith("."),
            not p.startswith("#"),
            not p.startswith("@"),
            not p.startswith("*"),
        ]):
            prefix = p
            if "." in prefix:
                prefix = prefix.split(".")[0]
            if ":" in prefix:
                prefix = prefix.split(":")[0]
            if prefix not in [
                "p",
                "a",
                "div",
                "html",
                "head",
                "body",
                "span",
                "tspan",
                "text",
                "textPath",
                "b",
                "i",
                "del",
                "svg",
                "g",
                "rect",
                "circle",
                "ellipse",
            ]:
                print("unexpected css prefix:", prefix)
                assert False


def simple_unfold_css(thing):
    if isinstance(thing, dict):
        css = thing

    else:
        css = {}
        rules = parse_rule_list(thing, skip_whitespace=True)
        for r in rules:
            if r.type == "qualified-rule":
                targets = target_list_from_prelude(r.prelude)

            elif r.type == "at-rule":
                assert len(r.prelude) == 1
                assert isinstance(r.prelude[0], WhitespaceToken)
                assert r.at_keyword != ""
                targets = ["@" + r.at_keyword]

            else:
                assert False

            for t in targets:
                assert isinstance(t, str)
                check_css_selector_health(t)
                serialized_value = "".join([c.serialize() for c in r.content])

                if t in css:
                    if (
                        t == ".subgrid"
                        and serialized_value == "stroke:#ec6c10;stroke-width:.1"
                        and css[t] == "stroke:#9e9ed1;stroke-width:.6;display:block"
                    ) or (
                        t == ".labelbox"
                        and serialized_value == "stroke:#00ff0088"
                        and css[t]
                        == "fill:none;stroke-width:2;stroke:#ff000088;display:none"
                    ):
                        print(
                            "~~~ warning warning (svgo bug?) ~~~ simple_unfold_css seeing duplicate",
                            t,
                            "style, ignoring first isinstance",
                        )

                    else:
                        print("t == '" + t + "' and")
                        print("serialized_value == '" + serialized_value + "' and")
                        print("css[t] == '" + css[t] + "'")
                        print("---\nthing:", thing)
                        print("".join([c.serialize() for c in r.content]))
                        assert False

                css[t] = serialized_value

    return css


def double_unfold_css(thing):
    if isinstance(thing, dict):
        css = thing
        for key in thing:
            thing[key] = style_parse(thing[key])

    else:
        css = {}
        rules = parse_rule_list(thing, skip_whitespace=True, skip_comments=True)
        for r in rules:
            if r.type == "qualified-rule":
                targets = target_list_from_prelude(r.prelude)
                c = style_parse(r.content)
                for index, t in enumerate(targets):
                    if t in css:
                        print(f"~~~ warning: ~~~ target {t} already in css; updating")
                        css[t].update(c)

                    else:
                        css[t] = c if index == 0 else deepcopy(c)

            elif r.type == "at-rule":
                target = "@" + r.at_keyword
                num = 1
                while target + f"_nr{num}" in css:
                    num += 1
                target = target + f"_nr{num}"
                assert target not in css
                css[target] = style_parse(r.content)

            else:
                print("r.type:", r.type)
                assert False

    return css


def load_css_from_file(filename):
    # absolute_path = get_absolute_path(filename)
    absolute_path = filename

    try:
        f = open(absolute_path, "r")

    except FileNotFoundError:
        print("os.getcwd():", os.getcwd())
        raise

    return double_unfold_css(f.read())


# load_css_from_file('../../html/css/css_can_edit.css')
