import json
import yaml
from lib_lookahead import lookahead, lookbothways


##
# yaml
##


def load_yaml(filename_or_string):
    with open(filename_or_string, 'r') as f:
        return yaml.load(f, Loader=yaml.SafeLoader)


def load_yamls(filename_or_string):
    try:
        with open(filename_or_string, 'r') as f:
            for t in yaml.load_all(f, Loader=yaml.SafeLoader):
                yield t
    except IOError:
        for t in yaml.load_all(filename_or_string):
            yield t


def dump_yaml(filename, d, indent=4):
    with open(filename, 'w') as f:
        f.write(yaml.dump(d, default_flow_style=False, indent=indent))


##
# json (hacked to allow easy commenting)
##


def strip_comments_from_json(filename):
    string = ''
    with open(filename, 'r') as f:
        comment_depth = 0
        for line in f:
            newline = ""
            double_slash = False
            for a, b, c in lookbothways(line):
                if b == '/' and c == '/':
                    double_slash = True
                if b == '/' and c == '*':
                    comment_depth += 1
                if comment_depth == 0 and not double_slash:
                    newline += b
                if a == '*' and b == '/':
                    comment_depth -= 1
                    assert comment_depth >= 0
            if len(newline) <= 0 or newline[- 1] != '\n':
                newline += '\n'
            if not newline.isspace():
                string += newline
    return string


def strip_final_unwanted_commas_from_json(string):
    lines = string.split("\n")
    hello = ""
    for l, m in lookahead(lines):
        if m and m.strip()[0] in ']}':
            hello += l.rstrip(', ') + '\n'
        else:
            hello += l + '\n'
    return hello


def load_json(filename_or_string):
    try:
        string = file2string(filename_or_string)
    except IOError:
        string = filename_or_string
    string = strip_comments_from_json(string)
    string = strip_final_unwanted_commas_from_json(string)
    return json.loads(string)


###############
# file2string #
###############


def file2string(filename):
    try:
        f = open(filename, 'r')
        string = ''
        for line in f:
            string += line
        return string
    except IOError:
        raise
