import yaml
import re

from lib_load_stuff     import load_yamls
from lib_dict_utilities import deepexists, deepget
from numbers            import Number


def encodes_integer(thing):
    try:
        z = float(thing)
        if z == int(z):
            return True
    except ValueError:
        pass
    return False


def is_star_number(thing):
    if not isinstance(thing, str) or not thing.startswith('*'):
        return False

    try:
        float(thing[1:])
    except ValueError:
        return False

    return True


def load_yaml_file(filename_in):
    with open(filename_in, 'r') as f:
        return yaml.load(f)


def extend_trace(trace, new):
    return trace + ' -> ' + new


def is_at_number(thing):
    if not isinstance(thing, str) or not thing.startswith('@'):
        return False

    try:
        num = float(thing[1:])
    
    except ValueError:
        num = -1   # for some reason the old 'at_number_parser' expects a nonnegative value...
    
    return num >= 0


def is_at_plus_number(thing):
    if not isinstance(thing, str) or thing.startswith('@'):
        return False

    two_pieces = thing.split('+')

    if not len(two_pieces) == 2:
        return False

    try:
        num1 = float(two_pieces[0])
        float(two_pieces[1])

    except ValueError:
        num1 = -1

    return num1 >= 0


def equal_as_yamls(a, b):
    if a == b:
        return True
    if type(a) == dict and type(b) == dict:
        if a.keys() != b.keys():
            return False
        for k in a:
            if not equal_as_yamls(a[k], b[k]):
                return False
        return True
    if type(a) == list and type(b) == list:
        if len(a) != len(b):
            return False
        for u, v in zip(a, b):
            if not equal_as_yamls(u, v):
                return False
        return True
    return False


def all_equal_as_yamls(a):
    for i in range(len(a)):
        for j in range(i + 1, len(a)):
            if not equal_as_yamls(a[i], a[j]):
                return False
    return True


def template_string_contains_atom(string, *args):
    atoms = string.strip().split('|')
    return any(a in atoms for a in args)


def template_string_allows_numbers(string):
    return template_string_contains_atom(string, '*1', '[number]', '[*number]')


def template_string_allows_star_numbers(string):
    return template_string_contains_atom(string, '*1', '[*number]')


def template_string_allows_at_numbers(string):
    return template_string_contains_atom(string, '@0', '[@number]', '@0+0', '[@number+number]')


def template_string_allows_at_plus_numbers(string):
    return template_string_contains_atom(string, '@0+0', '[@number+number]')


def template_string_allows_empty_string(string):
    return template_string_contains_atom(string, '[empty]')


def template_string_allows_literal(string, literal):
    return template_string_contains_atom(string, literal)


def follow_path_recursive(element, path):
    if path == []:
        return True, element
    if isinstance(element, dict) and path[0] in element:
        return follow_path_recursive(element[path[0]], path[1:])
    if isinstance(element, list) and \
       encodes_integer(path[0]) and \
       0 <= float(path[0]) < len(element):
        return follow_path_recursive(element[int(float(path[0]))], path[1:])
    return False, None


def follow_path(root, element, string):
    path = string.split('/')
    if path[0] == '[this]' or path[0] == '.':
        start = element
        path = path[1:]
    else:
        start = root
    return follow_path_recursive(start, path)


def matches_template_r(root, parent, thing, template, trace):
    assert thing is not None

    if isinstance(template, str):
        if template == '[any]':
            return True, ''

        bits = template.strip().split('|')
        for b in bits:
            if b.startswith('yaml_template_for_') and b.endswith('.yaml'):
                for inner_template in load_yamls(b):
                    print('hi, trying inner_template of type:', type(inner_template))
                    print('and right now trace is:', trace)
                    matches, T = matches_template(thing, inner_template)
                    if matches:
                        return True, ''
                    inner_name = b[len('yaml_template_for_'): -len('.yaml')]
                    trace += f' [nomatch {inner_name}.yaml + {T}]'

    if isinstance(template, dict):
        # if not isinstance(template, dict):  # what the?
        #     return False, trace + ' (template not a dict)'

        for k in thing:
            t_k = k
            if isinstance(k, Number):
                t_k = '[number]'

            def is_other_option_for_t_k(template_key):
                if template_key == t_k or template_key.startswith('[any]'):
                    return True
                  
                if not isinstance(template_key, str) or not template_key.startswith(t_k + '-v'):
                    return False

                tail = template_key[len(t_k + '-v'):]

                try:
                    int(tail)

                except ValueError:
                    assert False  # (bit surprised to be here but not impossible)

                return True
            
            options = [x for x in template if is_other_option_for_t_k(x)]

            if len(options) == 1 and t_k in template:
                if thing[k] is None:
                    print("dictionary field is None", k)
                    assert False

                matches, T = matches_template_r(root, thing, thing[k], template[t_k], extend_trace(trace, str(k)))
                if not matches:
                    return False, T

            else:
                found = False
                all_traces = ''
                for K in options:
                    matches, T = matches_template_r(root, thing, thing[k], template[K], extend_trace(trace, K))
                    if matches:
                        found = True
                        break
                    assert T[:len(trace)] == trace
                    all_traces += '{' + T[len(trace + ' -> '):] + '} '
                if not found:
                    return False, extend_trace(trace, str(k) + all_traces) + ' (no key match, t_k = %s)' % t_k

            if t_k + '-excludes' in template:
                forbidden = template[t_k + '-excludes']
                if type(forbidden) is not list:
                    forbidden = [forbidden]
                for path in forbidden:
                    followed_to_end, _ = follow_path(root, thing, path)
                    if followed_to_end:
                        return False, extend_trace(trace, k + ' (conflicts with excluded path %s)' % path)

    elif isinstance(thing, list):
        if not isinstance(template, list):
            return False, trace + ' (template is not a list)'

        assert len(template) > 0

        if len(template) > 1 and all_equal_as_yamls(template):
            if len(thing) != len(template):
                # print('the thing:', thing)
                # print('the template:', template)
                return False, trace + ' (list length mismatch)'
            for i in range(len(thing)):
                matches, T = matches_template_r(root, thing, thing[i], template[0], extend_trace(trace, f'[{i}]/[0]'))
                if not matches:
                    return False, T

        else:
            for i in range(len(thing)):
                if len(template) > 1:
                    found = False
                    all_traces = ''
                    for j in range(len(template)):
                        matches, T = matches_template_r(root, thing, thing[i], template[j], extend_trace(trace, f'vs[{j}|{len(template)}]'))
                        if matches:
                            found = True
                            break
                        assert T[:len(trace)] == trace
                        all_traces += '{' + T[len(trace + ' -> '):] + '} '
                    if not found:
                        return False, extend_trace(trace, ('[%d] ' % i) + all_traces) + ' (no list member match)'
                else:
                    matches, T = matches_template_r(root, thing, thing[i], template[0], extend_trace(trace, f'[{i}]'))
                    if not matches:
                        return False, T

    elif isinstance(thing, str):
        if not isinstance(template, str):
            if not isinstance(template, Number):
                return False, trace + f' (template "{template}" not a string, not even a number) (type = {type(thing)})'  # if you see 'class <str>' printed that's a python bug, sorry change the string to get it recognized as a string

            prog = re.compile(r'^\[([^\[\]]*)\]$')

            def points_to_a_number(string, already_visited):
                match = re.fullmatch(prog, string)
                msg_beginning = " --> ".join(already_visited) + " --> " + string + " --> "
                if not match:
                    return False, msg_beginning + "string doesn't match def format"
                path = match.group(1)
                split_path = path.split('/')
                if not deepexists(root, *split_path):
                    return False, path + msg_beginning + "doesn't exist in root"
                val = deepget(root, *split_path)
                if isinstance(val, Number):
                    return True, ""
                if val in already_visited:
                    return False, "circular reference"
                return points_to_a_number(val, already_visited + [val])

            result, message = points_to_a_number(thing, [])

            if not result:
                return False, trace + " (template not a string and " + message + ")"

        elif template != '':
            found = False

            if is_star_number(thing):
                if template_string_allows_star_numbers(template):
                    found = True
                else:
                    return False, trace + " (template string doesn't allow *-numbers)"

            if is_at_number(thing):
                if template_string_allows_at_numbers(template):
                    found = True
                else:
                    return False, trace + " (template string doesn't allow @-numbers)"

            if is_at_plus_number(thing):
                if template_string_allows_at_plus_numbers(template):
                    found = True
                else:
                    return False, trace + " (template string doesn't allow @+ numbers)"

            if thing == '':
                if template_string_allows_empty_string(template):
                    found = True
                else:
                    return False, trace + " (template string doesn't allow empty string)"

            if not found:
                if not template_string_allows_literal(template, thing):
                    return False, trace + f" (template string '{template}' doesn't allow literal \"{thing}\")"

    elif isinstance(thing, Number):
        if not isinstance(template, Number):
            if type(template) != str:
                return False, trace + ' (template not a number)'
            if not template_string_allows_numbers(template):
                return False, trace + ' (template not a number)'

    elif type(thing) == bool:
        if not type(template) == bool:
            return False, trace + ' (template not a boolean)'

    elif isinstance(thing, object):
        if type(template) != str:
            return False, trace + ' (template not a string, and thus does not contain object specifier ' + str(type(thing)) + ')'
        obj_type = str(type(thing))
        if obj_type not in template:
            return False, trace + f' (object {obj_type} not found in template)'

    else:
        print('type is:', type(thing))
        assert False, 'unprepared type in matches_template'

    return True, ''


def matches_template(root, filename_or_template):
    if type(filename_or_template) == str:
        template = load_yaml_file(filename_or_template)
    else:
        template = filename_or_template
    return matches_template_r(root, None, root, template, 'ROOT')


def assert_matches_template(root, filename_or_template):
    matches, trace = matches_template(root, filename_or_template)
    if not matches:
        trace = trace.replace('}', '\n}').replace('{', '{\n\t')
        assert False, trace
