def zipit3(L):
    return zip(L, L[1:], L[2:])


def zipit(L):
    return zip(L, L[1:])


def lookahead(iterable):
    it = iter(iterable)
    a = next(it)
    for b in it:
        yield a, b
        a = b
    yield a, None


def lookaheadnotNone(iterable):
    it = iter(iterable)
    a = next(it)
    for b in it:
        yield a, b
        a = b


def lookback(iterable):
    it = iter(iterable)
    a = next(it)
    yield None, a
    for b in it:
        yield a, b
        a = b


def lookbacknotNone(iterable):
    return lookaheadnotNone(iterable)


def lookbothways(iterable):
    it = iter(iterable)
    a = next(it)
    try:
        b = next(it)
        yield None, a, b
        for c in it:
            yield a, b, c
            a = b
            b = c
        yield a, b, None
    except StopIteration:
        yield None, a, None


def TrueAtFirst():
    yield True
    while True:
        yield False
