from lib_lUnAtIc import lUnAtIc


def __generic_constructor(type, attrs, children=[]):
    assert isinstance(type, str)
    assert isinstance(attrs, dict)
    assert isinstance(children, list)
    to_return = lUnAtIc(type, attrs)
    if len(children) > 0:
        to_return.append_all(children)
    return to_return


def svg_g(attrs={}, children=[]):
    return __generic_constructor("g", attrs, children)


def svg_svg(attrs={}, children=[]):
    return __generic_constructor("svg", attrs, children)


def external_svg(attrs={}, children=[]):
    return __generic_constructor("svg.preserveAspectRatio=xMinYMin", attrs, children).add_xmlns().add_xlink()


def inkscape_svg(attrs={}, children=[]):
    return external_svg(attrs, children).add_inkscape_stuff()


def svg_path(attrs={}):
    return __generic_constructor("path", attrs)


def svg_line(attrs={}):
    return __generic_constructor("line", attrs)


def svg_circle(attrs={}):
    return __generic_constructor("circle", attrs)


def svg_rect(attrs={}):
    return __generic_constructor("rect", attrs)


def svg_defs(attrs={}, children=[]):
    return __generic_constructor("defs", attrs, children)


def svg_use(attrs={}):
    return __generic_constructor("use", attrs)


def svg_tspan(attrs={}, children=[]):
    return __generic_constructor("tspan", attrs, children)


def svg_text(attrs={}, children=[]):
    return __generic_constructor("text", attrs, children)


def svg_mask(attrs={}, children=[]):
    return __generic_constructor("mask", attrs, children)


def svg_clipPath(attrs={}, children=[]):
    return __generic_constructor("clipPath", attrs, children)


def html_html(attrs={}, children=[]):
    return __generic_constructor("html", attrs, children)


def html_head(attrs={}, children=[]):
    return __generic_constructor("head", attrs, children)


def html_body(attrs={}, children=[]):
    return __generic_constructor("body", attrs, children)


def html_meta(attrs={}):
    return __generic_constructor("meta", attrs)


def html_div(attrs={}, children=[]):
    return __generic_constructor("div", attrs, children)


def html_script(attrs={}, text=""):
    return __generic_constructor("script", attrs).set_text(text)


def html_style(text={}):
    return lUnAtIc("style").update_css(text)


def html_textarea(attrs={}):
    return __generic_constructor("textarea", attrs)


def svg_polyline(attrs={}):
    return __generic_constructor("polyline", attrs)


def html_span(attrs={}, children=[]):
    return __generic_constructor("span", attrs, children)


def html_i(attrs={}, children=[]):
    return __generic_constructor("i", attrs, children)


def svg_a(attrs={}, children=[]):
    return __generic_constructor("a", attrs, children)


def html_p(attrs={}, children=[]):
    return __generic_constructor("p", attrs, children)


def svg_filter(attrs={}, children=[]):
    return __generic_constructor("filter", attrs, children)


def svg_feGaussianBlur(attrs={}, children=[]):
    return __generic_constructor("feGaussianBlur", attrs, children)


def svg_feMerge(attrs={}, children=[]):
    return __generic_constructor("feMerge", attrs, children)


def svg_feMergeNode(attrs={}):
    return __generic_constructor("feMergeNode", attrs)


def svg_linearGradient(attrs, children):
    return __generic_constructor("linearGradient", attrs, children)


def svg_radialGradient(attrs, children):
    return __generic_constructor("radialGradient", attrs, children)


def svg_stop(attrs={}):
    return __generic_constructor("stop", attrs)


def html_img(attrs={}):
    return __generic_constructor("img", attrs)


def svg_ellipse(attrs={}):
    return __generic_constructor("ellipse", attrs)


def svg_feDropShadow(attrs={}, children=[]):
    return __generic_constructor("feDropShadow", attrs, children)


def svg_polygon(attrs={}):
    return __generic_constructor("polygon", attrs)


def svg_style(attrs={}):
    return __generic_constructor("style", attrs)


def svg_image(attrs={}):
    return __generic_constructor("image", attrs)


def svg_link(attrs={}):
    return __generic_constructor("link", attrs)


def svg_favicon_link(attrs={}):
    attrs['rel'] = 'icon'
    return __generic_constructor("link", attrs)


def gaussian_blur_factory(extent, intensity, include_original=False, x='-50%', y='-50%', width='200%', height='200%'):
    assert 1 <= intensity <= 9
    assert isinstance(intensity, int)
    assert 0.1 <= extent <= 9.9

    id = 'gaussian_blur_' + str(extent) + \
        '_' + str(intensity) + \
        '_' + str(int(include_original)) + \
        '_' + str(x) + \
        '_' + str(y) + \
        '_' + str(width) + \
        '_' + str(height)

    merge_nodes = [
        svg_feMergeNode({ 'in': '¢' }) for i in range(intensity)
    ]

    if include_original:
        merge_nodes.append(svg_feMergeNode({ 'in': 'SourceGraphic' }))

    return svg_filter({
        'id': id,
        'x': x,
        'y': y,
        'width': width,
        'height': height,
    }, [
        svg_feGaussianBlur({
            'in': 'SourceGraphic',
            'result': '¢',
            'stddeviation': extent,
        }),
        svg_feMerge({}, merge_nodes)
    ])
