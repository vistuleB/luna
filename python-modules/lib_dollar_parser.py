def comments_and_dollars_parser(text, strip_comments=False):
    def wrap_up_chunk(type, start, end):
        assert type in ['text', 'comment', 'inline', 'display']
        assert 0 <= start < end <= len(text)
        contents = text[start:end]
        if strip_comments and type == 'comment':
            contents = contents.strip()
        results.append({'type': type, 'contents': contents})

    def report_status():
        guys = (
            {'name': 'in_comment', 'value': in_comment},
            {'name': 'in_double_dollar', 'value': in_double_dollar},
            {'name': 'in_single_dollar', 'value': in_single_dollar},
            {'name': 'in_latex_text', 'value': in_latex_text},
            {'name': 'in_single_dollar_within_latex_text', 'value': in_single_dollar_within_latex_text},
            {'name': 'last_char_unescaped_backslash', 'value': last_char_unescaped_backslash}
        )

        num_active = sum(int(g['value']) for g in guys)

        if num_active == 0:
            print("status: all false")
        else:
            print("status:")
            ", ".join(g['name'] for g in guys if g['value'] is True)

    in_comment = False
    in_double_dollar = False
    in_single_dollar = False
    in_latex_text = False
    in_single_dollar_within_latex_text = False
    last_char_unescaped_backslash = False

    results = []

    chunk_start = 0
    i = 0

    while i < len(text):
        remaining_text = text[i:]

        if last_char_unescaped_backslash:
            assert in_double_dollar or in_single_dollar

        if remaining_text.startswith('<!--'):
            assert not in_double_dollar
            assert not in_single_dollar
            assert not in_latex_text
            assert not in_single_dollar_within_latex_text
            assert not in_comment
            if chunk_start >= 0:
                wrap_up_chunk('text', chunk_start, i)
            in_comment = True
            i += len('<!--')
            chunk_start = i
            continue

        if in_comment:
            if remaining_text.startswith('-->'):
                assert not in_double_dollar
                assert not in_single_dollar
                assert not in_latex_text
                assert not in_single_dollar_within_latex_text
                wrap_up_chunk('comment', chunk_start, i)
                i += len('-->')
                chunk_start = i
                in_comment = False
                continue
            i += 1
            continue

        assert not in_comment

        if in_single_dollar_within_latex_text:
            if remaining_text.startswith("$"):
                if last_char_unescaped_backslash:
                    last_char_unescaped_backslash = False
                    i += 1
                    continue
                in_single_dollar_within_latex_text = False
                i += 1
                continue
            if remaining_text.startswith("\\"):
                last_char_unescaped_backslash = not last_char_unescaped_backslash
                i += 1
                continue
            last_char_unescaped_backslash = False
            i += 1
            continue

        assert not in_comment
        assert not in_single_dollar_within_latex_text

        if in_latex_text:
            if remaining_text.startswith("$"):
                if last_char_unescaped_backslash:
                    last_char_unescaped_backslash = False
                    i += 1
                    continue
                assert not remaining_text.startswith("$$")
                in_single_dollar_within_latex_text = True
                i += 1
                continue
            if remaining_text.startswith("}"):
                assert not last_char_unescaped_backslash
                assert not in_single_dollar_within_latex_text
                in_latex_text = False
                i += 1
                continue
            if remaining_text.startswith("\\"):
                last_char_unescaped_backslash = not last_char_unescaped_backslash
                i += 1
                continue
            last_char_unescaped_backslash = False
            i += 1
            continue

        assert not in_comment
        assert not in_single_dollar_within_latex_text
        assert not in_latex_text

        if in_double_dollar:
            assert not in_single_dollar
            if remaining_text.startswith("$$"):
                if last_char_unescaped_backslash:
                    last_char_unescaped_backslash = False
                    i += 1
                    continue
                wrap_up_chunk('display', chunk_start, i)
                in_double_dollar = False
                i += len('$$')
                chunk_start = i
                continue
            if remaining_text.startswith("$"):
                if last_char_unescaped_backslash:
                    last_char_unescaped_backslash = False
                    i += 1
                    continue
                assert False
            if remaining_text.startswith("\\te{") or remaining_text.startswith("\\textrm{"):
                if last_char_unescaped_backslash:
                    last_char_unescaped_backslash = False
                    i += 1
                    continue
                assert not in_latex_text
                in_latex_text = True
                assert not in_single_dollar_within_latex_text  # recently un-commented-out... why was it commented out?
                if remaining_text.startswith("\\te{"):
                    i += len('\\te{')
                else:
                    i += len('\\textrm{')
                continue
            if remaining_text.startswith("\\"):
                last_char_unescaped_backslash = not last_char_unescaped_backslash
                i += 1
                continue
            last_char_unescaped_backslash = False
            i += 1
            continue

        assert not in_comment
        assert not in_single_dollar_within_latex_text
        assert not in_latex_text
        assert not in_double_dollar

        if in_single_dollar:
            if remaining_text.startswith("$"):
                if last_char_unescaped_backslash:
                    last_char_unescaped_backslash = False
                    i += 1
                    continue
                wrap_up_chunk('inline', chunk_start, i)
                in_single_dollar = False
                i += len('$')
                chunk_start = i
                continue
            if remaining_text.startswith("\\te{") or remaining_text.startswith("\\textrm{"):
                if last_char_unescaped_backslash:
                    last_char_unescaped_backslash = False
                    i += 1
                    continue
                in_latex_text = True
                assert not in_single_dollar_within_latex_text
                if remaining_text.startswith("\\te{"):
                    i += len('\\te{')
                else:
                    i += len('\\textrm{')
                continue
            if remaining_text.startswith("\\"):
                last_char_unescaped_backslash = not last_char_unescaped_backslash
                i += 1
                continue
            last_char_unescaped_backslash = False
            i += 1
            continue

        assert not last_char_unescaped_backslash

        if remaining_text.startswith('$$'):
            if chunk_start < i:
                wrap_up_chunk('text', chunk_start, i)
            in_double_dollar = True
            i += len('$$')
            chunk_start = i
            continue

        if remaining_text.startswith("$"):
            if chunk_start < i:
                wrap_up_chunk('text', chunk_start, i)
            in_single_dollar = True
            i += len('$')
            chunk_start = i
            continue

        i += 1
        continue

    assert i == len(text)
    assert not in_comment
    assert not in_double_dollar
    assert not in_single_dollar
    if chunk_start < i:
        wrap_up_chunk('text', chunk_start, i)

    return results
