import os
import copy
from lib_lUnAtIc import lUnAtIc
from lib_load_lUnAtIc import load_lUnAtIc
from lib_mock_display import virgin_mock_display
from lib_css import css_dimension_re
from lib_svg_matrix import TRBLObject
from svgpathsio import bbox2subpath, generate_transform
from lib_load_stuff import load_yaml
from lib_dollar_parser import comments_and_dollars_parser
from lib_constructors import html_p, svg_path
from lib_dirnames import luna_dir, svgs_headless_dir, node_headless_dir, deno_headless_dir


assert os.path.exists(luna_dir)
if not os.path.exists(svgs_headless_dir):
    os.mkdir(svgs_headless_dir)
assert os.path.exists(svgs_headless_dir)
assert os.path.exists(node_headless_dir)
assert os.path.exists(deno_headless_dir)


FONT_SIZE_AS_STRING = "20px"

g_ex_height_at_20_point_0_baskerville = 7.9978  # measured in browser per MathJax's description of 'ex' measures
g_ex_height_at_20_point_5_baskerville = 8.1979  # measured in browser per MathJax's description of 'ex' measures
g_ex_height_at_21_point_0_baskerville = 8.3978  # measured in browser per MathJax's description of 'ex' measures

g_ex_height_to_number_height_STIX = 12.866 / 8.1979  # measured in browser: some numbers were 12.866 high @ 20.5px, others 13.132
g_ex_height_to_number_height_default = g_ex_height_to_number_height_STIX  # have not measured yet

g_number_height_at_20_point_5_baskerville = 12.86  # measured in browser by inspecting size of single 'use' element

g_formula_dir = svgs_headless_dir
g_requests_file = luna_dir + "mockup_for_headless.html"
g_processed_file = luna_dir + "mockup_processed_by_headless.html"
g_request_filestring = ""
g_requested_paths = {}
g_alternate_glyphs_in = luna_dir + "yaml_alternate_glyphs.yaml"
g_list_of_alternate_glyphs = load_yaml(g_alternate_glyphs_in)
g_node_headless_cmd = f"node {node_headless_dir}puppeteer_node.js"
g_deno_headless_cmd = f"deno run --allow-read --allow-write --allow-run --allow-env --allow-net {deno_headless_dir}puppeteer_deno.ts"
use_node = True


def ex_height_to_number_height_for_font(fontname):
    if fontname == 'STIX':
        return g_ex_height_to_number_height_STIX
    
    assert fontname == 'default'
    return g_ex_height_to_number_height_default


def ex_height_for_fontpx(fontpx):
    if fontpx == 20:
        return g_ex_height_at_20_point_0_baskerville
    
    if fontpx == 20.5:
        return g_ex_height_at_20_point_5_baskerville
    
    if fontpx == 21:
        return g_ex_height_at_21_point_0_baskerville
    
    assert False

##
# receiving requests:
##

def sanitize_string_for_shell(string):
    t = ""
    for i in string:
        if i == "/":
            r = "_over_"
        elif i == "$":
            r = "_"
        elif i == "\\":
            r = "_bk_"
        elif i == "*":
            r == "_t_"
        else:
            r = i
        t += r
    return t


def add_dollars(string):
    to_return = string if string.startswith("$") else "$$" + string + "$$"

    if to_return.startswith("$$"):
        assert to_return.endswith("$$")
        assert len(to_return) > 4

    else:
        assert to_return.endswith("$")
        assert len(to_return) > 2

    return to_return


# def ze_filename_with_path(label):
#     filename = sanitize_string_for_shell(label.fontname + f"_{label.pts}_" + add_dollars(string))
#     return g_formula_dir + filename + ".svg"


def collect_svgs_and_stripped_comments_contents_recursive(node, depth=0, first_svg=None):
    in_order = []
    if node.type == "svg":
        assert 1 <= len(node) <= 2

        if len(node) == 2:
            assert first_svg is not None
            assert node[0].type == "defs"
            assert node[1].type == "g"

        elif first_svg is None:
            assert node[0].type == "defs"

        else:
            assert node[0].type == "g"

        in_order.append(node)

        if first_svg is None:
            first_svg = node

    if node.type != "svg" and isinstance(node.text, str):
        results = comments_and_dollars_parser(node.text, strip_comments=True)
        in_order.extend(g["contents"] for g in results if g["type"] == "comment")

    if node.type != "svg":
        for c in node:
            in_order.extend(collect_svgs_and_stripped_comments_contents_recursive(c, depth + 1, first_svg))
            if first_svg is None:
                for q in in_order:
                    if isinstance(q, lUnAtIc):
                        assert q.type == "svg"
                        first_svg = q
                        break

    if isinstance(node.tail, str):
        results = comments_and_dollars_parser(node.tail, strip_comments=True)
        in_order.extend(g["contents"] for g in results if g["type"] == "comment")

    return in_order


def collect_svgs_and_stripped_comments_contents_from_file(filename):
    root = load_lUnAtIc(filename, unfold=False, announce=False, remove_comments=False)
    return collect_svgs_and_stripped_comments_contents_recursive(root)


def touch_up_mathjax_svg(root):
    assert "xmlns:xlink" in root
    root.add_xmlns()
    root.cleanup_misc_mathjax_crap()

    for node in root.subtree_iterator():
        if node.type == "text":
            node.update_style_attribute({"font-family": "baskerville", "font-size": FONT_SIZE_AS_STRING,})

    root.unfold_style_attribute()
    
    if 'width' not in root:
        print("")
        print("root missing 'width':")
        print(root)
        assert False

    dimensions_parsed = {
        "width": css_dimension_re.fullmatch(root.pop("width")),
        "height": css_dimension_re.fullmatch(root.pop("height")),
        "vertical-align": css_dimension_re.fullmatch(root["style"].pop("vertical-align").val_str()),
    }

    for key, parse_result in dimensions_parsed.items():
        assert parse_result.group(2) == "ex"
        root["original-ex-" + key] = parse_result.group(1)


def mathjax_svg_specs_and_defs_and_g(label):
    try:
        root = load_lUnAtIc(label.path, unfold=False, announce=False)

    except IOError:
        g_requested_paths[label.path] = label
        return None, None, None

    assert len(root) == 2
    assert root[0].type == "defs"
    assert root[1].type == "g"
    root.unfold_viewbox()
    root.unfold_numbers(
        xtra_approved_number_fields=[
            "original-ex-width",
            "original-ex-height",
            "original-ex-vertical-align",
        ]
    )
    specs = {
        "viewbox-x": root["viewbox"][0],
        "viewbox-y": root["viewbox"][1],
        "viewbox-width": root["viewbox"][2],
        "viewbox-height": root["viewbox"][3],
        "original-ex-width": root["original-ex-width"],
        "original-ex-height": root["original-ex-height"],
        "original-ex-vertical-align": root["original-ex-vertical-align"],
    }
    g = root.find("g", {}).detach()
    g.pop('transform')
    return specs, root.ze_defs().detach(), g


####################
# FramedMathLabel object #
####################


class MathLabel:
    def __init__(
        self,
        string,
        fontpx,
        fontname,
        classname=None,
        translation=0+0j,
        h_align="start",     # start, middle, end, margins-left, margins-right
        v_align="baseline",  # baseline, x-middle, middle, number-middle, number-top, margins-top, margins-bottom
        margins_in_exes=[0, 0, 0, 0],
        angle=0,
        additional_scaling=1,
        alternate_glyphs={}
    ):
        if classname is not None:
            assert isinstance(classname, str)
            assert classname != ''

        self.formula = add_dollars(string)
        self.fontpx = fontpx
        self.fontname = fontname
        self.classname = classname
        self.translation = translation
        self.v_align = v_align
        self.h_align = h_align
        self.exes_margins = TRBLObject(
            margins_in_exes[0],
            margins_in_exes[1],
            margins_in_exes[2],
            margins_in_exes[3]
        )
        self.angle = angle
        self.additional_scaling = additional_scaling
        self.alternate_glyphs = alternate_glyphs
        self.x_middle_fudge_factor = 0
        self.number_middle_fudge_factor = 0
        self.filename = sanitize_string_for_shell(fontname + self.formula + f"@{fontpx}px.svg")
        self.path = svgs_headless_dir + self.filename

    def __str__(self):
        return (
            f"{self.formula} "
            + f"{self.x:0.4} {self.y:0.4}"
            + f'{self.h_align + " " if self.h_align != "start" else ""}'
            + f'{self.v_align + " " if self.v_align != "baseline" else ""}'
        )


def merge_mathjax_defs(root, mathjax_defs):
    for z in mathjax_defs:
        w = root.find(None, {"id": z["id"]})
        if w is not None:
            assert w in root.ze_defs()
            assert set(z()) == set(w())
            for a in z():
                if z()[a] != w()[a]:
                    print(a)
                    print(z()[a])
                    print(w()[a])
                    print(z)
                    print(w)
                    assert False

        else:
            root.get_or_create_defs().append(z.detached_copy())


def add_alternate_glyphs_to_defs(root, originalname, ext):
    the_defs = root.get_or_create_defs()
    the_id = originalname + "-" + ext
    the_path_string = g_list_of_alternate_glyphs[originalname][ext]
    assert isinstance(the_path_string, str)
    pre_existing_def = the_defs.find('path', { "id": the_id })
    if pre_existing_def is not None:
        assert 'd' in pre_existing_def
        assert the_path_string == pre_existing_def.get('d')

    else:
        the_defs.append(svg_path({'id': the_id, 'd': the_path_string}))
        assert the_defs.find('path', { "id": the_id }) is not None


##########################################
# public API
##########################################


def MathLabel_rendering_exists(label):
    return os.path.exists(label.path)


def destroy_preexisting_MathLabel_rendering(label):
    try:
        os.remove(label.path)

    except IOError:
        pass


def get_or_queue_MathLabel(root, label):
    specs, defs, g = mathjax_svg_specs_and_defs_and_g(label)
    if not specs:
        return None, None

    if root is not None:
        assert isinstance(root, lUnAtIc) and root.type == 'svg'
        merge_mathjax_defs(root, defs)
        for originalname, extension in label.alternate_glyphs.items():
            add_alternate_glyphs_to_defs(root, originalname, extension)

    ex_height = ex_height_for_fontpx(label.fontpx)
    
    inner_width = specs["viewbox-width"]
    inner_height = specs["viewbox-height"]
    inner_ex_height = inner_height / specs["original-ex-height"]
    inner_baseline = specs["original-ex-vertical-align"] * (-1) * inner_ex_height
    outer_width = specs["original-ex-width"] * ex_height
    outer_height = specs["original-ex-height"] * ex_height

    number_height = ex_height * ex_height_to_number_height_for_font(label.fontname)
    inner_number_height = number_height * inner_height / outer_height    

    margins_inner = copy.copy(label.exes_margins).scale(inner_ex_height)

    assert margins_inner.nonnegative()
    assert inner_ex_height > 0
    assert inner_height > 0
    assert outer_height > 0
    assert inner_number_height > 0

    ####################################################################################
    # pre-scaling translation Tx, Ty: place the origin at the scaling / rotating point #
    ####################################################################################

    if label.h_align == "start":
        Tx = 0
    elif label.h_align == "middle":
        Tx = -inner_width / 2
    elif label.h_align == "end":
        Tx = -inner_width
    elif label.h_align == "margins-left":
        Tx = margins_inner.left
    elif label.h_align == "margins-right":
        Tx = -inner_width - margins_inner.right
    else:
        print("offending label.h_align:", label.h_align)
        assert False

    if label.v_align == "baseline":
        Ty = 0
    elif label.v_align == "middle":
        Ty = -inner_baseline + inner_height * 0.5
    elif label.v_align == "x-middle":
        Ty = inner_ex_height * (0.5 + label.x_middle_fudge_factor)
    elif label.v_align == "number-middle":
        Ty = inner_number_height * (0.5 + label.number_middle_fudge_factor)
    elif label.v_align == "x-top":
        Ty = inner_ex_height
    elif label.v_align == "number-top":
        Ty = inner_number_height
    elif label.v_align == "margins-top":
        Ty = -inner_baseline + inner_height + margins_inner.top
    elif label.v_align == "margins-bottom":
        Ty = -inner_baseline - margins_inner.bottom
    else:
        print("offending label.h_align:", label.v_align)
        assert False

    ###########
    # scaling #
    ###########

    s = outer_width / inner_width
    s *= label.additional_scaling

    ############
    # rotation #
    ############

    angle = -label.angle

    ##################
    # modifying g... #
    ##################

    tokens = \
        ([ 'translate', label.translation ]) + \
        ([] if label.angle == 0 else [ 'rotate', -label.angle ]) + \
        ([ 'scale', s, 'translate', Tx, Ty, 'scale', 1, -1 ])

    g.set_attribute('transform', generate_transform(tokens))
    
    if label.classname is not None:
        g.add_class(label.classname)

    ####################################
    # replace refs to alternate glyphs #
    ####################################

    for originalname, extension in label.alternate_glyphs.items():
        assert isinstance(originalname, str) and not originalname.startswith('#')
        assert isinstance(extension, str)
        assert len(originalname) > 0 and len(extension) > 0
        before = "#" + originalname
        after = before + "-" + extension
        replaced = 0
        replaced += g.selective_set_attributes('use', { "xlink:href": before }, { "xlink:href": after }, True)
        replaced += g.selective_set_attributes('use', { "href": before }, { "xlink:href": after }, True)
        if replaced > 0:
            print("alternate glyph in math label: replaced", replaced, before, "with", after)

    ########################
    # bounding box subpath #
    ########################

    xmin = -margins_inner.left
    xmax = inner_width + margins_inner.right
    ymin = inner_baseline - inner_height - margins_inner.top
    ymax = inner_baseline + margins_inner.bottom

    bbox_subpath = bbox2subpath(xmin, xmax, ymin, ymax).translate(Tx, Ty).scale(s).rotate(angle).translate(label.translation)

    #########################
    # possible debug circle # g.wrap_self_in('g').append(svg_circle({ 'r': 2, 'cx': label.translation.x, 'cy': label.translation.y, 'fill': 'red' }))
    #########################

    return g, bbox_subpath


def flush_MathLabel_queue(allowfailure=False, pretty_up_processed=False):
    global g_requested_paths

    def get_first_value(d):
        to_return = None
        for _, val in d.items():
            to_return = val
        assert to_return is not None
        return to_return

    num_flushed = 0
    while len(g_requested_paths) > 0:
        label = get_first_value(g_requested_paths)
        num_flushed += flush_MathLabel_queue_at_fontpx_fontname(label.fontpx, label.fontname)
    
    assert len(g_requested_paths) == 0 or allowfailure
    g_requested_paths = {}

    return num_flushed


def flush_MathLabel_queue_at_fontpx_fontname(fontpx, fontname, allowfailure=False, pretty_up_processed=False):
    global g_requested_paths
    
    requested_this_batch = []
    for _, label in g_requested_paths.items():
        if label.fontpx == fontpx and label.fontname == fontname:
            requested_this_batch.append(label)
            
    num_requests = len(requested_this_batch)

    assert fontpx == 20 or fontpx == 20.5 or fontpx == 21

    def text_with_formulas():
        to_join = []
        for label in requested_this_batch:
            to_join.append(label.formula)
            to_join.append("<!-- " + label.formula + " -->")
        return "\n".join(to_join)

    def mockup_with_requests():
        root = virgin_mock_display(fontpx, fontname)
        text = text_with_formulas()
        paragraph_containing_our_formulas = html_p().set_text(text)
        root.body().insert_after(paragraph_containing_our_formulas, "p#first_p")
        return root

    root = mockup_with_requests()

    root.pretty_up().print_to(g_requests_file)
    print(f"executing headless on {g_requests_file} w/ {num_requests} requests")

    if use_node:
        os.system(f"{g_node_headless_cmd} {g_requests_file} > {g_processed_file}")

    else:
        print("")
        print("url being passed to deno:", f"file://{g_requests_file}", "[end]")
        print("")
        os.system(f"{g_deno_headless_cmd} file://{g_requests_file} > {g_processed_file}")

    f = open(g_processed_file, "r")
    pieces = f.read().split('<html>')
    f.close()
    assert len(pieces) == 2
    f = open(g_processed_file, "w")
    f.write("<html>" + pieces[1])
    f.close()

    if pretty_up_processed:
        print(f"prettying up {g_processed_file}")
        load_lUnAtIc(g_processed_file).pretty_up().print_to(g_processed_file)

    print(f"harvesting from {g_processed_file}")
    in_order = collect_svgs_and_stripped_comments_contents_from_file(g_processed_file)

    if not isinstance(in_order[0], lUnAtIc):
        print("")
        print("offending:")
        print(in_order[0])
        print(isinstance(in_order[0], str))
        print("")

    assert isinstance(in_order[0], lUnAtIc)
    assert in_order[0].type == "svg"
    assert len(in_order[0]) == 1
    assert in_order[0][0].type == "defs"

    svg_defs = in_order[0]

    num_successful = 0

    for label in requested_this_batch:
        assert '\t' not in label.formula
        found = False
        for index, thing in enumerate(in_order):
            if isinstance(thing, str) and thing == label.formula:
                root = in_order[index - 1]
                if not isinstance(root, lUnAtIc):
                    print("offending root:", root)
                    print("offending string_with_dollars:", label.formula)
                    assert False
                touch_up_mathjax_svg(root)
                assert (
                    root.find("defs") is None
                )  # if you're snagging on this then turn on useGlobalCache in mathjaxconfig.js
                gurgle = root.find_all(None, {"xlink:href": "[any]"})
                refs = [g["xlink:href"] for g in gurgle]
                our_defs = root.get_or_create_defs()
                for ref in refs:
                    assert ref.startswith("#")
                    z = svg_defs.find_unique(None, {"id": ref[1:]})
                    our_defs.append(z.detached_copy())

                assert len(root) == 2
                assert root[0].type == "defs"
                assert root[1].type == "g"
                root.pretty_up().print_to(label.path)
                found = True
                break

        if not found:
            print(f"")
            print(f"we have a sick puppy; label formula:")
            print(label.formula)
            print(f"[END]")
            print(f"unable to locate comment tag for {label.formula} in {g_processed_file}")
            print(f"")
            assert allowfailure
            
        else:
            num_successful += 1

    original_length = len(g_requested_paths)
    for label in requested_this_batch:
        g_requested_paths.pop(label.path)
    assert original_length - num_requests == len(g_requested_paths)

    print(f"processed {num_successful}/{num_requests} from g_requested_paths at '{fontname}' {fontpx}px")

    return num_successful
