import os 


luna_dir = '/'.join(os.path.dirname(__file__).split('/')[:-1]) + '/'
node_headless_dir = luna_dir + 'headless/'
deno_headless_dir = luna_dir + 'headless/'
svgs_headless_dir = luna_dir + 'svgs_headless/'
svgs_tmp_dir = luna_dir + 'svgs_tmp/'
yaml_dir = luna_dir + 'yaml/'
