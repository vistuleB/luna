from collections import OrderedDict
from timeit      import default_timer


all_stopwatches   = OrderedDict([])
stopwatch_stack   = []
beginning_of_time = default_timer()
last_report       = beginning_of_time


def report_all(newline=True):
    global last_report
    now = default_timer()
    all_time = now - beginning_of_time
    print("")
    for name in all_stopwatches:
        all_stopwatches[name].report(all_time)
    last_report = now
    if newline:
        print("")


class sToPwAtcH():
    def __init__(self, id):
        self.id = id
        self.total = 0
        self._on = None

    def on(self):
        assert self._on is None
        self._on = default_timer()
        return self

    def off(self):
        assert self._on is not None
        now = default_timer()
        assert now > self._on
        self.total += now - self._on
        self._on = None
        if now > last_report + 5:
            report_all()
        return self

    def report(self, all_time):
        handle = self.id + (50 - len(self.id)) * ' '
        total  = float(self.total)
        print(f": {handle} {total:6.2f} | {100 * total / all_time:6.2f}%")


def stopwatch(*args):
    global all_stopwatches
    global stopwatch_stack

    if len(args) > 0:
        assert len(args) == 1 and isinstance(args[0], str)
        if args[0] not in all_stopwatches:
            all_stopwatches[args[0]] = sToPwAtcH(args[0])
        w = all_stopwatches[args[0]].on()
        stopwatch_stack.append(w)

    else:
        assert len(stopwatch_stack) > 0
        stopwatch_stack.pop().off()
