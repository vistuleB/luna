##################
# dict utilities #
##################


def deepget(record, *path, default=None):
    if len(path) == 0:
        return record

    head, tail = path[0], path[1:]

    if isinstance(record, dict):
        return deepget(record[head], *tail, default=default) if head in record else default

    if isinstance(record, list):
        assert isinstance(head, int)  # until recently was allowing head to be a float, and converting... forgot why
        return deepget(record[head], *tail, default=default) if head < len(record) else default

    assert False


def deepexists(record, *path):
    if len(path) == 0:
        return True

    head, tail = path[0], path[1:]

    if isinstance(record, dict):
        return deepexists(record[head], *tail) if head in record else False

    if isinstance(record, list):
        assert float(head) == int(float(head))
        index = int(head)
        return \
            index < len(record) and \
            deepexists(record[index], *tail)

    assert False


def deepsetdefault(dIcT, head, *tail_and_value):
    assert len(tail_and_value) > 0

    if len(tail_and_value) == 1:
        dIcT.setdefault(head, tail_and_value[0])
        return dIcT[head]

    dIcT.setdefault(head, {})

    assert isinstance(dIcT[head], dict)

    return deepsetdefault(dIcT[head], *tail_and_value)


def update_dict_recursive(updated, updater):
    for k, v in updater.items():
        if k in updated and \
           isinstance(updated[k], dict) and \
           isinstance(v, dict):
            update_dict_recursive(updated[k], updater[k])

        else:
            updated[k] = updater[k]


def restrict(d, keys):
    throw_out = set(d).difference(set(keys))
    for k in throw_out:
        d.pop(k)


def update_dict_in_dict_and_create_if_missing(d, key, d2, assert_no_overwrite=False):
    if key not in d:
        d[key] = {}
    if assert_no_overwrite:
        for k in d2:
            assert k not in d[key]
    d[key].update(d2)


def setfresh(d, key, value):
    assert key not in d 
    d[key] = value
