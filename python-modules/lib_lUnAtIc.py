import numpy as np

from typing import Iterable
from numbers import Number, Real
from itertools import chain
from copy import deepcopy, copy
from math import sqrt

from svgpathsio import (
    parse_transform,
    generate_transform,
    generate_path,
    generate_transform_if_not_already_string,
    parse_path, 
    Path,
    Subpath,
    Segment,
    parse_subpath, 
    x_val_cut,
    random_color,
    real_numbers_in,
    compound_translations,
    custom_x_y_to_x_y_transform,
    HtmlColorsLowerCaseInverted,
)

from lib_svg_matrix import (
    Point,
    is_scaling_and_translation,
    extract_point,
    degree_angle,
    is_translation,
    BoundingBox,
    TRBLObject,
    ze_2f,
    int_else_float,
    is_identity_matrix,
)
from lib_css import (
    double_unfold_css, simple_unfold_css,
    style_parse, Css_Declaration, check_css_selector_health,
    convert_Css_Declaration_dict_to_ordinary_dict,
    generate_compact_style,
)
from lib_timing import stopwatch
from lib_dict_utilities import deepget

import re

from svgpathsio.path import multisplit

FLOAT_RE_NR_1 = re.compile(r"(\d+(?:\.\d*)?|\.\d+)")
FLOAT_RE_NR_2 = re.compile(r"[-+]?(\d+(?:\.\d*)?|\.\d+)")

SELF_CLOSING = [
    "circle",
    "ellipse",
    "rect",
    "path",
    "line",
    "polyline",
    "polygon",
    "use",
    "sodipodi:namedview",
    "img",
    "br",
    "col",
    "stop",
    "image",
    "feColorMatrix",
    "feGaussianBlur",
]
HTML_ELEMENTS_WITH_SVG_CHILDREN_POTENTIALLY = [
    "head",
    "body",
    "div",
    "p",
    "span",
    "td",
    "center",
    "button"  # new!
]
SELF_CLOSING_SKIP_SLASH = ["br", "col", "img"]
APPROVED_NUMBER_FIELDS = [
    "x",
    "y",
    "width",
    "height",
    "cx",
    "cy",
    "dx",
    "dy",
    "r",
    "rx",
    "ry",
    "x1",
    "y1",
    "x2",
    "y2",
    "stroke-width",
    "stroke-miterlimit",
    "inkscape:window-width",
    "inkscape:window-height",
    "inkscape:zoom",
    "fill-opacity",
    "stddeviation",
    "flood-opacity",
]
GEOMETRIC_ELEMENTS = [
    "path",
    "rect",
    "circle",
    "ellipse",
    "polyline",
    "line",
    "polygon",
]
INTANGIBLE_ELEMENTS = [
    "defs",
    "style",
    "head",
    "mask",
    "lineargradient",
    "radialgradient",
]
RENDERED_NON_GEOMETRIC_ELEMENTS = ["use", "text", "textPath"]
RENDERED_ELEMENTS = GEOMETRIC_ELEMENTS + RENDERED_NON_GEOMETRIC_ELEMENTS
TRANSFORMABLE = RENDERED_ELEMENTS + ["svg", "g"]

CAMEL_CASE_TYPES = [
    "clipPath",
    "linearGradient",
    "radialGradient",
    "feGaussianBlur",
    "feMerge",
    "feMergeNode",
    "feDropShadow",
    "flowRoot",
    "flowPara",
]
CAMEL_CASE_TYPES_DICTIONARY = {x.lower(): x for x in CAMEL_CASE_TYPES}

CAMEL_CASE_ATTRIBUTES = [
    "preserveAspectRatio",
    "viewBox",
    "stdDeviation",
    "gradientUnits",
    "gradientTransform",
]
CAMEL_CASE_ATTRIBUTES_DICTIONARY = {x.lower(): x for x in CAMEL_CASE_ATTRIBUTES}

STIXWEBMAIN_REPLACEMENT = "\u222b"
STIXWEBDOUBLESTRUCK_REPLACEMENT = "\u221a"
STIXWEBVARIANTS_REPLACEMENT = "\u2202"
MathJax_SVG_REPLACEMENT = "¥"
MathJax_SVG_Display_REPLACEMENT = "†"

MathJax_SVG_EXPECTED_STYLE = {
    "font-size": "100.0%",
    "display": "inline-block",
    "position": "relative",
}

MathJax_SVG_Display_EXPECTED_STYLE = {"text-align": "center"}

unique_object = object()


def is_whitespace(string):
    assert isinstance(string, str)
    return all(x in " \n\t" for x in string)


def is_string_and_good_classname(string):
    return \
        isinstance(string, str) and \
        len(string) > 0 and \
        ' ' not in string
        

def heuristic_display_classifier(thing):
    if thing.type == "p":
        return "block", 1

    elder_types = [node.type for node in thing.elders()]

    if "math" in elder_types:
        return "inline", 2

    if thing.type == "text":
        return "inline-block", 2.5

    if "svg" in elder_types and (
        "foreignobject" not in elder_types
        or elder_types[::-1].index("svg") < elder_types[::-1].index("foreignobject")
    ):
        return "block", 3
    
    classes = thing.class_attribute_as_list()

    def classes_contain(name):
        return name in classes
    
    def classes_contain_any(names):
        return any(name in classes for name in names)

    if classes_contain_any(["MathJax_SVG", MathJax_SVG_REPLACEMENT]):
        return "inline-block", 4

    if thing.type in ["b", "i", "img", "del", "span", "tspan", "sub", "sup", "a"]:
        return "inline", 5

    sss = thing.get("style")
    if sss is not None:
        if isinstance(sss, str):
            if sss == "white-space:nowrap" or sss == "white-space: nowrap":
                return "inline", 14

            if "absolute" in sss:
                return "inline-block", 15

            if "display" in sss:
                thing.unfold_style_attribute()
                sss = thing.get("style")

        if isinstance(sss, dict):
            cnvrted = convert_Css_Declaration_dict_to_ordinary_dict(sss)
            if cnvrted == {"white-space": "nowrap"}:
                return "inline", 16

            if cnvrted.get("position") in ["absolute", "absolute!important"]:
                return "inline-block", 17

            if cnvrted.get("display") == "inline-block":
                return "inline-block", 18

    if not any(tp in ["p", "li", "tr", "tspan", "span"] for tp in elder_types):
        return "block", 6

    if thing.type in ["br", "svg", "script", "futuremathjaxspan"]:
        return "inline-block", 7

    if thing.type in ["math", "futuremathjaxdiv"]:
        return "block", 8

    if classes_contain_any(["MathJax_SVG_Display", MathJax_SVG_Display_REPLACEMENT]):
        return "block", 9

    if classes_contain("MathJax_Preview"):
        return "inline-block", 10

    if classes_contain_any(["MathJax_SVG", MathJax_SVG_REPLACEMENT]):
        return "inline", 11

    if classes_contain("MJX_Assistive_MathML"):
        return "block", 12

    if classes_contain("space"):
        return "inline-block", 13

    if thing.type in ["span", "img", "tspan"]:
        assert False

    if thing.type in ["input", "label"]:
        return "inline-block", 19  # recently changed from 'inline' (2020)

    if thing.type in ["td"]:
        return "block", 20  # recently changed this from 'inline-block'

    if thing.matches("div", {"class": "column"}):
        return "block", 21

    if thing.matches("div", {"class": "[string-contains]arrow_gap_div"}):
        return "inline-block", 22

    if thing.matches("div", {"class": "mathblock"}):
        return "block", 23

    if thing.matches("div", {"class": "block"}):
        return "block", 24

    if thing.matches("button", {}):
        return "block", 25

    if 'id' in thing and thing['id'].startswith('_'):
        return "block", 26

    print("")
    print("unclassified node:", thing)
    print("")
    assert False


def heuristic_out_of_flow_classifier(thing):
    if thing.type in ["script", "style", "head", "meta", "link"]:
        return True

    if "style" in thing:
        sss = thing["style"]
        if isinstance(sss, str):
            if "absolute" in sss or "display: none" in sss or "display:none" in sss:
                return True

        elif sss.get("position") in ["absolute", "absolute!important"]:
            return True

    return False


fucking_flag = False


def remove_gratuitous_adjacencies(root):
    # phase 1: local whitespace injection based on display type
    tag = root.opening_tag
    assert tag is not None
    while tag is not None:
        # if tag.node.type == "pre":
        #     if tag.tagt == "opening":
        #         print(f"[start]{tag.node.text}[end]")
        #     tag = tag.next_tag()
        #     continue

        display, where = heuristic_display_classifier(tag.node)
        

        if display == "inline":
            if tag.is_opening() and tag.has_whitespace_to_right():
                tag.left_inject_whitespace()
                tag.right_trim_whitespace()

            if tag.is_closing() and tag.has_whitespace_to_left():
                tag.right_inject_whitespace()
                tag.left_trim_whitespace()

        elif display == "inline-block":
            if len(tag.node) > 0:
                if tag.is_opening():
                    tag.right_inject_whitespace()

                else:
                    tag.left_inject_whitespace()

        elif display == "block":
            if tag.node.type != "script" and tag.node.type != "style" and tag.node.type != "pre":
                tag.left_inject_whitespace()
                tag.right_inject_whitespace()

        else:
            print("display:", display)
            print("where:", where)
            assert False

        tag = tag.next_tag()

    # phase 2: out-of-flow whitespace propagation

    for dir in ["forward", "backward"]:
        node = root
        while node is not None:
            if heuristic_out_of_flow_classifier(node):
                if node.opening_tag.has_whitespace_to_left():
                    node.closing_tag.right_inject_whitespace()

                elif node.closing_tag.has_whitespace_to_right():
                    node.opening_tag.left_inject_whitespace()

            node = node.next() if dir == "forward" else node.prev()


def remove_gratuitous_spaces(root):
    tag = root.opening_tag
    assert tag is not None
    while tag is not None:
        display, where = heuristic_display_classifier(tag.node)
        
        if display == "inline":
            if tag.is_opening() and tag.has_whitespace_to_right():
                tag.left_inject_whitespace()
                tag.right_trim_whitespace()

            if tag.is_closing() and tag.has_whitespace_to_left():
                tag.right_inject_whitespace()
                tag.left_trim_whitespace()

        elif display == "inline-block":
            if tag.is_opening():
                tag.right_trim_whitespace()

            else:
                tag.left_trim_whitespace()

        elif display == "block":
            tag.left_trim_whitespace()
            tag.right_trim_whitespace()

        else:
            assert False

        tag = tag.next_tag()


class BoundingBoxNode(BoundingBox):
    def __init__(self, node, default_style_without_tagging_classes, tagging_classes):
        super().__init__()
        self.node = node
        self.children = []
        self.default_style = default_style_without_tagging_classes
        self.classes = set([])
        self.contains_nonempty_text_element = False

        if "class" in node:
            to_ingest = node["class"]
            if isinstance(to_ingest, str):
                to_ingest = to_ingest.split()

            assert isinstance(to_ingest, list)
            assert all(isinstance(x, str) and " " not in x for x in to_ingest)

            for x in to_ingest:
                if x in tagging_classes:
                    self.classes.add(x + "_bbox")

    def init(self, *args):
        assert self.is_empty()
        assert len(self.children) == 0

        if len(args) == 4:
            super().init(*args)
            return self

        assert len(args) == 1
        assert isinstance(args[0], BoundingBoxNode)

        b = args[0]

        assert len(b.classes) == 0
        assert b.default_style == self.default_style

        if b.is_empty():
            assert len(b.children) == 0
            return self

        self.xmin = b.xmin
        self.xmax = b.xmax
        self.ymin = b.ymin
        self.ymax = b.ymax
        self.children = [z for z in b.children]
        self.check_integrity()

        return self

    def ingest_nonempty(self, child):
        assert isinstance(child, BoundingBoxNode)
        if child.is_nonempty():
            self.agglomerate(child)
            self.children.append(child)

        if child.contains_nonempty_text_element:
            self.contains_nonempty_text_element = True

    def render(self, recursive=True):
        assert self.is_nonempty()

        if len(self.classes) > 0:
            style_string = ""
            class_string = "." + ".".join(self.classes)

        else:
            style_string = "." + generate_compact_style(self.default_style, separator=".")
            class_string = ""

        assert (style_string == "") != (class_string == "")

        us = lUnAtIc(
            f"""rect{class_string}{style_string}.x={self.xmin}.y={self.ymin}.width={self.width()}.height={self.height()}"""
        )

        if recursive and len(self.children) > 0:
            return lUnAtIc("g.bbox_dad").append_all(
                us,
                lUnAtIc("g.bbox_children").append_all(
                    [x.render(recursive=True) for x in self.children]
                ),
            )

        return us

    def subtree_iterator(self):
        yield self
        for c in self.children:
            for q in c.subtree_iterator():
                yield q


class lUnAtIc_tAg:
    def __init__(self, node, tagtype):
        assert isinstance(node, lUnAtIc)
        assert tagtype in ["opening", "closing"]
        self.node = node
        self.tagt = tagtype

    def nodetype(self):
        return self.node.type

    def is_opening(self):
        return self.tagt == "opening"

    def is_closing(self):
        return self.tagt == "closing"

    def next_tag(self):
        node = self.node
        if self.is_opening():
            if len(node) > 0:
                return node.first_child().opening_tag
            else:
                return node.closing_tag
        else:
            if node.next_sibling() is not None:
                return node.next_sibling().opening_tag
            if node.parent:
                return node.parent.closing_tag
            return None

    def prev_tag(self):
        node = self.node
        if self.is_opening():
            if node.prev_sibling() is not None:
                return node.prev_sibling().closing_tag
            if node.parent:
                return node.parent.opening_tag
            return None
        else:
            if len(node) > 0:
                return node.last_child().closing_tag
            else:
                return node.opening_tag

    def text_at_right(self):
        return self.node.text if self.is_opening() else self.node.tail

    def text_at_left(self):
        T = self.prev_tag()
        return T.text_at_right() if T is not None else None

    def has_whitespace_helper(self, text, index):
        if text is None:
            return False
        if isinstance(text, dict):
            if text == {}:
                return False
            return True
        assert isinstance(text, str)
        assert len(text) == 0 or text[index] not in "\t\n"
        return len(text) > 0 and text[index] == " "

    def has_whitespace_to_left(self):
        return self.has_whitespace_helper(self.text_at_left(), -1)

    def has_whitespace_to_right(self):
        return self.has_whitespace_helper(self.text_at_right(), 0)

    def left_inject_whitespace(self):
        text = self.text_at_left()
        if isinstance(text, str):
            newtext = self.inject_whitespace_helper(text, len(text))
            self.set_text_at_left(newtext)

    def right_inject_whitespace(self):
        text = self.text_at_right()
        if isinstance(text, str):
            newtext = self.inject_whitespace_helper(text, 0)
            self.set_text_at_right(newtext)

    def inject_whitespace_helper(self, text, index):
        newtext = text[:index] + " " + text[index:]
        if newtext.startswith("  "):
            assert index == 0 or text == " "
            newtext = newtext[1:]
        if newtext.endswith("  "):
            assert index == len(text)
            newtext = newtext[:-1]
        assert not newtext.startswith("  ")
        assert not newtext.endswith("  ")
        return newtext

    def left_trim_whitespace(self):
        text_at_left = self.text_at_left()
        if isinstance(text_at_left, str):
            self.set_text_at_left(text_at_left.rstrip())

    def right_trim_whitespace(self):
        text_at_right = self.text_at_right()
        if isinstance(text_at_right, str):
            self.set_text_at_right(text_at_right.lstrip())

    def set_text_at_right(self, text):
        if self.is_opening():
            self.node.text = text
        else:
            self.node.tail = text

    def set_text_at_left(self, text):
        prev = self.prev_tag()
        assert prev is not None
        prev.set_text_at_right(text)

    def synopsis(self):
        return self.tagt + " tag of type " + self.nodetype()


########################
# dictionary utilities #
########################


def non_overwriting_dictionary_update(d1, d2):
    for k in d2:
        assert k not in d1 and d2[k] is not None
        d1[k] = d2[k]


def update_style_dict_with(style_dict, newcomer):
    assert isinstance(newcomer, dict)
    for k in newcomer:
        assert isinstance(k, str)
        new_val = newcomer[k]
        if new_val is None:
            if k in style_dict:
                del style_dict[k]
            assert k not in style_dict
        else:
            if type(new_val) in [str, int, float]:
                new_val = Css_Declaration(k + ":" + str(new_val))
            else:
                assert isinstance(new_val, Css_Declaration)
                new_val = deepcopy(new_val)
            assert isinstance(new_val, Css_Declaration)
            style_dict[k] = new_val


def check_inner_style_health(dIcT):
    assert isinstance(dIcT, dict)
    assert all(isinstance(dIcT[k], Css_Declaration) for k in dIcT)
    for k, value in dIcT.items():
        if value.key_str() != k + ":":
            print(k)
            print(value.key_str())
            assert False
    assert all(dIcT[k].key_str() == k + ":" for k in dIcT)


def unfold_attribute_for_use_of_attributes_match(key, value):
    if isinstance(value, str):
        if key == "viewbox":
            value = [int_else_float(x) for x in value.split()]

        elif key == "transform":
            value = parse_transform(value)

        elif key == "d":
            value = parse_path(value)

        elif key == "points":
            numbers = [float(x) for x in value.replace(",", " ").split()]
            assert len(numbers) >= 4
            assert len(numbers) % 2 == 0
            value = []
            i = 0
            while i < len(numbers):
                value.append(Point(numbers[i], numbers[i + 1]))
                i += 2

        elif key == "style":
            value = style_parse(value)

        else:
            pass

    elif (
        key not in ["viewbox", "transform", "d", "style", "points"]
        and key not in APPROVED_NUMBER_FIELDS
    ):
        print("offending key:", key)
        print("type of value:", type(value))
        assert False

    return value


def attributes_match(d1, d2):
    ##
    # basically checks if d1 is a subset of d2 (with matching fields)
    # while unfolding fields for 'even' comparison; not recursive
    ##
    def ftype(thing):
        if isinstance(thing, Number):
            return Number
        return type(thing)

    def matches_at_key(k, k2):
        if isinstance(d1[k], str) and d1[k].startswith("[string-contains]"):
            term = d1[k][len("[string-contains]") :]
            return term in str(d2[k2])

        if isinstance(d1[k], str) and d1[k].startswith("[appears-as-string-block-or-in-list]"):
            term = d1[k][len("[appears-as-string-block-or-in-list]"):]
            if isinstance(d2[k2], list): # we're accommodating the possibility of 'class' value being a list, though it's never the case rn
                return term in d2[k2]
            if isinstance(d2[k2], str):
                return term in d2[k2].split(" ")
            assert False

        if isinstance(d1[k], str) and d1[k].startswith("[string-startswith]"):
            term = d1[k][len("[string-startswith]"):]
            return str(d2[k2]).startswith(term)

        if isinstance(d1[k], str) and d1[k].startswith("[string-endswith]"):
            term = d1[k][len("[string-endswith]"):]
            return str(d2[k2]).endswith(term)

        if k == "class" and k2 == "class":
            cls1 = d1[k].split()
            cls2 = d2[k].split()
            return all(x in cls2 for x in cls1)

        unfolded_d1_attr = unfold_attribute_for_use_of_attributes_match(k, d1[k])
        unfolded_d2_attr = unfold_attribute_for_use_of_attributes_match(k2, d2[k2])

        if ftype(unfolded_d1_attr) != ftype(unfolded_d2_attr):
            assert k == "[any]"
            return False

        if isinstance(unfolded_d1_attr, dict):
            assert k == "style" and k2 == "style"

            for k_prime in unfolded_d1_attr:
                if k_prime == "any-key":
                    assert unfolded_d1_attr[k_prime] is not None
                    ze_val = unfolded_d1_attr[k_prime].val_str()
                    if not any(
                        ze_val == unfolded_d2_attr[k_prime2].val_str()
                        for k_prime2 in unfolded_d2_attr
                    ):
                        return False

                elif unfolded_d1_attr[k_prime] is None:
                    if k_prime in unfolded_d2_attr:
                        return False

                else:
                    assert isinstance(unfolded_d1_attr[k_prime], Css_Declaration)
                    if k_prime not in d2:
                        return False
                    assert isinstance(unfolded_d2_attr[k_prime], Css_Declaration)
                    if (
                        unfolded_d1_attr[k_prime].val_str()
                        != unfolded_d2_attr[k_prime].val_str()
                    ):
                        return False

            return True

        else:
            return unfolded_d1_attr == unfolded_d2_attr

    for k in d1:
        if d1[k] is None:
            if k in d2:
                return False
            continue

        elif d1[k] == "[any]":
            if k not in d2:
                return False
            continue

        elif k in ["[tail-contains]", "[text-contains]"]:
            continue

        elif k == "[any]":
            keys2 = [x for x in d2]

        elif isinstance(k, str) and k.startswith("[string-endswith]"):
            term = k[len("[string-endswith]"):]
            keys2 = [x for x in d2 if x.endswith(term)]

        elif isinstance(k, str) and k.startswith("[string-startswith]"):
            term = k[len("[string-startswith]"):]
            keys2 = [x for x in d2 if x.startswith(term)]

        elif isinstance(k, str) and k.startswith("[string-contains]"):
            term = k[len("[string-contains]"):]
            keys2 = [x for x in d2 if term in x]

        elif isinstance(k, str) and k.startswith("[appears-as-string-block-or-in-list]"):
            assert False # we are not currently expecting this condition to appear as a condition on keys, only on values

        else:
            keys2 = [k] if k in d2 else []

        if not any(matches_at_key(k, x) for x in keys2):
            return False

    return True


def matching_type_args(*args):
    return (
        len(args) == 2
        and (
            args[0] is None
            or (
                isinstance(args[0], str)
                and all(x not in args[0] for x in ".:#")
                and args[0].islower()
            )
        )
        and isinstance(args[1], dict)
    )


def process_newlines_etc(text):  # copy-pasted from lib_load_lUnAtIc (sorry)
    if len(text) == 0:
        return ""
    first_is_space = text[0].isspace()
    last_was_space = text[-1].isspace()
    text = text.strip(" \n\t")
    text = "\n".join([x.strip(" \t") for x in text.split("\n")])
    if first_is_space:
        assert len(text) == 0 or text[0] != " "
        text = " " + text
    if last_was_space and text[-1] != " ":
        text += " "
    return text


def _parse_type_string(type_string, other_attrs, for_matching=False):
    attrs = {}

    def assign_attribute(key, val):
        assert all(t not in key for t in " =#")
        if isinstance(val, str):
            assert ":" not in val or key == "style" or key == 'xmlns' or key == 'src'
            assert "=" not in val or "config=" in val

        tweaked = key.lower()
        if tweaked != key and key not in CAMEL_CASE_ATTRIBUTES:
            print(tweaked, key)
            assert False

        if tweaked == "href":
            if type_string.startswith("use.") or \
               type_string.startswith("use#") or \
               type_string == 'use':
                assert val.startswith("#")
                tweaked = "xlink:href"
            
            elif type_string.startswith("use"):
                print("surprised by type_string:", type_string)
                assert False
        
        if "xref" in tweaked:
            print("")
            print(f"suspicious of href-like attribute name: {tweaked} --- do you mean 'xlink:href'? (type_string: {type_string})")
            print("")
            assert False

        assert tweaked not in attrs
        attrs[tweaked] = val

    if type_string is not None:
        assert isinstance(type_string, str)

        # splits off square bracket groups not preceded by a dot, not at beginning of string
        # note: only closing brackets need to be escaped, within such groups
        splitter = re.compile(r"((?<!^)(?<![\.=])\[(?:(?:(?<![\\])\\(?:\\\\)*\])|(?:[^\]]))*\])")
        pieces = [x for x in re.split(splitter, type_string) if x != ""]
        assert len(pieces) in [1, 2, 3]
        for i, p in enumerate(pieces):
            if i == 0 and p.startswith("[any]"):
                continue

            if p[0] == "[":
                assert i >= 1
                assert p[-1] == "]" and p[-2:] != r"\]"
                assert not for_matching

            else:
                assert i == 0

        text = pieces[1][1:-1].replace(r"\]", "]") if len(pieces) >= 2 else ""
        tail = pieces[2][1:-1].replace(r"\]", "]") if len(pieces) >= 3 else ""
        text = process_newlines_etc(text)
        tail = process_newlines_etc(tail)

        type_string = pieces[0]

        assert ";" not in type_string
        assert "\n" not in type_string
        assert "href#" not in type_string
        assert "src#" not in type_string

        # special guys: href, src (contain periods); compute rest
        if ".href=" in type_string:
            assert "src=" not in type_string
            assert "xlink:href=" not in type_string
            [rest, href] = type_string.split(".href=")
            assert len(href) > 0 and all(_ not in href for _ in "'\"[]:=")
            assert href.startswith("#") or rest.startswith("link")
            assign_attribute("href", href)

        elif ".xlink:href=" in type_string:
            assert "src=" not in type_string
            [rest, href] = type_string.split(".xlink:href=")
            assert len(href) > 0 and all(_ not in href for _ in "'\"[]:=")
            assign_attribute("xlink:href", href)

        elif ".src=" in type_string:
            [rest, src] = type_string.split(".src=")
            assert len(src) > 0
            assert all(_ not in src for _ in "'\"[]:"), src
            assign_attribute("src", src)

        else:
            rest = type_string

        # split rest
        # stopgap: should really match . outside of string, not in front of digit, not in front of dot
        splitter = re.compile(r"\.(?![\.\d ]|html|svg)")
        rest_components = splitter.split(rest)
        assert len(rest_components) > 0 and all(len(n) > 0 for n in rest_components)

        # reconstitute decimal numbers
        for index in range(len(rest_components) - 1):
            here = rest_components[index]
            there = rest_components[index + 1]
            if there.isdigit():
                rest_components[index] = here + "." + there
                rest_components[index + 1] = ""
        rest_components = [r for r in rest_components if r != ""]

        # id, type computation
        if "#" in rest_components[0]:
            [type, id_str] = rest_components[0].split("#")
            if ":" in id_str:
                print(
                    "maybe I'm wrong, but this doesn't look like an id to me:", id_str,
                )
                assert False
            assign_attribute("id", id_str)

        else:
            type = rest_components[0]

        if type is not None:
            if not type.islower() and not type.isupper():
                if type not in CAMEL_CASE_TYPES:
                    print(f"\noffending type: {type}\n")
                    assert False
            type = type.lower()

        if type == "[any]":
            assert for_matching is True
            type = None

        if type is not None:
            assert len(type) > 0
            if any(_ in type for _ in "'\"\n;.()[]{}\\ "):
                print("")
                print("offending type:", type)
                print("from type_string:", type_string)
                print("")
            assert all(_ not in type for _ in "'\"\n;.()[]{}\\ ")

        class_str = None
        style_str = ""

        for z in rest_components[1:]:
            if "=" in z:
                [key, val] = z.split("=")
                assert key != "class"
                assert key != "style"
                assign_attribute(key, val)

            elif ":" in z:
                style_str += z + ";"

            else:
                if any(q in z for q in "#()="):
                    print("z:", z)
                assert all(q not in z for q in "#()=")
                if class_str is not None:
                    class_str += " " + z

                else:
                    class_str = z

        assert "class" not in attrs and "style" not in attrs

        if class_str is not None:
            assign_attribute("class", class_str)

    else:
        assert for_matching is True
        type = None
        style_str = ""
        text = ""
        tail = ""

    assert isinstance(other_attrs, dict)
    assert all(isinstance(k, str) for k in other_attrs)
    xtra_style_attr = other_attrs.pop("style", None)

    for k in other_attrs:
        if other_attrs[k] is None and not for_matching:
            print(f"\noffending attribute with None value: {k}\n")

        assert other_attrs[k] is not None or for_matching is True

        if k == "x1y1":
            assert "x1" not in attrs
            assert "y1" not in attrs
            assert isinstance(other_attrs[k], Point)
            attrs["x1"] = other_attrs[k].x
            attrs["y1"] = other_attrs[k].y
            continue

        if k == "x2y2":
            assert "y2" not in attrs
            assert "x2" not in attrs
            assert isinstance(other_attrs[k], Point)
            attrs["x2"] = other_attrs[k].x
            attrs["y2"] = other_attrs[k].y
            continue

        if other_attrs[k] != "nOnE":
            assign_attribute(k, other_attrs[k])

    if xtra_style_attr is not None:
        if isinstance(xtra_style_attr, str):
            assign_attribute("style", style_str + xtra_style_attr)

        elif isinstance(xtra_style_attr, dict):
            assign_attribute("style", style_parse(style_str))
            for key in xtra_style_attr:
                assert key not in attrs
                val = xtra_style_attr[key]
                if isinstance(val, Css_Declaration):
                    attrs["style"][key] = val
                    
                else:
                    str_val = str(xtra_style_attr[key])
                    assert not str_val.startswith("[") # can't use search patterns inside style dictionary :/ (b/c we convert to Css_Declaration)
                    attrs["style"][key] = Css_Declaration(key + ":" + str_val)

        else:
            assert False

    elif len(style_str) > 0:
        attrs["style"] = style_str

    if for_matching and "style" in attrs and isinstance(attrs["style"], str):
        attrs["style"] = style_parse(attrs["style"])

    if type == "use":
        assert "href" not in attrs

    if for_matching:
        assert text == ""
        assert tail == ""
        return type, attrs

    else:
        to_pop = [key for key in attrs if isinstance(attrs[key], str) and attrs[key] == ""]
        for key in to_pop:
            attrs.pop(key)
        return type, attrs, text, tail


def divide_block_string(string):
    def leading_spaces(a):
        return len(a) - len(a.lstrip(" "))

    def first_non_space_is(string, thing):
        assert isinstance(thing, str) and len(thing) == 1
        for c in string:
            if c == " ":
                continue
            if c == thing:
                return True
            return False
        return False

    assert isinstance(string, str)
    assert len(string) > 0
    assert not any(string.startswith(x) for x in ":.#")

    # splits off square brackets groups, and newlines outside of square bracket groups;
    # note: square bracket groups coming after a dot are also split off
    splitter = re.compile(r"((?:\[(?:(?:(?<![\\])\\(?:\\\\)*\])|(?:[^\]]))*\])|(?:\n))")
    pieces = [x for x in re.split(splitter, string) if x.strip() != ""]

    # now we reassemble pieces such that .-continuations end up on the same line
    for i in range(len(pieces) - 1, 0, -1):
        if pieces[i - 1].endswith(".") or first_non_space_is(pieces[i], "."):
            assert not pieces[i].startswith("[")
            if pieces[i - 1].endswith(".") and pieces[i].startswith("."):
                pieces[i] = pieces[i].lstrip(".")
            pieces[i - 1] += pieces[i].strip()
            pieces[i] = ""

        elif pieces[i].startswith("["):
            pieces[i - 1] += pieces[i]
            pieces[i] = ""

    # throw out commented out pieces
    type_strings = [x for x in pieces if x != "" and not first_non_space_is(x, "#")]

    pairs = [(leading_spaces(a), a) for a in type_strings]
    assert len(pairs) > 0
    header_level = pairs[0][0]

    if any(q[0] < header_level for q in pairs[1:]):
        print("offending string:")
        print(string)
        assert False

    if all((q[0] - header_level) % 4 == 0 for q in pairs):
        tab = 4

    elif all((q[0] - header_level) % 2 == 0 for q in pairs):
        tab = 2

    else:
        assert False

    if any(q[0] > p[0] + tab for p, q in zip(pairs, pairs[1:])):
        print(string)
        print(pairs)
        assert False

    def append_if_not_none():
        if next_block is not None:
            blocks.append(next_block)

    header_level = pairs[0][0]
    header = None
    next_block = None
    blocks = []

    if all(q[0] > header_level for q in pairs[1:]):
        header = type_strings[0].strip()

    for spaces, line in pairs[int(bool(header)) :]:
        if spaces == header_level + tab * int(bool(header)):
            append_if_not_none()
            next_block = line
            continue
        next_block += "\n" + line
    append_if_not_none()

    assert isinstance(header, str) or header is None

    return header, blocks


class lUnAtIc:
    def __init__(self, block_string, xtra_attrs={}, xtra_children=[]):
        self.parent = None
        self.__children = []
        self.__tab_spaces = 4
        self.__forbid_child_detachment = False
        self.__forbid_child_attachment = False
        self.f = None
        self.repr_buffer = None
        self.parent_forces_inline_repr = None
        self.reduce_whitespace = None
        self._print_incrementally = False
        self._last_used_filename = None
        self.closing_tag = lUnAtIc_tAg(self, "closing")
        self.opening_tag = lUnAtIc_tAg(self, "opening")

        type_string, children_type_strings = divide_block_string(block_string)
        assert len(children_type_strings) == 0 or len(xtra_children) == 0

        self.type, self.attr, self.text, self.tail = _parse_type_string(type_string, xtra_attrs)

        if self.type == "use":
            assert "href" not in self.attr

        assert isinstance(self.type, str)
        assert isinstance(self.attr, dict)
        assert isinstance(self.text, str)
        assert isinstance(self.tail, str)

        self.normalize_style_attribute()

        for block in chain(children_type_strings, xtra_children):
            self.append(lUnAtIc(block))

    def add_xmlns(self):
        assert self.type == "svg"
        self["xmlns"] = "http://www.w3.org/2000/svg"
        return self

    def add_xlink(self):
        assert self.type == "svg"
        self["xmlns:xlink"] = "http://www.w3.org/1999/xlink"
        return self

    def add_inkscape_stuff(self, narrow=False):
        assert self.type == "svg"
        assert 'xmlns' in self  # it doesn't make sense to inkscapify if you're not an external svg
        self["xmlns:inkscape"] = "http://www.inkscape.org/namespaces/inkscape"
        self["xmlns:sodipodi"] = "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
        self.prepend(
            "sodipodi:namedview",
            {
                "inkscape:window-width": 1026 if narrow else 1415,
                "inkscape:window-height": 905,
                "inkscape:zoom": 1,
            },
        )
        return self

    def __getitem__(self, key, default=unique_object):
        if isinstance(key, int):
            return self.__children[key]

        if isinstance(key, str):
            if default != unique_object:
                return self.attr.get(key, default)

            if key not in self:
                if self.type == "rect" and key in ["x", "y"]:
                    return 0

                elif self.type == "use" and key in ["x", "y"]:
                    return 0

                elif self.type == "circle" and key in ["cx", "cy"]:
                    return 0

                elif self.type == "line" and key in ["x1", "y1", "x2", "y2"]:
                    return 0

                elif self.type == "ellipse" and key in ["cx", "cy"]:
                    return 0

                else:
                    print("self.type, key:", self.type, key)
                    assert False

            return self.attr[key]

        if isinstance(key, slice):
            return self.__children[key]

        assert False

    def __setitem__(self, key, item):
        if self.type == "style" and key == "id":
            print("warning: lib_lUnAtIc refusing to set id of style element")
            return  # anti-inkscape measures... hope it doesn't come back to bite me

        # newly added feature (or item == '' ... even more newly added):
        if item is None or (isinstance(item, str) and item == ''):
            self.pop(key, None)
            return

        assert self.type != "style" or (key == "type" and item == "text/css")

        if isinstance(key, int):
            assert isinstance(item, lUnAtIc)
            assert 0 <= key < len(self.__children)
            self.remove_child(self.__children[key])
            self.insert(key, item)
            return

        assert isinstance(key, str)

        if not key.islower():
            assert key in CAMEL_CASE_ATTRIBUTES
            key = key.lower()

        assert key.islower()

        if key == "style":
            self.remove_attribute("style")
            self.update_style_attribute(item)
            return

        if key == "d" and self.type not in ["path", "glyph"]:
            print("self.type:", self.type)
            assert False

        assert key != "xlink"

        if key == "src":
            assert "xlink:href" not in self
            assert "href" not in self

        if key == "xlink:href":
            assert "src" not in self
            assert "href" not in self

        if key == "href":
            assert "src" not in self
            assert "xlink:href" not in self 
            isinstance(item, str)

            if self.type == 'use':
                key = "xlink:href"

            else:
                assert not item.startswith('#')

        self.attr[key] = item if isinstance(item, str) else deepcopy(item)

    def setdefault(self, key, default=None):
        return self.attr.setdefault(key, default)

    def set_attribute(self, key, value):
        self.__setitem__(key, value)
        return self

    def set_attributes(self, new_attrs, recursive=False):
        if isinstance(new_attrs, str):
            _, new_attrs = _parse_type_string(self.type + "." + new_attrs, {})[:2]
        assert isinstance(new_attrs, dict)
        for k in new_attrs:
            assert isinstance(k, str)
            self.set_attribute(k, new_attrs[k])
        if recursive:
            for c in self:
                c.set_attributes(new_attrs, True)
        return self

    def selective_set_attributes(self, type1, attr1, attr2, recursive=True):
        total = 0
        assert isinstance(attr1, dict) and isinstance(attr2, dict)
        if self.__matches(type1, attr1):
            self.set_attributes(attr2)
            total += 1
        if recursive:
            for c in self.__children:
                total += c.selective_set_attributes(type1, attr1, attr2, True)
        return total

    def increase_number_attribute(self, key, delta):
        if key not in self:
            self.__setitem__(key, 0)
        val = float(self.__getitem__(key))
        self.__setitem__(key, val + delta)
        return self

    def __delitem__(self, key):
        if key == 'id' and key in self.attr and self.attr[key] == "E1-STIXWEBMAIN-38":
            assert False
        del self.attr[key]

    def __iter__(self):
        return iter(self.__children)

    def __len__(self):
        return len(self.__children)

    def __getslice__(self, i, j):
        return self.__children[i:j]

    def __contains__(self, other):
        if isinstance(other, lUnAtIc):
            return any(x is other for x in self.__children)

        if isinstance(other, str):
            return other in self.attr

        assert False

    def __call__(self):
        return self.attr

    ##########
    # adding #
    ##########

    def progressive_factory(self, *args):
        assert len(args) > 0

        if isinstance(args[0], str):
            if len(args) > 1 and isinstance(args[1], dict):
                # print("A")
                return lUnAtIc(args[0], args[1]), args[2:]

            type_string, children_type_strings = divide_block_string(args[0])
            if type_string is None:
                assert len(children_type_strings) > 1
                # print("B")
                return (
                    lUnAtIc(children_type_strings[0]),
                    children_type_strings[1:] + list(args[1:]),
                )

            # print("C")
            return lUnAtIc(type_string, {}, children_type_strings), args[1:]

        if isinstance(args[0], lUnAtIc):
            # print("D")
            return args[0], args[1:]

        if isinstance(args[0], list):
            if len(args[0]) == 0:
                if len(args) == 1:
                    return None, []
                # print("E")
                return self.progressive_factory(args[1:])
            first = args[0][0]
            rest = args[0][1:]
            item, re_first = self.progressive_factory(first)
            # print("F")
            return item, list(chain(re_first, rest, args[1:]))

        print("")
        print(len(args), args[0])
        print("")
        assert False

    def maybe_factory(self, *args, **kwargs):
        assert len(args) > 0

        if isinstance(args[0], str):
            item = None if args[0] == "nOnE" else lUnAtIc(*args)

        else:
            assert len(args) == 1 and isinstance(args[0], lUnAtIc)
            item = args[0]
            assert item.parent is None or kwargs.get("reattach", None) is True

        return item

    def insert(self, i, *args, **kwargs):
        assert all(a is not None for a in args)
        assert 0 <= i <= len(self)

        child = self.maybe_factory(*args, **kwargs)

        if child is not None:
            assert not self.__forbid_child_attachment
            assert self.type != "style"
            assert child.type != "style" or self.type in ["svg", "g", "head"]
            assert child.type != "svg" or self.type in HTML_ELEMENTS_WITH_SVG_CHILDREN_POTENTIALLY
            assert child.type != "defs" or self.type in ["svg", "p"]

            if child.type == "defs" and self.type not in ["svg", "g"]:
                print("~~~ warning ~~~: 'defs' child of '" + self.type + "'")

            if child.parent is not None:
                assert kwargs.get("reattach", None) is True
                child.detach()

            assert child.parent is None

            child.parent = self
            self.__children.insert(i, child)

        return child

    def append(self, *args, **kwargs):
        if len(args) == 1 and args[0] is None:
            print("")
            print("BADNESS: lUnAtIc.append() called with None")
            print("")

        assert all(a is not None for a in args)

        to_return = self.insert(len(self), *args, **kwargs)
        assert to_return is not None or kwargs.get("None_OK", False)
        return to_return

    def append_path(self, *args):
        if len(args) == 1:
            if isinstance(args[0], Path) or isinstance(args[0], Subpath):
                ze_path = args[0]

            elif isinstance(args[0], list):
                ze_path = parse_subpath(args[0], accept_paths=True)

            else:
                assert False

        else:
            ze_path = parse_subpath(args[0], accept_paths=True)

        newguy = lUnAtIc("path", {"d": ze_path})
        self.append(newguy)

    def append_and(self, *args, **kwargs):
        self.insert(len(self), *args, **kwargs)
        return self

    def prepend(self, *args, **kwargs):
        return self.insert(0, *args, **kwargs)

    def prepend_and(self, *args, **kwargs):
        self.insert(0, *args, **kwargs)
        return self

    def iterator_factory(self, *args):
        while len(args) > 0:
            thing, args = self.progressive_factory(*args)
            if thing is None:
                assert len(args) == 0
                break
            assert isinstance(thing, lUnAtIc)
            yield thing

    def append_all(self, *args, reattach=False):
        if len(args) == 0:
            ze_iterator = []

        else:
            ze_iterator = self.iterator_factory(*args)

        for g in ze_iterator:
            self.append(g, reattach=reattach)

        return self

    def prepend_all(self, *args, reattach=False):
        guys = list(self.iterator_factory(*args))
        assert all(isinstance(x, lUnAtIc) for x in guys)

        for g in reversed(guys):
            self.prepend(g, reattach=reattach)

        return self

    def insert_after(self, *args):
        if isinstance(args[0], list):
            args[0].extend(args[1:])
            args = args[0]
            assert args is not None

        newitems = []
        remaining_args = args
        assert remaining_args is not None
        while not matching_type_args(*remaining_args) and len(remaining_args) > 1:
            a, remaining_args = self.progressive_factory(*remaining_args)
            assert isinstance(a, lUnAtIc)
            assert remaining_args is not None and len(remaining_args) >= 1
            newitems.append(a)

        assert len(newitems) > 0
        if len(remaining_args) == 1 and isinstance(remaining_args[0], lUnAtIc):
            putative_child = remaining_args[0]

        else:
            assert len(remaining_args) <= 2
            type_string, xtra_attrs = _parse_type_string(remaining_args[0], remaining_args[1] if len(remaining_args) > 1 else {}, for_matching=True)
            putative_child = self.find_among_children(type_string, xtra_attrs)
            if putative_child is None:
                print(f"\ncould not find: type_string = {type_string}, xtra_attrs = {xtra_attrs}\n")
                assert False
                
        assert isinstance(putative_child, lUnAtIc)
        assert putative_child is not self

        index = self.index_of_putative_child(putative_child)
        for item in reversed(newitems):
            self.insert(index + 1, item)
        return newitems[-1]

    def insert_after_last_of(self, *args):
        newitem, remaining_args = self.progressive_factory(*args)
        reached = -1
        
        for index, c in enumerate(self):
            reached = index
            if not c.matches(*remaining_args):
                break
            reached = index + 1  # because otherwise "reached" won't reach 1 for 1 matching child, e.g.

        if reached < 0:
            assert len(self) == 0
            self.append(newitem)

        else:
            assert len(self) > 0
            self.insert(reached, newitem)

        return newitem

    def insert_all_after_defs_and_style(self, *args):
        index = -1
        for index, c in enumerate(self):
            if c.type != "defs" and c.type != "style":
                break

        if index == -1:
            self.prepend_all(*args)

        else:
            assert isinstance(c, lUnAtIc)
            self.insert_all_before(*args, c)

        return self

    def insert_after_defs_and_style(self, guy):
        if not isinstance(guy, lUnAtIc):
            guy = lUnAtIc(guy)
        self.insert_all_after_defs_and_style(self, guy)
        return guy

    def insert_all_after(self, guys, c):
        index = self.index_of_putative_child(c)
        for g in reversed(guys):
            self.insert(index + 1, g)

    def insert_before(self, *superargs, **kwargs):
        newitem, args = self.progressive_factory(*superargs)

        if len(args) == 1 and isinstance(args[0], lUnAtIc):
            putative_child = args[0]

        else:
            putative_child = self.find_among_children(*args)

        index = self.index_of_putative_child(putative_child)
        self.insert(index, newitem, **kwargs)
        return newitem

    def insert_nonempty_group_before(self, guy, c):
        assert isinstance(guy, lUnAtIc)
        assert guy.type == "g"
        if len(guy) > 0:
            self.insert_before(guy, c)
            return True
        return False

    def insert_all_before(self, *args):
        guys = list(self.iterator_factory(*args))
        assert len(guys) >= 1
        index = self.index_of_putative_child(guys.pop())
        for g in reversed(guys):
            self.insert(index, g)

    def insert_before_self(self, *args, reattach=False):
        assert self.parent is not None
        return self.parent.insert_before(*args, self, reattach=reattach)

    def insert_after_self(self, *args):
        assert self.parent is not None
        return self.parent.insert_after(*args, self)

    def previous_sibling_or_else_parent(self):
        assert self.parent is not None
        index = self.parent.index_of_putative_child(self)
        return self.parent if index == 0 else self.parent[index - 1]

    def insert_text_before_self(self, text):
        prev = self.previous_sibling_or_else_parent()
        prev.text = (prev.text + " " + text).strip()

    ########################
    # removing / detaching #
    ########################

    def forbid_child_detachment(self):
        self.__forbid_child_detachment = True

    def allow_child_detachment(self):
        self.__forbid_child_detachment = False

    def forbid_child_attachment(self):
        self.__forbid_child_attachment = True

    def allow_child_attachment(self):
        self.__forbid_child_attachment = False

    def removed_child(
        self, *args, with_tail=False, tail_aware=False, ok_not_there=False
    ):
        num_matching = self.num_children_matching(*args)

        if num_matching == 0:
            assert ok_not_there
            return None

        assert num_matching == 1

        ze_index = self.index_of_first_child_matching(*args)
        assert 0 <= ze_index < len(self.__children)

        reject = self[ze_index]

        assert reject is args[0] or reject.__matches(*args)
        assert not self.__forbid_child_detachment

        assert reject is self.__children.pop(ze_index)
        assert reject not in self

        reject.parent = None

        if with_tail:
            pass  # do nothing

        else:
            if ze_index > 0:
                prev = self[ze_index - 1]
                prev.tail = (prev.tail.rstrip() + " " + reject.tail.lstrip()).rstrip()

            else:
                assert isinstance(self.text, str)
                self.text = (self.text.rstrip() + " " + reject.tail.lstrip()).rstrip()

            reject.tail = ""

        return reject

    def remove_child(
        self, *args, with_tail=False, tail_aware=False, ok_not_there=False
    ):
        self.removed_child(
            *args, with_tail=with_tail, tail_aware=tail_aware, ok_not_there=ok_not_there
        )
        return None  # change this to self when/after bugs are caught that expect this to be the child (recent change)

    def remove_among_children(self, header, xtra_attrs):
        type, attrs, _, _ = _parse_type_string(header, xtra_attrs, for_matching=True)
        assert matching_type_args(type, attrs)
        to_remove = [c for c in self if c.__matches(type, attrs)]
        for c in to_remove:
            c.detach()
        return to_remove

    def detached_copy(self):
        parent = self.parent
        self.parent = None
        copy = deepcopy(self)
        self.parent = parent
        return copy

    def possible_detached_copy(self):
        return self if self.parent is None else self.detached_copy()

    def copy(self):
        assert self.parent is None
        return deepcopy(self)

    def detached_copy_if_parent(self):
        if self.parent is None:
            return self
        return self.detached_copy()

    def cloned_detached_children_as_list(self):
        return [c.detached_copy() for c in self]

    def detach(self, with_tail=False, tail_aware=False):
        assert isinstance(self.tail, str)
        assert tail_aware or is_whitespace(self.tail)
        parent = self.parent
        assert parent is not None
        parent.remove_child(self, with_tail=with_tail, tail_aware=tail_aware)
        assert self not in parent
        return self

    def detach_if_no_children(self):
        if len(self) == 0:
            self.detach()
        return self

    def delete(self, type_string, xtra_attrs={}):
        type, d = _parse_type_string(type_string, xtra_attrs, for_matching=True)
        return self.delete_all([(type, d)])

    def delete_all(self, matching_types):
        deleted = []
        c = self.first_child()
        while c is not None:
            d = c.next_sibling()
            if c.matches_one_of(matching_types):
                self.remove_child(c, tail_aware=True)
                deleted.append(c)
            else:
                deleted.extend(c.delete_all(matching_types))
            c = d
        return deleted

    def detach_all(self, matching_types):
        deleted = []
        c = self.first_child()
        while c is not None:
            d = c.next_sibling()
            if c.matches_one_of(matching_types):
                self.remove_child(c)
                deleted.append(c)
            else:
                deleted.extend(c.delete_all(matching_types))
            c = d
        return deleted

    #############################
    # cut / paste (specialties) #
    #############################

    def give_children_to_parent_and_delete(self, with_text=False):
        if with_text and len(self.text) > 0:
            self.insert_text_before_self(self.text)
            self.text = ""
        while len(self) > 0:
            self.insert_before_self(self[0].detach(with_tail=with_text))
        self.parent.remove_child(self)
        return self  # should we return the parent?

    def replace_with(self, *args):
        item = self.maybe_factory(*args)
        assert item.parent is None
        assert self.parent is not None
        mi_index = self.parent.index_of_putative_child(self)
        self.parent.insert(mi_index, item)
        self.parent.remove_child(self)
        return item

    def replace_in_place(self, *args):
        original_length = len(self)
        if self.parent is not None:
            newguy = self.replace_with(*args)
        else:
            newguy = self.maybe_factory(*args)
        assert len(newguy) == 0  # seems like generic case...
        newguy.take_children_from(self)
        assert len(newguy) == original_length
        return newguy

    def take_children_from(self, other):
        other.transfer_children_to(self)
        return self

    def prepend_children_from(self, other):
        children = [c for c in other]
        for c in reversed(children):
            self.prepend(c.detach())
        return self

    def transfer_children_to(self, other):
        children = [c for c in self]
        for c in children:
            other.append(c.detach(tail_aware=True))
        return self

    def merge_with_children_of_by_id(self, other, detach=False):
        to_be_moved_over = []
        for z in other:
            assert "id" in z
            w = self.__find_among_children(None, {"id": z["id"]})

            if w is not None:
                if (
                    z != w
                    and str(z).strip() != str(w).strip()
                    and str(z.pretty_up()).strip() != str(w.pretty_up()).strip()
                ):
                    print("z:", z, "[end]")
                    print("w:", w, "[end]")
                    assert False

            else:
                to_be_moved_over.append(z)

        for z in to_be_moved_over:
            assert z not in self
            self.append(z.detach() if detach else z.detached_copy())

        return self

    def steal_defs_from(self, other):
        return self.take_defs_from(other, detach=True)

    def steal_defs_from_and_cleanup(self, other):
        return self.take_defs_from(
            other, detach=True, remove_empty_svgs=True, remove_attributeless_gs=True
        )

    def take_defs_from(
        self,
        other,
        detach=False,
        remove_empty_svgs=False,
        remove_attributeless_gs=False,
    ):
        assert other is not self

        their_defs = other.find_all("defs")

        our_defs = None

        if self.type == "defs":
            our_defs = self

        elif sum(len(d) for d in their_defs) > 0:
            our_defs = self.get_or_create_first_level_defs()
            assert our_defs is not None

        if any(d is our_defs for d in their_defs):
            print(self.pretty_up())
            assert False

        parents = (
            [d.parent for d in their_defs if d.parent is not None]
            if remove_empty_svgs
            else []
        )

        if detach:
            [d.detach() for d in their_defs if d.has_parent()]

        if remove_empty_svgs:
            assert detach
            for p in parents:
                if p.type == "svg" and len(p) == 0 and p.parent is not None:
                    p.detach()

        if remove_attributeless_gs:
            for p in parents:
                if p.type == "g" and len(p.attr) == 0 and p.parent is not None:
                    g.give_children_to_parent_and_delete()
                    break

        for d in their_defs:
            assert d is not our_defs
            if len(d) > 0:
                our_defs.merge_with_children_of_by_id(d, detach=detach)

        return self

    def take_styles_from(self, other, detach=False, on_overwrite="crash"):
        assert self is not other
        their_styles = other.find_all("style")

        if detach:
            for s in their_styles:
                s.detach()

        had_style_to_start_with = (
            False if self.find_among_children("style") is None else True
        )
        our_style = self.get_or_create_style()
        our_style.simple_unfold_css()

        for s in their_styles:
            assert s is not our_style
            s.simple_unfold_css()
            our_style.add_css_styles(s.text, on_overwrite)

        if not had_style_to_start_with and len(our_style.text) == 0:
            our_style.detach()

        return self

    def take_defs_and_styles_from(self, other, detach=False, on_overwrite="crash"):
        self.take_defs_from(other, detach=detach)
        self.take_styles_from(other, detach=detach, on_overwrite=on_overwrite)
        return self

    def promote_defs_and_styles(self, on_overwrite="crash"):
        assert self.parent is None
        for c in self:
            if c.type in ["defs", "style"]:
                continue
            self.take_defs_and_styles_from(c, detach=True, on_overwrite=on_overwrite)
        return self

    def promote_defs(self):
        assert self.parent is None
        for c in self:
            if c.type == "defs":
                continue
            self.take_defs_from(c, detach=True)
        return self

    def cleanup_defs_and_styles(self, on_overwrite="crash"):
        self.promote_defs_and_styles(on_overwrite=on_overwrite)
        self.remove_unused_definitions()
        self.remove_unused_styles()
        self.remove_empty_elements(types=["defs", "style"])
        return self

    def is_or_has_text_or_textPath_descendant(self):
        if self.type == "text" or self.type == "textPath":
            return True
        return any(c.is_or_has_text_or_textPath_descendant() for c in self)
    
    def use_html_named_colors(self):
        self.unfold_style_attribute(recursive=True)
        for c in self.subtree_iterator():
            for name in ['stroke', 'fill']:
                if name in c and c[name] in HtmlColorsLowerCaseInverted:
                    c[name] = HtmlColorsLowerCaseInverted[name]
                if 'style' in c and name in c['style']:
                    value = c['style'][name].val_str()
                    if value in HtmlColorsLowerCaseInverted:
                        c['style'][name] = Css_Declaration(f'{name}:{HtmlColorsLowerCaseInverted[value]}')

    def cleanup_misc_mathjax_crap(self):
        if self.type == "svg":
            self.try_pop_all("role", "focusable")
        if self.get("stroke", "") == "currentColor":
            self.pop("stroke")
        if self.get("fill", "") == "currentColor":
            self.pop("fill")
        if self.get("stroke-width", "") == "0":
            self.pop("stroke-width")
        for c in self:
            c.cleanup_misc_mathjax_crap()
        return self

    def cleanup_more_inkscape_crap(self):
        if (self.type == 'svg'):
            self.pop('version', None)
            self.pop('inkscape:version', None)
            self.pop('sodipodi:docname', None)
            self.pop('xmlns:inkscape', None)
            self.pop('xmlns:sodipodi', None)
            self.pop('xmlns:svg', None)
        self.pop('sodipodi:role', None)
        self.pop('inkscape:label', None)
        self.pop('inkscape:groupmode', None)
        self.pop('inkscape:connector-curvature', None)
        self.pop('inkscape:transform-center-x', None)
        self.pop('inkscape:transform-center-y', None)
        if 'style' in self:
            self.unfold_style_attribute();
            d = self['style']
            d.pop('display', None)
            if 'stroke-dashoffset' in d and d['stroke-dashoffset'].value() == 0:
                d.pop('stroke-dashoffset')
            if 'fill-opacity' in d and d['fill-opacity'].value() == 1:
                d.pop('fill-opacity')
            if 'fill' in d and d['fill'] == "#000000":
                d.pop('fill')
            if 'stroke' in d and d['stroke'] == 'none':
                d.pop('stroke')
                d.pop('stroke-width', None)
            if 'stroke' not in d:
                d.pop('stroke-width', None)
            if 'stroke-miterlimit' in d and d['stroke-miterlimit'].value() == 4:
                d.pop('stroke-miterlimit')
            if len(d) == 0:
                self.pop('style')
        for c in self:
            c.cleanup_more_inkscape_crap()
        for c in self:
            if (c.type == 'sodipodi:namedview'):
                c.detach()
        return self

    def cleanup_misc_inkscape_crap(self):
        if not self.is_or_has_text_or_textPath_descendant() and "style" in self:
            self.unfold_style_attribute()
            to_remove = [
                a
                for a in self["style"]
                if a.startswith("font-")
                or a.startswith("-inkscape")
                or a == "line-height"
            ]
            if self["style"].get("stroke-dasharray", "") == "none":
                to_remove.append("stroke-dasharray")
                to_remove.append("stroke-dashoffset")
            if self["style"].get("stroke-opacity", "") == "1":
                to_remove.append("stroke-opacity")
            if self["style"].get("stroke-linejoin", "") == "miter":
                to_remove.append("stroke-linejoin")
            if self["style"].get("stroke-linecap", "") == "butt":
                to_remove.append("stroke-linecap")
            for a in to_remove:
                self["style"].pop(a, None)
                
            if 'stroke-dasharray' not in self['style'] and 'stroke-dashoffset' in self['style']:
                self['style'].pop('stroke-dashoffset')
                
        if self.type == "svg":
            self.remove_child("metadata", {}, ok_not_there=True)

        self.pop("sodipodi:nodetypes", None)

        for c in self:
            c.cleanup_misc_inkscape_crap()

        self.remove_empty_elements()

        return self
    
    def remove_all_clip_rule_nonzero_styles(self):
        self.unfold_style_attribute(recursive=True)
        for c in self.subtree_iterator():
            if 'style' in c:
                c['style'].pop('clip-rule', None)
                if len(c['style']) == 0:
                    c.pop('style')
        return self
    
    def remove_all_text_align_and_text_anchor_styles_in_paths(self):
        self.unfold_style_attribute(recursive=True)
        for c in self.subtree_iterator():
            if c.type != 'path' or 'style' not in c:
                continue
            c['style'].pop('text-align', None)
            c['style'].pop('text-anchor', None)
            if len(c['style']) == 0:
                c.pop('style')
        return self
    
    def remove_all_aria_label_attributes(self):
        for c in self.subtree_iterator():
            c.pop('aria-label', None)
        return self
    
    def remove_all_direction_ltr_styles_and_attributes(self): # since 'ltr' is the default (?)
        self.unfold_style_attribute(recursive=True)
        for c in self.subtree_iterator():
            if c.get('direction', None) == 'ltr':
                c.pop('direction')
            if 'style' not in c:
                continue
            if c['style'].get('direction', None) == 'ltr':
                c['style'].pop('direction')
            if len(c['style']) == 0:
                c.pop('style')
        return self
    
    def remove_all_direction_styles_from_gs_and_paths(self):
        self.unfold_style_attribute(recursive=True)
        for c in self.subtree_iterator():
            if c.type != 'path' and c.type != 'g' or 'style' not in c:
                continue
            c['style'].pop('direction', None)
            if len(c['style']) == 0:
                c.pop('style')
        return self

    def cleanup_gs_with_no_attributes(self, remove_aria_label=False):
        if (self.type == "g"
            and remove_aria_label
            and len(self.attr) == 1
            and "aria-label" in self
        ):
            self.pop("aria-label")
        if self.type == "g" and len(self.attr) == 0 and self.parent is not None:
            if len(self) > 0:
                self[0].cleanup_gs_with_no_attributes()  # we must leave parent something that is already cleaned up, in the place where we used to be
            self.give_children_to_parent_and_delete()
        for c in self:
            c.cleanup_gs_with_no_attributes(remove_aria_label)
        return self

    def g_push_styles_to_all_children(self):
        if self.type == "g" and "style" in self:
            self.unfold_style_attribute()
            original = copy(self["style"])
            for child in self:
                child.unfold_style_attribute()
                child_style = child.get("style", {})
                final_style = copy(self["style"])
                final_style.update(child_style)
                assert "transform" not in final_style
                assert "stroke" not in final_style or (
                    "stroke" not in self and "stroke" not in child
                )
                assert "fill" not in final_style or (
                    "fill" not in self and "fill" not in child
                )
                assert "stroke-width" not in final_style or (
                    "stroke-width" not in self and "stroke-width" not in child
                )
                if len(final_style) > 0:
                    child["style"] = final_style
            assert self["style"] == original
            self.pop("style")
        for c in self:
            c.g_push_styles_to_all_children()
        return self

    def transfer_g_attributes_to_single_children(self):
        if self.type == "g" and len(self) == 1:
            child = self[0]

            # style
            self.unfold_style_attribute()
            child.unfold_style_attribute()
            g_style = self.get("style", {})
            child_style = child.get("style", {})
            final_style = copy(g_style)
            final_style.update(child_style)
            assert "transform" not in final_style  # should be an attribute
            assert "stroke" not in final_style or (
                "stroke" not in self and "stroke" not in child
            )
            assert "fill" not in final_style or (
                "fill" not in self and "fill" not in child
            )
            assert "stroke-width" not in final_style or (
                "stroke-width" not in self and "stroke-width" not in child
            )
            if len(final_style) > 0:
                child["style"] = final_style
            self.pop("style", "")

            # transform
            if "transform" in self and "transform" in child:
                g_transform = self["transform"]
                child_transform = child["transform"]
                if isinstance(g_transform, list) and isinstance(child_transform, list):
                    final_transform = g_transform + child_transform
                elif isinstance(g_transform, np.ndarray) and isinstance(
                    child_transform, np.ndarray
                ):
                    final_transform = g_transform.dot(child_transform)
                else:
                    final_transform = generate_transform_if_not_already_string(
                        g_transform
                    ) + generate_transform_if_not_already_string(
                        child_transform
                    )
                child["transform"] = final_transform
            elif "transform" in self:
                assert "transform" not in child
                child["transform"] = self["transform"]
            self.pop("transform", "")

            # class (WARNING: refactored long after use, to remove use of 'unfold_class_attribute')
            if "class" in self and "class" in child:
                our_classes = self.class_attribute_as_list()
                child_classes = child.class_attribute_as_list()
                to_remove = [name for name in our_classes if name in child_classes]
                for z in to_remove:
                    self.remove_class(z)
            elif "class" in self:
                assert "class" not in child
                assert self.class_attribute_is_nonempty()
                child["class"] = self["class"]
                self.pop("class")

            to_pop = []
            for name in self.attr:
                assert name != "style"
                assert name != "transform"
                if name == "class":
                    assert "class" in child
                    continue
                if name not in child:
                    child[name] = self[name]
                    to_pop.append(name)
            for x in to_pop:
                self.pop(x)

        for c in self:
            c.g_attributes_to_single_child()

        return self

    def is_empty_path_or_empty_group(self):
        if self.type == "path":
            if isinstance(self['d'], Path) or isinstance(self['d'], Subpath):
                return self["d"].is_empty()
            return parse_path(self['d']).is_empty()

        if self.type == "g":
            return len(self) == 0

        return False

    def tail_and_subsequent_siblings_to_new_node(self, type, attr):
        new_node = lUnAtIc(type, attr)
        new_node.text = self.tail
        self.tail = ""
        while self.next_sibling() is not None:
            new_node.append(self.parent.removed_child(self.next_sibling()))
        assert self is self.parent.last_child()
        return new_node

    def split_after_child(self, c, attr):
        split = c.tail_and_subsequent_siblings_to_new_node(self.type, attr)
        if split.text != "" or len(split) > 0:
            self.parent.insert_after(split, self)
            assert split.tail == ""
            split.tail = self.tail
        self.tail = ""

    def promulgate_child(self, c, attr):
        assert c in self
        self.split_after_child(c, attr)
        assert c is self.last_child()
        self.remove_child(c)
        self.parent.insert_after(c, self)

    def enclose_in(self, *args):
        item = self.maybe_factory(*args)
        assert len(item) == 0
        if self.parent is not None:
            self.replace_with(item).append(self)
        else:
            item.append(self)
        return self.parent

    def enclose_in_and(self, *args):
        self.enclose_in(*args)
        return self

    def wrap_self_in(self, *args):
        return self.enclose_in(*args)

    def wrap_children_in(self, *args):
        original_parent = self.parent
        original_length = len(self)
        newguy = self.replace_in_place(*args)
        newguy.enclose_in(self)
        assert self.parent is original_parent
        assert newguy.parent is self
        assert len(newguy) == original_length
        assert len(self) == 1
        return newguy

    def wrap_children_slice_in(self, start, end, *args):
        assert len(self) > 0
        start = self[0] if start is None else start
        assert start in self
        assert end is None or end in self
        start_index = self.index_of_putative_child(start)
        end_index = (
            self.index_of_putative_child(end)
            if (end is not None and end in self)
            else len(self)
        )
        return self.wrap_children_slice_in_index_version(start_index, end_index, *args)
        
    def wrap_children_slice_in_index_version(self, start_index, end_index, *args):
        assert 0 <= start_index < end_index <= len(self)
        start = self[start_index]
        end = None if end_index == len(self) else self[end_index]

        wrapper = self.maybe_factory(*args)
        assert len(wrapper) == 0
        self.insert_before(wrapper, start)
        
        node = start
        while node is not end:
            m = node.next_sibling()
            wrapper.append(node.detach(tail_aware=True))
            assert node not in self
            node = m
        assert start not in self and start in wrapper
        assert end is None or end in self
        assert wrapper in self
        assert len(wrapper) == end_index - start_index
        return wrapper

    ##############
    # navigating #
    ##############

    def first_child(self):
        return self[0] if len(self) > 0 else None

    def last_child(self):
        return self[-1] if len(self) > 0 else None

    def next_child(self, c):
        ze_index = self.index_of_putative_child(c)
        return self[ze_index + 1] if ze_index + 1 < len(self) else None

    def prev_child(self, c):
        ze_index = self.index_of_putative_child(c)
        return self[ze_index - 1] if ze_index - 1 >= 0 else None

    def next_sibling(self):
        if not self.parent:
            return None
        return self.parent.next_child(self)

    def prev_sibling(self):
        if not self.parent:
            return None
        return self.parent.prev_child(self)

    def root(self):
        if self.parent is None:
            return self
        return self.parent.root()

    def elders(self):  # [root, ..., me.parent]
        if self.parent is None:
            return []
        to_return = self.parent.elders()
        to_return.append(self.parent)
        return to_return

    def depth(self):
        if self.parent is None:
            return 0
        return self.parent.depth() + 1

    def next_node_outside_subtree(self):
        if self.next_sibling() is not None:
            return self.next_sibling()
        
        if self.has_parent():
            return self.parent.next_node_outside_subtree()
        
        return None

    def last_descendant(self):
        return self if len(self) == 0 else self.last_child().last_descendant()

    def prev(self):
        if self.prev_sibling() is not None:
            return self.prev_sibling().last_descendant()
        return self.parent

    def next(self):
        if self.first_child() is not None:
            return self.first_child()
        return self.next_node_outside_subtree()

    def subtree_iterator(self):
        yield self
        for c in self:
            for t in c.subtree_iterator():
                yield t

    def has_parent(self):
        return self.parent is not None

    def has_ancestor(self, putative_ancestor):
        c = self.parent
        while c is not None:
            if c is putative_ancestor:
                return True
            c = c.parent
        return False

    #####################################
    # unused 'in_subtree_of', 'beneath' #
    #####################################

    def in_subtree_of(self, element):
        if self == element:
            return True
        if not self.parent:
            return False
        return self.parent.in_subtree_of(element)

    def beneath(self, element):
        if self.parent == element:
            return True
        if not self.parent:
            return False
        return self.parent.beneath(element)

    ###########
    # finding #
    ###########

    def parse_match_types(self, *args):
        assert len(args) > 0
        if isinstance(args[0], list):
            assert len(args) == 1
            args = args[0]

        results = []
        i = 0
        while i < len(args):
            if isinstance(args[i], str) or args[i] is None:
                type_string = args[i]
                attrs = {}
                if len(args) > i + 1 and isinstance(args[i + 1], dict):
                    attrs = args[i + 1]
                    i += 1
                results.append(_parse_type_string(type_string, attrs, for_matching=True))

            elif isinstance(args[i], lUnAtIc):
                results.append((args[i], None))

            else:
                print("args[i]:", args[i])
                assert False

            i += 1
    
        return results

    def __matches(self, type, attrs):
        if isinstance(type, lUnAtIc):
            assert False  # (I'm curious I forgot when this really happens)
            assert attrs is None
            return self is type
        assert type is None or (
            isinstance(type, str) and "." not in type and "#" not in type
        )
        assert isinstance(attrs, dict)
        if type not in [self.type, None]:
            return False
        if "[text-contains]" in attrs and attrs["[text-contains]"] not in self.text:
            return False
        if "[tail-contains]" in attrs and attrs["[tail-contains]"] not in self.tail:
            return False
        return attributes_match(attrs, self.attr)

    def matches(self, *args):
        match_types = self.parse_match_types(*args)
        return any(self.__matches(x[0], x[1]) for x in match_types)

    def matches_one_of(self, matching_types):
        for m in matching_types:
            assert len(m) == 2
            if self.__matches(m[0], m[1]):
                return True
        return False

    def find(self, type_string, xtra_attrs={}):
        type, d = _parse_type_string(type_string, xtra_attrs, for_matching=True)
        to_return = self.__find(type, d)
        assert to_return is None or isinstance(to_return, lUnAtIc)
        return to_return

    def __find(self, type, d):
        if self.__matches(type, d):
            return self
        for c in self.__children:
            result = c.__find(type, d)
            if result is not None:
                assert isinstance(result, lUnAtIc)
                return result
        return None

    def find_among_children(self, type_string, xtra_attrs={}):
        type, d = _parse_type_string(type_string, xtra_attrs, for_matching=True)
        return self.__find_among_children(type, d)
    
    def find_first_ancestor(self, type_string, xtra_attrs={}, or_self=False):
        type, d = _parse_type_string(type_string, xtra_attrs, for_matching=True)
        return self.__find_first_ancestor(type, d, or_self)

    def find_unique_among_children(self, type_string, xtra_attrs={}):
        type, d = _parse_type_string(type_string, xtra_attrs, for_matching=True)
        found = self.__find_all_among_children(type, d)
        if len(found) != 1:
            print("found:", found)
            assert False
        return found[0]

    def __find_among_children(self, type, d):
        for c in self:
            if c.__matches(type, d):
                return c
        return None

    def __find_first_ancestor(self, type, d, or_self):
        a = self if or_self else self.parent
        while a is not None:
            if a.__matches(type, d):
                return a
            a = a.parent
        return None

    def __find_all_among_children(self, type, d):
        return [c for c in self if c.__matches(type, d)]

    def find_all(self, type_string, xtra_attrs={}, enter_matching_elements=True):
        type, d = _parse_type_string(type_string, xtra_attrs, for_matching=True)
        return self.__find_all(type, d, enter_matching_elements)

    def __find_all(self, type, attrs, enter_matching_elements=True):
        found = [self] if self.__matches(type, attrs) else []

        if enter_matching_elements or found == []:
            for c in self:
                found.extend(c.__find_all(type, attrs, enter_matching_elements))

        return found

    def find_unique(self, type_string, xtra_attrs={}):
        type, d = _parse_type_string(type_string, xtra_attrs, for_matching=True)
        return self.__find_unique(type, d)

    def find_else(self, arg1, arg2, arg3=None, arg4=None):
        assert isinstance(arg1, str)
        type_string_1 = arg1

        if isinstance(arg2, dict):
            assert isinstance(arg3, str)
            xtra_attrs_1 = arg2
            type_string_2 = arg3
            xtra_attrs_2 = arg4 if isinstance(arg4, dict) else {}

        else:
            xtra_attrs_1 = {}
            type_string_2 = arg2
            assert isinstance(type_string_2, str)
            assert isinstance(arg3, dict) or arg3 is None
            assert arg4 is None
            xtra_attrs_2 = arg3 if isinstance(arg3, dict) else {}

        type_string_1, attrs_1 = _parse_type_string(
            type_string_1, xtra_attrs_1, for_matching=True
        )
        type_string_2, attrs_2 = _parse_type_string(
            type_string_2, xtra_attrs_2, for_matching=True
        )

        res = self.__find(type_string_1, attrs_1)

        if res is not None:
            return res

        return self.__find(type_string_2, attrs_2)

    def __find_unique(self, type, d):
        found = self.__find_all(type, d)
        if len(found) != 1:
            print("found", len(found), "of", type, d)
            assert False
        return found[0]

    def process_matching_types_list(self, matching_types_raw):
        matching_types = []
        for c in matching_types_raw:
            if isinstance(c, str):
                type, d = _parse_type_string(c, {}, for_matching=True)
            else:
                assert len(c) == 2
                type, d = _parse_type_string(c[0], c[1], for_matching=True)
            matching_types.append((type, d))
        return matching_types

    def find_of(self, matching_types_raw):
        matching_types = self.process_matching_types_list(matching_types_raw)
        return self.__find_of(matching_types)

    def __find_of(self, matching_types):
        assert type(matching_types) == list and len(matching_types) > 0
        for c in matching_types:
            assert type(c[0]) in [str, type(None)]
            assert type(c[1]) == dict
            if self.__matches(c[0], c[1]):
                return self
        for c in self.__children:
            result = c.__find_of(matching_types)
            if result is not None:
                return result
        return None

    def find_all_of(self, matching_types_raw, enter_matching_elements=True):
        matching_types = self.process_matching_types_list(matching_types_raw)
        return self.__find_all_of(matching_types, enter_matching_elements)

    def has_none_of(self, matching_types_raw, enter_matching_elements=False):
        return len(self.find_all_of(matching_types_raw, enter_matching_elements)) == 0

    def __find_all_of(self, matching_types, enter_matching_elements=True):
        found = []
        for c in matching_types:
            assert type(c[0]) in [str, type(None)]
            assert type(c[1]) == dict
            if self.__matches(c[0], c[1]):
                found.append(self)
                break
        if enter_matching_elements or not found:
            for c in self.__children:
                found.extend(c.__find_all_of(matching_types, enter_matching_elements))
        return found

    def find_by(self, callback):
        if callback(self):
            return self
        for c in self.__children:
            result = c.find_by(callback)
            if result is not None:
                return result
        return None

    def style_aware_attribute(self, name):
        self.unfold_style_attribute()
        our_style = self.get("style", {})

        if name in our_style:
            print("name:", name)
            print("our_style:", our_style)
        assert name not in our_style

        if name in self:
            return self[name]

        classes = []
        if "class" in self:
            classes = self.class_attribute_as_list()

        to_return = None
        for c in classes:
            s = self.root().find_style("." + c, unfold=True)
            if s is not None and name in s:
                if to_return is not None:
                    print(f"(note: overwritten style: {c})")
                to_return = s[name].val_str()

        return to_return

    def style_aware_property(self, name, allow_attribute=False):
        ####
        # (attributes only taken into account on element itself)
        ####
        self.unfold_style_attribute()
        our_style = self.get("style", {})

        assert name not in self or name not in our_style

        if name in our_style:
            return our_style[name].val_str()

        if name in self:
            assert allow_attribute
            return self[name]

        classes = []
        q = self
        while q is not None:
            classes.extend(q.class_attribute_as_list())
            q = q.parent

        to_return = None
        for c in reversed(classes):
            s = self.root().find_style("." + c, unfold=True)
            if s is not None and name in s:
                if to_return is not None:
                    print(f"(note: overwritten style: {c})")
                to_return = s[name].val_str()

        return to_return

    def find_style(self, name, unfold=False):
        stopwatch("find_style")

        if self.type not in ["html", "head", "svg", "style", "g"]:
            print("~~~ warning ~~~: find_style called on a", self.type)

        check_css_selector_health(name)

        to_return = None

        for node in self.__find_all("style", {}):
            assert node.type == "style"

            stopwatch("simple_unfold_css in find_style")
            node.simple_unfold_css()
            stopwatch()

            if name in node.text:
                if unfold:
                    stopwatch("unfolding style in find_style")
                    node.text[name] = style_parse(node.text[name])
                    stopwatch()

                assert to_return is None
                to_return = node.text[name]

        stopwatch()
        return to_return

    def head(self):
        return self.__find("head", {})

    def body(self):
        return self.__find("body", {})

    def defs(self):
        return self.__find("defs", {})

    #####################################
    # child finding (including indices) #
    #####################################

    def index_of_putative_child(self, *args):
        assert 1 <= len(args) <= 2

        if isinstance(args[0], lUnAtIc):
            assert len(args) == 1
            for index, c in enumerate(self):
                if c is args[0]:
                    assert not any(t is c for t in self[index + 1 :])
                    return index
            assert False

        assert len(args) == 1 or isinstance(args[1], dict)
        assert isinstance(args[0], str) or (args[0] is None and len(args) == 2)

        type_string, attrs = _parse_type_string(
            args[0], {} if len(args) == 1 else args[1], for_matching=True
        )

        for index, c in enumerate(self):
            if c.__matches(type_string, attrs):
                assert not any(
                    d.__matches(type_string, attrs) for d in self[index + 1 :]
                )
                return index

        assert False

    def own_index(self):
        assert self.parent is not None
        return self.parent.index_of_putative_child(self)

    def num_children_matching(self, *args):
        num = 0
        if len(args) == 1 and isinstance(args[0], lUnAtIc):
            for c in self:
                if c is args[0]:
                    num += 1

        elif len(args) == 2 and matching_type_args(*args):
            for c in self:
                if c.__matches(*args):
                    num += 1

        else:
            assert False

        return num

    def index_of_first_child_matching(self, *args):
        if len(args) == 1 and isinstance(args[0], lUnAtIc):
            for index, c in enumerate(self):
                if c is args[0]:
                    return index

        elif len(args) == 2 and matching_type_args(*args):
            for index, c in enumerate(self):
                if c.__matches(*args):
                    return index

        else:
            assert False

        return -1

    def child_with_id(self, ze_id):
        assert type(ze_id) == str
        return self.__find_among_children(None, {"id": ze_id})

    ####################################
    # defs / style access and creation #
    ####################################

    def get_or_create_first_level_defs(self):
        assert self.type == "svg" or self is self.root()
        for c in self:
            if c.type == "defs":
                return c
        return self.insert_after_last_of("defs", "style")

    def get_or_create_defs(self):
        if self.type not in ["svg", "defs"]:
            print("self.type:", self.type)
            assert False
        defs = self.__find("defs", {})
        if defs is None:
            defs = self.insert_after_last_of("defs", "sodipodi:namedview", "style")
        return defs

    def ze_defs(self):
        assert self.type == "svg" or self is self.root()
        defs = self.__find_all("defs", {})
        if len(defs) != 1:
            print(self)
            assert False
        assert defs[0] in self.__children
        return defs[0]

    def get_or_create_head(self):
        assert self.type == "html"
        head = self.__find("head", {})
        if head is not None:
            assert head is self.__children[0]
        else:
            head = self.prepend("head", {})
        return head

    def get_or_create_body(self):
        assert self.type == "html"
        head = self.find_among_children('head')
        body = self.find_among_children('body')
        if body is None:
            if head is None:
                self.insert_after(head, "body")
            else:
                self.append("body")
            body = self.find_among_children('body')
        assert body is not None
        return body

    def get_or_create_style(self):
        assert self.type in ["style", "svg", "head", "html"]
        style = self.__find("style", {})
        if style is None:
            if self.type == "head":
                style = self.append("style", {"type": "text/css"})

            elif self.type == "svg":
                style = self.insert_after_last_of("style", {"type": "text/css"}, "sodipodi:namedview")

            else:
                assert self.type == "html"
                head = self.get_or_create_head()
                style = head.append("style", {"type": "text/css"})

        if self.parent is not None:
            assert style.parent.type in ["head", "svg", "g"]

        return style

    def append_style_element(self, text):
        self.append("style")
        self[-1].text = text

    ###########################
    # <style> element editing #
    ###########################

    def remove_unused_styles(self):
        for style_el in self.__find_all("style", {}):
            style_el.simple_unfold_css()
            root = self.root()
            to_delete = []
            for stylename in style_el.text:
                assert stylename.startswith(".")
                if (
                    root.__find(
                        # Nb: we're accommodating the possibility of 'class' value being a list, though it's never the case rn:
                        None, {"class": "[appears-as-string-block-or-in-list]" + stylename[1:]}
                    )
                    is None
                ):
                    to_delete.append(stylename)

            for stylename in to_delete:
                style_el.text.pop(stylename)

        return self

    def update_a_css_stylename(self, old_name, new_name):
        found_already = False
        assert old_name != new_name

        for style_el in self.__find_all("style", {}):
            style_el.simple_unfold_css()
            assert new_name not in style_el

            if old_name in style_el.text:
                assert found_already is False
                found_already = True
                style_el.text[new_name] = style_el.text.pop(old_name)

    def remove_unused_ids(self, but_keep_these=[]):
        assert self.type == "svg" or self.type == "html"

        # to avoid a quadratic blowup (every id looking through every element)
        # we make a preliminary list of suspects:

        href_people = self.find_all(None, {"[string-endswith]href": "[string-startswith]#"})
        url_people = self.find_all(None, {"[any]": "[string-startswith]url(#"})
        style_url_people_tmp = self.find_all(None, {"style": {}})
        style_url_people = []
        for s in style_url_people_tmp:
            s.unfold_style_attribute()
            for _, css_dec in s['style'].items():
                string = css_dec.val_str()
                if string.startswith("url(#"):
                    assert string.endswith(")")
                    style_url_people.append(s)
                    break

        def internal_first_use_of_id(ze_id):
            lists_and_matching_dicts = [
                (href_people, {"[string-endswith]href": "#" + ze_id}),
                (url_people, {"[any]": f"url(#{ze_id})"}),
                (style_url_people, {"style": {"any-key": f"url(#{ze_id})"}})
            ]
            for ze_list, ze_matching_dict in lists_and_matching_dicts:
                for c in ze_list:
                    if c.matches(None, ze_matching_dict):
                        return c
            return None

        for c in self.subtree_iterator():
            if c is self:
                continue
            if 'id' not in c:
                continue
            ze_id = c['id']
            if ze_id in but_keep_these:
                continue
            first_use = internal_first_use_of_id(ze_id)
            if first_use is None:
                c.pop('id')

        return self
    
    def first_use_of_id(self, ze_id):
        assert self.type == "svg" or self.type == "html"

        candidate_a = self.find(None, {"[string-endswith]href": "#" + ze_id})
        if candidate_a is not None:
            return candidate_a
        
        candidate_b = self.find(None, {"[any]": f"url(#{ze_id})"})
        if candidate_b is not None:
            return candidate_b

        candidate_c = self.find(None, {"style": {"any-key": f"url(#{ze_id})"}})
        if candidate_c is not None:
            return candidate_c
        
        return None

    def remove_unused_definitions(self):
        assert self.type == "svg" or self.type == "html"
        if self.parent:
            print("warning: remove_unused_definitions being called on node with parent")
        all_defs = self.find_all("defs")
        keep_going = True
        while keep_going:
            keep_going = False
            to_remove = []
            for defs in all_defs:
                for d in defs:
                    if "id" not in d:
                        print(
                            "warning: element without id in defs in remove_unused_definitions:"
                        )
                        print(d)
                        assert False  # (used to be 'continue'...)
                    ze_id = d["id"]
                    if self.first_use_of_id(ze_id) is None:
                        print("removing def with id", ze_id)
                        to_remove.append(d)
            for d in to_remove:
                keep_going = True
                d.detach()
        return self

    def no_children_and_text_is_whitespace_or_empty_dict(self):
        if len(self) > 0:
            return False

        if isinstance(self.text, str):
            return is_whitespace(self.text)

        elif isinstance(self.text, dict):
            return len(self.text) == 0

        assert False

    def remove_empty_elements(
        self, types=["g", "defs", "style", "text", "flowroot", "flowpara"]
    ):
        to_remove = []
        for c in self:
            c.remove_empty_elements(types=types)
            if c.type in types and c.no_children_and_text_is_whitespace_or_empty_dict():
                to_remove.append(c)

        for c in to_remove:
            self.remove_child(c)

        return

    def promote_elements(self):
        for c in self:
            c.promote_elements()
        if self.type == "g" and len(self.attr) == 0:
            self.give_children_to_parent_and_delete()

    def add_a_css_style(self, name, content, on_overwrite="crash"):
        """
        possible formats for content:
            - ordinary css in one string
            - a dictionary
        """
        stopwatch("add_style")
        check_css_selector_health(name)

        if isinstance(content, str):
            dIcT = style_parse(content)

        elif isinstance(content, dict):
            dIcT = {}
            for key, value in content.items():
                dIcT[key] = (
                    value
                    if isinstance(value, Css_Declaration)
                    else Css_Declaration(key + ":" + str(value))
                )

        else:
            assert False

        style_element_dict = self.get_or_create_style().simple_unfold_css().text

        if name in style_element_dict:
            assert on_overwrite in ["crash", "warn", "ignore"]
            d1 = style_element_dict[name]
            d2 = dIcT

            if d1 != d2:
                if on_overwrite == "crash":
                    print("~~~ crashing ~~~ overwriting style", name)
                    print("old:", style_element_dict[name])
                    print("new:", dIcT)
                    print(all(k in d2 and d2[k] == d1[k] for k in d1))
                    print(all(k in d1 and d2[k] == d1[k] for k in d2))
                    print(d1["fill"] == d2["fill"])
                    print(type(d1["fill"]), type(d2["fill"]))
                    assert False

                elif on_overwrite == "warn":
                    print("~~~ warning ~~~ overwriting style", name)
                    print("old:", style_element_dict[name])
                    print("new:", dIcT)

        style_element_dict[name] = dIcT
        check_inner_style_health(style_element_dict[name])
        stopwatch()
        return self
    
    def add_a_stroke_style(self, className, width, color):
        assert isinstance(width, Real)
        self.get_or_create_style().add_a_css_style(className, {'fill': 'none', 'stroke-width': width, 'stroke': color})
    
    def add_a_fill_style(self, className, color):
        self.get_or_create_style().add_a_css_style(className, {'stroke': 'none', 'fill': color})
    
    def add_css_styles(self, styles, on_overwrite="crash"):
        for stylename, value in styles.items():
            self.add_a_css_style(stylename, value, on_overwrite=on_overwrite)

    def update_a_css_style(self, name, dIcT_or):
        stopwatch("update_style")
        style_dict = self.find_style(name, unfold=True)

        if style_dict is not None:
            assert isinstance(style_dict, dict)
            if isinstance(dIcT_or, str):
                dIcT_or = style_parse(dIcT_or)
            assert isinstance(dIcT_or, dict)
            update_style_dict_with(style_dict, dIcT_or)
            check_inner_style_health(style_dict)

        else:
            self.add_a_css_style(name, dIcT_or)

        stopwatch()

    def update_css(self, dict_or_string):
        unfolded = double_unfold_css(dict_or_string)
        for key, item in unfolded.items():
            self.update_a_css_style(key, item)
        return self

    def append_external_stylesheet(self, filename):
        assert self.type in ["head", "svg"]
        self.append("link", {"href": filename, "type": "text/css", "rel": "stylesheet"})

    ####################
    # attribute lookup #
    ####################

    def get(self, name, default=None):
        if isinstance(default, int):
            try:
                return int(self[name]) if name in self else default

            except ValueError:
                return float(self[name]) if name in self else default

        if isinstance(default, float):
            return float(self[name]) if name in self else default

        # attribute value should never be None, so this is practical/good
        if name in self:
            assert self[name] is not None
            return self[name]

        return default

    def get_float(self, name, default=None):
        thing = self.get(name, default)
        if thing is None:
            print("name, default, self.type:", name, default, self.type)
            assert False
        return float(thing)

    def has(self, name, value):
        return name in self and self[name] == value

    def style_property_is_x_heuristic(self, name, x):
        self.unfold_style_attribute()

        if deepget(self(), "style", name) == x:
            return True

        if "class" in self:
            is_not_x = False
            is_x = False
            for c in self["class"].split():
                style_dict = self.root().find_style("." + c, unfold=True)
                if style_dict is not None and name in style_dict:
                    if x in style_dict[name].val_str():
                        is_x = True

                    else:
                        is_not_x = True

            assert not (is_x and is_not_x)

            if is_x:
                return True

        return False

    def is_display_none_heuristic(self):
        return self.style_property_is_x_heuristic("display", "none")

    def is_visibility_hidden_heuristic(self):
        return self.style_property_is_x_heuristic("visibility", "hidden")

    def viewbox_x(self):
        assert "viewbox" in self
        self.unfold_viewbox()
        return self["viewbox"][0]

    def viewbox_y(self):
        assert "viewbox" in self
        self.unfold_viewbox()
        return self["viewbox"][1]

    def viewbox_width(self):
        assert "viewbox" in self
        self.unfold_viewbox()
        return self["viewbox"][2]

    def viewbox_height(self):
        assert "viewbox" in self
        self.unfold_viewbox()
        return self["viewbox"][3]

    #####################
    # attribute editing #
    #####################

    def remove_attribute(self, key, recursive=False):
        if key.startswith("[string-startswith]"):
            suffix = key[19:]
            to_pop = [k for k in self.attr if k.startswith(suffix)]

        else:
            to_pop = [key] if key in self else []

        popped = [self.attr.pop(k) for k in to_pop]
        count = len(popped)

        if key.startswith("[string-startswith]"):
            if count == 0:
                popped_to_return = None

            else:
                assert count == 1
                popped_to_return = popped[0]

        else:
            popped_to_return = popped

        if recursive:
            for c in self:
                count += c.remove_attribute(key, True)

        return count if recursive else popped_to_return

    def pop(self, key, default=unique_object):
        if default is unique_object:
            return self.attr.pop(key)
        return self.attr.pop(key, default)

    def pop_int(self, key, default=unique_object):
        if default is unique_object:
            assert key in self.attr
            return int(self.attr.pop(key))

        elif key in self.attr:
            return int(self.attr.pop(key))

        return default

    def pop_float(self, key, default=unique_object):
        if default is unique_object:
            assert key in self.attr
            return float(self.attr.pop(key))

        elif key in self.attr:
            return float(self.attr.pop(key))

        return default

    def pop_and(self, key, default=unique_object):
        self.pop(key, default)
        return self

    def pop_all(self, *args):
        if len(args) == 1 and isinstance(args[0], list):
            args = args[0]
        for a in args:
            self.pop(a)
        return self

    def try_pop_all(self, *args):
        for a in args:
            self.pop(a, default=None)
        return self

    def pop_0_xs_ys(self):
        if self.get("x", None) == 0:
            self.pop("x")

        if self.get("y", None) == 0:
            self.pop("y")

        for c in self:
            c.pop_0_xs_ys()

        return self

    def update_style_attribute(self, new_material):
        if isinstance(new_material, str) and (
            "style" not in self
            or self["style"] == ""
            or (isinstance(self["style"], dict) and len(self["style"]) == 0)
        ):
            self.attr["style"] = new_material
            return self

        self.ensure_unfolded_style_attribute()
        new_dict = new_material if isinstance(new_material, dict) else style_parse(new_material)
        update_style_dict_with(self["style"], new_dict)

        return self

    def normalize_style_attribute(self):
        if "style" in self and isinstance(self["style"], str):
            endswith_sem = self["style"].rstrip().endswith(";")
            self["style"] = ";".join(
                [x.strip() for x in self["style"].split(";") if x != ""]
            )
            if endswith_sem:
                self["style"] += ";"
            assert "\n" not in self["style"]
        return self

    def update_attribute(self, key, new_val):
        print("\nDEPRECATED; please use .update_style_attribute or .set_attribute, depending on your needs\n")
        assert False

    def update_attributes(self, new_attrs, recursive=False):
        print("\nDEPRECATED; use .set_attributes please\n")
        assert False

    def selective_update_attributes(self, type1, attr1, attr2, recursive=True):
        print("\nDEPRECATED; use .selective_set_attributes please\n")
        assert False

    def dressed_in_black(self):
        assert self.type in GEOMETRIC_ELEMENTS
        return self.detached_copy().update_style_attribute(
            {"fill": "#000", "stroke": "none", "stroke-width": None}
        )

    def set_cxy_attributes(self, *args):
        if len(args) == 2:
            x, y = args[0], args[1]

        elif len(args) == 1:
            x, y = real_numbers_in(args[0])

        else:
            assert False

        assert isinstance(x, Real)
        assert isinstance(y, Real)

        self["cx"] = x
        self["cy"] = y

        return self

    def steal_attribute_if_there(self, other, name):
        assert name not in self
        if name in other:
            self[name] = other.pop(name)
        return self

    def set_text(self, string):
        assert isinstance(string, str)
        assert self.type != "style"
        self.text = string
        return self

    #################
    # class editing #
    #################

    def class_attribute_as_list(self):
        to_return = self['class'].split() if 'class' in self else []
        assert len(to_return) > 0 or 'class' not in self
        assert all(isinstance(x, str) for x in to_return)
        if any(not is_string_and_good_classname(x) for x in to_return):
            print("there is an offending classname here:")
            for x in to_return:
                print("---:", x)
        assert all(is_string_and_good_classname(x) for x in to_return)
        return to_return

    def class_attribute_as_set(self):
        return set(self.class_attribute_as_list())

    def set_class_attribute_from_set(self, classes):
        assert isinstance(classes, set)
        assert all(is_string_and_good_classname(x) for x in classes)
        if len(classes) > 0:
            self['class'] = ' '.join(list(classes))
        else:
            self.pop('class', None)
        return self

    def set_class_attribute_from_list(self, classes):
        assert isinstance(classes, list)
        return self.set_class_attribute_from_set(set(classes))

    def remove_class(self, name, recursive=False):
        assert isinstance(name, str)
        self.set_class_attribute_from_set(self.class_attribute_as_set() - set([name]))
        if recursive:
            for c in self:
                c.remove_class(name, True)
        return self

    def add_class(self, name):
        assert is_string_and_good_classname(name)
        have_to_name = self.class_attribute_as_set()
        have_to_name.add(name)
        return self.set_class_attribute_from_set(have_to_name)
    
    def add_class_maybe(self, name):
        if name is None:
            return self
        return self.add_class(name)

    def add_classes(self, *args):
        classnames = args[0] if len(args) == 1 and isinstance(args[0], Iterable) else args
        for name in classnames:
            self.add_class(name)
        return self

    def remove_classes(self, *args):
        classnames = args[0] if len(args) == 1 and isinstance(args[0], Iterable) else args
        for name in classnames:
            self.remove_class(name)
        return self    

    def class_attribute_is_nonempty(self):
        if 'class' not in self:
            return False
        assert isinstance(self['class'], str)
        return not is_whitespace(self['class'])

    def has_class(self, name):
        assert isinstance(name, str)
        return name in self.class_attribute_as_list()
    
    def has_one_of_classes(self, classes):
        return any(self.has_class(name) for name in classes)

    def has_or_inherits_class(self, name_or_list):
        classnames = name_or_list if isinstance(name_or_list, list) else [name_or_list]
        assert all(is_string_and_good_classname(x) for x in classnames)
        c = self
        while c is not None:
            if any(c.has_class(x) for x in classnames):
                return True
            c = c.parent
        return False

    def has_none_of_classes(self, *args):
        assert all(isinstance(arg, str) for arg in args)
        our_classes = self.class_attribute_as_list()
        return not any(arg in our_classes for arg in args)
    
    def factor_out_common_styles_as_classes(self, min_num=3, style_decimals=None, classnameroot="class"):
        assert self.type == 'svg'
        assert not classnameroot.startswith('.')

        for c in self.subtree_iterator():
            if 'style' not in c:
                continue
            c['style'] = c.attribute_to_string('style', None, style_decimals)
            assert isinstance(c['style'], str)
            
        frequency_dictionary = {}
        for c in self.subtree_iterator():
            if 'style' not in c:
                continue
            value = c['style']
            frequency_dictionary[value] = frequency_dictionary.get(value, -1) + 1  # clever
        
        added_styles = {}
        class_num = 0
        for style_string, frequency in frequency_dictionary.items():
            if frequency < min_num:
                continue
            class_num += 1
            class_name = f".{classnameroot}{class_num}"
            assert class_name not in added_styles
            added_styles[class_name] = style_string
        
        self.add_css_styles(added_styles)

        for c in self.subtree_iterator():
            if 'style' not in c:
                continue
            
            for class_name, value in added_styles.items():
                if c['style'] == value:
                    c.pop('style')
                    c.add_class(class_name[1:])  # avoid '.'
                break
                
        return self
                
    
    def create_groups_for_common_styles(self, min_length=3, recursive=True):
        assert min_length >= 2

        if recursive:
            for c in self:
                c.create_groups_for_common_styles(min_length=min_length)
                
        self.unfold_style_attribute()
        
        pairs = []
        equality_starts_at = 0
        
        for i, c in enumerate(self):
            if not c.style_attribute_equals_nonempty_style_attribute_of(self[equality_starts_at]):
                if i - equality_starts_at >= min_length:
                    pairs.append((equality_starts_at, i))
                equality_starts_at = i
                
        if len(self) - equality_starts_at >= min_length:
            pairs.append((equality_starts_at, len(self)))

        for pair in reversed(pairs):
            style = self[pair[0]]['style']
            g = lUnAtIc('g', {'style': style})
            self.wrap_children_slice_in_index_version(pair[0], pair[1], g)
        
    
    def create_groups_for_common_classes(self, min_length=3, recursive=True):
        assert min_length >= 2

        if recursive:
            for c in self:
                c.create_groups_for_common_classes(min_length=min_length)

        while True:
            # this shall be a dictionary with keys / values of the form:
            # (name, start_index_of_run) / end-index-or-None-if-not-yet-determined
            end_indices = {}

            for i, c in enumerate(self):
                c_classnames = c.class_attribute_as_list() if 'class' in c else []

                # first task: introduce new counters
                for name in c_classnames:
                    found = False
                    for name_and_start_index, end_index in end_indices.items():
                        if name_and_start_index[0] != name:
                            continue
                        if end_index is not None:
                            continue
                        found = True
                        break
                    if not found:
                        end_indices[(name, i)] = None

                # second task: close discontinued counters
                for name_and_start_index, end_index in end_indices.items():
                    if end_index is not None: # (already closed)
                        assert end_index < i
                        continue
                    if name_and_start_index[0] in c_classnames:
                        continue # we're not closing you!
                    # huh-oh...
                    end_indices[name_and_start_index] = i

            # close final laggards
            for name_and_start_index, end_index in end_indices.items():
                if end_index is not None: # (already closed)
                    assert end_index < len(self)
                    continue
                end_indices[name_and_start_index] = len(self)

            longest_run = 0

            for name_and_start_index, end_index in end_indices.items():
                assert end_index is not None
                assert 1 <= end_index <= len(self)
                length = end_index - name_and_start_index[1]
                assert 1 <= length <= len(self)
                if length > longest_run:
                    longest_run = length
                    start_index_of_longest_run = name_and_start_index[1]
                    end_index_of_longest_run = end_index
                    longest_run_classname = name_and_start_index[0]

            if longest_run == 0:
                break

            if longest_run == len(self):
                for c in self:
                    c.remove_class(longest_run_classname)
                self.add_class(longest_run_classname)
                continue

            if longest_run >= min_length:
                old_len_self = len(self)
                new_g = lUnAtIc('g',  {'class': longest_run_classname})
                to_move = []
                for c in self[start_index_of_longest_run:end_index_of_longest_run]:
                    to_move.append(c)
                assert len(to_move) == longest_run
                for c in to_move:
                    c.remove_class(longest_run_classname)
                    c.detach()
                new_g.append_all(to_move)
                self.insert(start_index_of_longest_run, new_g)
                assert len(self) == old_len_self - longest_run + 1
                new_g.create_groups_for_common_classes(min_length=min_length, recursive=recursive)
                continue

            break

        return self

    def promote_classes(self, recursive=True): # see also 'create_groups_for_common_classes', above
        if recursive:
            for c in self:
                c.promote_classes(True)

        if len(self) > 0:
            possibilities = self[0].class_attribute_as_set()
            retained = [classname for classname in possibilities if all(c.has_class(classname) for c in self[1:])]
            self.add_classes(retained)
            for c in self:
                c.remove_classes(retained)

        return self

    #################
    # css unfolding #
    #################

    def double_unfold_css(self, recursive=False):
        if self.type == "style":
            self.text = double_unfold_css(self.text)
        if recursive or self.parent is None:
            for c in self:
                c.double_unfold_css(True)

    def simple_unfold_css(self, recursive=False):
        if self.type == "style":
            self.text = simple_unfold_css(self.text)

        if recursive:
            for c in self:
                c.simple_unfold_css(True)

        return self

    ##########################
    # unfolding other things #
    ##########################

    def ensure_unfolded_style_attribute(self):
        attr = self.attr
        if "style" in attr:
            attr["style"] = style_parse(attr["style"])

        else:
            attr["style"] = {}

        return self

    def unfold_numbers(self, recursive=False, xtra_approved_number_fields=[]):
        attr = self.attr
        for key, value in attr.items():
            if not isinstance(value, str):
                continue
            if (
                key not in APPROVED_NUMBER_FIELDS
                and key not in xtra_approved_number_fields
            ):
                continue

            try:
                new_value = float(value)
                if new_value == int(new_value):
                    new_value = int(new_value)
                attr[key] = new_value

            except ValueError:
                continue

        if recursive:
            for c in self:
                c.unfold_numbers(recursive=True)

        return self

    def unfold_style_attribute(self, recursive=False):
        attr = self.attr
        if "style" in attr:
            attr["style"] = style_parse(attr["style"])

        if recursive:
            for c in self.__children:
                c.unfold_style_attribute(True)

        return self

    def unfolded_d_attribute(self, subpath_please=False):
        assert "d" in self
        if subpath_please:
            self["d"] = parse_subpath(self["d"], accept_paths=True)
            assert isinstance(self["d"], Subpath)

        else:
            self["d"] = parse_path(self["d"])
        return self['d']

    def unfold_d_attribute(self, recursive=False, subpath_please=False):
        if "d" in self and (isinstance(self["d"], str) or isinstance(self["d"], list)):
            # assert False
            self['d'] = self.unfolded_d_attribute(subpath_please=subpath_please)

        if recursive:
            for c in self.__children:
                c.unfold_d_attribute(True)

        return self

    def unfold_d_attributes(self, subpath_please=False):
        assert self.type == 'svg'
        self.unfold_d_attribute(recursive=True, subpath_please=subpath_please)
        return self

    def fold_d_attribute(self, decimals=None, recursive=False):
        if "d" in self:
            if isinstance(self["d"], str):
                assert decimals is None
                return
            if isinstance(self["d"], list):
                self["d"] = parse_path(self["d"])
            assert isinstance(self["d"], Path) or isinstance(self["d"], Subpath)
            self["d"] = self["d"].d(decimals=decimals)
            assert len(self) == 0

        else:
            assert recursive
            for c in self:
                c.fold_d_attribute(decimals=decimals, recursive=True)

        return self

    def unfold_points(self, recursive=False):
        if "points" in self:
            assert self.type in ["polyline", "polygon"]
            self["points"] = unfold_attribute_for_use_of_attributes_match("points", self["points"])

        if recursive:
            for c in self.__children:
                c.unfold_points(True)

        return self

    def unfold_viewbox(self, recursive=False):
        if "viewbox" in self and isinstance(self["viewbox"], str):
            self["viewbox"] = [int_else_float(x) for x in self["viewbox"].split()]

        if recursive:
            for c in self.__children:
                c.unfold_viewbox(True)

        return self  # recently changed from returning the viewbox...

    def unfolded_viewbox(self):  # previously named viewbox_as_list
        assert 'viewbox' in self
        vb = self['viewbox']
        if isinstance(vb, list):
            return vb
        assert isinstance(vb, str)
        return [int_else_float(x) for x in vb.split()]

    def unfold_transform_attribute(self, recursive=False):
        if 'transform' in self and isinstance(self['transform'], str):
            self['transform'] = parse_transform(self['transform'])

        if recursive:
            for c in self.__children:
                c.unfold_transform_attribute(True)

    def fold_transform_attribute(self, recursive=False):
        if 'transform' in self and not isinstance(self['transform'], str):
            self['transform'] = generate_transform(self['transform'])

        if recursive:
            for c in self.__children:
                c.fold_transform_attribute(True)

    ##############
    # unfold all #
    ##############

    def unfold_all(self, recursive, msg=True):
        if msg:
            # print('start unfold_all...')
            stopwatch("unfold_all: double_unfold_css")
        self.double_unfold_css(True)
        if msg:
            stopwatch()
            stopwatch("unfold_all: unfold_style_attribute")
        self.unfold_style_attribute(True)
        if msg:
            stopwatch()
            stopwatch("unfold_all: unfold_transform_attribute")
        self.unfold_transform_attribute(True)
        if msg:
            stopwatch()
            stopwatch("unfold_all: unfold_d_attribute")
        self.unfold_d_attribute(True)
        if msg:
            stopwatch()
            stopwatch("unfold_all: unfold_viewbox")
        self.unfold_viewbox(True)
        if msg:
            stopwatch()

    def __eq__(self, other, recursive=True):
        assert isinstance(other, lUnAtIc)
        assert self is not other  # (expensive comparison)

        if self.type != other.type:
            return False

        if set(self()) != set(other()):
            return False

        self.unfold_all(recursive=False)
        other.unfold_all(recursive=False)

        for key in self.attr:
            try:
                if self[key] != other[key]:
                    return False

            except ValueError:
                if any(x is True for x in (self[key] != other[key])):
                    return False

        for key in other.attr:
            assert key in self

        if recursive:
            if len(self) != len(other):
                return False

            for index, c in enumerate(self):
                if c != other[index]:
                    return False

        return True

    #########################
    # svg transform editing #
    #########################

    def transform(self, *args):
        assert self.type in TRANSFORMABLE
        uf = parse_transform(*args)
        if not np.array_equal(uf, np.identity(3)):
            tf = (
                parse_transform(self["transform"])
                if "transform" in self
                else np.identity(3)
            )
            self["transform"] = uf.dot(tf)
        return self

    def translate(self, *args):
        return self.transform(parse_transform("translate", *args))

    def scale(self, sx, sy=None):
        return self.transform(parse_transform("scale", sx, sy))

    def rotate(self, angle):
        return self.transform(parse_transform("rotate", angle))

    def compound_transformation(self):
        t = parse_transform(self.get("transform", ""))

        if self.has_parent():
            t = self.parent.compound_transformation().dot(t)

        return t

    def compound_transformation_cum_width(self):
        t = self.compound_transformation()
        r = self.root()
        if "width" in r and "viewbox" in r:
            r.unfold_viewbox()
            t.scale_by(r["width"] / r["viewbox"][2])

        else:
            print("warning: width or viewbox missing compound_transformation_cum_width")

        return t

    def left_compose_transform(self, *args):
        assert self.type != "defs"
        assert self.type != "style"
        assert self.type != "svg"

        multiplier = list(args) if len(args) > 1 else args[0]
        existing = self.pop("transform", [])
        
        if isinstance(multiplier, np.ndarray) or isinstance(existing, np.ndarray):
            multiplier = parse_transform(multiplier)
            existing = parse_transform(existing)
            new_transform = multiplier.dot(existing)

        else:
            assert isinstance(multiplier, list) or isinstance(multiplier, str)
            assert isinstance(existing, list) or isinstance(existing, str)
            new_transform = compound_translations(multiplier, existing)

        if not np.array_equal(parse_transform(new_transform), np.identity(3)):
            self["transform"] = new_transform

        return self

    def custom_x_y_to_x_y_transform(self, x_y_map):
        if self.type in GEOMETRIC_ELEMENTS and self.type != "path":
            assert False

        if self.type in RENDERED_ELEMENTS and self.type != "path":
            assert False

        if self.type == "path":
            self.unfold_d_attribute()
            self["d"] = custom_x_y_to_x_y_transform(self["d"], x_y_map)

        for c in self:
            c.custom_x_y_to_x_y_transform(x_y_map)

        return self

    def x_cut_at(self, x_val):
        if self.type in RENDERED_ELEMENTS and self.type != "path":
            assert False

        assert "transform" not in self

        if isinstance(x_val, Real):
            x_val = [x_val]

        assert isinstance(x_val, list) and all(isinstance(x, Real) for x in x_val)

        if self.type == "path":
            self.unfold_d_attribute()
            for x in x_val:
                self["d"] = x_val_cut(self["d"], x)

        for c in self:
            c.x_cut_at(x_val)

        return self

    def converted_to_bezier(self):
        if self.type == "path":
            self.unfold_d_attribute()
            self["d"] = self["d"].converted_to_bezier()

        for c in self:
            c.converted_to_bezier()

        return self

    ######################
    # bounding box stuff #
    ######################

    def path_equivalent(self, make_a_copy=False):
        assert self.type in GEOMETRIC_ELEMENTS

        self.unfold_numbers()

        if self.type == "path":
            assert "d" in self
            current = self["d"]

            if isinstance(current, str) or isinstance(current, list):
                return parse_subpath(current, accept_paths=True)

            assert isinstance(current, Path) or isinstance(current, Subpath)

            return current.cloned() if make_a_copy else current

        if self.type == "rect":
            return parse_subpath(
                "M", self["x"], self["y"],
                "h", self["width"],
                "v", self["height"],
                "H", self["x"],
                "Z",
            )

        if self.type == "circle":
            o = Point(self["cx"], self["cy"])
            e = Point(self["r"], 0)
            s = Point(0, self["r"])
            R = e + s
            return parse_subpath(
                "M", o + e, "A", R, 0, 0, 1, o + s,
                "A", R, 0, 0, 1, o - e,
                "A", R, 0, 0, 1, o - s,
                "A", R, 0, 0, 1, o + e,
                "Z",
            )

        if self.type == "ellipse":
            o = Point(self["cx"], self["cy"])
            e = Point(self["rx"], 0)
            s = Point(0, self["ry"])
            R = e + s
            return parse_subpath(
                "M", o + e,
                "A", R, 0, 0, 1, o + s,
                "A", R, 0, 0, 1, o - e,
                "A", R, 0, 0, 1, o - s,
                "A", R, 0, 0, 1, o + e,
                "Z",
            )

        if self.type == "polyline" or self.type == "polygon":
            if isinstance(self["points"], str):
                values = [float(t) for t in self["points"].replace(",", " ").split()]

            elif isinstance(self["points"], list):
                values = self["points"]

            else:
                assert False

            if self.type == "polygon":
                values.append('Z')

            return parse_subpath("M", *values)

        if self.type == "line":
            return parse_subpath("M", self["x1"], self["y1"], self["x2"], self["y2"])

        assert False

    def __bbox_tree(
        self,
        tf,
        with_strokes,
        default_style_without_tagging_classes,
        tagging_classes,
        is_root_call,
        include_display_none,
        include_visibility_hidden,
        matching_type_string,
        matching_attrs,
    ):
        ze_box = BoundingBoxNode(
            self, default_style_without_tagging_classes, tagging_classes
        )

        if self.type in INTANGIBLE_ELEMENTS:
            assert not is_root_call
            return ze_box

        if (
            not is_root_call
            and not include_display_none
            and self.is_display_none_heuristic()
        ):
            return ze_box

        if (
            not is_root_call
            and not include_visibility_hidden
            and self.is_visibility_hidden_heuristic()
        ):
            return ze_box

        if "mask" in self:
            print("~~~ WARNING ~~~: skipping masked element in bbox computation")
            return ze_box

        if "clip-path" in self:
            # right now we ignore, but what we should really do is intersect our bbox with the
            # clip-path's bbox, though that's still inexact... to be exact you would have to do a
            # whole thing where each child clips itself, being aware of its clip-path ancestor (s)...
            print("~~~ WARNING ~~~: skipping clipped element in bbox computation")
            return ze_box

        if self.type == "svg":
            # I think it's 'better form to put transform in the style attribute of an svg element:
            assert "transform" not in self
            m = None
            # while we're at it:
            assert is_root_call

        else:
            m = self.style_aware_attribute("transform")

        if m is not None:
            tf = tf.dot(parse_transform(m))

        matches = True
        if matching_type_string or len(matching_attrs) > 0:
            matches = self.__matches(matching_type_string, matching_attrs)

        if matches and self.type in GEOMETRIC_ELEMENTS:
            stroke_width = None
            if with_strokes:
                stroke = self.style_aware_property("stroke", allow_attribute=True)
                if stroke not in ["none", "auto"]:
                    stroke_width = self.style_aware_property(
                        "stroke-width", allow_attribute=True
                    )
                    if stroke_width is not None:
                        if stroke is None:
                            print(
                                "mild warning: __bbox_tree using a stroke-width whose stroke it couldn't find",
                                self.get("class", "(unknown class)"),
                            )
                        stroke_width = float(stroke_width)
                        if stroke_width == 0:
                            stroke_width = None
            path = self.path_equivalent()
            if stroke_width is not None:
                path = path.stroke(stroke_width)
            if not is_identity_matrix(tf):
                path = path.transformed(tf)
            ze_box.init(*path.bbox())

        elif matches and self.type == "use":
            if "xlink:href" in self:
                assert "href" not in self
                ze_id = self["xlink:href"]

            elif "href" in self:
                assert False  # trying to get rid at ingestion level

            else:
                assert False

            assert ze_id.startswith("#")
            assert len(self) == 0

            x = float(self["x"])
            y = float(self["y"])

            if x or y:
                tf = tf.dot(parse_transform("translate", x, y))

            alouah = self.root().find_unique(None, {"id": ze_id[1:]})
            ze_box.init(
                alouah.__bbox_tree(
                    tf,
                    with_strokes,
                    default_style_without_tagging_classes,
                    tagging_classes,
                    False,
                    include_display_none,
                    include_visibility_hidden,
                    None,
                    {},
                )
            )

        elif (matches and self.type == "text" and any(x != " " for x in self.text)) or (
            matches and self.type == "textPath" and any(x != " " for x in self.text)
        ):
            ze_box.contains_nonempty_text_element = True

        for c in self:
            child_tree = c.__bbox_tree(
                tf,
                with_strokes,
                default_style_without_tagging_classes,
                tagging_classes,
                False,
                include_display_none,
                include_visibility_hidden,
                matching_type_string if not matches else None,
                matching_attrs if not matches else {},
            )
            ze_box.ingest_nonempty(child_tree)

        assert isinstance(ze_box, BoundingBoxNode)
        return ze_box

    def bbox(
        self,
        matching=None,
        with_strokes=False,
        box_style=None,
        include_display_none=True,
        include_visibility_hidden=True,
    ):
        type_string, attrs = None, {}

        if isinstance(matching, str):
            type_string, attrs = _parse_type_string(matching, {}, for_matching=True)

        elif isinstance(matching, list) or isinstance(matching, tuple):
            assert len(matching) == 2
            type_string, attrs = _parse_type_string(*matching, for_matching=True)

        elif isinstance(matching, dict):
            attrs = matching

        else:
            assert matching is None

        assert matching_type_args(type_string, attrs)

        default_style = style_parse(
            """
            fill:none;
            stroke:#0f08;
            stroke-width:4;
        """
        )

        assert all(isinstance(x, Css_Declaration) for key, x in default_style.items())

        if box_style is not None:
            if isinstance(box_style, str):
                box_style = style_parse(box_style)
            assert all(isinstance(x, Css_Declaration) for key, x in box_style.items())
            default_style.update(box_style)

        root_box = self.__bbox_tree(
            np.identity(3),
            with_strokes=with_strokes,
            default_style_without_tagging_classes=default_style,
            tagging_classes=[],
            is_root_call=True,
            include_display_none=include_display_none,
            include_visibility_hidden=include_visibility_hidden,
            matching_type_string=type_string,
            matching_attrs=attrs,
        )

        return root_box

    def add_current_bbox(
        self,
        matching=None,
        with_strokes=False,
        box_style=None,
        include_display_none=False,
        include_visibility_hidden=True,
    ):
        if box_style is None:
            box_style = "fill:none;stroke:#0f08;stroke-width:1"

        root = self.bbox(
            matching,
            with_strokes,
            box_style,
            include_display_none,
            include_visibility_hidden,
        )

        self.append(root.render(recursive=False))

        return self

    def add_current_bboxes(
        self,
        matching=None,
        with_strokes=False,
        box_style=None,
        include_display_none=False,
        include_visibility_hidden=True,
    ):
        if box_style is None:
            box_style = "fill:none;stroke:#0f08;stroke-width:1"

        root = self.bbox(
            matching,
            with_strokes,
            box_style,
            include_display_none,
            include_visibility_hidden,
        )

        self.append(root.render(recursive=True))

        return self

    def viewbox_as_bbox(self):
        assert "viewbox" in self
        return BoundingBox().init_by_viewbox(self["viewbox"])

    def cull_by_bbox(
        self,
        matching=None,
        with_strokes=False,
        include_display_none=False,
        include_visibility_hidden=False,
    ):
        viewbox_bbox = self.viewbox_as_bbox()

        box_style = style_parse(
            """fill:none;stroke:#0f08;stroke-width:4;"""
        )  # ya never know... for debugging?
        root = self.__bbox_tree(
            np.identity(3),
            with_strokes,
            box_style,
            [],
            is_root_call=True,
            include_display_none=include_display_none,
            include_visibility_hidden=include_visibility_hidden,
        )

        last_detached = None
        for q in root.subtree_iterator():
            if q.node.has_ancestor(last_detached) or q.node.has_elder_of_type("defs"):
                continue

            if q.contains_nonempty_text_element:
                continue

            if q.is_nonempty() and q.doesnt_overlap_with(viewbox_bbox):
                print("CUTTING SOME SHIT OUT")
                q.node.detach()
                last_detached = q.node

        return self

    def center_about_bbox(self, where=None, list_form=True):
        assert "transform" not in self
        self["transform"] = self.bbox().centering_translation(
            where=where, list_form=list_form
        )
        return self

    def vertically_center(self, list_form=False):
        return self.center_about_bbox(where="vertical", list_form=list_form)

    def horizontally_center(self, list_form=False):
        return self.center_about_bbox(where="horizontal", list_form=list_form)

    def lower_left_center(self, list_form=False):
        return self.center_about_bbox(where="ll", list_form=False)

    #################
    # viewbox stuff #
    #################

    def scale_viewbox(self, s):
        self.unfold_viewbox()
        self["viewbox"] = [val * s for val in self["viewbox"]]
        return self

    def center_about_0_0(self):
        assert "viewbox" in self
        vb = self.unfold_viewbox()["viewbox"]
        x = vb[0]
        y = vb[1]
        w = vb[2]
        h = vb[3]
        nx = -w / 2
        ny = -h / 2
        transform = ["translate", nx - x, ny - y]
        for c in self:
            if c.type not in ["style", "defs"]:
                c.left_compose_transform(transform)
        self["viewbox"] = [-w / 2, -h / 2, w, h]
        return self

    def computed_viewbox(
        self,
        matching=None,
        with_strokes=False,
        include_display_none=False,
        include_visibility_hidden=True,
    ):
        box = self.bbox(
            matching,
            with_strokes,
            box_style=None,
            include_display_none=include_display_none,
            include_visibility_hidden=include_visibility_hidden,
        )
        assert box.is_nonempty()
        return box.xmin, box.ymin, box.xmax - box.xmin, box.ymax - box.ymin

    def reset_viewbox_from_element(self, el):
        X, Y, W, H = el.computed_viewbox()
        self["viewbox"] = [X, Y, W, H]
        return self

    def reset_viewbox(
        self,
        *args,
        width_relative=True,
        height_relative=False,
        decimals=None,
        with_strokes=False,
        include_display_none=False,
        include_visibility_hidden=True,
        matching=None,
    ):
        def encodes_float(thing):
            try:
                float(thing)
                return True

            except ValueError:
                return False

        def is_star_number(thing):
            return (
                isinstance(thing, str)
                and thing.startswith("*")
                and encodes_float(thing[1:])
            )

        def triage(args):
            star_factors = []
            margins = []

            # first, sorting the arguments
            for c in args:
                if is_star_number(c):
                    star_factors.append(float(c[1:]))

                elif isinstance(c, Real):
                    margins.append(c)

                else:
                    assert False

            # numerical margins, star_factors
            for a in [margins, star_factors]:
                assert len(a) in [0, 1, 2, 4]

                if len(a) == 0:
                    a.append(0)

                if len(a) == 1:
                    a.append(a[0])

                if len(a) == 2:
                    a.append(a[0])
                    a.append(a[1])

            # and...
            return TRBLObject(*margins), TRBLObject(*star_factors)

        assert self.type == "svg"

        margins, factors = triage(args)

        assert isinstance(margins, TRBLObject)
        assert isinstance(factors, TRBLObject)
        assert all(0 <= x <= 1 for x in factors)
        assert all(0 <= x for x in margins)

        x, y, width, height = self.computed_viewbox(
            matching=matching,
            with_strokes=with_strokes,
            include_display_none=include_display_none,
            include_visibility_hidden=include_visibility_hidden,
        )

        sx = width if not height_relative else height
        sy = width if width_relative and not height_relative else height
        final_margins = margins + factors.scaled_by(sx, sy)

        assert all(0 <= x for x in final_margins)

        X = x - final_margins.left
        Y = y - final_margins.top
        W = width + final_margins.left + final_margins.right
        H = height + final_margins.top + final_margins.bottom

        self["viewbox"] = [X, Y, W, H]

        if decimals is not None:
            self["viewbox"] = " ".join(
                ze_2f(t, decimals=decimals) for t in self["viewbox"]
            )

        return self

    def set_viewbox_by_bbox(self, bbox, decimals=None, margins=None):
        assert isinstance(bbox, BoundingBox)
        assert self.type == "svg"
        # final_bbox = bbox.expanded_by_trbl(margins)
        final_bbox = bbox if margins is None else bbox.expanded_by_trbl(margins)
        self["viewbox"] = (
            final_bbox.viewbox_list()
            if decimals is None
            else final_bbox.viewbox_string(decimals=decimals)
        )
        return self

    def reset_viewbox_x_while_preserving_far_side(self, new_x):
        self.unfold_viewbox()
        cur_far_side = self["viewbox"][0] + self["viewbox"][2]
        assert new_x < cur_far_side
        self["viewbox"][0] = new_x
        self["viewbox"][2] = cur_far_side - new_x
        return self

    def enlarge_viewbox_to_contain(self, *args, decimals=None):
        if len(args) == 1:
            assert isinstance(args[0], Point)
            x, y = args[0].x, args[0].y

        elif len(args) == 2:
            x, y = args[0], args[1]

        else:
            assert False

        assert isinstance(x, Real)
        assert isinstance(y, Real)

        self["viewbox"] = (
            BoundingBox("xywidthheight", self.unfold_viewbox()["viewbox"])
            .agglomerate(Point(x, y))
            .viewbox_string(decimals=decimals)
        )

        return self

    def viewbox_path(self):
        assert self.type == "svg" and "viewbox" in self
        bbox = BoundingBox("xywidthheight", self.unfold_viewbox()["viewbox"])
        return bbox.d()

    def viewbox_rect(self, classname=None):
        assert self.type == "svg"
        assert "viewbox" in self

        vb = self.unfold_viewbox()["viewbox"]

        assert vb is not None

        rect = lUnAtIc(f"rect#viewbox.x={vb[0]}.y={vb[1]}.width={vb[2]}.height={vb[3]}")
        if classname is not None:
            rect.add_class(classname)

        return rect

    def add_current_viewbox(self, classname="viewbox"):
        self.append(self.viewbox_rect(classname))
        m = min(self["viewbox"][2], self["viewbox"][3])
        if classname == "viewbox" and self.find_style(".viewbox") is None:
            self.add_a_css_style(
                ".viewbox", f"stroke:#ff000088;stroke-width:{m/100};fill:none;"
            )
        return self

    def add_viewbox_rect(self, classname="viewbox"):
        return self.add_current_viewbox(classname)

    def add_to_viewbox_height(self, how_much):
        assert self.type == "svg"
        assert "viewbox" in self

        vb = self.unfold_viewbox()["viewbox"]
        vb[3] += how_much
        assert vb[3] >= 0
        return self

    def add_to_viewbox_width(self, how_much):
        assert self.type == "svg"
        assert "viewbox" in self

        vb = self.unfold_viewbox()["viewbox"]
        vb[2] += how_much
        assert vb[2] >= 0
        return self

    def translate_viewbox(self, x, y=None):
        assert self.type == "svg"
        assert "viewbox" in self

        vb = self.unfold_viewbox()["viewbox"]

        if y is None:
            if isinstance(x, Point):
                x = x.x
                y = x.y

            elif isinstance(x, Real):
                y = 0

            else:
                assert False

        vb[0] += x
        vb[1] += y
        return self

    def append_red_dot(self, *args, r=2):
        p = extract_point(*args)
        self.append(f"circle.fill:red.stroke:none.{p.c_2f()}.r={r}")

    def prepend_background_rect(self, color=None, classname=None):
        assert self.type == "svg"
        assert "viewbox" in self

        self.unfold_viewbox()

        attrs = {
            "width": "100%",
            "height": "100%",
            "x": self["viewbox"][0],
            "y": self["viewbox"][1],
        }

        if classname is not None:
            attrs["class"] = classname

        if color is not None:
            attrs.update({"fill": color})

        elif classname is None:
            attrs.update({"fill": random_color()})

        newguy = lUnAtIc("rect", attrs)

        inserted = False
        c = None
        for c in self:
            if c.type not in ["style", "defs", "sodipodi:namedview"]:
                self.insert_before(newguy, c)
                inserted = True
                break

        if not inserted:
            self.append(newguy)

        return self

    def get_height_if_width_is(self, width):
        assert self.type == "svg"
        vb = self.unfold_viewbox()["viewbox"]
        assert vb[2] > 0.01
        assert width > 0
        return width * (vb[3] / vb[2])

    def set_width_from_viewbox(self, multiplier=1):
        assert self.type == "svg"
        assert "viewbox" in self
        vb = self.unfold_viewbox()["viewbox"]
        self["width"] = vb[2] * multiplier
        return self

    def set_style_width_from_viewbox(self):
        assert self.type == "svg"
        assert "viewbox" in self
        vb = self.unfold_viewbox()["viewbox"]
        self.update_style_attribute({"width": str(vb[2]) + "px"})
        return self

    def set_height_from_width(self, decimals=None):
        assert self.type == "svg"
        assert "viewbox" in self
        assert "width" in self

        self.unfold_viewbox()
        self["width"] = float(self["width"])
        self["height"] = self["width"] * self["viewbox"][3] / self["viewbox"][2]

        if decimals is not None:
            if self["width"] < 0.1:
                print("~~~ WARNING ~~~ very small width")
            self["width"] = ze_2f(self["width"], decimals)
            self["height"] = ze_2f(self["height"], decimals)

        return self

    def scale_width_from_viewbox(self, scale):
        assert self.type == "svg"
        assert "viewbox" in self

        self.unfold_viewbox()
        self["width"] = self["viewbox"][2] * scale
        return self

    def set_width_height_from_viewbox(self, decimals=None, scale=1):
        assert self.type == "svg"
        assert "viewbox" in self

        self.unfold_viewbox()
        self["width"] = self["viewbox"][2] * scale
        self["height"] = self["viewbox"][3] * scale

        if decimals is not None:
            self["width"] = ze_2f(self["width"], decimals)
            self["height"] = ze_2f(self["height"], decimals)

        return self

    def format_viewbox(self, decimals=2):
        assert decimals >= 0
        self.unfold_viewbox()
        self["viewbox"] = " ".join(
            [ze_2f(z, decimals=decimals) for z in self["viewbox"]]
        )
        return self

    def viewbox_center(self):
        assert "viewbox" in self
        vb = self.unfolded_viewbox()
        return Point(vb[0] + 0.5 * vb[2], vb[1] + 0.5 * vb[3])

    ##############
    # flattening #
    ##############

    def convert_to_polygon(self):
        assert self.type == "rect"
        assert "points" not in self
        self.type = "polygon"
        x = self.pop_float("x")
        y = self.pop_float("y")
        w = self.pop_float("width")
        h = self.pop_float("height")
        self["points"] = [
            Point(x, y),
            Point(x + w, y),
            Point(x + w, y + h),
            Point(x, y + h),
        ]
        return self

    def convert_to_path(self):
        if self.type == "path":
            pass

        elif self.type == "rect":
            self.type = "path"
            assert "d" not in self
            self["d"] = parse_path(
                "M", float(self["x"]), float(self["y"]),
                "h", float(self["width"]),
                "v", float(self["height"]),
                "h", -float(self["width"]),
                "z",
            )
            self.pop_all("x", "y", "width", "height")

        elif self.type == "circle":
            o = float(self["cx"]) + 1j * float(self["cy"])
            r = float(self["r"])
            self.type = "path"
            assert "d" not in self
            self["d"] = parse_path(
                "M", o + 1j * r,
                "A", r, r, 0, 0, 0, o + r,
                "A", r, r, 0, 0, 0, o - 1j * r,
                "A", r, r, 0, 0, 0, o - r,
                "A", r, r, 0, 0, 0, o + 1j * r,
                "z",
            )
            self.try_pop_all("cx", "cy", "r")

        elif self.type == "ellipse":
            self.type = "path"
            o = float(self["cx"]) + 1j * float(self["cy"])
            rx = float(self["rx"])
            ry = float(self["ry"])
            assert rx != 0
            assert ry != 0
            assert "d" not in self
            self["d"] = parse_path(
                "M", o + 1j * ry,
                "A", rx, ry, 0, 0, 0, o + rx,
                "A", rx, ry, 0, 0, 0, o - 1j * ry,
                "A", rx, ry, 0, 0, 0, o - rx,
                "A", rx, ry, 0, 0, 0, o + 1j * ry,
                "z",
            )
            self.pop_all("cx", "cy", "rx", "ry")

        else:
            assert False

    def contains_rendered_element(self):
        return self.type != "defs" and (
            self.type in RENDERED_ELEMENTS
            or any(c.contains_rendered_element() for c in self),
        )

    # used only one place and misleadingly named:
    def worth_flattening(self):
        if "transform" in self and self.type in GEOMETRIC_ELEMENTS:
            return True

        return any(c.worth_flattening() for c in self)

    def swallow_transform(self, tf, keep_objects=True):
        assert "transform" not in self
        tf = parse_transform(tf)

        if np.array_equal(tf, np.identity(3)):
            return self

        if "mask" in self:
            print(f"{self.type} (id={self.get('id', '...')}) cannot swallow transform because it has mask!")
            self["transform"] = tf
            return self

        if (
            not keep_objects
            and not is_scaling_and_translation(tf)
            and self.type in ["rect", "circle", "ellipse"]
        ):
            if self.type in ["circle", "ellipse"]:
                self.convert_to_path()

            elif self.type == "rect":
                self.convert_to_polygon()

            else:
                assert False

        if self.type == "rect":
            if not is_scaling_and_translation(tf):
                self["transform"] = tf
                return self
            x = self.get_float("x", 0)
            y = self.get_float("y", 0)
            w = self.get_float("width")
            h = self.get_float("height")
            assert w > 0
            assert h > 0
            tl = Point(x, y)
            br = Point(x + w, y + h)
            tl_prime = tl.transformed(tf)
            br_prime = br.transformed(tf)
            self["x"] = tl_prime.x
            self["y"] = tl_prime.y
            self["width"] = br_prime.x - tl_prime.x
            self["height"] = br_prime.y - tl_prime.y
            assert self["width"] > 0
            assert self["height"] > 0

        elif self.type == "circle":
            cx = self.get_float("cx", 0)
            cy = self.get_float("cy", 0)
            r = self.get_float("r")
            core = tf[0:2, 0:2]
            A = np.linalg.inv(core)
            Sym = A.T.dot(A)
            v, w = np.linalg.eig(Sym)
            vec1 = w[:, 0]
            rx = r * sqrt(1 / v[0])
            ry = r * sqrt(1 / v[1])
            angle = degree_angle(vec1[0], vec1[1])
            if rx != ry:
                self.type = "ellipse"  # man!
                self.pop("r")
                self["rx"] = rx
                self["ry"] = ry
                new_c = Point(cx, cy).transformed(tf).rotated(-angle)
                if angle % 360 != 0:
                    assert "transform" not in self
                    self["transform"] = ["rotate", angle]
                    assert len(self) == 0
                self["cx"] = new_c.x
                self["cy"] = new_c.y

            else:
                self["r"] = rx
                new_c = Point(cx, cy).transformed(tf)
                self["cx"] = new_c.x
                self["cy"] = new_c.y

        elif self.type == "ellipse":
            cx = self.get_float("cx")
            cy = self.get_float("cy")
            o_rx = self.get_float("rx")
            o_ry = self.get_float("ry")
            core = tf[0:2, 0:2]
            D = np.diag([1 / o_rx ** 2, 1 / o_ry ** 2])
            A = np.linalg.inv(core)
            Sym = A.T.dot(D).dot(A)
            v, w = np.linalg.eig(Sym)
            vec1 = w[:, 0]
            rx = sqrt(1 / v[0])
            ry = sqrt(1 / v[1])
            angle = degree_angle(vec1[0], vec1[1])
            if rx != ry:
                self["rx"] = rx
                self["ry"] = ry
                new_c = Point(cx, cy).transformed(tf).rotated(-angle)
                if angle % 360 != 0:
                    assert "transform" not in self
                    self["transform"] = ["rotate", angle]
                    assert len(self) == 0
                self["cx"] = new_c.x
                self["cy"] = new_c.y

            else:
                assert rx > 0
                self.type = "circle"
                self.pop("rx")
                self.pop("ry")
                self["r"] = rx
                new_c = Point(cx, cy).transformed(tf)
                self["cx"] = new_c.x
                self["cy"] = new_c.y

        elif self.type == "path":
            self.unfold_d_attribute()
            self["d"] = self["d"].transform(tf)
            assert len(self) == 0

        elif self.type == "line":
            s = Point(self["x1"], self["y1"])
            e = Point(self["x2"], self["y2"])
            new_s = s.transformed(tf)
            new_e = e.transformed(tf)
            self["x1"], self["y1"] = new_s.x, new_s.y
            self["x2"], self["y2"] = new_e.x, new_e.y

        elif self.type == "polyline" or self.type == "polygon":
            self.unfold_points()
            points = self["points"]
            assert len(points) >= 2 if self.type == "polyline" else 3
            new_points = [p.transformed(tf) for p in points]
            self["points"] = new_points

        elif self.type in RENDERED_ELEMENTS:
            assert self.type in ["use", "text", "textPath"]
            assert "transform" not in self  # jes' double-checking...
            if is_translation(tf):
                self["x"] = self.get_float("x", 0) + tf[0, 2]
                self["y"] = self.get_float("y", 0) + tf[1, 2]

            else:
                self["transform"] = tf

        return self

    def flatten(self, recursive=True, total=False, keep_objects=True):
        original_transform = self.pop("transform", "matrix(1 0 0 1 0 0)")
        tf = parse_transform(original_transform)

        if not self.contains_rendered_element() or "flattenstop" in self.get("class", []):
            if not np.array_equal(tf, np.identity(3)):
                self["transform"] = original_transform
            return self

        self.swallow_transform(original_transform, keep_objects=keep_objects)

        assert "transform" not in self or not total
        survivor_tf = self.get("transform", "matrix(1 0 0 1 0 0)")
        survivor_tf = parse_transform(survivor_tf)
        tf_to_push = np.linalg.inv(survivor_tf).dot(tf)  # need survivor_tf.dot(tf_to_push) == tf

        for c in self:
            if c.type not in ["style", "defs"]:
                c.left_compose_transform(tf_to_push)

        if recursive:
            for c in self:
                c.flatten(recursive=True, total=total, keep_objects=keep_objects)

        return self.cleanup_gs_with_no_attributes()

    ########
    # html #
    ########

    def has_elder_of_type(self, t):
        for c in self.elders():
            if c.type == t:
                return True
        return False

    def style_attribute_matches(self, d):
        assert False  # (not used much? haha)

        if "style" not in self:
            return len(d) == 0  # fancy!

        self.unfold_style_attribute()

        for k, thing in d.items():
            if isinstance(thing, str):
                acceptable = [thing, thing + "!important"]

            else:
                acceptable = [thing]

            if self["style"].get(k) not in acceptable:
                return False

        return True
    
    def style_attribute_equals(self, d):
        assert isinstance(d, dict)

        if "style" not in self:
            return len(d) == 0

        self.unfold_style_attribute()
        assert isinstance(self['style'], dict)

        if any(k not in self['style'] for k in d):
            return False
        
        if any(k not in d for k in self['style']):
            return False
        
        return all(self['style'][k] == d[k] for k in d)
    
    def style_attribute_equals_nonempty_style_attribute_of(self, guy):
        assert isinstance(guy, lUnAtIc)
        
        if 'style' not in self or 'style' not in guy:
            return False
        
        guy.unfold_style_attribute()

        return self.style_attribute_equals(guy['style'])

    ############
    # printing #
    ############

    def externalify(self):
        assert self.type == "svg"
        self["xmlns"] = "http://www.w3.org/2000/svg"
        self["xmlns:xlink"] = "http://www.w3.org/1999/xlink"
        return self

    def inkscapify(self):
        assert self.type == "svg"
        self["xmlns:inkscape"] = "http://www.inkscape.org/namespaces/inkscape"
        self["xmlns:sodipodi"] = "http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
        self.prepend(
            "sodipodi:namedview",
            {
                "inkscape:window-width": 1415,
                "inkscape:window-height": 855,
                "inkscape:zoom": 4,
            },
        )
        return self

    def pretty_up(self):
        remove_gratuitous_adjacencies(self)
        return self

    def compactify(self):
        remove_gratuitous_spaces(self)
        return self

    def minify_mathjax(self):
        if self.parent is None:
            self.update_a_css_style(".MathJax_SVG", MathJax_SVG_EXPECTED_STYLE)
            self.update_a_css_stylename(".MathJax_SVG", "." + MathJax_SVG_REPLACEMENT)
            self.update_a_css_style(
                ".MathJax_SVG_Display", MathJax_SVG_Display_EXPECTED_STYLE
            )
            self.update_a_css_stylename(
                ".MathJax_SVG_Display", "." + MathJax_SVG_Display_REPLACEMENT
            )

        self.pop("xmlns:xlink", None)
        self.pop("role", None)

        if "class" in self and "MathJax_SVG_Display" in self["class"]:
            self["class"] = self["class"].replace(
                "MathJax_SVG_Display", MathJax_SVG_Display_REPLACEMENT
            )

            assert "style" in self
            self.unfold_style_attribute()
            d = self["style"]

            for key in MathJax_SVG_Display_EXPECTED_STYLE:
                if (
                    key not in d
                    or d[key].val_str() != MathJax_SVG_Display_EXPECTED_STYLE[key]
                ):
                    assert False

                else:
                    d.pop(key)

            assert len(self["style"]) == 0

        if "class" in self and "MathJax_SVG" in self["class"]:
            self["class"] = self["class"].replace(
                "MathJax_SVG", MathJax_SVG_REPLACEMENT
            )

            assert "style" in self
            self.unfold_style_attribute()
            d = self["style"]

            for key in MathJax_SVG_EXPECTED_STYLE:
                if key not in d or d[key].val_str() != MathJax_SVG_EXPECTED_STYLE[key]:
                    assert False

                else:
                    d.pop(key)

            assert len(self["style"]) == 0

        if "style" in self and (
            self["style"] == ""
            or (isinstance(self["style"], dict) and len(self["style"]) == 0)
        ):
            self.pop("style")

        for key in ["id", "xlink:href"]:
            if key in self:
                value = self[key]

                if "STIXWEBMAIN" in value:
                    self[key] = value.replace("STIXWEBMAIN", STIXWEBMAIN_REPLACEMENT)

                elif "STIXWEBDOUBLESTRUCK" in value:
                    self[key] = value.replace(
                        "STIXWEBDOUBLESTRUCK", STIXWEBDOUBLESTRUCK_REPLACEMENT
                    )

                elif "STIXWEBVARIANTS" in value:
                    self[key] = value.replace(
                        "STIXWEBVARIANTS", STIXWEBVARIANTS_REPLACEMENT
                    )

        if self.type == "use" and "x" in self and float(self["x"]) == 0:
            self.pop("x")
            assert "x" not in self

        if self.type == "use" and "y" in self and float(self["y"]) == 0:
            self.pop("y")
            assert "y" not in self

        for c in self:
            c.minify_mathjax()

        return self

    def minify(self, also_mathjax=True, also_spaces=True):
        if also_mathjax:
            self.minify_mathjax()

        if also_spaces:
            remove_gratuitous_spaces(self)

        return self

    def get_img_or_image_link(self):
        if self.type == "img":
            return self["src"]

        if self.type == "image":
            assert "href" not in self
            return self["xlink:href"]

        if self.type == "link" and self.get("rel", "") == "icon":
            assert "xlink:href" not in self
            return self["href"]

        return None

    def set_img_or_image_link(self, value):
        assert isinstance(value, str)

        if self.type == "img":
            self["src"] = value

        elif self.type == "image":
            assert "href" not in self
            self["xlink:href"] = value

        elif self.type == "link":
            assert "xlink:href" not in self
            self["href"] = value

        else:
            assert False

        return self
    
    def check_style(self):
        if 'style' not in self:
            return
        if isinstance(self['style'], str):
            return
        assert isinstance(self['style'], dict)
        for key, value in self['style'].items():
            assert isinstance(value, Css_Declaration)

    def attribute_to_string(self, key, decimals, style_decimals):
        assert key in self

        if key == "style" and style_decimals is not None:
            self.unfold_style_attribute()

        value = self[key]

        decimals_plus_one = decimals + 1 if decimals is not None else None

        if isinstance(value, str):
            if key == "style":
                pieces = [x.strip().replace("\n", "") for x in value.split(";")]
                to_return = ";".join(x for x in pieces if x != "")

            else:
                to_return = value

        elif isinstance(value, dict):
            assert key == "style"
            pieces = []
            alphabetically_sorted_keys = sorted(value.keys(), key=lambda x: x.lower())
            for k in alphabetically_sorted_keys:
                css_decl = value[k]
                if not isinstance(css_decl, Css_Declaration):
                    print("")
                    print("offending value:", css_decl)
                    print("")
                assert isinstance(css_decl, Css_Declaration)
                pieces.append(css_decl.__str__(space=False, decimals=style_decimals))
                assert len(pieces[-1]) > 0
            to_return = ";".join(pieces)

        elif key == "style" and style_decimals is not None:
            assert False

        elif isinstance(value, Path) or isinstance(value, Subpath):
            to_return = value.d(decimals=decimals)

        elif isinstance(value, Segment):
            to_return = Subpath(value).d(decimals=decimals)

        elif isinstance(value, np.ndarray) and key == "transform":
            to_return = generate_transform(value, decimals=decimals_plus_one)

        elif isinstance(value, list) and key in ["transform", "base-transform"]:
            to_return = generate_transform(value, decimals=decimals_plus_one)

        elif isinstance(value, list) and key == "viewbox":
            to_return = " ".join([ze_2f(x, decimals_plus_one) for x in value])

        elif isinstance(value, list) and key == "d":
            to_return = generate_path(value, decimals=decimals)

        elif isinstance(value, list) and key == "points":
            if all(isinstance(x, Point) for x in value):
                to_return = " ".join(x._2f(decimals=decimals) for x in value)

            elif all(isinstance(x, tuple) and len(x) == 2 for x in value):
                to_return = " ".join(ze_2f(x[0]) + "," + ze_2f(x[1]) for x in value)

            else:
                assert False

        elif isinstance(value, Real):
            to_return = ze_2f(value, decimals)

        else:
            print("offending key, value:", key, value)
            assert False

        return to_return

    def our_attributes_to_string(self, decimals=5, style_decimals=5, spaces_for_individual_lines_pretty_printing=None):
        def appropriate_quote(string):
            if "'" not in string:
                return "'" + string + "'"
            if '"' not in string:
                return '"' + string + '"'
            assert False

        individual_strings = []

        if "id" in self.attr:
            individual_strings.append("id=" + "'" + self["id"] + "'")

        if "class" in self.attr:
            individual_strings.append("class=" + "'" + self["class"] + "'")

        if "xmlns" in self.attr:
            individual_strings.append("xmlns=" + "'" + self["xmlns"] + "'")

        if "xmlns:xlink" in self.attr:
            individual_strings.append("xmlns:xlink=" + "'" + self["xmlns:xlink"] + "'")

        if "style" in self.attr:
            value_string = self.attribute_to_string("style", decimals, style_decimals)
            individual_strings.append("style=" + appropriate_quote(value_string))
            
        if "transform" in self.attr:
            value_string = self.attribute_to_string("transform", decimals, style_decimals)
            if value_string != "":
                individual_strings.append("transform=" + appropriate_quote(value_string))

        for key in self.attr:
            if (
                key == "id"
                or key == "class"
                or key == "style"
                or key == "xmlns"
                or key == "xmlns:xlink"
                or key == "transform"
            ):
                continue

            if not key.islower():
                print("key:", key)

            assert key != "async"
            assert key.islower()

            key_string = CAMEL_CASE_ATTRIBUTES_DICTIONARY.get(key, key)
            if self.type == "use" and key == "href":
                print(self.attr)
                assert False

            value_string = self.attribute_to_string(key, decimals, style_decimals)
            individual_strings.append(key_string + "=" + appropriate_quote(value_string))

        joiner = (
            "\n" + spaces_for_individual_lines_pretty_printing
        ) if (
            spaces_for_individual_lines_pretty_printing is not None and len(individual_strings) > 3
        ) else (
            " "
        )
        space = " " if len(individual_strings) > 0 else ""

        return space + joiner.join(individual_strings)

    def style_dict_style_format(
        self, style_dict, precision=None, reduce_whitespace=False
    ):
        newline_etc = "\n" + self.__tab_spaces * " " if not reduce_whitespace else ""
        colon = ": " if not reduce_whitespace else ":"
        space = " " if not reduce_whitespace else ""
        newline = "\n" if not reduce_whitespace else ""

        if True:
            newline_etc = " " if not reduce_whitespace else ""
            newline = " " if not reduce_whitespace else ""

        val = ""
        for k in style_dict:
            assert isinstance(style_dict[k], Css_Declaration)
            val += newline_etc + k + colon + style_dict[k].val_str() + ";"
        val += space if len(style_dict) == 0 else newline

        return val

    def css_class_2_string(self, name, precision=None, reduce_whitespace=False):
        # @font-face can appear several times as a key, hence...
        # ...this stuff:
        space = "" if reduce_whitespace else " "

        adjusted_name = name
        if name.startswith("@") and "_nr" in name:
            adjusted_name = re.sub("_nr[0-9]*$", "", name)

        assert "@" not in adjusted_name or "_nr" not in adjusted_name

        if isinstance(self.text[name], str):
            self.text[name] = style_parse(self.text[name])

        return \
            adjusted_name + space + "{" + \
            self.style_dict_style_format(self.text[name], reduce_whitespace=reduce_whitespace) + \
            "}"

    def self_closing(self, formatted_text):
        return (
            self.type in SELF_CLOSING and 
            formatted_text == "" and
            len(self) == 0
        )

    def stdout_open(self, f_or_filename_or_None, parent_inline_repr, reduce_whitespace):
        assert self.repr_buffer is None
        assert self.f is None
        assert self.parent_forces_inline_repr is None

        self.parent_forces_inline_repr = parent_inline_repr
        self.reduce_whitespace = reduce_whitespace

        if f_or_filename_or_None is None:
            self.repr_buffer = ""

        elif isinstance(f_or_filename_or_None, str):
            self.f = open(f_or_filename_or_None, "w")

        else:
            # print(type(f_or_filename_or_None))
            # assert False
            self.f = f_or_filename_or_None

    def stdout(self, string):
        assert (self.repr_buffer is None) != (self.f is None)
        assert isinstance(self.parent_forces_inline_repr, bool)
        assert string is not None

        if self.repr_buffer is not None:
            self.repr_buffer += string

        else:
            self.f.write(string)

    def stdout_close(self, dummy=None):
        assert (self.repr_buffer is None) != (self.f is None)
        assert type(self.parent_forces_inline_repr) == bool
        self.parent_forces_inline_repr = None
        self.reduce_whitespace = None

        if self.repr_buffer is not None:
            to_return = self.repr_buffer
            self.repr_buffer = None

        else:
            to_return = None
            if dummy is not None:
                self.f.close()
            self.f = None

        assert (self.repr_buffer is None) and (self.f is None)
        return to_return

    def inline_repr(self):
        if self.parent_forces_inline_repr is True:
            return True

        if self.reduce_whitespace:
            return True

        if self.type not in ["futuremathjaxspan", "span", "i", "b", "tspan", "text"]:
            return False

        if any(not c.inline_repr() for c in self.__children):
            return False

        if "\n" in self.text:
            return False

        if self.has_class("MathJax"):
            return False

        if self.type == "span" and len(self.text) > 100:
            return False

        return True

    def should_feed_newline_to_putative_first_child(self):
        if len(self.__children) == 0:
            return False

        if self.inline_repr():
            return False

        if type(self.text) == str and self.text != "" and self.text[-1] != " ":
            assert self.text[-1] != "\n"
            return False

        return True

    def should_feed_newline_to_putative_next_sibling(self):
        sib = self.next_sibling()
        if sib is None:
            return False
        if sib.inline_repr() and self.inline_repr():
            return False
        if len(self.tail) > 0 and self.tail[-1] != " ":
            assert self.tail[-1] != "\n"
            return False
        return True

    def closing_tag_glued_to_end_of_last_text_or_tail_field(self):
        if type(self.text) != str:
            return False
        last_field = self.text if len(self) == 0 else self.__children[-1].tail
        if last_field == "" or last_field[-1] != " ":
            return True
        if last_field == " " and len(self) == 0:
            return True
        return False
    
    def is_or_has_ancestor_of_type(self, type):
        node = self
        while node is not None:
            if node.type == type:
                return True
            node = node.parent
        return False

    def __2string(self, depth, decimals, style_decimals):
        def castafiore_text_processor(text):
            if self.is_or_has_ancestor_of_type("pre"):
                return text

            if text == "":
                return ""

            if text == " ":
                if self.should_feed_newline_to_putative_first_child():
                    return "\n" + new_indent

                if heuristic_display_classifier(self) == "inline":
                    return " "

                return ""

            if not self.inline_repr() and text[0] == " ":
                text = "\n" + text[1:]

            if self.reduce_whitespace:
                text = text.replace("\n", " ")
                while "  " in text:
                    text = text.replace("  ", " ")

            else:
                text = text.replace("\n", "\n" + new_indent)

            if (
                text[-1] == " "
                and not self.inline_repr()
                and (
                    self.next_sibling() is None or not self.next_sibling().inline_repr()
                )
            ):
                text = text[:-1]
                if self.should_feed_newline_to_putative_first_child():
                    text += "\n" + new_indent

            return text

        def tournesol_text_processor():
            assert self.type == "style" and isinstance(self.text, dict)

            whole_css = ""

            for target in self.text:
                whole_class = self.css_class_2_string(target, reduce_whitespace=self.reduce_whitespace)

                if not self.reduce_whitespace:
                    whole_css += "\n"
                    whole_class = new_indent + whole_class.replace("\n", "\n" + new_indent)

                whole_css += whole_class

            return whole_css

        def haddock_text_processor(tail):
            if tail == "":
                return ""

            if tail == " ":
                if self.should_feed_newline_to_putative_next_sibling():
                    return "\n" + indent
                if self.next_sibling() is not None:
                    return " "
                return ""

            if not self.inline_repr() and tail[0] == " ":
                tail = "\n" + tail[1:]

            if self.reduce_whitespace:
                tail = tail.replace("\n", " ")
                while "  " in tail:
                    tail = tail.replace("  ", " ")

            else:
                tail = tail.replace("\n", "\n" + indent)

            if (
                tail[-1] == " "
                and (self.parent is None or not self.parent.inline_repr())
                and (
                    self.next_sibling() is None or not self.next_sibling().inline_repr()
                )
            ):
                tail = tail[:-1]
                if self.should_feed_newline_to_putative_next_sibling():
                    tail += "\n" + indent

            return tail

        if self.type == "html" and self.parent is None:
            assert depth == 0
            self.stdout("<!doctype html>\n")

        indent = " " * self.__tab_spaces * depth
        new_indent = " " * self.__tab_spaces * (depth + 1)
        spaces_for_individual_lines_pretty_printing = "     " if depth == 0 and self.type == "svg" and not self.reduce_whitespace else None

        ze_tag = CAMEL_CASE_TYPES_DICTIONARY.get(self.type, self.type)
        ze_attributes = self.our_attributes_to_string(decimals, style_decimals, spaces_for_individual_lines_pretty_printing)
        ze_text = castafiore_text_processor(self.text) if isinstance(self.text, str) else tournesol_text_processor()
        ze_slash = "/" if self.self_closing(ze_text) and self.type not in SELF_CLOSING_SKIP_SLASH else ""

        self.stdout(f"<{ze_tag}{ze_attributes}{ze_slash}>{ze_text}")

        for c in self.__children:
            c.stdout_open(self.f, self.inline_repr(), self.reduce_whitespace)

        for c in self.__children:
            c.__2string(depth + 1, decimals, style_decimals)

        for c in self.__children:
            self.stdout(c.stdout_close())

        if not self.self_closing(ze_text):
            ze_pre_closing_tag_newline_and_indent = (
                ""
            ) if (
                self.inline_repr() or self.closing_tag_glued_to_end_of_last_text_or_tail_field()
            ) else (
                "\n" + indent
            )
            self.stdout(f"{ze_pre_closing_tag_newline_and_indent}</{ze_tag}>")

        self.stdout(haddock_text_processor(self.tail))

    def print_to(
        self,
        filename,
        announce=True,
        decimals=5,
        style_decimals=None,
        pretty_up=False,
        minify=False,
        reduce_whitespace=False,
        bottom_newline=False,
        print_incrementally=False,
    ):
        self._last_used_filename = filename
        self._print_incrementally = print_incrementally

        if pretty_up:
            remove_gratuitous_adjacencies(self)

        if minify:
            self.minify()
            reduce_whitespace = True

        if self._print_incrementally:
            self.stdout_open(filename, self.inline_repr(), reduce_whitespace)
            self.__2string(0, decimals, style_decimals)
            self.stdout_close(filename)

        else:
            string = self.__str__(
                decimals=decimals,
                reduce_whitespace=reduce_whitespace,
                style_decimals=style_decimals,
            )
            with open(filename, "w") as handle:
                handle.write(string)
                handle.close()

        if announce:
            print("")
            print("printed:", filename)

            if bottom_newline:
                print("")

    def last_used_filename(self):
        return self._last_used_filename

    def set_tab(self, num):
        self.__tab_spaces = num
        for c in self:
            c.set_tab(num)
        return self

    def __str__(self, decimals=5, reduce_whitespace=False, style_decimals=None):
        self.stdout_open(None, self.inline_repr(), reduce_whitespace)
        self.__2string(0, decimals, style_decimals)
        return self.stdout_close()

    def __repr__(self):
        return self.__str__()
