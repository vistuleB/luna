from html.parser import HTMLParser
from lib_lUnAtIc import lUnAtIc
from lib_timing  import stopwatch


SELF_CLOSING = ['img', 'col', 'rect', 'br', 'link', 'input']


def drop_html_comments(text):
    index = text.find('<!--')
    if index == -1:
        return text
    closing = text.find('-->')
    if closing == -1 or closing <= index:
        print("text:", text)
        print("index", index)
        print("closing", closing)
        assert False
    return drop_html_comments(text[:index] + text[closing + 3:])
    

def process_newlines_etc(text, remove_comments):
    if remove_comments:
        text = drop_html_comments(text)
    if len(text) == 0:
        return ''
    first_is_space = (text[0].isspace())
    last_was_space = (text[-1].isspace())
    text = text.strip(' \n\t')
    text = '\n'.join([x.strip(' \t') for x in text.split('\n')])
    if first_is_space:
        assert len(text) == 0 or text[0] != ' '
        text = ' ' + text
    if last_was_space and text[-1] != ' ':
        text += ' '
    return text


class MyXmlParser(HTMLParser):
    def __init__(self, remove_comments):
        super().__init__(convert_charrefs=False)
        self.node_stack = []
        self.current_text = ""
        self.root = None
        self.just_saw_self_closing = False
        self.remove_comments = remove_comments

    # Overridable -- finish processing of start+end tag: <tag.../>
    def handle_startendtag(self, tag, attrs):
        self.handle_starttag(tag, attrs)
        self.handle_endtag(tag)

    def close_last_data(self):
        def text_should_be_processed(node):
            if node is None:
                return True
            if node.has_class('space_sensitive') or node.type == "script" or node.type == "style" or node.type == "pre":
                return False
            return True
        
        guy_with_tail_to_process = None
        guy_with_text_to_process = None
        
        if len(self.node_stack) > 0:
            last = self.node_stack[-1]
            if len(last) > 0:
                guy_with_tail_to_process = last.last_child()
            else:
                guy_with_text_to_process = last
                
        if guy_with_tail_to_process is not None and text_should_be_processed(guy_with_tail_to_process.parent):
            guy_with_tail_to_process.tail = process_newlines_etc(guy_with_tail_to_process.tail, self.remove_comments)

        if guy_with_text_to_process is not None and text_should_be_processed(guy_with_text_to_process) and text_should_be_processed(guy_with_text_to_process.parent):
            guy_with_text_to_process.text = process_newlines_etc(guy_with_text_to_process.text, self.remove_comments)
        

    # Overridable -- handle start tag
    def handle_starttag(self, tag, attrs):
        assert self.root is None
        self.close_last_data()

        if tag.lower() == 'radialgradient':
            tag = 'radialGradient'

        elif tag.lower() == 'lineargradient':
            tag = 'linearGradient'

        node = lUnAtIc(tag, {})
        for i, c in enumerate(attrs):
            if c[1] is None:
                continue  # ? recently added ? should this be the behavior ?

            if not isinstance(c[1], str):
                print(f"attrs (offending #{i}):")
                print(attrs)
                assert False
            c_1 = c[1].replace('"', '&quot')
            strip = False
            if node.type == 'defs':
                if c[0] == 'id' and \
                   c_1 != 'MathJax_SVG_glyphs':
                    strip = True
                    print(f"(parser: stripping '{c[0]}={c_1}' attribute pair from {node.type})")
            if strip is False:
                node[c[0]] = c_1
        node_stack = self.node_stack
        if self.just_saw_self_closing:
            last = node_stack.pop()
            assert last.tail == ''
            last.tail = last.text
            last.text = ''
        if len(node_stack) > 0:
            node_stack[-1].append(node)
        node_stack.append(node)
        self.just_saw_self_closing = False
        if tag.lower() in SELF_CLOSING:
            self.just_saw_self_closing = True

    # Overridable -- handle end tag
    def handle_endtag(self, tag):
        assert len(self.node_stack) > 0
        assert self.root is None
        self.close_last_data()

        if tag.lower() == 'radialgradient':
            tag = 'radialGradient'

        elif tag.lower() == 'lineargradient':
            tag = 'linearGradient'

        if tag != self.node_stack[-1].type:
            if self.just_saw_self_closing:
                last = self.node_stack.pop()
                assert len(self.node_stack) > 0
                assert last.tail == ''
                last.tail = last.text
                last.text = ''
        if tag.lower() != self.node_stack[-1].type:
            print("OK; here's us tag:", tag)
            print("node_stack[-1].type:", self.node_stack[-1].type)
            print("node_stack[-1].text:", self.node_stack[-1].text)
            assert False
        if len(self.node_stack) == 1:
            self.root = self.node_stack.pop()
        else:
            self.node_stack.pop()
        self.just_saw_self_closing = False

    # Overridable -- handle character reference
    def handle_charref(self, name):
        if name in ['x2193', '128561', '128378', '129417', '248', '32', 'x2F', 'x27', 'x200b']:  # recently added x2F
            self.handle_data('&')
            self.handle_data('#')
            self.handle_data(name)
            self.handle_data(';')

        else:
            print("name:", name)
            assert False

    # Overridable -- handle entity reference
    def handle_entityref(self, name):
        self.handle_data('&')
        self.handle_data(name)
        self.handle_data(';')

    # Overridable -- handle data
    def handle_data(self, data):
        assert isinstance(data, str)
        assert len(self.node_stack) > 0 or data.isspace() or data.startswith('<!--')
        if len(self.node_stack) == 0:
            return
        last = self.node_stack[-1]
        if len(last) > 0:
            last.last_child().tail += data
        else:
            last.text += data

    # Overridable -- handle comment
    def handle_comment(self, data):
        self.handle_data('<!--' + data + '-->')

    # Overridable -- handle declaration
    def handle_decl(self, decl):
        if 'doctype html' != decl.lower():
            print("(parser ignoring fancy doc type:", decl.lower() + ")")

    # Overridable -- handle processing instruction
    def handle_pi(self, data):
        print("(parser ignoring processing instruction: " + data + ")")

    def unknown_decl(self, data):
        assert False

    def superfeed(self, text):
        self.feed(text)
        return self.get_root()

    def get_root(self):
        self.close()
        if self.root is not None:
            assert len(self.node_stack) == 0
            return self.root
        print("oops, root not here!")
        print("len(self.node_stack):", len(self.node_stack))
        assert False


def load_lUnAtIc_from_string(text, unfold=False, remove_comments=True):
    # some anti-Inkscape hack:
    text = text.replace('vertical-align: ;', 'vertical-align: inherit;')

    stopwatch('load_lUnAtIc_from_string parsing')
    root = MyXmlParser(remove_comments).superfeed(text)
    stopwatch()

    if unfold:
        stopwatch('load_lUnAtIc unfold')
        root.unfold_all(True, msg=True)
        stopwatch()

    return root


def load_rootless_lUnAtIc(filename, unfold=False):
    try:
        text = open(filename, 'r').read()
        text = text.replace('vertical-align: ;', 'vertical-align: inherit;')
        text = '<div class="rootless">' + text + '</div>'
    except IOError:
        raise

    return load_lUnAtIc_from_string(text, unfold=unfold)


def load_lUnAtIc(filename, unfold=False, announce=True, remove_comments=True):
    try:
        text = open(filename, 'r').read()

        if announce:
            print(f"parsing: {filename}")

        if '",' in text and not filename.endswith('fc.html'):
            print(f"WARNING: '{filename}' might have comma-separated attributes")

    except IOError:
        if (filename[0] == '<'):
            text = filename

        else:
            raise

    return load_lUnAtIc_from_string(text, unfold=unfold, remove_comments=remove_comments)


def grab_inkscape_flattened_paths(filename, pop_id=True):
    svg = load_lUnAtIc(filename).flatten()
    paths = svg.find_all('path')
    if pop_id:
        return [p.detach().try_pop_all("style", "inkscape:connector-curvature", "sodipodi:nodetypes", "id") for p in paths]

    else:
        return [p.detach().try_pop_all("style", "inkscape:connector-curvature", "sodipodi:nodetypes") for p in paths]
