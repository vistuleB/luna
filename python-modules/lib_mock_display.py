import os
from numbers import Real

from svgpathsio import open_in_browser
from lib_load_lUnAtIc import load_lUnAtIc
from lib_lUnAtIc import lUnAtIc
from lib_constructors import *
from lib_svg_matrix import int_else_float
from lib_dirnames import luna_dir


def parse_px_float(thing, NoneValue=0):
    if thing is None:
        return NoneValue

    if isinstance(thing, Real):
        return int_else_float(thing)

    if isinstance(thing, str) and thing.endswith('px'):
        return int_else_float(thing[:-2])

    if isinstance(thing, str):
        return int_else_float(thing)

    assert False
    

def scripts():
    return [
        html_script(
            { "type": "text/javascript" },
            r"""
                function recenter_top() {
                    let total_width = document.body.scrollWidth;
                    let inner_width = window.innerWidth;
                    window.scrollTo((total_width - inner_width) / 2, 0);
                }

                window.addEventListener(
                    'load', 
                    () => {
                        setTimeout(
                            () => { setTimeout(recenter_top, 0); },
                            20
                        )
                    }
                );
            """
        )
    ]


def head_element(fontpx, mathfontname):
    assert fontpx == 20 or fontpx == 20.5 or fontpx == 21
    assert mathfontname == 'default' or mathfontname == 'STIX'
    
    fontselectionstring = ''
    if mathfontname == 'STIX':
        fontselectionstring = "font: STIX-Web, "

    return html_head(
        {},
        [
            html_style(
                """
                    * { border: 0; padding: 0; margin: 0; line-height: 0; }
                    body {
                        width: 3480px;
                        font: 400 """ + f"{fontpx}" + """px/1.5 Baskerville;
                    }
                    p {
                        width: 456px;
                        margin-left: 1500px;
                        line-height: 1.5;
                        text-indent: 40px;
                    }
                    .centered {
                        text-align: center;
                    }
                """
            ),
            html_script({
                'type': 'text/javascript',
                'src': 'MathJax-master/MathJax.js?config=TeX-AMS_SVG'
            }),
            html_script(
                {
                    "type": "text/javascript"
                },
                text=r"""
MathJax.Hub.Config({
    ShowMathMenu: false,
    extensions: ["tex2jax.js"],
    "skipStartupTypeset": false,
    "SVG": {""" +  fontselectionstring +  """mtextFontInherit: true, useGlobalCache: true},
    // "SVG": { mtextFontInherit: true, useGlobalCache: true},
    // "HTML-CSS": {mtextFontInherit: true, font: "STIX-Web"},
    tex2jax: {inlineMath: [['$', '$']], processEscapes: true},
    TeX: {
        extensions: ["color.js"],
        Macros: {
            dblcol: "\\!\\rt{0.1}\\mathrel{\\raise.13ex{\\substack{\\small \\circ \\\\ \\small \\circ}}}",
            hc: "\\!\\rt{0.1}\\mathrel{\\raise.13ex{\\substack{\\small \\circ \\\\ \\small \\circ}}}",
            rr: "\\mathbb{R}",
            zz: "\\mathbb{Z}",
            nn: "\\mathbb{N}",
            ww: "\\mathbb{W}",
            qq: "\\mathbb{Q}",
            te: "\\text",
            dom: "\\text{dom}\\,",
            degree: "\\text{deg}\\,",
            f: "\\Rule{0.12em}{0.8pt}{-0.8pt}f",
            fsp: "\\hspace{0.06em}\\Rule{0.12em}{0.8pt}{-0.8pt}f",
            sp: "\\Rule{0.08em}{0.8pt}{-0.8pt}",
            ra: "\\rightarrow",
            back: "\\backslash",
            sqt: "{\\color{white} *\\!\\!\\!}",
            up: ["\\rule{0pt}{#1em}", 1],  // vspace doesn't seem to work / exist ?
            dn: ["\\Rule{0pt}{0em}{#1em}", 1],
            rt: ["\\hspace{#1em}", 1],
            hlfbk: "\\!\\hspace{0.1em}",
            fl: ["\\lfloor #1 \\rfloor", 1],
            cl: ["\\lceil #1 \\rceil", 1],
            FL: ["\\left\\lfloor #1 \\right\\rfloor", 1],
            CL: ["\\left\\lceil #1 \\right\\rceil", 1],
            implies: "\\Longrightarrow",
            psa: "{}\\!\\hspace{0.0691em}",
            psb: "{}\\!\\hspace{0.06901311249137em}",
            ncdot: "\\re\\cdot\\re",
            re: "\\!\\hspace{0.1em}",
            bk: "\\!\\hspace{0.1em}",
            gbk: "\\!\\hspace{0.15em}",
            fw: "\\hspace{0.1em}",
            hfbk: "\\!\\hspace{0.2em}",
            deg: "\\circ",
            km: "[\\text{km}]",
            ddx: "{d \\over dx}\\hspace{0.1em}",
            ddt: "{d \\over dt}\\hspace{0.1em}",
            ddu: "{d \\over du}\\hspace{0.1em}",
            ddz: "{d \\over dz}\\hspace{0.1em}",
            ov: ["\\overline{#1}", 1],
            floor: ["\\lfloor{#1}\\rfloor", 1],
            faketextelement: "{\\color{white}\\text{*}}\\!\\!\\!\\rt{0.1}"
        } // end Macros
    },
});"""
            )
        ]
    )


def first_paragraph():
    return lUnAtIc("""
        p#first_p.nb
            b[Heading.][ No quo alii iudicabit dissentiet. Ad eros dicam volutpat quo, ut
            eos eligendi principes. Quidam facilisis at vix. Ei deserunt invenire quo...]
    """)


def middle_paragraph():
    return lUnAtIc("""
        p.text-indent:0px[
            ...totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et
            quasi architecto beatae vitae dicta sunt...
        ]
    """)


def last_paragraphs():
    return [
        html_p({'class': 'noi'}).set_text("""...excepteur sint occaecat cupidatat non proident, sunt inn culpa qui officia deserunt mollit anim id."""),
        html_p().set_text("""Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab $x$ illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo..."""),
        lUnAtIc("""
            p[
                $$
                f(x) = -2x
                $$
                <!-- $$f(x) = -2x$$ -->
            ]
        """),
        lUnAtIc("""
            p.noi[
                ...nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit,
                sed quia consequuntur magni dolores eos.
            ]
        """),
        lUnAtIc("""
            div.display:block.height:200px
        """)
    ]


def img_container(name, margin_top=None, margin_bottom=None, width=None, height=None, background_color=None):
    assert \
        isinstance(name, lUnAtIc) or \
        isinstance(name, str)

    inline = \
        isinstance(name, lUnAtIc) or \
        name.startswith('inline->')

    if inline and isinstance(name, str):
        name = name[len('inline->'):]

    if inline or width is None or height is None:
        root = \
            load_lUnAtIc(name) if isinstance(name, str) \
            else name

        assert isinstance(root, lUnAtIc)

    is_inline_html = inline and root.type != 'svg'

    if not is_inline_html and (width is None or height is None):
        if width is None and height is not None:
            width = height * root.viewbox_width() / root.viewbox_height()

        if height is None and width is not None:
            height = width * root.viewbox_height() / root.viewbox_width()

        if width is None and 'width' in root:
            width = root['width']

        if height is None and 'height' in root:
            height = root['height']

        width = root.viewbox_width() if width is None else width
        height = root.viewbox_height() if height is None else height

    elif is_inline_html and (width is not None or height is not None):
        print("(warning: width and/or height being ignored for html blurb)")

    width = parse_px_float(width)
    height = parse_px_float(height)
    margin_top = parse_px_float(margin_top)
    margin_bottom = parse_px_float(margin_bottom)

    if not is_inline_html:
        s = ''
        s += f".width:{width}px" if width is not None else ''
        # s += f".height:{height}px" if height is not None else ''
        s += f".margin-top:{margin_top}px" if margin_top is not None else ''
        s += f".margin-bottom:{margin_bottom}px" if margin_bottom is not None else ''
        s += f".background-color:{background_color}" if background_color is not None else ''

    if is_inline_html:
        if root.has_class('column'):
            outer = root

        else:
            outer = lUnAtIc(f"div.centered.height:{height}px").append_and(root)

    elif inline:
        outer = lUnAtIc(f"""
            div.centered
                div.display:inline-block.position:relative{s}
        """)
        outer[0].append(root.detached_copy_if_parent())

    else:
        outer = lUnAtIc(f"""
            div.centered
                img{s}.src={name}
        """)

    return outer


def virgin_mock_display(fontpx, mathfontname):
    root = lUnAtIc("html")
    root.append(head_element(fontpx, mathfontname))
    body = root.append("body.background_color")
    body.append(first_paragraph())
    body.append_all(last_paragraphs())
    root.append_all(scripts())
    return root


def mock_multidisplay(array, display_filename=None, msg=True, open=True, decimals=None, xtra_js=[], fontpx=20, mathfontname='default'):
    assert len(xtra_js) == 0

    if display_filename is None:
        display_filename = luna_dir + "mock_display.html"

    if not display_filename.endswith('.html'):
        display_filename += '.html'

    to_append = []
    for i, fields in enumerate(array):
        assert isinstance(fields, tuple)
        assert isinstance(fields[0], lUnAtIc) or isinstance(fields[0], str)
        to_append.append(img_container(*fields))
        if i < len(array) - 1:
            to_append.append(middle_paragraph())

    root = virgin_mock_display(fontpx, mathfontname)
    if len(to_append) > 0:
        root.body().insert_after(to_append, 'p#first_p')
    root.pretty_up().print_to(display_filename, decimals=decimals)

    if msg:
        print(f"\n{display_filename} now contains mock display of:")
        for img in array:
            if isinstance(img[0], lUnAtIc):
                print(f" - [inline] (width {img[3]})")

            else:
                print(f" - {img[0]} (width {img[3]})")

        print("")

    if open:
        print("calling open_in_browser Agada")
        open_in_browser(display_filename)
        print("returned from open_in_browser")


ze_delayed_list = []


def mock_display(
        img_filename_or_svg_or_list_thereof=[],
        open=True,
        margin_top=20, margin_bottom=20,
        width=None, height=None,
        background_color=None,
        display_filename=None,
        msg=True,
        decimals=None,
        delayed=False,
        xtra_js=[]
    ):
    global ze_delayed_list
    ze_list = []

    if not isinstance(img_filename_or_svg_or_list_thereof, list):
        img_filename_or_svg_or_list_thereof = [img_filename_or_svg_or_list_thereof]

    assert all(isinstance(img, lUnAtIc) or isinstance(img, str) for img in img_filename_or_svg_or_list_thereof)

    ze_list = [(
        img,
        margin_top,
        margin_bottom,
        width,
        height,
        background_color
    ) for img in img_filename_or_svg_or_list_thereof]

    ze_delayed_list.extend(ze_list)

    if delayed:
        return

    mock_multidisplay(
        ze_delayed_list,
        display_filename,
        msg,
        decimals=decimals,
        open=open,
        xtra_js=xtra_js,
    )

    ze_delayed_list = []
