import re
import sys
import copy
import os

from itertools import chain
from math import exp, floor, ceil, log, log10, sin, cos, pi, sqrt, atan
from lib_svg_matrix import cos_deg, sin_deg, degrees
from numbers import Complex, Real
from random import randrange
from lib_dirnames import luna_dir, yaml_dir

eta = pi / 2

from lib_an_svg import an_inkscape_layer, an_ordinary_layer, an_svg
from lib_constructors import svg_circle, svg_g, svg_line, svg_path, svg_rect, svg_use
from lib_dict_utilities import (
    deepexists,
    deepget,
    deepsetdefault,
    setfresh,
    update_dict_recursive,
)
from lib_MathLabel_rendering import (
    MathLabel,
    flush_MathLabel_queue,
    get_or_queue_MathLabel,
    g_ex_height_at_20_point_0_baskerville,
)
from lib_load_lUnAtIc import load_lUnAtIc
from lib_load_stuff import load_yaml
from lib_lookahead import lookback, lookbothways, zipit, zipit3
from lib_lUnAtIc import lUnAtIc
from lib_matches_template import assert_matches_template
from lib_mock_display import mock_multidisplay
from lib_svg_matrix import (
    BoundingBox,
    Interval,
    Point,
    cot_deg,
    four_way_intersection,
    ze_2f,
    interpolate_along_segment,
    isclose
)
from lib_timing import report_all, stopwatch
from numpy import arctan2
from svgpathsio import (
    HtmlColorsLowerCase,
    Line,
    Path,
    bbox2path,
    crop,
    generate_transform,
    is_css_color,
    parse_path,
    parse_subpath,
    parse_transform,
)
from svgpathsio.misctools import Rgba
from svgpathsio.path import CubicBezier, Subpath
from lib_brace_assignments import (
    underbrace_tip_stroke_width_default,
    underbrace_body_stroke_width_default,
    underbrace_end_tip_length_over_body_width,
    underbrace_midd_tip_length_over_body_width,
    underbrace_left_top,
    underbrace_left_bot,
    underbrace_righ_top,
    underbrace_righ_bot,
    underbrace_midd_top,
    underbrace_midd_bot,
)

# to avoid linter complaint about functions that might only be used in 'eval':

exp(2) * floor(2) * ceil(2) * log(2) * log10(2) * sin(2) * cos(2) * sqrt(2) * atan(2)
sin_deg(2) * cos_deg(2) * degrees(0)

# universal constants:

SMALL_DIVISION = 1e-7

g_x_height = g_ex_height_at_20_point_0_baskerville  # recently changed!!

# default values:

g_default_cell = 50
g_default_arrow_width_in_exes = 1.1
g_default_arrow_length_as_pct_of_width = 0.75
g_tick_labels_margin_t = 0.4
g_tick_labels_margin_r = 0.2
g_tick_labels_margin_b = 0.4
g_tick_labels_margin_l = 0.2
g_default_ecr = 0.07
g_default_margin_top = 1
g_default_margin_right = 100
g_default_margin_bottom = 1
g_default_margin_left = 100
g_default_top_extension = 0
g_default_right_extension = 0
g_display_margin_top = 24
g_display_margin_bottom = 20
g_default_grid_x_every = 1
g_default_grid_y_every = 1
g_default_subgrid_x_every = 0.1
g_default_subgrid_y_every = 0.1
g_default_tranche_sep = 20
g_x_middle_fudge = 0.05
g_middle_number_fudge = -0.03

g_east_arrow_path_data_v2 = 'M -1,-1 C -0.934,-0.626 -0.2,-0.063 0,0 C -0.2,0.063 -0.934,0.626 -1,1'
g_east_arrow_path_data_v3 = 'M -1,-1 C -1,-0.6 -0.25,-0.0 0,0 C -0.25,0.0 -1,0.6 -1,1'

g_default_yellow_arrow_nose_half_angle = 45
g_default_yellow_arrow_half_width = 4.2
g_default_yellow_arrow_lip = 5
g_default_yellow_arrow_stem_length = 13.11
g_default_yellow_arrow_classname = "yellow-arrows"

g_default_fat_vector_arrow_nose_half_angle = 90 - 57.77
g_default_fat_vector_arrow_half_width = 4.7
g_default_fat_vector_arrow_lip = 7.2
g_default_fat_vector_arrow_classname = "fat-vector-arrows"

g_fontname = 'default'
g_fontpx = 20

if False:
    yaml_dir = "/Users/jpsteinb/src/gitlab.com/vistuleB/yaml/"
    directories_list_name = yaml_dir + "yaml_directories.yaml"

    g_short_dirnames_to_abspath_map = load_yaml(directories_list_name)
    print(g_short_dirnames_to_abspath_map)

else:
    g_short_dirnames_to_abspath_map = {}


def classname_setdefault(d, name, key='classname'):
    assert isinstance(name, str)
    assert name != ''
    d.setdefault(key, name)
    if d[key] == '':
        d[key] = name


def print_mock_display_width_height(plan, label="(unspecified)"):
    md = plan['mock-display']
    print(f"width @ pt {label}:", md.get("width", None))
    print(f"height @ pt {label}:", md.get("height", None))


def foreground_contour_classname_from_classname(classname, for_real=True):
    if not for_real:
        return classname
    return "computed-" + classname + "-foreground-contour"


def origin_underbrace( # assumes no knowledge of any outside coordinate system
    length,
    desired_body_thickness,
    desired_mid_thickness,
    end_tip_length_over_body_thickness,
    mid_length_over_body_thickness,
    end_tip_additional_y_scaling=1,
    mid_tip_additional_y_scaling=1
):
    l_inner = parse_subpath(underbrace_left_top) # Q1 -> 0        inner side of left tip
    l_outer = parse_subpath(underbrace_left_bot) # 0 -> Q1        outer side of left tip
    r_outer = parse_subpath(underbrace_righ_bot) # Q2 -> 0        outer side of right tip
    r_inner = parse_subpath(underbrace_righ_top) # 0 -> Q2        inner side of right tip
    m_inner = parse_subpath(underbrace_midd_top) # Q4 -> 0 -> Q3  inner side of mid
    m_outer = parse_subpath(underbrace_midd_bot) # Q3 -> 0 -> Q4  outer side of mid

    def inQ1(c):
        return c.real > 0 and c.imag > 0

    def inQ2(c):
        return c.real < 0 and c.imag > 0

    def inQ3(c):
        return c.real < 0 and c.imag < 0

    def inQ4(c):
        return c.real > 0 and c.imag < 0
    
    def at0(c):
        return c == 0
    
    def current_l_body_thickness():
        return l_outer.end.imag - l_inner.start.imag
    
    def current_r_body_thickness():
        return r_outer.start.imag - r_inner.end.imag
    
    def current_body_thickness():
        l = current_l_body_thickness()
        r = current_r_body_thickness()
        assert isclose(l, r)
        return l
    
    def current_l_end_tip_length():
        return max(l_inner.start.real - l_inner.end.real, l_outer.end.real - l_outer.start.real)

    def current_r_end_tip_length():
        return max(r_inner.start.real - r_inner.end.real, r_outer.end.real - r_outer.start.real)
    
    def current_end_tip_length():
        return 0.5 * (current_l_end_tip_length() +  current_r_end_tip_length())

    def current_outer_mid_length():
        return m_outer.end.real - m_outer.start.real

    def current_inner_mid_length():
        return m_inner.start.real - m_inner.end.real
    
    def current_mid_thickness():
        return m_outer[0].end.imag - m_inner[0].end.imag
    
    def current_end_tip_length_over_body_thickness():
        return current_end_tip_length() / current_body_thickness()
    
    def current_mid_length():
        return max(current_outer_mid_length(), current_inner_mid_length())

    def current_mid_length_over_body_thickness():
        return current_mid_length() / current_body_thickness()
    
    def outer_l_connection_height_difference():
        return m_outer.start.imag - l_outer.end.imag

    def outer_r_connection_height_difference():
        return m_outer.end.imag - r_outer.start.imag

    def total_outer_connection_height_difference_abs():
        return abs(outer_l_connection_height_difference()) + abs(outer_r_connection_height_difference())

    def inner_l_connection_height_difference():
        return m_inner.end.imag - l_inner.start.imag

    def inner_r_connection_height_difference():
        return m_inner.start.imag - r_inner.end.imag
    
    def total_inner_connection_height_difference_abs():
        return abs(inner_l_connection_height_difference()) + abs(inner_r_connection_height_difference())
    
    assert len(m_inner) == 2
    assert len(m_outer) == 2
                                                                                # clockwise around the brace if you
                                                                                # orient the axes in usual cartesian fashion:
    assert inQ1(l_inner.start) and at0(l_inner.end)                             # l_inner Q1 -> 0
    assert at0(l_outer.start) and inQ1(l_outer.end)                             # l_outer 0 -> Q1
    assert inQ3(m_outer.start) and at0(m_outer[0].end) and inQ4(m_outer.end)    # m_outer Q3 -> 0 -> Q4
    assert inQ2(r_outer.start) and at0(r_outer.end)                             # r_outer Q2 -> 0
    assert at0(r_inner.start) and inQ2(r_inner.end)                             # r_inner 0 -> Q2
    assert inQ4(m_inner.start) and at0(m_inner[0].end) and inQ3(m_inner.end)    # m_inner Q4 -> 0 -> Q3

    assert 8.8 < current_l_end_tip_length() < 9
    assert 8.8 < current_r_end_tip_length() < 9
    assert isclose(1.9246, current_l_body_thickness())
    assert isclose(1.9246, current_l_body_thickness())
    assert 15 < current_outer_mid_length() < 16
    assert 17 < current_inner_mid_length() < 18

    desired_end_tip_length = desired_body_thickness * end_tip_length_over_body_thickness
    desired_mid_length = desired_body_thickness * mid_length_over_body_thickness

    end_tip_x_scale = desired_end_tip_length / current_end_tip_length()
    end_tip_y_scale = end_tip_x_scale * end_tip_additional_y_scaling

    l_inner.scale(end_tip_x_scale, end_tip_y_scale)
    r_inner.scale(end_tip_x_scale, end_tip_y_scale)
    l_outer.scale(end_tip_x_scale, end_tip_y_scale)
    r_outer.scale(end_tip_x_scale, end_tip_y_scale)

    xtra_outer_y_scaling = (l_inner.start.imag + desired_body_thickness) / l_outer.end.imag

    l_outer.scale(1, xtra_outer_y_scaling)
    r_outer.scale(1, xtra_outer_y_scaling)

    mid_x_scale = desired_mid_length / current_mid_length()
    mid_y_scale = mid_x_scale * mid_tip_additional_y_scaling

    m_inner.scale(mid_x_scale, mid_y_scale)
    m_outer.scale(mid_x_scale, mid_y_scale)

    assert at0(m_inner[0].end)
    assert at0(m_outer[0].end)

    current_midd_inner_height = m_inner[0].end.imag - m_inner.start.imag
    current_midd_outer_height = m_outer[0].end.imag - m_outer.start.imag

    # s * current_midd_inner_height + desired_mid_thickness - current_midd_outer_height == desired_body_thickness
    # s * current_midd_inner_height == desired_body_thickness - desired_mid_thickness + current_midd_outer_height
    # s == (desired_body_thickness - desired_mid_thickness + current_midd_outer_height) / current_midd_inner_height
    s = (desired_body_thickness - desired_mid_thickness + current_midd_outer_height) / current_midd_inner_height

    print(desired_body_thickness - desired_mid_thickness + current_midd_outer_height)
    
    m_inner.scale(1, s)
    m_inner.translate(0, -desired_mid_thickness)

    current_midd_inner_height = m_inner[0].end.imag - m_inner.start.imag
    
    # alright... presumably the pieces are all the right shape

    r_outer.translate(length, 0)
    r_inner.translate(length, 0)

    m_inner.translate(length / 2, l_inner.start.imag - m_inner.end.imag)
    m_outer.translate(length / 2, l_outer.end.imag - m_outer.start.imag)

    assert isclose(current_body_thickness(), desired_body_thickness)
    assert isclose(current_end_tip_length(), desired_end_tip_length)
    assert isclose(current_mid_length(), desired_mid_length)
    assert isclose(current_mid_thickness(), desired_mid_thickness)
    assert isclose(current_end_tip_length_over_body_thickness(), end_tip_length_over_body_thickness)
    assert isclose(current_mid_length_over_body_thickness(), mid_length_over_body_thickness)
    assert isclose(total_outer_connection_height_difference_abs(), 0)
    assert isclose(total_inner_connection_height_difference_abs(), 0)
    assert l_outer.start.real < l_outer.end.real < m_outer.start.real < m_outer.end.real < r_outer.start.real < r_outer.end.real
    assert l_inner.end.real < l_inner.start.real < m_inner.end.real < m_inner.start.real < r_inner.end.real < r_inner.start.real

    to_return = Subpath(l_outer)
    to_return.append(m_outer, bridge_discontinuity=True)
    to_return.append(r_outer, bridge_discontinuity=True)
    to_return.append(r_inner, bridge_discontinuity=True)
    to_return.append(m_inner, bridge_discontinuity=True)
    to_return.append(l_inner, bridge_discontinuity=True)

    assert len(to_return) == len(l_outer) + 1 + len(m_outer) + 1 + len(r_outer) + len(r_inner) + 1 + len(m_inner) + 1 + len(l_inner)
    to_return.set_Z()

    return to_return


def right_handed_brace_from_page_cors(tip1, tip2, a):
    return origin_underbrace(
        (tip1 - tip2).norm(),
        desired_body_thickness=a['brace-page-cor-body-thickness'],
        desired_mid_thickness=a['brace-page-cor-mid-thickness'],
        end_tip_length_over_body_thickness=a['brace-tip-length-over-body-thickness'],
        mid_length_over_body_thickness=a['brace-mid-length-over-body-thickness']
    ).rotate((tip2 - tip1).arg_in_degrees()).translate(tip1)


# notches stuff

g_default_notch_label_margins = [0.4, 0.4, 0.4, 0.4]  # trbl
g_default_visible_notch_length_in_exes = 1
g_default_non_visible_notch_length_in_exes = 0.3


def new_layer(plan, name):
    return an_inkscape_layer(name) if plan["svg-type"] == "inkscape" else an_ordinary_layer(name)


##########################
# pre-processing of plan #
##########################


def parse_square_bracket(key):
    if not isinstance(key, str):
        return key, []
    prog = re.compile(r"([-_A-Za-z0-9]*)-\[([0-9\., ]*)\]")
    match = re.fullmatch(prog, key)
    if match is None:
        assert not key.endswith("[1]")
        return key, []
    return match.group(1), [int(x) for x in match.group(2).split(",")]


def find_all_square_bracket_numbers(d):
    numbers = set([])
    rec_numbers = set([])
    should_be_gt_0 = False

    if isinstance(d, dict):
        if 'in' in d and isinstance(d['in'], int):
            numbers.add(d['in'])
            assert len(numbers) > 0

        elif 'in' in d and isinstance(d['in'], list):
            numbers = numbers.union(set(d['in']))

        number_lists = [
            x for key in d for x in parse_square_bracket(key) if isinstance(x, list)
        ]

        for ll in number_lists:
            numbers = numbers.union(set(ll))

        if len(numbers) > 0:
            should_be_gt_0 = True

    if isinstance(d, dict):
        for key in d:
            rec_numbers = find_all_square_bracket_numbers(d[key])
            numbers = numbers.union(rec_numbers)

    if isinstance(d, list):
        for thing in d:
            rec_numbers = find_all_square_bracket_numbers(thing)
            numbers = numbers.union(rec_numbers)

    if len(rec_numbers) > 0 or should_be_gt_0:
        assert len(numbers) > 0

    return numbers


def pruned_tsbn_rec(d, number):
    assert isinstance(number, int)

    if not isinstance(d, dict) and not isinstance(d, list):
        return d

    if isinstance(d, dict):
        pruned_copy = {}
        for key in d:
            new_key, numbers = parse_square_bracket(key)
            if len(numbers) == 0 or number in numbers:
                pruned_copy[new_key] = pruned_tsbn_rec(d[key], number)
        return pruned_copy

    if isinstance(d, list):
        return [
            pruned_tsbn_rec(x, number)
            for x in d
            if (
                not isinstance(x, dict)
                or 'in' not in x
                or (isinstance(x['in'], Real) and int(float(x['in'])) == number)
                or (isinstance(x['in'], list) and number in x['in'])
            )
        ]

    assert False


def remove_graphs_with_empty_data(d):
    if "graphs" in d:
        survivors = [
            g
            for g in d["graphs"]
            if "data" in g or g["type"] not in ["dotplot", "linepath"]
        ]
        d["graphs"] = survivors


def pruned_to_square_bracket_number(d, number):
    assert isinstance(number, int)
    pruned = pruned_tsbn_rec(d, number)
    remove_graphs_with_empty_data(pruned)
    return pruned


class YamlComputerNode:
    def __init__(self, parent):
        self.parent = parent
        self.tokens = []

    def feed(self, v):
        assert v != ""
        self.tokens.append(v)

    def close(self):
        assert len(self.tokens) % 2 == 1
        for i, z in enumerate(self.tokens):
            assert bool(i % 2) is (z in ["*", "/", "-", "+", "%", "++", "OF"])
        for op in ["*", "/", "-", "+", "%", "++", "OF"]:
            changed = True
            while changed:
                changed = False
                for i, z in enumerate(self.tokens):
                    if z != op:
                        continue
                    assert i > 0 and i < len(self.tokens) - 1
                    prev = self.tokens[i - 1]
                    next = self.tokens[i + 1]
                    if op == "OF":
                        assert isinstance(prev, str)
                        assert isinstance(next, Real)
                        if not prev.endswith("(x)"):
                            prev += "(x)"
                        expression = "(lambda x: " + prev + ")(" + str(next) + ")"
                        result = eval(expression)
                        if not isinstance(result, Real):
                            print("offending expression:", expression)
                            assert False

                    else:
                        type = Real if op in ["*", "/", "-", "+", "%"] else str
                        assert isinstance(prev, type)
                        assert isinstance(next, type)
                        result = eval("prev " + op + " next")
                        assert isinstance(result, type)

                    self.tokens[i - 1] = result
                    del self.tokens[i]
                    del self.tokens[i]
                    changed = True
                    break

        assert len(self.tokens) == 1
        assert self.tokens[0] not in ["*", "/", "-", "+", "%", "++", "OF"]

        if self.parent:
            self.parent.feed(self.tokens[0])
            return None

        return self.tokens[0]


def replace_references_recursive(plan, d, depth):
    definitions_isolator = re.compile(r">>{([^{}]*)}")
    expression_tokenizer = re.compile(r"(\+\+|\+|\*|\-|/[^a-zA-Z]|[^ a-zA-Z]/|\(|\)|\s\-\s|\sOF\s)")

    def token_handler(token):
        assert isinstance(token, str)

        token = token.strip()
        
        if token in ["(", ")", "-", "+", "*", "/", "++", "OF"]:
            return token

        try:
            f = float(token)
            return int(f) if int(f) == f else f

        except ValueError:
            pass

        if "/" not in token:
            if any(x in token for x in "{}"):
                print("offending token:", token)
            assert not any(x in token for x in "{}")
            return token

        token_splitter = re.compile(r"/|(?:\[([0-9]+)\])")

        split_path = []
        raw_parts = [x for x in re.split(token_splitter, token) if x != "" and x is not None]
        for x in raw_parts:
            assert isinstance(x, str)
            assert x != ""
            try:
                z = float(x)
                assert z == int(z)
                split_path.append(int(z))
            except ValueError:
                split_path.append(x)

        assert len(split_path) > 0

        if split_path[0] == ".":
            assert isinstance(d, dict)
            where_to_look = d
            split_path = split_path[1:]

        else:
            where_to_look = plan

        value = deepget(where_to_look, *split_path)

        if value is None:
            print("d:", d)
            print("token:", token)
            print("split_path:", split_path)
            assert False

        if isinstance(value, str):
            value = rec_value_of(value)

        return value

    def rec_value_of(string):
        def atomize_and_evaluate(expression):
            tokens = [
                token_handler(x)
                for x in re.split(expression_tokenizer, expression)
                if x.strip() != ""
            ]

            original_parent = YamlComputerNode(None)
            current_node = original_parent

            for t in tokens:
                if t == "(":
                    current_node = YamlComputerNode(current_node)

                elif t == ")":
                    assert current_node != original_parent
                    current_node.close()
                    current_node = current_node.parent

                else:
                    current_node.feed(t)

            assert current_node == original_parent

            return original_parent.close()

        def to_string(thing):
            if isinstance(thing, str):
                return thing
            if not isinstance(thing, Real):
                print("offending thing:", thing)
                assert False
            if isinstance(thing, int):
                return str(thing)
            return f"{thing:.5}"

        if string == "":
            return ""

        if string.startswith("external[") and string.endswith("]"):
            filename, path = string[len("external[") : -len("]")].split(":")
            assert len(filename) > 0 and "/" not in filename
            assert len(path) > 0
            if filename not in g_external_yamls:
                try:
                    g_external_yamls[filename] = load_yaml(filename)

                except FileNotFoundError:
                    g_external_yamls[filename] = load_yaml(yaml_dir + filename)

                replace_references(g_external_yamls[filename])
            return deepget(
                g_external_yamls[filename], *[x for x in path.split("/") if x != ""]
            )
            
        to_return = string
        last_string = None

        while isinstance(to_return, str) and to_return != last_string:
            last_string = to_return

            pieces = re.split(definitions_isolator, to_return)
            reassembled = []
            
            print("pieces", pieces)
            
            for i, p in enumerate(pieces):
                if p == "":
                    continue

                if i % 2 == 0:
                    reassembled.append(p)
                    assert not p.startswith('defs/')
                    continue

                to_append = atomize_and_evaluate(p)
                
                assert to_append is not None
                assert isinstance(to_append, str) or isinstance(to_append, Real)
                
                reassembled.append(to_append)

            if len(reassembled) == 0:
                print("to_return:", to_return)
                print("pieces:", pieces)
                assert False

            if len(reassembled) > 1:
                to_return = "".join(to_string(x) for x in reassembled)
                if any(any(isinstance(t, str) and u in t for u in ['+', '-', '/', '*', '++']) for t in reassembled):
                    to_return = '(' + to_return + ')'   # recently added god bless

            else:
                to_return = reassembled[0]
            
        return to_return

    #####
    # body of function starts here
    #####

    if isinstance(d, dict):
        for key, value in d.items():
            if isinstance(value, str):
                ref_value = rec_value_of(value)
                if ref_value != value:
                    d[key] = ref_value

            if isinstance(value, dict) or isinstance(value, list):
                replace_references_recursive(plan, value, depth + 1)

    elif isinstance(d, list):
        for i, value in enumerate(d):
            if isinstance(value, str):
                ref_value = rec_value_of(value)
                if ref_value != value:
                    d[i] = ref_value

            if isinstance(value, dict) or isinstance(value, list):
                replace_references_recursive(plan, value, depth + 1)

    else:
        assert False


def replace_references(plan):
    replace_references_recursive(plan, plan, 0)


######################
# graph data loading #
######################


def load_coordinate_data_of_graph(plan, g):
    assert "data" in g
    assert "file-dir" not in g  # we should already have replaced it with path flesh_out_graphs

    data = g['data']

    if isinstance(data, str):
        assert "path" in g
        filename = g["path"] + data
        pts = [Point(c) for c in load_yaml(filename)]

    elif isinstance(data, list):
        pts = [Point(c) for c in data]

    elif isinstance(data, dict):
        assert 'y-index' in data
        assert 'filename' in data
        assert 'path' in g

        filename = g['path'] +  data['filename']
        x_index = data.setdefault('x-index', 0)
        y_index = data['y-index']

        assert isinstance(x_index, int)
        assert isinstance(y_index, int)
        assert x_index != y_index

        rows = load_yaml(filename)

        assert isinstance(rows, list)
        assert len(rows) > 1
        assert all(isinstance(row, list) for row in rows)
        
        row_length = len(rows[0])

        assert all(len(row) == row_length for row in rows)
        assert 0 <= x_index < row_length
        assert 0 <= y_index < row_length
        assert all(isinstance(row[x_index], Real) for row in rows)
        assert all(isinstance(row[y_index], Real) for row in rows)

        pts = [Point(row[x_index], row[y_index]) for row in rows]

    else:
        assert False

    def are_colinear(p0, p1, p2):
        return (p1 - p0).det(p2 - p1) == 0
    
    # remove colinear points
    if len(pts) >= 3:
        last_pt = pts[0]
        filtered_pts = [last_pt]
        for p1, p2 in zip(pts[1:], pts[2:]):
            if not are_colinear(last_pt, p1, p2):
                filtered_pts.append(p1)
                last_pt = p1
        filtered_pts.append(pts[-1])

        if len(filtered_pts) != len(pts):
            print(f"removing {len(pts) - len(filtered_pts)} from {g['type']} graph due to colinearity")
        pts = filtered_pts

    num_points = len(pts)

    if "transform" in g:
        t = parse_transform(g["transform"])
        for c in pts:
            c.transform_by(t)

    if "transforms" in g:
        for q in g["transforms"]:
            t = parse_transform(q["t"])
            if "if-x-in" in q:
                good = Interval(q["if-x-in"])
                print("selective number interval:", good)

            else:
                good = Interval(min([c.x for c in pts]), max([c.x for c in pts]))

            for c in pts:
                if good.contains(c.x):
                    c.transform_by(t)

    assert len(pts) == num_points

    if g.get('cull-data-outside-lr', False):
        assert 'cull-data-outside-lr' in g
        # this keeps first and last protruding points, if you 
        # want a different behavior invent your own flag, like 'strict-cull-data-outside-lr'
        index_start = -1
        index_end = -1
        l = lr(plan)
        for i, pt in enumerate(pts):
            if l.contains(pt.x):
                assert index_end == -1
                if index_start == -1:
                    index_start = i
            else:
                if index_start >= 0 and index_end == -1:
                    index_end = i
        if index_end == -1 and index_start != -1:
            index_end = len(pts)
        if index_start != -1:
            survivors = []
            for j in range(index_start - 1, index_end + 1):
                if 0 <= j <= len(pts) - 1:
                    survivors.append(pts[j])
            if len(survivors) < len(pts):
                print(f"removing {len(pts) - len(survivors)} points from {g['type']} graph due to lr-culling")
            pts = survivors
        else:
            assert index_end == -1

    if g.get('manual-clip-linepath-to-lr', False):
        assert g['cull-data-outside-lr']
        assert len(pts) >= 2
        p0 = pts[0]
        p1 = pts[1]
        if p0.x < l.lo:
            assert p1.x >= l.lo
            pts[0] = interpolate_along_segment(p0, p1, l.lo)
        p3 = pts[-2]
        p4 = pts[-1]
        if p4.x > l.hi:
            assert p3.x <= l.hi
            pts[-1] = interpolate_along_segment(p3, p4, l.hi)
        assert pts[0].x >= l.lo
        assert pts[-1].x <= l.hi

    for c in pts:
        c.cleanup()

    if g["type"] == "linepath":
        print(pts)
        check_linepath_integrity(pts)

    return pts


def graph_data_all_points(plan):
    pts = []
    if "graphs" in plan:
        assert len(plan["graphs"]) > 0
        for g in plan["graphs"]:
            new_pts = load_coordinate_data_of_graph(plan, g)
            assert len(new_pts) > 0
            pts.extend(new_pts)
            assert len(pts) >= len(new_pts)
        assert len(pts) > 0
    return pts


def graph_data_lr_points(plan):
    l = lr(plan)
    pts = graph_data_all_points(plan)
    return [p for p in pts if l.contains(p.x)]


def graph_data_bbox(plan):
    pts = graph_data_all_points(plan)
    if 'lr' in plan and isinstance(plan['lr'], Interval):
        lr = plan['lr']
        pts = [p for p in pts if lr.contains(p.x)]
    return BoundingBox().agglomerate_all(pts)


def graph_data_lr_bbox(plan):
    pts = graph_data_lr_points(plan)
    return BoundingBox().agglomerate_all(pts)


##################################
# various graph_cors / page_cors #
##################################


def graph_cors_to_page_cors_transform_tokens(plan):
    ux, uy = scaled_x_y_units(plan)
    return ["scale", ux, -uy, "translate", -lr(plan).lo, -bt(plan).hi]


def page_cors_to_graph_cors_transform_tokens(plan):
    ux, uy = scaled_x_y_units(plan)
    return ["translate", lr(plan).lo, bt(plan).hi, "scale", 1 / ux, -1 / uy]


def graph_cors_to_page_cors_transform(plan):
    return graph_cors_to_page_cors_transform_tokens(plan)


def page_cors_to_graph_cors_transform(plan):
    return page_cors_to_graph_cors_transform_tokens(plan)


def graph_cors_to_page_cors_matrix(plan):
    return parse_transform(graph_cors_to_page_cors_transform_tokens(plan))


def page_cors_to_graph_cors_matrix(plan):
    return parse_transform(page_cors_to_graph_cors_transform_tokens(plan))


def graph_cors_slope_to_page_slope(plan, s):
    ux, uy = scaled_x_y_units(plan)
    return s * uy / ux


def page_cors_to_graph_cors_scale(plan):
    ux, uy = scaled_x_y_units(plan)
    return f"scale({1 / ux}, {-1 / uy})"


def graph_cors_to_page_cors_scale(plan):
    ux, uy = scaled_x_y_units(plan)
    return f"scale({ux}, {-uy})"


def add_path_from_page_cor_tokens(parent, tokens, classname):
    parent.append("path", {"d": parse_path(tokens, auto_add_M=True), "class": classname})


def add_path_from_graph_cor_tokens(parent, plan, tokens, classname):
    m = graph_cors_to_page_cors_matrix(plan)
    path = parse_path(tokens, auto_add_M=True).transform(m)
    if classname is not None:
        parent.append("path", {"d": path, "class": classname})

    else:
        parent.append("path", {"d": path})


def add_new_path_from_page_cor_tokens(parent, tokens, classname):
    path = parse_path(tokens, auto_add_M=True)
    if classname is not None:
        parent.append("path", {"d": path, "class": classname})

    else:
        parent.append("path", {"d": path})


def graph_cors_to_page_cors(plan, x, y=None):
    if y is None:
        if isinstance(x, Point):
            return x.transform_by(graph_cors_to_page_cors_matrix(plan))

        elif isinstance(x, Complex):
            return Point(x).transform_by(graph_cors_to_page_cors_matrix(plan))
        
        elif isinstance(x, list) and len(x) == 2:
            return Point(x[0], x[1]).transform_by(graph_cors_to_page_cors_matrix(plan))

        else:
            assert False
    
    assert isinstance(x, Real)
    assert isinstance(y, Real)

    m = graph_cors_to_page_cors_matrix(plan)
    v = m.dot([[x], [y], [1]])
    return v[0, 0], v[1, 0]


def graph_x_to_page_x(plan, x):
    return graph_cors_to_page_cors(plan, x, 0)[0]


def graph_y_to_page_y(plan, y):
    return graph_cors_to_page_cors(plan, 0, y)[1]


def add_element_at_graph_cors(parent, plan, element, pt):
    X, Y = graph_cors_to_page_cors(plan, pt.x, pt.y)
    element.translate(X, Y).flatten()
    parent.append(element)


###################################
# format distinct, clip path crap #
###################################


def clip_element(element, clip_path_id):
    element.set_attributes(
        {
            "clip-path": f"url(#{clip_path_id})",
            # 'style': 'clip-rule:nonzero',
            # 'protected': 'yes'
        }
    )


def format_distinct(x1, x2):
    assert x1 != x2

    def format_precision_p(p, x):
        if isinstance(x, int):
            return str(x)
        return f"{x:0.{p}}"

    p = 3
    while format_precision_p(p, x1) == format_precision_p(p, x2):
        p += 1
    return format_precision_p(p, x1), format_precision_p(p, x2)


def trbl_clip_path_id(plan):
    T, R, B, L = trbl_in_page_cors(plan)
    return f"trbl_clip_{ze_2f(T)}_{ze_2f(R)}_{ze_2f(B)}_{ze_2f(L)}"


def trbl_path(plan):
    T, R, B, L = trbl_in_page_cors(plan)
    return bbox2path(L, R, T, B)


def add_trbl_clip_path(root, plan):
    assert isinstance(root, lUnAtIc)
    the_id = trbl_clip_path_id(plan)
    if not root.ze_defs().find(None, {"id": the_id}):
        root.ze_defs().append("clipPath", {"id": the_id}).append("path", {"d": trbl_path(plan)})


#################
# area-painting #
#################


class AreaBlurb:
    def __init__(self, pts, sign):
        for p in pts:
            assert isinstance(p, Point)
        self.__pts = copy.deepcopy(pts)
        self.sign = sign
        self.completed = False
        self.check_integrity()

    def assimilate(self, other):
        assert isinstance(other, AreaBlurb)
        assert other.left() == self.right()
        assert not self.completed
        assert not other.completed
        assert self.sign == other.sign
        for pre, cur in lookback(other):
            if not pre:
                if cur != self.last():
                    self.append(cur)
            else:
                self.append(cur)
        self.check_integrity()

    def last(self):
        return self.__pts[-1]

    def first(self):
        return self.__pts[0]

    def check_integrity(self):
        pts = self.__pts
        sign = self.sign
        assert len(pts) >= 2
        assert type(self.__pts[0].x) in [float, int]
        if sign != 0:
            assert any(c.y * sign > 0 for c in pts)
            assert all(c.y * sign >= 0 for c in pts)
        if sign == 0:
            assert all(c.y == 0 for c in pts)
        assert all(isinstance(p, Point) for p in pts)
        assert all(c != d for c, d in zipit(pts))
        assert all(c.x <= d.x for c, d in zipit(pts))
        assert all(c.x < d.x or d.x < e.x for c, d, e in zipit3(pts))

    def __iter__(self):
        return iter(self.__pts)

    def append(self, c):
        assert isinstance(c, Point)
        assert not self.completed
        pts = self.__pts
        sign = self.sign
        assert c.x > pts[-1].x or c.x == pts[-1].x and c.y != pts[-1].y
        assert c.y * sign >= 0
        assert sign or not c.y
        self.__pts.append(c)

    def left(self):
        return self.__pts[0].x

    def right(self):
        return self.__pts[-1].x

    def complete(self):
        pts = self.__pts
        start = Point(pts[0].x, 0)
        end = Point(pts[-1].x, 0)
        if pts[0].y != 0:
            pts.insert(0, start)
        if pts[-1].y != 0:
            pts.append(end)
        self.completed = True

    def to_page_cor_path(self, plan):
        assert self.completed
        m = graph_cors_to_page_cors_matrix(plan)
        return Path([Point(c).transform_by(m) for c in self.__pts])


def zero_of_segment(a, b):
    assert abs(b.y - a.y) >= SMALL_DIVISION
    one_over_slope = (b.x - a.x) / (b.y - a.y)
    return a.x - a.y * one_over_slope


def minimum_notable_point_on_segment_gt_leq(a, b, x, m):
    m = min(m, b.x)
    y_val = value_of_segment_at(a, b, m)
    if a.y * b.y < 0:
        zero = zero_of_segment(a, b)
        assert a.x < zero < b.x
        if x < zero < m:
            m = zero
            y_val = 0
    return m, y_val


def next_area_blurb_in_linepath(plan, pts, x1, end):
    a, b = segment_whose_half_open_x_interval_contains(pts, x1)
    y1 = value_of_segment_at(a, b, x1)
    x2, y2 = minimum_notable_point_on_segment_gt_leq(a, b, x1, end)
    assert x1 < x2 <= end
    assert y1 * y2 >= 0
    sign = 0
    if y1 > 0 or y2 > 0:
        sign = 1
    if y1 < 0 or y2 < 0:
        sign = -1
    corners = [Point(z) for z in [[x1, y1], [x2, y2]]]
    return AreaBlurb(pts=corners, sign=sign)


def get_next_area_blurb_helper(plan, graph, left, end):
    assert left < end
    if graph["type"] == "linepath":
        return next_area_blurb_in_linepath(plan, graph["pts"], left, end)
    else:
        assert False


def get_next_area_blurb_going_no_further_than(plan, graph, left, end):
    big_blurb = None
    while left < end:
        blurb = get_next_area_blurb_helper(plan, graph, left, end)
        if big_blurb:
            assert blurb.left() == left
            assert big_blurb.right() == blurb.left()
        if big_blurb and big_blurb.sign != blurb.sign:
            break
        if big_blurb:
            big_blurb.assimilate(blurb)
        else:
            big_blurb = blurb
        left = big_blurb.right()
    big_blurb.complete()
    return big_blurb


def name_graph_area_blurb(graph, blurb):
    left_str, right_str = format_distinct(blurb.left(), blurb.right())
    return graph["name"] + " area " + left_str + "---" + right_str


def path_overlaps_trbl_window(plan, path):
    return path.crosses_interior_of(trbl_path(plan))


def plot_graph_area(root, plan, graph, start, end):
    assert start < end
    end = min(max([c.x for c in graph["pts"]]), end)
    start = max(min([c.x for c in graph["pts"]]), start)
    assert start < end
    left = start
    while left < end:
        blurb = get_next_area_blurb_going_no_further_than(plan, graph, left, end)
        assert blurb.left() == left
        assert blurb.left() < blurb.right() <= end
        left = blurb.right()
        if blurb.sign == 0:
            continue
        blurb_windowized = blurb.to_page_cor_path(plan)
        if not path_overlaps_trbl_window(plan, blurb_windowized):
            continue
        layer_name = name_graph_area_blurb(graph, blurb)
        layer = root.append(new_layer(plan, layer_name))
        assert layer is not None
        style = "areas-positive" if blurb.sign > 0 else "areas-negative"
        child = layer.append("path", {"d": blurb_windowized, "class": style})
        clip_element(child, trbl_clip_path_id(plan))


def add_areas(root, plan):
    for graph in plan["graphs"]:
        if "areas" in graph:
            for a in graph["areas"]:
                intvl = Interval(a["delims"])
                plot_graph_area(root, plan, graph, intvl.lo, intvl.hi)


################
# graph-making #
################


def add_dot(parent, plan, pt, ecr, classname):
    x, y = graph_cors_to_page_cors(plan, pt.x, pt.y)
    ecr_scaled = ecr * scaled_x_unit(plan)
    parent.append(svg_circle({"cx": x, "cy": y, "r": ecr_scaled, "class": classname}))


def radian_angle(x, y):
    return arctan2(y, x)


def ecr_in_direction_of(plan, A, B, ecr):
    assert A is not None and B is not None
    assert A.x != B.x
    ux = scaled_x_unit(plan)
    uy = scaled_y_unit(plan)
    AB_in_page_cors_unit = (B - A).scaled(ux, uy).normalized()
    ecr_in_page_cors = ecr * ux
    return A + ((AB_in_page_cors_unit * ecr_in_page_cors).scaled(1 / ux, 1 / uy))


def mr_linepath_diagnostic(prev, cur, next):
    assert prev is None or prev.x < cur.x or prev.y != cur.y
    assert next is None or next.x > cur.x or next.y != cur.y
    diagnostics = [
        p[1]
        for p in [
            (not prev, "left-endpoint"),
            (not next, "right-endpoint"),
            (prev and prev.x == cur.x, "left-discontinuous"),
            (next and next.x == cur.x, "right-discontinuous"),
            (prev and next and prev.x < cur.x < next.x, "continuous"),
        ]
        if p[0]
    ]
    assert len(diagnostics) == 1
    return diagnostics[0]


def value_of_segment_at(a, b, x):
    assert abs(b.x - a.x) >= SMALL_DIVISION
    for z in [a, b]:
        if x == z.x:
            return z.y
    slope = (b.y - a.y) / (b.x - a.x)
    return a.y + slope * (x - a.x)


def segment_whose_half_open_x_interval_contains(pts, x):
    for a, b in zipit(pts):
        if a.x <= x < b.x:
            return a, b
    raise ValueError


def value_of_linepath_at(pts, x):
    try:
        a, b = segment_whose_half_open_x_interval_contains(pts, x)
        return value_of_segment_at(a, b, x)
    except ValueError:
        if x == pts[-1].x:
            return pts[-1].y
        raise ValueError


def the_next_function_is_currently_unused_haha(yurmom):
    pass


def linepath_truncated_to_interval(pts, left, right):
    assert left < right
    newpts = [c for c in pts if left <= c.x <= right]
    if len(newpts) == 0 or newpts[0].x != left:
        try:
            y = value_of_linepath_at(pts, left)
            newpts.insert(0, Point(left, y))
        except ValueError:
            pass
    if newpts[-1].x != right:
        try:
            y = value_of_linepath_at(pts, right)
            newpts.append(Point(right, y))
        except ValueError:
            pass
    check_linepath_integrity(newpts)
    return newpts


def check_linepath_integrity(pts):
    assert len(pts) >= 2
    for pre, cur, nex in lookbothways(pts):
        if nex:
            if cur.x > nex.x:
                print(f"\ncur.x {cur.x} {nex.x}")
            assert cur.x <= nex.x
            if cur == nex:
                print(cur)
            assert cur != nex
        if nex and pre:
            assert pre.x != cur.x or cur.x != nex.x
        if not nex:
            assert pre.x != cur.x
        if not pre:
            assert nex.x != cur.x


def plot_dotplot(parent, plan, graph):
    ecr = graph["ecr"]
    dot_style = graph['classname']
    for p in graph["pts"]:
        if p.inside_trbl(*trbl(plan)):
            add_dot(parent, plan, p, ecr, dot_style)


def plot_glowdots(parent, plan, graph):
    for i, cors in enumerate(graph["data"]):
        cx, cy = graph_cors_to_page_cors(plan, cors[0], cors[1])
        parent.append(
            svg_circle(
                {
                    "cx": cx,
                    "cy": cy,
                    "r": graph["inner-ecr-in-abs-units"],
                    "fill": graph["inner-color"][i],
                }
            )
        )
        parent.append(
            svg_circle(
                {
                    "cx": cx,
                    "cy": cy,
                    "r": graph["outer-ecr-in-abs-units"],
                    "fill": graph["outer-color"][i],
                }
            )
        )


def plot_thick_tangents(parent, plan, graph):
    xs = graph["data"]
    ys = graph["function"]
    slopes = graph["derivative"]
    thicknesses = graph["thickness"]
    lengths = graph["length"]
    colors = graph["color"]
    graph_thickness = graph["function-stroke-width-in-abs-units"]
    assert len(ys) >= len(xs)
    assert len(slopes) >= len(xs)
    assert len(thicknesses) >= len(xs)
    assert len(lengths) >= len(xs)
    assert len(lengths) >= len(xs)
    for x, y, slope, thickness, length, color in zip(
        xs, ys, slopes, thicknesses, lengths, colors
    ):
        abs_x, abs_y = graph_cors_to_page_cors(plan, x, y)
        abs_slope = graph_cors_slope_to_page_slope(plan, slope)
        perp = -1 / abs_slope
        sign = -1 if graph["above-or-below"] == "above" else 1
        o = (
            Point(abs_x, abs_y)
            + Point(1, -perp) * sign * (thickness + graph_thickness) * 0.5
        )
        s = o - Point(1, -abs_slope).normalized(length=length / 2)
        e = o + Point(1, -abs_slope).normalized(length=length / 2)
        parent.append(
            svg_line(
                {
                    "x1": s.x,
                    "y1": s.y,
                    "x2": e.x,
                    "y2": e.y,
                    "stroke-width": thickness,
                    "stroke": color,
                }
            )
        )


def plot_anchor_dotted_line(parent, plan, graph):
    s_page = graph_cors_to_page_cors(plan, graph['s'])
    e_page = graph_cors_to_page_cors(plan, graph['e'])
    assert e_page.x > s_page.x
    page_distance = (e_page - s_page).norm()
    ecr_page = graph["ecr-in-abs-units"]
    num_dots = int(page_distance / ecr_page / 3.6)
    abs_x_spacing = (e_page.x - s_page.x) / num_dots
    assert abs_x_spacing > 1
    l = graph_x_to_page_x(plan, graph["x-range"][0])
    r = graph_x_to_page_x(plan, graph["x-range"][1])
    slope = (e_page.y - s_page.y) / (e_page.x - s_page.x)

    def abs_to_abs_f(x):
        return s_page.y + (x - s_page.x) * slope

    if r < e_page.x:
        print("graph['s']:", graph['s'])
        print("graph['e']:", graph['e'])
        print("graph['x-range'][1]:", graph["x-range"][1])
        print("r", r)
        print("e_page:", e_page)
        print("")
        print("plan[defs]:")
        for z in plan['defs']:
            print("   ", z)
        # are you reading these words?
        # right here? (that's me!)
        # just execute mk_graph.py again, this sometimes
        # goes away on its own
        assert False

    assert l <= s_page.x
    assert r >= e_page.x

    g = svg_g({"class": "an_anchor_dotted_line"})
    if graph.get('classname', None) is not None:
        g.add_class(graph['classname'])
    i = 0
    while True:

        def append_circle_at_x(x):
            y = abs_to_abs_f(x)
            g.append(svg_circle({"cx": x, "cy": y, "r": ecr_page}))

        x_right = s_page.x + i * abs_x_spacing
        x_left = s_page.x - (i + 1) * abs_x_spacing
        if x_left < l and x_right > r:
            break
        if x_right <= r:
            append_circle_at_x(x_right)
        if x_left >= l:
            append_circle_at_x(x_left)
        i += 1
    parent.append(g)


def plot_eval(parent, plan, graph):
    formula = graph["data"]
    assert isinstance(formula, str)
    num_steps = graph["num-steps"]
    assert num_steps >= 1
    t, r, b, l = trbl(plan)
    y_interval = Interval(b, t)

    def f(x):
        return eval(formula)

    tokens = []
    prev = Point(l, f(l))
    i = 1
    while i <= num_steps:
        x = l + (i / num_steps) * (r - l)
        next = Point(x, f(x))
        if y_interval.contains_any(prev.y, next.y):
            if len(tokens) == 0 or tokens[-1] is not prev:
                tokens.append("M")
                tokens.append(prev)
            tokens.append(next)
        prev = next
        i += 1

    add_path_from_graph_cor_tokens(parent, plan, tokens, graph['classname'])
    
    
def plot_parameterized_cubic_bezier(parent, plan, graph):
    assert 'ts' in graph
    ts = graph['ts']

    assert len(ts) >= 2
    assert all(isinstance(t, Real) for t in ts)

    assert 'x-function' in graph
    assert 'y-function' in graph
    assert 'x-derivative' in graph
    assert 'y-derivative' in graph
    assert 'function' not in graph
    assert 'derivative' not in graph

    ## x_function and x_prime
    x_function_string = graph['x-function']
    assert 'x' not in x_function_string or 'exp' in x_function_string, "\nplease use t as a variable, not x\n"
    assert isinstance(x_function_string, str)
    x_function = eval("(lambda t:" + x_function_string + ")")
    if isinstance(graph['x-derivative'], str):
        x_derivative_string = graph['x-derivative']
        assert 'x' not in x_derivative_string or 'exp' in x_derivative_string, "\nplease use t as a variable, not x\n"
        x_prime = eval("(lambda t: " + x_derivative_string + ")")
    else:
        assert isinstance(graph['x-derivative'], Real)
        h = graph['x-derivative']
        x_prime = (lambda t: (x_function(t+h) - x_function(t-h)) / (2*h))

    ## y_function and y_prime
    y_function_string = graph['y-function']
    assert 'x' not in y_function_string or 'exp' in y_function_string, "\nplease use t as a variable, not x\n"
    assert isinstance(y_function_string, str)
    y_function = eval("(lambda t:" + y_function_string + ")")
    if isinstance(graph['y-derivative'], str):
        y_derivative_string = graph['y-derivative']
        assert 'x' not in y_derivative_string or 'exp' in y_derivative_string, "\nplease use t as a variable, not x\n"
        y_prime = eval("(lambda t: " + y_derivative_string + ")")
    else:
        assert isinstance(graph['y-derivative'], Real)
        h = graph['y-derivative']
        y_prime = (lambda t: (y_function(t+h) - y_function(t-h)) / (2*h))

    tokens = []
    for t0, t3 in zip(ts, ts[1:]):
        tdif = (t3 - t0) / 3

        x0 = x_function(t0)
        x3 = x_function(t3)
        x1 = x0 + x_prime(t0) * tdif
        x2 = x3 - x_prime(t3) * tdif

        y0 = y_function(t0)
        y3 = y_function(t3)
        y1 = y0 + y_prime(t0) * tdif
        y2 = y3 - y_prime(t3) * tdif

        c0 = Point(x0, y0)
        c1 = Point(x1, y1)
        c2 = Point(x2, y2)
        c3 = Point(x3, y3)

        if len(tokens) == 0:
            tokens.extend(['M', c0, 'C'])
        tokens.extend([c1, c2, c3])
        
    add_path_from_graph_cor_tokens(parent, plan, tokens, graph['classname'])

    marked_ts = graph['marked-ts']
    
    tokens = []
    for t in marked_ts:
        tx = x_prime(t)
        ty = y_prime(t)
        c = Point(x_function(t), y_function(t))
        delta = Point(-ty, tx).normalized() * graph['marked-ts-size']
        tokens.extend(['M', c - delta, 'L', c + delta])
    
    if len(tokens) > 0:
        add_path_from_graph_cor_tokens(parent, plan, tokens, graph['marked-ts-classname'])


def plot_cubic_bezier(parent, plan, graph):
    assert 'xs' in graph
    xs = graph['xs']
    
    assert len(xs) >= 2
    assert all(isinstance(x, Real) for x in xs)

    assert 'function' in graph
    assert 'derivative' in graph

    function_string = graph['function']
    assert isinstance(function_string, str)
    f = eval("(lambda x: " + function_string + ")")

    if isinstance(graph['derivative'], str):
        derivative_string = graph['derivative']
        f_prime = eval("(lambda x: " + derivative_string + ")")

    else:
        assert isinstance(graph['derivative'], Real)
        h = graph['derivative']
        f_prime = (lambda x: (f(x+h) - f(x-h)) / (2 * h))

    tokens = []
    for x0, x3 in zip(xs, xs[1:]):
        dif = (x3 - x0) / 3
        x1 = x0 + dif
        x2 = x3 - dif
        y0 = f(x0)
        y3 = f(x3)
        y1 = y0 + f_prime(x0) * dif
        y2 = y3 - f_prime(x3) * dif
        c0 = Point(x0, y0)
        c1 = Point(x1, y1)
        c2 = Point(x2, y2)
        c3 = Point(x3, y3)
        if len(tokens) == 0:
            tokens.extend(['M', c0, 'C'])
        tokens.extend([c1, c2, c3])
    add_path_from_graph_cor_tokens(parent, plan, tokens, graph['classname'])


def equally_spaced(l, r, n):
    pass


def plot_x_squared(parent, plan, graph):
    n = graph["num-cubics"]
    l, r = l_and_r(plan)
    step = (r - l) / n
    xs = []
    for i in range(n):
        xs.append(l + i * step)
    xs.append(r)
    for i, _ in enumerate(xs[1:], 1):
        assert xs[i] > xs[i - 1]

    def f(x):
        return x * x

    def f_prime(x):
        return 2 * x

    def tangent(x):
        return Point(1, f_prime(x))

    subpath = Subpath()

    for j, x in enumerate(xs[1:], 1):
        x0 = xs[j - 1]
        x1 = xs[j]
        X0 = Point(x0, f(x0))
        X1 = Point(x1, f(x1))
        A = Point(x0 + (x1 - x0) / 3)
        B = Point(x0 + 2 * (x1 - x0) / 3)
        C0 = four_way_intersection(X0, tangent(x0), A, Point(0, 1))
        C1 = four_way_intersection(X1, tangent(x1), B, Point(0, 1))
        subpath.append(CubicBezier(start=X0, control1=C0, control2=C1, end=X1))

    m = graph_cors_to_page_cors_matrix(plan)
    subpath.transform(m)
    print(subpath)
    parent.append("path", {"d": subpath, "class": graph['classname']})


def plot_linepath(parent, plan, graph, left_full=False):
    ecr = graph["ecr"]
    dots = []
    tokens = []

    for prev, cur, next in lookbothways(graph["pts"]):
        # whats_happening
        whats_happening = mr_linepath_diagnostic(prev, cur, next)
        assert whats_happening in [
            "left-endpoint",
            "left-discontinuous",
            "right-endpoint",
            "right-discontinuous",
            "continuous",
        ]

        # adding dot
        if whats_happening != 'continuous':
            dot_classname = graph[whats_happening + "-dot-classname"]
            dots.append([cur, dot_classname])

        # pos and extending tokens
        pos = cur
        if dot_classname != "none":
            if whats_happening.startswith("left-"):
                pos = ecr_in_direction_of(plan, cur, next, ecr)

            else:
                pos = ecr_in_direction_of(plan, cur, prev, ecr)

        if whats_happening.startswith("left-"):
            tokens.extend(["M", pos])

        else:
            tokens.extend([pos])

    add_path_from_graph_cor_tokens(parent, plan, tokens, graph['classname'])

    for (pos, classname) in dots:
        add_dot(parent, plan, pos, ecr, classname)


def graph_plot_dispatcher(root, parent, plan, graph):
    if "own-layer" in graph:
        parent = new_layer(plan, graph["own-layer"])
        root.append(parent)

    if graph["clip"]:
        parent = parent.append("g", {})
        clip_element(parent, trbl_clip_path_id(plan))

    if graph["type"] == "linepath":
        plot_linepath(parent, plan, graph)

    elif graph["type"] == "dotplot":
        plot_dotplot(parent, plan, graph)

    elif graph["type"] == "x squared":
        plot_x_squared(parent, plan, graph)

    elif graph["type"] == "eval":
        plot_eval(parent, plan, graph)

    elif graph["type"] == "anchor dotted line":
        print("\nabout to plot an anchor dotted line!\n")
        plot_anchor_dotted_line(parent, plan, graph)

    elif graph["type"] == "glowdots":
        plot_glowdots(parent, plan, graph)

    elif graph["type"] == "thick tangents":
        plot_thick_tangents(parent, plan, graph)

    elif graph['type'] == 'cubic bezier':
        plot_cubic_bezier(parent, plan, graph)

    elif graph['type'] == 'parameterized cubic bezier':
        plot_parameterized_cubic_bezier(parent, plan, graph)

    else:
        assert False


def add_graphs_to_parent(root, parent, plan, above_grid):
    # badly named function; search for 'own-layer' to see what I mean
    # assert len(plan['graphs']) > 0
    root.append(parent)
    for graph in plan["graphs"]:
        if graph.get("beneath-grid", False) == above_grid:
            continue
        if 'only-in' not in graph or plan['graph-no'] in graph['only-in']:
            graph_plot_dispatcher(root, parent, plan, graph)

    if len(parent) == 0:
        parent.detach()


def add_beneath_grid_graphs(root, plan):
    parent = new_layer(plan, "pre-grid graphs")
    add_graphs_to_parent(root, parent, plan, False)


def add_above_grid_graphs(root, plan):
    name = "post-grid graphs" if any(g.get("beneath-grid", False) for g in plan["graphs"]) else "graphs"
    parent = new_layer(plan, name)
    add_graphs_to_parent(root, parent, plan, True)


###############
# hand-dashes #
###############


g_unique_ids_dict = {}


def unique_id(prefix):
    if prefix in g_unique_ids_dict:
        g_unique_ids_dict[prefix] += 1

    else:
        g_unique_ids_dict[prefix] = 0

    return prefix + str(g_unique_ids_dict[prefix])


def add_dashes_data(root, filename):
    if filename in g_dashsets:
        # there was a side effect from the first time around that we 
        # to recreate now:
        defs = root.get_or_create_defs()
        for path in g_dashsets[filename]['defs_elements']:
            defs.append(path, reattach=True)
        return g_dashsets[filename]

    if "dashes" not in filename:
        print(f"\nmk_graph might overwrite {filename} so would rather see 'dashes' in this file name\n")
        assert False

    load_root = load_lUnAtIc(filename)
    dashset = []
    gs = []

    have_to_reprint_file = False

    if load_root.worth_flattening():
        load_root.flatten()
        have_to_reprint_file = True
    assert not load_root.worth_flattening()

    node = load_root
    while node is not None:
        if (
            node.type == "g"
            and len(node) == 4
            and node[0].matches('path')
            and node[1].matches('rect', {'class': 'bbox'})
            and node[2].matches('circle', {'class': 'lower_mid'})
            and node[3].matches('circle', {'class': 'upper_mid'})
        ):
            gs.append(node)
            bbox = node[1].bbox()
            for circ in [node[2], node[2]]:
                assert float(circ["cx"]) == bbox.mid_x()
                assert bbox.ymin <= float(circ["cy"]) <= bbox.ymax
            assert float(node[2]["cy"]) < float(node[3]["cy"])
            dashset.append({
                "object": parse_path(node[0]["d"]),
                "bbox": bbox,
                "lower_mid": float(node[2]["cy"]),
                "upper_mid": float(node[3]["cy"]),
            })
            node = node.next_node_outside_subtree()
            continue

        if node.type == "path":
            have_to_reprint_file = True
            black_node = node.dressed_in_black()
            bbox = black_node.bbox()
            intersections_pairs, ray = black_node['d'].xray(bbox.mid_x())
            assert len(intersections_pairs) == 2
            c1 = Point(ray.point(intersections_pairs[0][1]))
            c2 = Point(ray.point(intersections_pairs[1][1]))
            assert c1.x == bbox.mid_x()
            assert c2.x == bbox.mid_x()
            if c1.y > c2.y:
                c1, c2 = c2, c1
            assert c1.y < c2.y
            dashset.append({
                "object": parse_path(node["d"]),
                "bbox": bbox,
                "lower_mid": c1.y,
                "upper_mid": c2.y,
            })
            xmin, xmax, ymin, ymax = bbox.xmin, bbox.xmax, bbox.ymin, bbox.ymax
            rect = lUnAtIc(f"rect.bbox.x={xmin}.y={ymin}.width={xmax - xmin}.height={ymax - ymin}")
            circle1 = lUnAtIc(f"circle.lower_mid.cx={c1.x}.cy={c1.y}.r={1}")
            circle2 = lUnAtIc(f"circle.upper_mid.cx={c2.x}.cy={c2.y}.r={1}")
            new_g = svg_g({}, [black_node, rect, circle1, circle2])
            gs.append(new_g)
            node.replace_with(new_g)
            node = new_g.next_node_outside_subtree()
            continue

        node = node.next()

    if have_to_reprint_file:
        outroot = an_svg("inkscape")
        outroot.update_css("""
            .bbox {fill:none; stroke:#F00;opacity:0.5;stroke-width:1;}
            .lower_mid {fill:#0F0;opacity:0.5;stroke:none;}
            .upper_mid {fill:#0F0;opacity:0.5;stroke:none;}
        """)
        outroot.append_all(gs, reattach=True)
        outroot.pretty_up().print_to(filename)

    assert len(dashset) > 0
    assert all(dash['lower_mid'] < dash['upper_mid'] for dash in dashset)

    to_append_to_defs = []
    for dash in dashset:
        x = dash['bbox'].mid_x()
        y = dash['lower_mid']
        assert dash['bbox'].project('y').contains(y)
        dash['length'] = dash['upper_mid'] - dash['lower_mid']
        dash['id'] = unique_id('dash')
        dash['object'].translate(-x, -y)
        to_append_to_defs.append(svg_path({'id': dash['id'], 'd': dash['object']}))

    avg_width = sum(dash['bbox'].width() for dash in dashset) / len(dashset)
    avg_length = sum(dash['length'] for dash in dashset) / len(dashset)

    defs = root.get_or_create_defs()
    for thing in to_append_to_defs:
        defs.append(thing)

    g_dashsets[filename] = {
        "dash_collection": dashset,
        "avg_width": avg_width,
        "avg_length": avg_length,
        "defs_elements": to_append_to_defs
    }

    return g_dashsets[filename]


def add_hand_dashes(root, plan):
    if "hand-dashes" not in plan:
        return

    layer = new_layer(plan, "hand dashes")

    for line in plan["hand-dashes"]:
        assert "start" in line
        assert "end" in line
        assert isinstance(line["start"], Point)
        assert isinstance(line["end"], Point)
        length = (line["end"] - line["start"]).norm()
        sep = line["sep"]
        phase = line["phase"]

        dashes_data = add_dashes_data(root, line["path"] + line["filename"])
        if isinstance(line["length"], Real):
            l_factor = line["length"] / dashes_data["avg_length"]

        else:
            assert line["length"] == "inferred"
            assert isinstance(line["width"], Real)
            l_factor = line["width"] / dashes_data["avg_width"]

        if isinstance(line["width"], Real):
            w_factor = line["width"] / dashes_data["avg_width"]

        else:
            assert line["width"] == "inferred"
            assert isinstance(line["length"], Real)
            w_factor = line["length"] / dashes_data["avg_length "]

        dash_collection = dashes_data["dash_collection"]
        assert isinstance(dash_collection, list)
        assert len(dash_collection) > 0

        assert sep > 0
        a = []
        zoobey = 0
        while len(a) == 0 or zoobey < length:
            new_index = randrange(0, len(dash_collection))
            new_dash = dash_collection[new_index]
            new_length = new_dash['length'] * l_factor
            if len(a) == 0 and phase != 0:
                zoobey = phase * new_length
            assert new_length > 0
            a.append({
                "start": zoobey + sep,
                "end": zoobey + sep + new_length,
                "index": new_index,
            })
            zoobey = a[-1]["end"]

        group_transform = generate_transform(
            graph_cors_to_page_cors_transform_tokens(plan) + ['translate', line['start'], 'rotate', (line['end'] - line['start']).arg_in_degrees() - 90]
        )

        g = lUnAtIc(f'g.hand_dashes.transform={group_transform}')

        assert len(a) > 0
        for b in a:
            assert isinstance(b["start"], Real)
            local_transform = generate_transform("translate", 0, b["start"], "scale", w_factor, l_factor)
            g.append(svg_use({
                "xlink:href": "#" + dash_collection[b["index"]]["id"],
                "transform": local_transform,
            }))

        layer.append(g)

    if len(layer) > 0:
        root.append(layer)


#################
# mathjax stuff #
#################


def math_label_finish_initialization(plan, label):
    label.x_middle_fudge_factor = plan["x-middle-fudge-%"]
    label.middle_number_fudge_factor = plan["number-middle-fudge-%"]
    label.additional_scaling *= font_scaling(plan)    


def math_label_render_and_append(root, label, parent, include_labelbox, labelbox_classname):
    assert isinstance(label, MathLabel)
    assert isinstance(include_labelbox, bool)

    g, bbox_subpath = get_or_queue_MathLabel(root, label)

    if g is None:
        return
    
    parent.append(g)
    if include_labelbox:
        parent.append('path', {'d': bbox_subpath, 'class': labelbox_classname})


#########################
# document manipulation #
#########################


def area_layers_of(root, verbose=False):
    elements = []
    for child in root:
        if "inkscape:label" in child:
            if "area" in child["inkscape:label"]:
                elements.append(child)
    if verbose:
        print("found %d area layers" % len(elements))
    return elements


###############
# annotations #
###############


def add_annotations(root, plan):
    parent = new_layer(plan, "annotations")
    annots = plan["annotations"]
    for a in annots:
        anchor = graph_cors_to_page_cors(plan, Point(a['x'], a['y']))
        local_scaling = plan["annotations-font-scaling-factor"] * a["individual-font-scaling-factor"]
        alfie = MathLabel(
            a["string"],
            fontpx=g_fontpx,
            fontname=g_fontname,
            classname="axes-labels",
            translation=anchor,
            h_align=a["h-alignment"],
            v_align=a["v-alignment"],
            margins_in_exes=[0, 0, 0, 0],
            angle=a["angle"],
            additional_scaling=local_scaling,
            alternate_glyphs=a["alternate-glyphs"],
        )
        math_label_finish_initialization(plan, alfie)
        math_label_render_and_append(root, alfie, parent, a['include-labelbox'], 'annotation-labelbox')

    if len(parent) > 0:
        root.append(parent)


#################
# yellow arrows #
#################


def full_arrow_path_by_total_length(nose_half_angle, lip, half_width, total_length):
    x1 = cot_deg(nose_half_angle) * (lip + half_width)
    assert total_length > x1
    x2 = total_length
    y1 = half_width
    y2 = lip + half_width
    return parse_subpath('M', 0, 0, 'L', x1, y2, 'L', x1, y1, 'L', x2, y1, 'L', x2, -y1, 'L', x1, -y1, 'L', x1, -y2, 'Z')


def full_arrow_path_by_stem_length(nose_half_angle, lip, half_width, stem_length):
    x1 = cot_deg(nose_half_angle) * (lip + half_width)
    x2 = x1 + stem_length
    y1 = half_width
    y2 = lip + half_width
    return parse_subpath('M', 0, 0, 'L', x1, y2, 'L', x1, y1, 'L', x2, y1, 'L', x2, -y1, 'L', x1, -y1, 'L', x1, -y2, 'Z')


def add_fat_vector_arrows(root, plan):
    if "fat-vector-arrows" not in plan:
        return
    
    layer = new_layer(plan, "fat vector arrows")

    for a in plan['fat-vector-arrows']['list']:
        start = graph_cors_to_page_cors(plan, a['start'])
        end = graph_cors_to_page_cors(plan, a['end'])
        lip = a['page-cors-lip']
        half_width = a['page-cors-half-width']
        nose_half_angle = a['nose-half-angle']
        length = (end - start).norm()
        d = full_arrow_path_by_total_length(nose_half_angle, lip, half_width, length)
        angle = (end - start).arg_in_degrees()
        d.rotate(180 + angle).translate(end)
        layer.append(svg_path({'d': d, 'class': a['classname']}))

    if len(layer) > 0:
        root.append(layer)


def add_yellow_arrows(root, plan):
    if "yellow-arrows" not in plan:
        return

    layer = new_layer(plan, "yellow arrows")

    for a in plan["yellow-arrows"]["list"]:
        lip = a["page-cors-lip"]
        half_width = a["page-cors-half-width"]
        stem = a["page-cors-stem-length"]

        print("half_width:", half_width)

        scaling_factor = a["scaling-factor"]
        stem *= scaling_factor
        lip *= scaling_factor
        half_width *= scaling_factor
        nose_half_angle = a["nose-half-angle"]

        d = full_arrow_path_by_stem_length(nose_half_angle, lip, half_width, stem)
        tip_x, tip_y = graph_cors_to_page_cors(plan, a["x"], a["y"])
        d.rotate(-a['angle']).translate(tip_x, tip_y)
        layer.append(svg_path({"d": d, "class": a['classname']}))

    if len(layer) > 0:
        root.append(layer)


def add_svg_fragments(root, plan):
    for z in plan["svg-fragments"]:
        el = z["element"]
        if el.find("defs") is not None:
            defs = el.find_unique("defs").detach()
            root.defs().merge_with_children_of_by_id(defs)
        root.append(el, reattach=True)


##############
# add styles #
##############


def add_dots_to_key_names(yo):
    ya = {}
    for k in yo:
        assert k[0] != "."
        ya["." + k] = yo[k]
    return ya


def add_styles(root, plan):
    styles_with_dots = add_dots_to_key_names(plan["styles"])
    root.add_css_styles(styles_with_dots)


def add_root_style(root, plan):
    if "style" in plan:
        assert "style" not in root
        root["style"] = plan["style"]


###############
# add_viewbox #
###############


def recompute_viewbox(root, plan):
    ux, uy = scaled_x_y_units(plan)
    vb = plan["viewbox"]
    comp = vb["computed"] = {}

    root_bbox = root.bbox()
    grid_bbox = BoundingBox(0, lr(plan).length() * ux, 0, bt(plan).length() * uy)

    if root_bbox.ymin is None:
        print("")
        print("###########################")
        print("# WARNING root_boox is empty; using grid_bbox as replacement")
        print("# (this can be caused by clipped graphs, that .bbox() skips in its computation, + no axes + no grid)")
        print("###########################")
        print("")
        root_bbox = grid_bbox

    if "page-cor-width-around-grid-mid" in vb:
        w = vb["page-cor-width-around-grid-mid"]
        setfresh(comp, "x", grid_bbox.mid_x() - w / 2)
        setfresh(comp, "width", w)

    if "page-cor-width-around-elements-mid" in vb:
        w = vb["page-cor-width-around-elements-mid"]
        setfresh(comp, "x", root_bbox.mid_x() - w / 2)
        setfresh(comp, "width", w)

    if "page-cor-margin-bottom-top-from-elements" in vb:
        m = vb["page-cor-margin-bottom-top-from-elements"]
        setfresh(comp, "y", root_bbox.ymin - m)
        setfresh(comp, "height", root_bbox.height() + 2 * m)

    if "page-cor-margin-top-from-elements" in vb:
        m = vb["page-cor-margin-top-from-elements"]
        setfresh(comp, "y", root_bbox.ymin - m)

    if "page-cor-margin-top-from-grid" in vb:
        m = vb["page-cor-margin-top-from-grid"]
        setfresh(comp, "y", grid_bbox.ymin - m)

    if "page-cor-margin-bottom-from-elements" in vb:
        assert "y" in comp
        m = vb["page-cor-margin-bottom-from-elements"]
        setfresh(comp, "height", root_bbox.ymax + m - comp["y"])

    if "page-cor-margin-left-from-elements" in vb:
        m = vb["page-cor-margin-left-from-elements"]
        setfresh(comp, "x", root_bbox.xmin - m)

    if "page-cor-margin-left-from-grid" in vb:
        m = vb["page-cor-margin-left-from-grid"]
        setfresh(comp, "x", grid_bbox.xmin - m)

    if "page-cor-margin-right-from-elements" in vb:
        assert "x" in comp
        m = vb["page-cor-margin-right-from-elements"]
        setfresh(comp, "width", root_bbox.xmax + m - comp["x"])

    if "page-cor-margin-right-from-grid" in vb:
        assert "x" in comp
        m = vb["page-cor-margin-right-from-grid"]
        setfresh(comp, "width", grid_bbox.xmax + m - comp["x"])

    if "page-cor-margin-left-right-from-grid" in vb:
        m = vb["page-cor-margin-left-right-from-grid"]
        setfresh(comp, "x", grid_bbox.xmin - m)
        setfresh(comp, "width", grid_bbox.width() + 2 * m)

    if "page-cor-margin-bottom-from-grid" in vb:
        assert "y" in comp
        m = vb["page-cor-margin-bottom-from-grid"]
        setfresh(comp, "height", grid_bbox.ymax + m - comp["y"])

    if "page-cor-margin-bottom-top-from-grid" in vb:
        m = vb["page-cor-margin-bottom-top-from-grid"]
        setfresh(comp, "y", grid_bbox.ymin - m)
        setfresh(comp, "height", grid_bbox.height() + 2 * m)

    if "page-cor-margin-trbl-from-elements" in vb:
        m = vb["page-cor-margin-trbl-from-elements"]
        setfresh(comp, 'x', root_bbox.xmin - m[3])
        setfresh(comp, 'y', root_bbox.ymin - m[0])
        setfresh(comp, 'width', root_bbox.width() + m[1] + m[3])
        setfresh(comp, 'height', root_bbox.height() + m[0] + m[2])

    if "page-cor-margin-trbl-from-grid" in vb:
        m = vb["page-cor-margin-trbl-from-grid"]
        setfresh(comp, 'x', grid_bbox.xmin - m[3])
        setfresh(comp, 'y', grid_bbox.ymin - m[0])
        setfresh(comp, 'width', grid_bbox.width() + m[1] + m[3])
        setfresh(comp, 'height', grid_bbox.height() + m[0] + m[2])

    if not all(x in comp for x in ["x", "y", "width", "height"]):
        print('')
        print("comp:", comp)
        print("vb:", vb)
        print('')
        assert False


def add_viewbox_element(root, plan):
    if "viewbox" in root and plan["viewbox"]["include"]:
        root.add_current_viewbox(classname="viewbox")


def add_viewbox_attribute(root, plan):
    recompute_viewbox(root, plan)
    computed = plan["viewbox"]["computed"]
    root["viewbox"] = [
        computed["x"], 
        computed["y"], 
        computed["width"], 
        computed["height"]
    ]


def complete_width_height_pair_from_computed_viewbox(plan, w, h):
    assert w is None or w > 0
    assert h is None or h > 0

    computed_width = plan["viewbox"]["computed"]["width"]
    computed_height = plan["viewbox"]["computed"]["height"]

    assert computed_width > 0 and computed_height > 0

    if w and not h:
        h = w * computed_height / computed_width

    if h and not w:
        w = h * computed_width / computed_height

    if not w and not h:
        w = computed_width
        h = computed_height

    assert w > 0 and h > 0

    return w, h


def add_width_and_height_attributes_to_root(root, plan):
    w = plan.get("width", None)
    h = plan.get("height", None)
    w, h = complete_width_height_pair_from_computed_viewbox(plan, w, h)
    root["width"] = w
    root["height"] = h


def add_background_color(root, plan):
    if "background-color" in plan:
        computed = plan["viewbox"]["computed"]
        rect = svg_rect(
            {
                "x": computed["x"],
                "y": computed["y"],
                "width": "100%",
                "height": "100%",
                "fill": plan["background-color"],
            }
        )
        root.insert_after_last_of(rect, "defs", "style")


def add_pinholes(root, plan):
    if "pinholes" not in plan:
        return
    recompute_viewbox(root, plan)
    ux = scaled_x_unit(plan)

    pinholes = plan["pinholes"]
    holes = pinholes["holes"]
    r = pinholes["r"]

    assert len(holes) > 0
    assert "background-color" not in plan

    computed = plan["viewbox"]["computed"]
    tl = Point(computed["x"], computed["y"])
    w = computed["width"]
    h = computed["height"]

    cover_path = parse_path("M", tl, "h", w, "v", h, "h", -w, "Z")

    tf = graph_cors_to_page_cors_transform(plan)

    for hole in holes:
        x = hole["x"]
        y = hole["y"]
        circle_path = parse_subpath(
            [
                "M",
                x + r,
                y,
                "A",
                r,
                r,
                0,
                0,
                0,
                x,
                y + r,
                "A",
                r,
                r,
                0,
                0,
                0,
                x - r,
                y,
                "A",
                r,
                r,
                0,
                0,
                0,
                x,
                y - r,
                "A",
                r,
                r,
                0,
                0,
                0,
                x + r,
                y,
                "Z",
            ]
        ).transformed(tf)
        cover_path.append(circle_path)
        X, Y = graph_cors_to_page_cors(plan, x, y)
        R = r * ux
        root.insert_after_last_of(
            svg_circle(
                {"cx": X, "cy": Y, "r": R, "fill": pinholes["hole-background-color"],}
            ),
            "defs",
            "style",
        )

    root.append(
        svg_path(
            {
                "id": "ze_cover_path",
                "d": cover_path,
                "fill": "#fdfdfd",
                "fill-opacity": pinholes["cover-opacity"],
            }
        )
    )


####################
# add_notch_labels #
####################


def acid_attack_recursive(element, acids):
    if hasattr(element, "bbox_for_mk_graph_acid_attack"):
        stopwatch("acid attack cropping an element")
        bbox1 = element.bbox_for_mk_graph_acid_attack
        for acid in acids:
            if element["d"].is_empty():
                break
            bbox2 = acid.bbox_for_mk_graph_acid_attack
            if bbox1.doesnt_overlap_with(bbox2):
                continue
            element["d"] = crop(element["d"], acid, crop_to_inside=False)
            # no real harm in not recomputing the bbox for element, etc
        stopwatch()

    for c in element:
        acid_attack_recursive(c, acids)

    empty_children = [c for c in element if c.is_empty_path_or_empty_group()]
    for c in empty_children:
        element.remove_child(c)


def acid_attack_prep(root, acids, victim_classnames):
    stopwatch("acid attack prep")

    for acid in acids:
        assert isinstance(acid, Subpath)
        acid.bbox_for_mk_graph_acid_attack = BoundingBox(acid.bbox())

    for c in root.subtree_iterator():
        if c.has_or_inherits_class(victim_classnames) and c.type == "path":
            c.unfold_d_attribute()
            c.bbox_for_mk_graph_acid_attack = BoundingBox(c.bbox())

    stopwatch()


def acid_attack(root, acids, victim_classnames):
    acid_attack_prep(root, acids, victim_classnames)
    acid_attack_recursive(root, acids)


def add_notch_labels(root, plan, acid_victims):
    if "notches" not in plan:
        return

    stopwatch("add_notch_labels")

    parent = root.append(new_layer(plan, "notch labels"))

    n = plan["notches"]
    xs = n["x"]
    ys = n["y"]

    if oy_in_bt(plan):
        for z in xs:
            if not z['include']:
                continue
            labels = z["labels"]
            classname = labels['classname']
            x_offsets = labels["x-offsets-in-exes"]
            y_offsets = labels["y-offsets-in-exes"]
            for s in labels["notch-segment-refs"]:
                pos = s.x()
                assert pos in x_offsets
                assert pos in y_offsets
                anchor = exes_to_page_cors(plan, Point(x_offsets[pos], y_offsets[pos])) + graph_cors_to_page_cors(plan, Point(s.end))
                if not lr(plan).contains(pos):
                    print("warning: skipping label with out-of-range lr position")
                    continue
                print(f"y_offsets[{pos}] = {y_offsets[pos]}, s.end: {s.end}")
                alfie = MathLabel(
                    labels["strings"][pos],
                    fontpx=g_fontpx,
                    fontname=g_fontname,
                    classname=classname,
                    translation=anchor,
                    h_align=labels["h-alignments"][pos],
                    v_align=labels["v-alignments"][pos],
                    margins_in_exes=labels["margins-in-exes"],
                    angle=labels["angle"],
                    additional_scaling=plan["notch-labels-font-scaling-factor"],
                )
                math_label_finish_initialization(plan, alfie)
                math_label_render_and_append(root, alfie, parent, True, 'notch-label-labelbox')

    if ox_in_lr(plan):
        for z in ys:
            if not z['include']:
                continue
            labels = z["labels"]
            classname = labels['classname']
            x_offsets = labels["x-offsets-in-exes"]
            y_offsets = labels["y-offsets-in-exes"]
            for s in labels["notch-segment-refs"]:
                pos = s.y()
                assert pos in x_offsets
                assert pos in y_offsets
                if not bt(plan).contains(pos):
                    print("warning: skipping label with out-of-range bt position")
                    continue
                anchor = \
                    graph_cors_to_page_cors(plan, Point(s.end)) + \
                    exes_to_page_cors(plan, Point(x_offsets[pos], y_offsets[pos]))
                alfie = MathLabel(
                    labels["strings"][pos],
                    fontpx=g_fontpx,
                    fontname=g_fontname,
                    classname=classname,
                    translation=anchor,
                    h_align=labels["h-alignments"][pos],
                    v_align=labels["v-alignments"][pos],
                    margins_in_exes=labels["margins-in-exes"],
                    angle=labels["angle"],
                    additional_scaling=plan["notch-labels-font-scaling-factor"],
                )
                math_label_finish_initialization(plan, alfie)
                math_label_render_and_append(root, alfie, parent, True, 'notch-label-labelbox')

    labelboxes = parent.find_all(None, {'class': 'notch-label-labelbox'})

    if not plan["flags"]["skip-acid-attack"]:
        acid_attack(root, [ el['d'] for el in labelboxes ], acid_victims)
    
    if not n['include-labelboxes']:
        for z in labelboxes:
            z.detach()

    parent.promote_classes()

    stopwatch()


#####################
# difference arrows #
#####################


def add_difference_arrows(root, plan):
    parent = root.append(new_layer(plan, "difference arrows"))
    ha = plan["horizontal-difference-arrows"]
    va = plan["vertical-difference-arrows"]

    for a in chain(ha, va):
        if not a['include']:
            continue

        if "start-x" in a:
            x0 = a["start-x"]
            x1 = a["end-x"]
            y1 = y0 = a["y"]

        else:
            y0 = a["start-y"]
            y1 = a["end-y"]
            x1 = x0 = a["x"]

        # from now on it's page_cors, baby!

        t = graph_cors_to_page_cors(plan, Point(x0, y0))
        h = graph_cors_to_page_cors(plan, Point(x1, y1))

        is_vertical = True if t.x == h.x else False

        if is_vertical:
            forward_unit = Point(0, 1 if h.y > t.y else -1)
            arrow_direction = 90 if forward_unit.y > 0 else 270
            bottom_right_unit = Point(1, 0)

        else:
            forward_unit = Point(1 if h.x > t.x else -1, 0)
            arrow_direction = 0 if forward_unit.x > 0 else 180
            bottom_right_unit = Point(0, 1)

        # endpoint adjustments based on stroke widths
        bookend_stroke_half_width = a["computed-bookend-stroke-width"] / 2
        if "computed-tail-flush-stroke-width" in a:
            tail_flush_stroke_half_width = a["computed-tail-flush-stroke-width"] / 2
            t = (
                t
                + forward_unit * bookend_stroke_half_width
                - forward_unit * tail_flush_stroke_half_width
            )

        if "computed-head-flush-stroke-width" in a:
            tail_flush_stroke_half_width = a["computed-head-flush-stroke-width"] / 2
            h = (
                h
                - forward_unit * bookend_stroke_half_width
                + forward_unit * tail_flush_stroke_half_width
            )

        # arrow measurements and bookend width
        arrow_W = exes_to_page_cors(plan, a["arrow-width-in-exes"])
        arrow_L = exes_to_page_cors(plan, a["arrow-width-in-exes"] * a["arrow-length-as-%-of-width"])
        bookend_W = arrow_W * a["bookend-width-in-arrow-widths"]

        # the tail endpoints
        zero = Point(0, 0)
        t_br = t + (zero if a['tail-bookend-type'] in ['none', 'top-left'] else bottom_right_unit * bookend_W)
        t_tl = t - (zero if a['tail-bookend-type'] in ['none', 'bottom-right'] else bottom_right_unit * bookend_W)
        h_br = h + (zero if a['head-bookend-type'] in ['none', 'top-left'] else bottom_right_unit * bookend_W)
        h_tl = h - (zero if a['head-bookend-type'] in ['none', 'bottom-right'] else bottom_right_unit * bookend_W)

        # adding paths (+ possible contour)
        for doing_foreground_contour in [True, False]:
            if doing_foreground_contour and not a["include-foreground-contour"]:
                continue

            bd_classname = foreground_contour_classname_from_classname(a["body-classname"], doing_foreground_contour)
            bk_classname = foreground_contour_classname_from_classname(a["bookend-classname"], doing_foreground_contour)
            tp_classname = foreground_contour_classname_from_classname(a["arrow-classname"], doing_foreground_contour)

            add_path_from_page_cor_tokens(parent, [t, h], bd_classname)

            if a["tail-bookend-type"] != "none":
                add_path_from_page_cor_tokens(parent, [t_tl, t_br], bk_classname)

            if a["head-bookend-type"] != "none":
                add_path_from_page_cor_tokens(parent, [h_tl, h_br], bk_classname)

            parent.append(svg_path({
                "d": parse_subpath(g_east_arrow_path_data_v3).transform([
                    'translate', h - forward_unit * arrow_L * 0.1,
                    'rotate', arrow_direction,
                    'scale', arrow_L, arrow_W,
                ]),
                "class": tp_classname,
            }))

        if a["include-brace"]:
            if a["brace-side"] == "bottom-right":
                brace_dir_unit = bottom_right_unit
                bt = t_br + a["brace-sep-in-page-cors"] * brace_dir_unit
                bh = h_br + a["brace-sep-in-page-cors"] * brace_dir_unit
                is_right_handed_brace = arrow_direction == 0 or arrow_direction == 270

            elif a["brace-side"] == "top-left":
                brace_dir_unit = -bottom_right_unit
                bt = t_tl + a["brace-sep-in-page-cors"] * brace_dir_unit
                bh = h_tl + a["brace-sep-in-page-cors"] * brace_dir_unit
                is_right_handed_brace = arrow_direction == 180 or arrow_direction == 90

            else:
                assert False

            if a["brace-ends"] == "flush":
                bt -= forward_unit * bookend_stroke_half_width
                bh += forward_unit * bookend_stroke_half_width
                # print("bookend_stroke_half_width:", bookend_stroke_half_width)
                # print("forward_unit:", forward_unit)
                # assert False

            brace_path = (
                right_handed_brace_from_page_cors(bt, bh, a)
                if is_right_handed_brace
                else right_handed_brace_from_page_cors(bh, bt, a)
            )

            if a["include-foreground-contour"]:
                parent.insert_before(
                    svg_path({
                        "class": "a_brace_contour",
                        "d": brace_path,
                        "style": f'fill:none;stroke-width:{2 * a["foreground-contour-offset-in-page-cors"]}',
                        "stroke": "#fdfdfd",
                    }),
                    parent.find('path', {'class': bd_classname}),
                )

            parent.append(svg_path({"class": "a_brace", "d": brace_path}))

            if a['brace-label'] != "":
                if a["brace-side"] == "bottom-right" and not is_vertical:
                    label_v_align = "x-top"
                    label_h_align = "middle"

                elif a["brace-side"] == "top-left" and not is_vertical:
                    label_v_align = "baseline"
                    label_h_align = "middle"

                elif a["brace-side"] == "bottom-right" and is_vertical:
                    label_v_align = "x-middle"
                    label_h_align = "start"

                elif a["brace-side"] == "top-left" and is_vertical:
                    label_v_align = "x-middle"
                    label_h_align = "end"

                else:
                    print(a["brace-side"])
                    print(is_vertical)
                    assert False

                anchor = (bt + bh) * 0.5 + brace_dir_unit * exes_to_page_cors(plan, a["brace-label-sep-in-exes"])

                label = MathLabel(
                    a['brace-label'],
                    fontpx=g_fontpx,
                    fontname=g_fontname,
                    classname='a_brace_label',
                    translation=anchor,
                    h_align=label_h_align,
                    v_align=label_v_align,
                    additional_scaling=a["brace-label-additional-scaling"],
                )
                math_label_finish_initialization(plan, label)
                math_label_render_and_append(root, label, parent, a['include-brace-label-labelbox'], 'brace-label-labelbox')


###################################
# hairlines (attached to notches) #
###################################


def add_hairlines_for_notch_batch(parent, plan, batch):
    for f in batch["hairlines-to-function"]:
        assert "as-graph-cor-segments" in f
        for s in f["as-graph-cor-segments"]:
            add_path_from_graph_cor_tokens(parent, plan, [s.start, s.end], f['classname'])


def add_notch_hairlines(root, plan):
    if "axes" in plan:
        parent = root.append(new_layer(plan, "hairlines (attached to notches)"))
        for xs_or_ys in chain(plan["notches"]["x"], plan["notches"]["y"]):
            add_hairlines_for_notch_batch(parent, plan, xs_or_ys)


def add_squarecap_paths_with_outlines(root, plan):
    parent = new_layer(plan, 'squarecap paths with outlines')

    for l in plan['squarecap-paths-with-outlines']:
        pts = l['graph-cor-pts']
        add_path_from_graph_cor_tokens(parent, plan, pts, l['outline-classname'])
        add_path_from_graph_cor_tokens(parent, plan, pts, l['classname'])

    if len(parent) > 0:
        root.append(parent)


###########
# notches #
###########


def add_x_notches(parent, plan):
    xs = plan["notches"]["x"]
    for z in xs:
        if not z['include']:
            continue
        if not z["visible"]:
            continue
        classname = z['classname']
        for s in z["as-graph-cor-segments"]:
            tokens = ["M", s.start, s.end]
            add_path_from_graph_cor_tokens(parent, plan, tokens, classname)


def add_y_notches(parent, plan):
    ys = plan["notches"]["y"]
    for z in ys:
        if not z['include']:
            continue
        if not z["visible"]:
            continue
        classname = z['classname']
        for s in z["as-graph-cor-segments"]:
            assert s.is_horizontal()
            # assert bt(plan).contains(s.y())
            tokens = ["M", s.start, s.end]
            add_path_from_graph_cor_tokens(parent, plan, tokens, classname)


def add_notches(root, plan):
    n = plan['notches']
    if 'axes' in plan:
        parent = new_layer(plan, "notches")
        if oy_in_bt(plan):
            add_x_notches(parent, plan)
        if ox_in_lr(plan):
            add_y_notches(parent, plan)

        # little bit of anal cleaning up:
        if len(parent) > 0:
            if all(p["class"] == parent[0]["class"] for p in parent):
                assert "class" not in parent
                parent["class"] = parent[0]["class"]
                for z in parent:
                    z.pop("class")
            root.append(parent)

###############
# axes labels #
###############


def exes_to_page_cors(plan, x, y=None):
    if y is None:
        return x * g_x_height * font_scaling(plan)

    assert isinstance(x, Real)
    assert isinstance(y, Real)

    return x * g_x_height * font_scaling(plan), y * g_x_height * font_scaling(plan)


def exes_to_x_units(plan, amnt, additional_font_scaling=1):
    answer = amnt
    answer *= g_x_height
    answer *= font_scaling(plan)
    answer *= additional_font_scaling
    answer /= scaled_x_unit(plan)
    return answer


def exes_to_y_units(plan, amnt, additional_font_scaling=1):
    answer = amnt
    answer *= g_x_height
    answer *= font_scaling(plan)
    answer *= additional_font_scaling
    answer /= scaled_y_unit(plan)
    return answer


def axis_label_iterator(plan, axis):
    for l in plan["axes"]["labels"][axis]:
        if not l["include"]:
            continue

        h_offset_graph_cors = exes_to_x_units(plan, l["h-offset-in-exes"], plan["axes-labels-font-scaling-factor"])
        v_offset_graph_cors = exes_to_y_units(plan, l["v-offset-in-exes"], plan["axes-labels-font-scaling-factor"])

        if l["type"] == "axis-tip":
            _, ref = axis_endpoints_in_graph_cors(plan, axis)

        elif l["type"] == "axis-mid":
            a, b = global_axis_endpoints_in_page_cors(plan, axis)
            ref = (a + b) / 2
            if axis == "x":
                print("got back ref =", ref)
            ref.transform_by(page_cors_to_graph_cors_matrix(plan))

        else:
            assert False

        bespoke_scaling = plan["axes-labels-font-scaling-factor"]
        bespoke_scaling *= l["individual-font-scaling-factor"]

        anchor = graph_cors_to_page_cors(plan, ref + Point(h_offset_graph_cors, v_offset_graph_cors))

        yield \
            MathLabel(
                l['string'],
                fontpx=g_fontpx,
                fontname=g_fontname,
                classname='axes-labels',
                translation=anchor,
                h_align=l["h-alignment"],
                v_align=l["v-alignment"],
                margins_in_exes=[0, 0, 0, 0],
                angle=l["angle"],
                additional_scaling=bespoke_scaling,
                alternate_glyphs=l["alternate-glyphs"],
            )


def add_axis_labels(root, parent, plan, axis):
    include_labelboxes = plan["axes"]["include-labelboxes"]
    for label in axis_label_iterator(plan, axis):
        math_label_finish_initialization(plan, label)
        math_label_render_and_append(root, label, parent, include_labelboxes, 'brace-label-labelbox')


def add_axes_labels(root, plan):
    if "axes" in plan:
        parent = root.append(new_layer(plan, "axes labels"))
        t, r, b, l = trbl(plan)
        ox, oy = oxoy(plan)
        if oy_in_bt(plan):
            add_axis_labels(root, parent, plan, "x")
        if ox_in_lr(plan):
            add_axis_labels(root, parent, plan, "y")
        parent.detach_if_no_children()


#############################
# add_axes, add_axes_arrows #
#############################


def add_axis_arrow_at_graph_cors(parent, plan, pt, angle):
    arrows = plan["axes"]["arrows"]
    W = exes_to_y_units(plan, arrows["width-in-exes"])
    L = exes_to_x_units(plan, arrows["width-in-exes"] * arrows["length-as-%-of-width"])
    element = svg_path({
        'class': arrows['classname'],
        'd': parse_subpath(g_east_arrow_path_data_v3).scale(
            L * scaled_x_unit(plan),
            W * scaled_y_unit(plan),
        ).rotate(angle),
    })
    add_element_at_graph_cors(parent, plan, element, pt)


def add_axes_arrows(root, plan):
    if plan["axes"]["arrows"]["include"] is True:
        parent = root.append(new_layer(plan, "axes arrows"))
        L, R = x_axis_endpoints_in_graph_cors(plan)
        B, T = y_axis_endpoints_in_graph_cors(plan)
        if L is not None:
            add_axis_arrow_at_graph_cors(parent, plan, R, 0)
        if B is not None:
            add_axis_arrow_at_graph_cors(parent, plan, T, -90)


def add_axes(root, plan):
    if "axes" in plan:
        parent = root.append(new_layer(plan, "axes"))
        L, R = x_axis_endpoints_in_graph_cors(plan)
        B, T = y_axis_endpoints_in_graph_cors(plan)
        print(f"L, R:", L, R)
        print(f"T, B:", T, B)
        if L is not None:
            add_path_from_graph_cor_tokens(parent, plan, ["M", L, R], "axes")
        if B is not None:
            add_path_from_graph_cor_tokens(parent, plan, ["M", B, T], "axes")
        if "arrows" in plan["axes"]:
            add_axes_arrows(root, plan)
    if "grid" in plan and "over-axes-outline" in plan["grid"]:
        oao = plan['grid']['over-axes-outline']
        if oao['include']:
            parent = root.append(new_layer(plan, "over-axes-grid-outline"))
            t, r, b, l = trbl(plan)
            add_path_from_graph_cor_tokens(parent, plan, ['M', l, t, r, t, r, b, l, b, 'Z'], oao['classname'])



#########################
# add_grid, add_subgrid #
#########################


def add_grid(root, plan):
    grid = plan['grid']
    if 'background-panel' in grid:
        parent = root.append(new_layer(plan, "polaroid-card-background"))
        bp = grid['background-panel']
        t, r, b, l = trbl_in_page_cors(plan)
        T = t - bp['margin-top']
        L = l - bp['margin-left']
        R = r + bp['margin-right']
        B = b + bp['margin-bottom']
        add_new_path_from_page_cor_tokens(parent, ["M", L, T, R, T, R, B, L, B, 'Z'], bp['classname'])
    if grid["include"]:
        parent = root.append(new_layer(plan, "grid"))
        t, r, b, l = trbl(plan)
        if "background-color" in grid:
            add_path_from_graph_cor_tokens(parent, plan, ["M", l, t, r, t, r, b, l, b, 'Z'], None)
            parent[-1].set_attribute('fill', grid['background-color'])
        for x in plan["grid"]["x"]["positions"]:
            assert l <= x <= r
            add_path_from_graph_cor_tokens(
                parent, plan, ["M", x, b, x, t], plan["grid"]['classname']
            )
        for y in plan["grid"]["y"]["positions"]:
            if b <= y <= t:
                add_path_from_graph_cor_tokens(
                    parent, plan, ["M", l, y, r, y], plan["grid"]['classname']
                )


def add_subgrid(root, plan):
    if "subgrid" in plan and plan['subgrid']['include']:
        parent = root.append(new_layer(plan, "subgrid"))
        parent = parent.append('g')
        t, r, b, l = trbl(plan)
        for x in plan["subgrid"]["x"]["positions"]:
            assert l <= x <= r
            add_path_from_graph_cor_tokens(parent, plan, ["M", x, b, x, t], "subgrid")
        for y in plan["subgrid"]["y"]["positions"]:
            if b <= y <= t:
                add_path_from_graph_cor_tokens(parent, plan, ["M", l, y, r, y], "subgrid")


####################################
# 'this_tranche' value extractions #
####################################


def oy_in_bt(plan):
    return bt(plan).contains(plan["oy"])


def ox_in_lr(plan):
    return lr(plan).contains(plan["ox"])


def y_axis_endpoints_in_graph_cors(plan):
    if ox_in_lr(plan):
        ox = plan["ox"]
        et, _er, _eb, el = axes_extensions_in_graph_cors(plan)
        t, _r, b, _l = trbl(plan)
        return Point(ox, b - el), Point(ox, t + et)
    else:
        return None, None


def x_axis_endpoints_in_graph_cors(plan):
    if oy_in_bt(plan):
        oy = plan["oy"]
        _et, er, _eb, el = axes_extensions_in_graph_cors(plan)
        _t, r, _b, l = trbl(plan)
        return Point(l - el, oy), Point(r + er, oy)
    else:
        return None, None


def axis_endpoints_in_graph_cors(plan, axis):
    if axis == "x":
        return x_axis_endpoints_in_graph_cors(plan)

    if axis == "y":
        return y_axis_endpoints_in_graph_cors(plan)

    assert False


def global_y_axis_endpoints_in_page_cors(plan):
    ux, uy = scaled_x_y_units(plan)
    et = plan["axes"]["extensions-in-exes"]["top"] * uy
    eb = plan["axes"]["extensions-in-exes"]["bottom"] * uy
    ox = plan["ox"]
    assert lr(plan).contains(ox)
    ox = (ox - lr(plan).lo) * ux
    top = Point(ox, -et)
    bottom = Point(ox, graph_bottom_page_cors(plan) + eb)
    return top, bottom


def global_x_axis_endpoints_in_page_cors(plan):
    ux, uy = scaled_x_y_units(plan)
    er = plan["axes"]["extensions-in-exes"]["right"] * ux
    el = plan["axes"]["extensions-in-exes"]["left"] * ux
    oy = plan["oy"]
    assert bt(plan).contains(oy)
    oy = (oy - bt(plan).lo) * uy
    left = Point(-el, graph_bottom_page_cors(plan) - oy)
    right = Point(lr(plan).length() * ux + er, graph_bottom_page_cors(plan) - oy)
    return left, right


def global_axis_endpoints_in_page_cors(plan, axis):
    if axis == "x":
        return global_x_axis_endpoints_in_page_cors(plan)
    if axis == "y":
        return global_y_axis_endpoints_in_page_cors(plan)
    assert False


#####################
# value extractions #
#####################


def lr(plan):
    assert isinstance(plan["lr"], Interval)
    return plan["lr"]


def l_and_r(plan):
    i = plan['lr']
    return i.lo, i.hi


def bt(plan):
    assert isinstance(plan["bt"], Interval)
    return plan["bt"]


def trbl(plan):
    hor = lr(plan)
    ver = bt(plan)
    return ver.hi, hor.hi, ver.lo, hor.lo


def trbl_in_page_cors(plan):
    t, r, b, l = trbl(plan)
    L, T = graph_cors_to_page_cors(plan, l, t)
    R, B = graph_cors_to_page_cors(plan, r, b)
    return T, R, B, L


def graph_cor_bounding_box(plan):
    assert isinstance(plan["lr"], Interval)
    assert isinstance(plan["bt"], Interval)
    return BoundingBox(
        plan["lr"], plan["bt"]
    )  # BoundingBox constructor knows how to interpret two Interval's


def oxoy(plan):
    return plan["ox"], plan["oy"]


def bbox(plan):
    t, r, b, l = trbl(plan)
    return BoundingBox(b, l, r - l, t - b)


# def x_axis_tip(plan):
#     x = lr(plan).hi + plan["axes"]["extensions-in-exes"]["right"]
#     y = plan["oy"]
#     return Point(x, y)


# def y_axis_tip(plan, index=None):
#     y = bt(plan, index).hi + plan["axes"]["extensions-in-exes"]["top"]
#     x = plan["ox"]
#     return Point(x, y)


def font_scaling(plan):
    non_stroke = plan["non-stroke-scaling-factor"]
    font = plan["font-scaling-factor"]
    return non_stroke * font


def stroke_scaling(plan):
    non_font = plan["non-font-scaling-factor"]
    stroke = plan["stroke-scaling-factor"]
    return non_font * stroke


def non_font_non_stroke_scaling(plan, axis):
    non_font = plan["non-font-scaling-factor"]
    non_stroke = plan["non-stroke-scaling-factor"]
    non_all = plan["non-font-non-stroke-scaling-factor"]
    axis_spec = plan["non-font-non-stroke-scaling-factor-" + axis]
    return non_font * non_stroke * non_all * axis_spec


def scaled_x_unit(plan):
    cell = plan["cell"]
    non_f_non_s = non_font_non_stroke_scaling(plan, "x")
    return cell * non_f_non_s


def scaled_y_unit(plan):
    cell = plan["cell"]
    non_f_non_s = non_font_non_stroke_scaling(plan, "y")
    return cell * non_f_non_s


def scaled_x_y_units(plan):
    return scaled_x_unit(plan), scaled_y_unit(plan)


def x_height_in_x_units(plan):
    return g_x_height / scaled_x_unit(plan)


def x_height_in_y_units(plan):
    return g_x_height / scaled_y_unit(plan)


def outer_margins(plan):
    mt = plan["outer-margins"]["top"]
    mr = plan["outer-margins"]["right"]
    mb = plan["outer-margins"]["bottom"]
    ml = plan["outer-margins"]["left"]
    return mt, mr, mb, ml


def axes_extensions_in_graph_cors(plan):
    if "axes" not in plan:
        return 0, 0, 0, 0
    t = plan["axes"]["extensions-in-exes"]["top"]
    r = plan["axes"]["extensions-in-exes"]["right"]
    b = plan["axes"]["extensions-in-exes"]["bottom"]
    l = plan["axes"]["extensions-in-exes"]["left"]
    t = exes_to_y_units(plan, t)
    b = exes_to_y_units(plan, b)
    r = exes_to_x_units(plan, r)
    l = exes_to_x_units(plan, l)
    return t, r, b, l


def graph_bottom_page_cors(plan):
    return bt(plan).length() * scaled_y_unit(plan)


def axis_interval(plan, z):
    if z == "x":
        return lr(plan)

    if z == "y":
        return bt(plan)

    assert False


################################
# stupid star number functions #
################################


def star_number_assimilate(d, key, star_number_or):
    if isinstance(star_number_or, Real):
        d[key] = star_number_or

    else:
        assert isinstance(star_number_or, str)
        assert star_number_or[0] == "*"
        d[key] *= float(star_number_or[1:])


def star_number_initialize(d, key, default_value):
    star_number_or = d.setdefault(key, "*1")
    d[key] = default_value
    star_number_assimilate(d, key, star_number_or)


################
# fleshing out #
################


def suggested_sep_for_axis(plan, z):
    if z == "x":
        unit = scaled_x_unit(plan)
        intv = lr(plan)
    else:
        unit = scaled_y_unit(plan)
        intv = bt(plan)
    suggested_notch_sep = 40 / unit
    suggested_num_notches = intv.length() / suggested_notch_sep
    starter = 10 ** int(log10(suggested_notch_sep))
    coeffs = [1, 2, 5, 10, 20, 50, 100]
    candidates = [c * starter / 10 for c in coeffs]
    best_sep = None
    scores = {}
    for sep in candidates:
        num_pos = len(intv.multiples_in_range(sep))
        if num_pos >= 2:
            scores[sep] = abs(suggested_num_notches - num_pos)
            if best_sep is None or scores[sep] < scores[best_sep]:
                best_sep = sep
    assert best_sep is not None
    return best_sep


def flesh_out_svg_fragments(plan):
    ux = scaled_x_unit(plan)
    uy = scaled_y_unit(plan)

    for z in plan.setdefault("svg-fragments", []):
        if "filename" in z:
            assert "id" in z
            assert "string" not in z
            replace_file_dir_with_path(z)
            print("loading:", z["filename"])
            el_root = load_lUnAtIc(z["path"] + z["filename"])
            el = el_root.find_unique(f"[any].id={z['id']}").detach()
            z.pop("filename")
            z.pop("path")
            z.pop("id")

        elif "string" in z:
            assert "id" not in z
            el_root = None
            el = lUnAtIc(z["string"])
            z.pop("string")

        else:
            assert False

        z["element"] = el
        el.flatten()  # too bad for any strokes

        # setup transforms
        pre_transforms = z.setdefault("pre-transforms", [])
        transforms = z.setdefault("transforms", [])
        post_transforms = z.setdefault("post-transforms", [])

        if "by-pacer" in z:
            zbp = z["by-pacer"]
            pacer = el.find("[any].id=[string-startswith]_pacer")
            assert pacer is None, "please change pacer id from starting with '_pacer' to starting with 'PACER'"
            pacer = el.find_unique("[any].id=[string-startswith]PACER")
            pacer_bbox = pacer.bbox()

            if zbp.setdefault("hide-pacer", True) is True:
                pacer.update_style_attribute("display:none")

            ts = []

            # translate pacer anchor to origin
            assert "anchor" in zbp
            corner = pacer_bbox.css_style_corner(zbp["anchor"])
            ts.insert(0, f"translate({-corner.x}, {-corner.y})")

            # scaling factors
            sx = sy = ""

            if "pacer-width-in-graph-cors" in zbp:
                sx = ux * zbp["pacer-width-in-graph-cors"] / pacer_bbox.width()
            elif "pacer-width-in-page-cors" in zbp:
                sx = zbp["pacer-width-in-graph-cors"] / pacer_bbox.width()

            if "pacer-height-in-graph-cors" in zbp:
                sy = uy * zbp["pacer-height-in-graph-cors"] / pacer_bbox.height()
            elif "pacer-height-in-page-cors" in zbp:
                sy = zbp["pacer-height-in-page-cors"] / pacer_bbox.height()

            if sy and not sx:
                sx = sy
            if sx == sy:
                sy = ""
            if sy:
                sy = ", " + str(sy)

            if sx or sy:
                ts.insert(0, f"scale({sx}{sy})")

            # translate origin to x, y page cors
            if "graph-cor-x" in zbp:
                x = graph_x_to_page_x(plan, zbp["graph-cor-x"])
            elif "page-cor-x" in zbp:
                x = zbp["page-cor-x"]
            else:
                x = graph_x_to_page_x(plan, 0)

            if "graph-cor-y" in zbp:
                y = graph_y_to_page_y(plan, zbp["graph-cor-y"])
            elif "page-cor-y" in zbp:
                y = zbp["page-cor-y"]
            else:
                y = graph_y_to_page_y(plan, 0)

            ts.insert(0, f"translate({x}, {y})")

            overall_transform = " ".join(ts)
            parse_transform(overall_transform)  # just making sure...

            transforms.append({"transform": overall_transform, "scale-strokes": True})

        elif len(transforms) == 0:
            print("warning: no transform on svg fragment")

        all_transforms = []
        all_transforms.extend(pre_transforms)
        all_transforms.extend(transforms)
        all_transforms.extend(post_transforms)

        for t in all_transforms:
            t.setdefault("scale-strokes", True)

        for x in all_transforms:
            t = x["transform"]

            if t == "graph-cors-to-page-cors-transform":
                x["transform"] = graph_cors_to_page_cors_transform(plan)

            elif t == "graph-cors-to-page-cors-scale":
                x["transform"] = graph_cors_to_page_cors_scale(plan)

            elif t == "page-cors-to-graph-cors-transform":
                x["transform"] = page_cors_to_graph_cors_transform(plan)

            elif t == "page-cors-to-graph-cors-scale":
                x["transform"] = page_cors_to_graph_cors_scale(plan)

            elif t == "viewbox-to-viewbox-translation":
                v1 = el_root.unfold_viewbox()
                recompute_viewbox(None, plan)
                v2 = plan["viewbox"]["computed"]
                x["transform"] = generate_transform("translate", v2["x"] - v1[0], v2["y"] - v1[1])

            elif "(" not in t:
                assert False

        if all(x["scale-strokes"] for x in all_transforms):
            el["transform"] = " ".join(x["transform"] for x in all_transforms)

        else:
            for x in all_transforms:
                assert "transform" not in el
                el["transform"] = x["transform"]
                el.flatten()
                x.pop("transform")


def flesh_out_fat_vector_arrows(plan):
    if "fat-vector-arrows" not in plan:
        return
    fva = plan["fat-vector-arrows"]
    include = fva.setdefault("include", True)
    list = fva.setdefault("list", [])
    if not include or len(list) == 0:
        plan.pop("fat-vector-arrows")
        return

    default_width = fva.setdefault("page-cors-half-width", g_default_fat_vector_arrow_half_width)
    default_lip = fva.setdefault("page-cors-lip", g_default_fat_vector_arrow_lip)
    default_nose_half_angle = fva.setdefault("nose-half-angle", g_default_fat_vector_arrow_nose_half_angle)
    default_scaling_factor = fva.setdefault("scaling-factor", 1)
    default_classname = fva.setdefault("classname", g_default_fat_vector_arrow_classname)

    for arrow in list:
        assert 'start' in arrow
        assert 'end' in arrow
        arrow.setdefault("page-cors-lip", default_lip)
        arrow.setdefault("page-cors-half-width", default_width)
        arrow.setdefault("nose-half-angle", default_nose_half_angle)
        arrow.setdefault("scaling-factor", default_scaling_factor)
        arrow.setdefault("classname", default_classname)
        arrow.setdefault("include", True)
        # print(arrow['classname'])
        # assert False

    for x in [
        "page-cors-lip",
        "nose-half-angle",
        "scaling-factor",
    ]:
        fva.pop(x)


def flesh_out_yellow_arrows(plan):
    if "yellow-arrows" not in plan:
        return
    ya = plan["yellow-arrows"]
    include = ya.setdefault("include", True)
    list = ya.setdefault("list", [])
    if not include or len(list) == 0:
        plan.pop("yellow-arrows")
        return

    default_width = ya.setdefault("page-cors-half-width", g_default_yellow_arrow_half_width)
    default_stem = ya.setdefault("page-cors-stem-length", g_default_yellow_arrow_stem_length)
    default_lip = ya.setdefault("page-cors-lip", g_default_yellow_arrow_lip)
    default_nose_half_angle = ya.setdefault("nose-half-angle", g_default_yellow_arrow_nose_half_angle)
    default_scaling_factor = ya.setdefault("scaling-factor", 1)
    default_classname = ya.setdefault("classname", g_default_yellow_arrow_classname)

    for arrow in list:
        arrow.setdefault("page-cors-stem-length", default_stem)
        arrow.setdefault("page-cors-lip", default_lip)
        arrow.setdefault("page-cors-half-width", default_width)
        arrow.setdefault("nose-half-angle", default_nose_half_angle)
        arrow.setdefault("scaling-factor", default_scaling_factor)
        arrow.setdefault("classname", default_classname)
        arrow.setdefault("include", True)

    for x in [
        "page-cors-stem-length",
        "page-cors-lip",
        "nose-half-angle",
        "scaling-factor",
    ]:
        ya.pop(x)


def setdefault_and_assert_type(d, key, val, value_type):
    d.setdefault(key, val)
    assert isinstance(d[key], value_type)


def is_list_of_two_reals(val):
    return (
        isinstance(val, list) and
        len(val) == 2 and
        isinstance(val[0], Real) and 
        isinstance(val[1], Real)
    )


def listify(d, key, value_type, num_repetitions=0, default_value=None):
    if default_value is not None:
        d.setdefault(key, default_value)

    else:
        d.setdefault(key, [])

    val = d[key]
    if isinstance(value_type, str):
        assert value_type == "cor_pair"
        if is_list_of_two_reals(val):
            d[key] = [val]
        assert all(is_list_of_two_reals(v) for v in val)

    else:
        if isinstance(val, value_type):
            assert not isinstance(val, list)
            d[key] = [val]

        else:
            assert isinstance(val, list) and len(val) in [0, 1, num_repetitions]

    assert isinstance(d[key], list)
    if len(d[key]) == 1:
        for i in range(1, num_repetitions):
            d[key].append(d[key][0])

    # final checks:
    assert isinstance(d[key], list)
    for q in d[key]:
        if isinstance(value_type, str):
            if value_type == "cor_pair":
                assert is_list_of_two_reals(q)
    
            else:
                assert False
        else:
            assert isinstance(q, value_type)

    if num_repetitions > 0:
        assert len(d[key]) in [0, num_repetitions]

    return d[key]


def flesh_out_graphs_part1(plan):
    graphs = plan.setdefault("graphs", [])
    for g in graphs:
        if 'file-dir' in g:
            replace_file_dir_with_path(g)


def flesh_out_graphs_part2(plan):
    def is_number_pair(thing):
        return isinstance(thing, list) and len(thing) == 2 and all(isinstance(x, Real) for x in thing)

    graphs = plan["graphs"]

    for g in graphs:
        assert "type" in g
        g.setdefault("name", "(no-name graph)")
        g.setdefault("cull-data-outside-lr", True)
        plan_bbox = graph_cor_bounding_box(plan)
        if 'only-in' in g:
            listify(g, "only-in", int)

        def load_pts_pop_transform():
            g["pts"] = load_coordinate_data_of_graph(plan, g)
            g.pop("data")
            g.pop("transform", None)
            g.pop("transforms", None)

        if g['type'] == 'polynomial interpolation':
            g['type'] = 'cubic bezier'
            assert 'points' in g
            assert len(g['points']) >= 2

            def product_string_and_value_for_index(i):
                string = ""
                product_without_multiplier = 1
                x = g['points'][i][0]
                y = g['points'][i][1]
                assert isinstance(x, Real)
                assert isinstance(y, Real)
                for j, p in enumerate(g['points']):
                    if j == i:
                        continue
                    string += f"*(x-({p[0]:.2f}))" # the first '*'
                    product_without_multiplier *= x - p[0]
                desired_multiplier = y / product_without_multiplier
                return f"({desired_multiplier:.3f}){string}"
            
            final_eval_string = ""
            for i in range(len(g['points'])):
                if i > 0:
                    final_eval_string += " + "
                final_eval_string += f"({product_string_and_value_for_index(i)})"
                
            g.pop('points')
            g['function'] = final_eval_string

        if g["type"] == "linepath":
            load_pts_pop_transform()
            g.setdefault("clip", False if all(plan_bbox.contains(pt) for pt in g["pts"]) else True)
            g.setdefault("manual-clip-linepath-to-lr", g['cull-data-outside-lr'] and not g['clip'])
            g.setdefault("ecr", g_default_ecr)
            classname_setdefault(g, "graph")
            g.setdefault("left-endpoint-dot-classname", "none")
            g.setdefault("left-discontinuous-dot-classname", "dots-open")
            g.setdefault("right-endpoint-dot-classname", "none")
            g.setdefault("right-discontinuous-dot-classname", "dots-open")
            areas = g.setdefault("areas", [])
            if is_number_pair(areas):
                areas = [{"delims": areas}]
            for a in areas:
                assert "delims" in a
                a.setdefault("invert-colors", False)
            g["areas"] = areas

        elif g['type'] == 'cubic bezier':
            g.setdefault('clip', True)
            classname_setdefault(g, 'graph')
            
            if 'num-cubics' in g:
                l, r = l_and_r(plan)
                assert 'xs' not in g
                n = g.pop('num-cubics')
                assert isinstance(n, int) and n > 0
                step = (r - l) / n
                g['xs'] = [(l + i * step) if i < n else r for i in range(n + 1)]

            else:
                assert 'xs' in g
                xs = g['xs']
                if isinstance(xs, list):
                    assert len(xs) >= 2

                else:
                    assert isinstance(xs, dict)
                    if 'start' not in xs:
                        assert 'end' not in xs
                        l, r = l_and_r(plan)
                        xs['start'] = l
                        xs['end'] = r

                    start = float(xs['start'])
                    end = float(xs['end'])

                    assert start < end

                    if 'every' in xs:
                        every = float(xs['every'])
                        
                    else:
                        assert 'num' in xs
                        num = float(xs['num'])
                        assert num >= 2
                        every = (end - start) / num
                    assert every > 0

                    xs = []
                    x = start
                    while x < end - every * 0.3:
                        xs.append(x)
                        x += every
                    xs.append(end)
                    assert all(isinstance(x, Real) for x in xs)
                    g['xs'] = xs
                
            if 'derivative' not in g:
                l, r = l_and_r(plan)
                g.setdefault('derivative', (r - l) / 100)
                
            assert 'derivative' in g
            
        elif g['type'] == 'parameterized cubic bezier':
            g.setdefault('clip', True)
            classname_setdefault(g, 'graph')
            
            if 'num-cubics' in g:
                assert 'tmin' in g
                assert 'tmax' in g
                assert 'ts' not in g
                tmin = g.pop('tmin')
                tmax = g.pop('tmax')
                n = g.pop('num-cubics')
                assert isinstance(n, int) and n > 0
                step = (tmax - tmin) / n
                g['ts'] = [(tmin + i * step) if i < n else tmax for i in range(n + 1)]
                assert tmin == g['ts'][0]
                assert tmax == g['ts'][-1]

            elif 'ts' in g:
                assert len(g['ts']) >= 2
                assert 'tmin' not in g
                assert 'tmax' not in g
                
            else:
                assert 'tmin' in g
                assert 'tmax' in g
                tmin = g.pop('tmin')
                tmax = g.pop('tmax')
                g['ts'] = [tmin, tmax]

            assert 'ts' in g
            ts = g['ts']
            tmin = ts[0]
            tmax = ts[-1]

            if 'x-derivative' not in g:
                print("\ndid you really want a default h for your derivative?\n")
                g['x-derivative'] = (tmax - tmin) / 100

            if 'y-derivative' not in g:
                print("\ndid you really want a default h for your derivative?\n")
                g['y-derivative'] = (tmax - tmin) / 100
                
            assert 'x-function' in g
            assert 'y-function' in g
            assert 'x-derivative' in g
            assert 'y-derivative' in g
            
            g.setdefault('marked-ts', [])
            g.setdefault('marked-ts-size', 0.1)
            g.setdefault('marked-ts-classname', 'axes')

        elif g["type"] == "dotplot":
            g.setdefault("clip", False)
            load_pts_pop_transform()
            g.setdefault("clip", False)
            classname_setdefault(g, "dots-full")

        elif g["type"] == "x squared":
            g.setdefault("clip", False)
            g.setdefault("num-cubics", 6)
            classname_setdefault(g, "graph")

        elif g["type"] == "eval":
            g.setdefault("clip", False)
            classname_setdefault(g, "graph")
            g.setdefault("clip", True)
            g.setdefault("num-steps", 10)

        elif g["type"] == "anchor dotted line":
            g.setdefault("clip", False)
            assert "data" in g
            assert "s" not in g and "e" not in g
            assert len(data) == 2
            g['s'] = Point(data[0])
            g['e'] = Point(data[1])
            t, r, b, l = trbl(plan)
            assert l <= g['s'].x <= r
            assert l <= g['e'].x <= r
            # assert b <= g['s'].y <= t
            # assert b <= g['e'].y <= t
            g.setdefault("x-range", [g["s"].x, g["e"].x])
            g.setdefault("clip", False)
            g.setdefault("ecr-in-abs-units", 1)

        elif g["type"] == "glowdots":
            g.setdefault("clip", False)
            data = listify(g, "data", "cor_pair")
            setdefault_and_assert_type(g, "inner-ecr-in-abs-units", 2, Real)
            setdefault_and_assert_type(
                g, "outer-ecr-in-abs-units", 2 * g["inner-ecr-in-abs-units"], Real
            )
            assert "inner-color" in g
            num_dots = len(data)
            listify(g, "inner-color", str, num_repetitions=num_dots)
            if "outer-color" not in g:
                g.setdefault("outer-opacity", 0.5)
            if "outer-opacity" in g:
                listify(g, "outer-opacity", Real, num_repetitions=2)
                assert "outer-color" not in g
                g["outer-color"] = [
                    Rgba(g["inner-color"][i])
                    .with_opacity(g["outer-opacity"][i])
                    .__str__()
                    for i in range(num_dots)
                ]
                g.pop("outer-opacity")
            listify(g, "outer-color", str, num_repetitions=num_dots)

        elif g["type"] == "thick tangents":
            g.setdefault("clip", False)
            data = listify(g, "data", Real)
            f = g["function"]
            if isinstance(f, str):
                g["function"] = [
                    eval("(lambda x: " + f + ")(" + str(x) + ")") for x in data
                ]
            else:
                assert isinstance(f, list)
                assert len(f) == len(data) or len(data) == 0
            der = g["derivative"]
            if isinstance(der, str):
                g["derivative"] = [
                    eval("(lambda x: " + der + ")(" + str(x) + ")") for x in data
                ]
            else:
                assert isinstance(der, list)
                assert len(der) == len(data) or len(data) == 0
            g.setdefault("function-stroke-width-in-abs-units", 1.7)
            listify(g, "length", Real, num_repetitions=len(data))
            listify(g, "thickness", Real, num_repetitions=len(data))
            listify(g, "color", str, num_repetitions=len(data), default_value="#000")
            g.setdefault("above-or-below", "below")
            g.setdefault("clip", False)

        else:
            print("unknown graph type:", g["type"])
            assert False


def flesh_out_axis_positions(plan, z, axis):
    if "positions" not in z:
        if "every" not in z:
            assert False
        every = z["every"]
        excpt = z.setdefault("except", [])

        if every == "from data":
            pts = graph_data_lr_points(plan)
            ps = list(set([p.project(axis) for p in pts]))
            ps = [x for x in ps if axis_interval(plan, axis).contains(x)]

        elif isinstance(every, str) and every.endswith(".yaml"):
            ps = load_yaml(every)
            ps = [x for x in ps if x in axis_interval(plan, axis)]

        else:
            ps = []
            ps.extend(axis_interval(plan, axis).multiples_in_range(every))

        ps = [x for x in ps if x not in excpt]

        z["positions"] = ps
        z.pop("every")
        z.pop("except")

    # just added this... correct?:
    assert "every" not in z
    assert "except" not in z


def flesh_out_notch_labels(plan, z, axis, classname_default):
    assert axis in ["x", "y"]

    greg = z["labels"]

    # misc
    greg.setdefault("angle", 0)
    classname_setdefault(greg, classname_default)

    # build positions

    segs = z["as-graph-cor-segments"]
    if axis == "x":
        notch_positions = [seg.x() for seg in segs]

    else:
        notch_positions = [seg.y() for seg in segs]

    greg.setdefault("positions", notch_positions)

    if isinstance(greg["positions"], str):
        at_string = greg["positions"]
        greg["positions"] = bbox(plan).project(axis).multiples_in_range(at_string)

    ps = greg.pop("positions")

    assert isinstance(ps, list)
    assert all(isinstance(w, Real) for w in ps)
    assert all(w in notch_positions for w in ps)

    rm = greg.get("positions-remove", [])
    ps = [x for x in ps if x not in rm]

    greg.pop("positions-remove", None)

    assert "notch-segment-refs" not in greg

    if axis == "x":
        greg["notch-segment-refs"] = [seg for seg in segs if seg.x() in ps]

    else:
        greg["notch-segment-refs"] = [seg for seg in segs if seg.y() in ps]

    # build strings

    greg.setdefault("decimals", 0)
    strings = greg.setdefault("strings", {})
    for w in ps:
        if greg["decimals"] == 1:
            strings.setdefault(w, "%.1f" % w)

        elif greg["decimals"] == 2:
            strings.setdefault(w, "%.2f" % w)

        elif greg["decimals"] > 2:
            print("please supply this case or use exec or whatever")
            assert False

        elif w == int(w):
            strings.setdefault(w, str(int(w)))

        else:
            strings.setdefault(w, str(w))

        if "nr_1" in strings:
            assert len(ps) >= 1
            strings[ps[0]] = strings["nr_1"]

        if "nr_2" in strings:
            assert len(ps) >= 2
            strings[ps[1]] = strings["nr_2"]

        if "nr_3" in strings:
            assert len(ps) >= 3
            strings[ps[2]] = strings["nr_3"]

    # h-alignment

    inverses = {
        "margins-left": "margins-right",
        "margins-right": "margins-left",
        "margins-top": "margins-bottom",
        "margins-bottom": "margins-top",
        "baseline": "number-top",
        "number-top": "baseline"
    }

    greg.setdefault("h-alignment", "middle" if axis == "x" else "margins-right")

    h_alignments = greg.setdefault("h-alignments", {})

    special_h_alignments = greg.setdefault('special-h-alignments', {})
    for w in special_h_alignments:
        h_alignments[w] = special_h_alignments[w]

    for w in ps:
        h_alignments.setdefault(w, greg["h-alignment"])
    greg.pop("h-alignment")

    if axis == "y":
        for w in z["invert-these"]:
            if h_alignments[w] not in inverses:
                print("sorry you can only invert notch when using ze standard margins-... alignments")
                assert False
            h_alignments[w] = inverses[h_alignments[w]]

    # v-alignment

    if axis == "x":
        greg.setdefault("v-alignment", "margins-top")

    else:
        greg.setdefault("v-alignment", "number-middle")

    v_alignments = greg.setdefault("v-alignments", {})
    for w in ps:
        v_alignments.setdefault(w, greg["v-alignment"])
    greg.pop("v-alignment")

    if axis == "x":
        for w in z["invert-these"]:
            if v_alignments[w] not in inverses:
                print("sorry you can only invert notch when using ze standard margins-... alignments")
                assert False
            v_alignments[w] = inverses[v_alignments[w]]

    # default offsets

    def_x_offset = greg.setdefault("x-offset-in-exes", 0)
    def_y_offset = greg.setdefault("y-offset-in-exes", 0)

    # process path-named-offsets

    x_offsets = greg.setdefault("special-x-offsets-in-exes", {})
    y_offsets = greg.setdefault("special-y-offsets-in-exes", {})

    for d in [x_offsets, y_offsets]:
        assert all(isinstance(d[key], Real) for key in d)

        if "nr_1" in x_offsets:
            assert len(ps) >= 1
            assert ps[0] not in x_offsets
            x_offsets[ps[0]] = x_offsets.pop("nr_1")
        if "nr_2" in x_offsets:
            assert len(ps) >= 2
            assert ps[1] not in x_offsets
            x_offsets[ps[1]] = x_offsets.pop("nr_2")
        if "nr_3" in x_offsets:
            assert len(ps) >= 3
            assert ps[2] not in x_offsets
            x_offsets[ps[2]] = x_offsets.pop("nr_3")

        assert all(key in ps for key in d)
        assert all(isinstance(d[key], Real) for key in d)

    # put default offsets in missing slots

    for w in ps:
        x_offsets.setdefault(w, def_x_offset)
        y_offsets.setdefault(w, def_y_offset)

    # pop, rename

    greg.pop("x-offset-in-exes")
    greg.pop("y-offset-in-exes")

    greg["x-offsets-in-exes"] = greg.pop("special-x-offsets-in-exes")
    greg["y-offsets-in-exes"] = greg.pop("special-y-offsets-in-exes")


def flesh_out_notches_and_their_labels(plan):
    plan.setdefault("notches", {})

    n = plan["notches"]
    n.setdefault('include', True)
    n.setdefault("x", [{}])
    n.setdefault("y", [{}])

    xs = n["x"]
    ys = n["y"]

    sum_defaults = {
        'two-sided': False,
        'classname': 'axes',
        'visible': lr(plan).lo or plan["oy"] == bt(plan).lo
    }

    if n.get('visible') == 'auto':
        n['visible'] = sum_defaults['visible']

    for name, default in sum_defaults.items():
        for z in chain(xs, ys):
            if name in n:
                setfresh(z, name, n[name])

            else:
                z.setdefault(name, default)

    # length
    if ('visible' not in n and sum_defaults['visible']) or ('visible' in n and n['visible']):
        star_number_initialize(n, "length-in-exes", g_default_visible_notch_length_in_exes)

    else:
        star_number_initialize(n, "length-in-exes", g_default_non_visible_notch_length_in_exes)

    for name in sum_defaults:
        n.pop(name, None)

    for z in chain(xs, ys):
        z.setdefault("length-in-exes", n["length-in-exes"])
    n.pop("length-in-exes")

    # label margins
    l_margins = n.setdefault("label-margins-in-exes", g_default_notch_label_margins)
    for z in chain(xs, ys):
        deepsetdefault(z, "labels", "margins-in-exes", l_margins)
    n.pop("label-margins-in-exes")

    # misc
    for z in chain(xs, ys):
        z.setdefault('include', n['include'])
        if "positions" not in z:
            z.setdefault("except", [])
        z.setdefault("invert-these", [])
    n.pop('include')

    # notch positions
    default_sep_x = suggested_sep_for_axis(plan, "x")
    default_sep_y = suggested_sep_for_axis(plan, "y")

    for z in xs:
        if "positions" not in z:
            z.setdefault("every", default_sep_x)

    for z in ys:
        if "positions" not in z:
            z.setdefault("every", default_sep_y)

    for z in xs:
        flesh_out_axis_positions(plan, z, "x")  # (pops 'every' and 'except')

    for z in ys:
        flesh_out_axis_positions(plan, z, "y")  # (pops 'every' and 'except')

    # hairlines
    for z in chain(xs, ys):
        hairlines = z.setdefault("hairlines-to-function", [])
        for f in hairlines:
            assert "function" in f
            classname_setdefault(f, "hairlines")
            f.setdefault("sep-in-exes", 0.4)
            f.setdefault("which", z["positions"])
            assert all(x in z["positions"] for x in f["which"])
            assert "as-graph-cor-segments" not in f
            f["as-graph-cor-segments"] = []

    # notch segments
    ox = plan["ox"]
    oy = plan["oy"]
    x_x = x_height_in_x_units(plan)
    x_y = x_height_in_y_units(plan)

    for z in xs:
        assert "as-graph-cor-segments" not in z
        assert 'length-in-exes' in z
        L = z["length-in-exes"] * x_y
        segs = []
        for x in z["positions"]:
            o = Point(x, oy)
            o_up = Point(x, oy + L)
            o_dn = Point(x, oy - L)
            if z["two-sided"] and x in z["invert-these"]:
                start, end = o_dn, o_up
            elif z["two-sided"]:
                start, end = o_up, o_dn
            elif x in z["invert-these"]:
                start, end = o, o_up
            else:
                start, end = o, o_dn
            segs.append(Line(start, end))
        z["as-graph-cor-segments"] = segs

    for z in ys:
        assert "as-graph-cor-segments" not in z
        L = z["length-in-exes"] * x_x
        segs = []
        for y in z["positions"]:
            o = Point(ox, y)
            o_r = Point(ox + L, y)
            o_l = Point(ox - L, y)
            start = o_r if z["two-sided"] else o
            end = o_l
            if y in z["invert-these"]:
                start, end = end, start
            segs.append(Line(start, end))
        z["as-graph-cor-segments"] = segs

    # hairline segments
    for z in xs:
        L = z["length-in-exes"] * x_y
        for f in z["hairlines-to-function"]:
            for i, x in enumerate(f["which"]):
                assert x in z["positions"]

                o = Point(x, oy)
                o_up = Point(x, oy + L) if z["two-sided"] else o
                o_dn = Point(x, oy - L)

                if x in z["invert-these"]:
                    o_up, o_dn = o_dn, o_up  # makes no difference (see below) but whatever

                if isinstance(f["function"], str):
                    expression = "(lambda x: " + f["function"] + ")(" + str(x) + ")"
                    y = eval(expression)

                elif isinstance(f["function"], Real):
                    y = f["function"]

                elif isinstance(f["function"], list):
                    assert len(f["function"]) == len(f["which"])
                    y = f["function"][i]
                    assert isinstance(y, Real)

                else:
                    assert False

                sep = f["sep-in-exes"] * x_y

                if y > oy:
                    notch_end = max(o_up.y, o_dn.y)
                    assert y > notch_end + sep
                    start = Point(x, notch_end + sep)

                elif y < oy:
                    notch_end = min(o_up.y, o_dn.y)
                    assert y < notch_end - sep
                    start = Point(x, notch_end - sep)

                else:
                    assert False

                end = Point(x, y)
                f["as-graph-cor-segments"].append(Line(start, end))

            assert len(f["which"]) == len(f["as-graph-cor-segments"])

    for z in ys:
        L = z["length-in-exes"] * x_x
        for f in z["hairlines-to-function"]:
            for i, y in enumerate(f["which"]):
                assert y in z["positions"]

                o = Point(ox, y)
                o_r = Point(ox + L, y) if z["two-sided"] else o
                o_l = Point(ox - L, y)

                if y in z["invert-these"]:
                    o_r, o_l = o_l, o_r  # makes no difference (see below) but whatever

                if isinstance(f["function"], str):
                    assert (
                        "y" in f["function"] or "x" not in f["function"]
                    )  # 'exp' makes our life bit harder here...
                    expression = "(lambda y: " + f["function"] + ")(" + str(y) + ")"
                    x = eval(expression)

                elif isinstance(f["function"], Real):
                    x = f["function"]

                elif isinstance(f["function"], list):
                    assert len(f["function"]) == len(f["which"])
                    x = f["function"][i]
                    assert isinstance(x, Real)

                else:
                    assert False

                sep = f["sep-in-exes"] * x_x

                if x > ox:
                    notch_end = max(o_r.x, o_l.x)
                    assert x > notch_end + sep
                    start = Point(notch_end + sep, y)

                elif x < ox:
                    notch_end = min(o_r.x, o_l.x)
                    assert x < notch_end - sep
                    start = Point(notch_end - sep, y)

                else:
                    assert False

                end = Point(x, y)
                f["as-graph-cor-segments"].append(Line(start, end))

            assert len(f["which"]) == len(f["as-graph-cor-segments"])

    # labels
    n.setdefault("include-labelboxes", False)
    labels_classname_default = "notch-labels-in-grid"
    
    if plan["bt"].lo == plan["oy"] or plan['grid']['include'] == False:
        labels_classname_default = "notch-labels-out-grid"
    
    labels_classname = n.pop("labels-classname", labels_classname_default)
    for z in xs:
        flesh_out_notch_labels(plan, z, "x", labels_classname)

    for z in ys:
        flesh_out_notch_labels(plan, z, "y", labels_classname)


def flesh_out_axes_arrows(plan):
    if 'arrows' in plan['axes']:
        a = plan['axes']['arrows']
        a.setdefault('include', True)
        a.setdefault('classname', 'axes-arrows')
        star_number_initialize(a, 'width-in-exes', g_default_arrow_width_in_exes)
        a.setdefault('length-as-%-of-width', g_default_arrow_length_as_pct_of_width)


def flesh_out_axes_extensions(plan):
    e = plan["axes"].setdefault("extensions-in-exes", {})

    visitors = ["top", "right", "bottom", "left"]

    if "trbl" in e:
        _trbl = e["trbl"]
        assert len(_trbl) == 4
        assert all(x not in e for x in chain(visitors, ["tr"]))
        e["top"] = _trbl[0]
        e["right"] = _trbl[1]
        e["bottom"] = _trbl[2]
        e["left"] = _trbl[3]
        e.pop("trbl")

    if "tr" in e:
        _tr = e["tr"]
        assert len(_tr) == 2
        assert all(x not in e for x in chain(visitors, ["trbl"]))
        e["top"] = _tr[0]
        e["right"] = _tr[1]
        e.pop("tr")

    if deepexists(plan, "axes", "arrows"):
        e.setdefault("top", g_default_top_extension)
        e.setdefault("right", g_default_right_extension)

    for x in visitors:
        e.setdefault(x, 0)


def flesh_out_axes_labels(plan):
    labs = plan["axes"].setdefault("labels", {})
    xs = labs.setdefault("x", [])
    ys = labs.setdefault("y", [])
    for l in xs:
        l.setdefault("type", "axis-tip")
        l.setdefault("h-offset-in-exes", 0)
        l.setdefault("v-offset-in-exes", 0)
        if l["type"] == "axis-tip":
            l.setdefault("h-alignment", "start")
            l.setdefault("v-alignment", "x-middle")
        elif l["type"] == "axis-mid":
            l.setdefault("h-alignment", "middle")
            l.setdefault("v-alignment", "number-top")
        l.setdefault("angle", 0)
        l.setdefault("individual-font-scaling-factor", 1)
        l.setdefault("alternate-glyphs", {})
        l.setdefault("include", True)
    for l in ys:
        l.setdefault("type", "axis-tip")
        l.setdefault("h-offset-in-exes", 0)
        l.setdefault("v-offset-in-exes", 0)
        l.setdefault("h-alignment", "middle")
        l.setdefault("v-alignment", "baseline")
        l.setdefault("angle", 0)
        l.setdefault("individual-font-scaling-factor", 1)
        l.setdefault("alternate-glyphs", {})
        l.setdefault("include", True)


def flesh_out_axes(plan):
    if "axes" in plan:
        axes = plan.setdefault("axes", {})
        axes.setdefault("include", True)
        axes.setdefault("include-labelboxes", False)
        if axes["include"]:
            flesh_out_axes_labels(plan)
            flesh_out_axes_extensions(plan)
            flesh_out_axes_arrows(plan)


def flesh_out_grid(plan):
    grid = plan.setdefault("grid", {})
    if not grid.setdefault("include", True):
        return
    classname_setdefault(grid, "grid")

    base_every = grid.pop("every", 1)
    x_default_every = suggested_sep_for_axis(plan, "x") if base_every == 'auto' else base_every
    y_default_every = suggested_sep_for_axis(plan, "y") if base_every == 'auto' else base_every

    x = grid.setdefault("x", {})
    y = grid.setdefault("y", {})

    for (z, e) in zip([x, y], [x_default_every, y_default_every]):
        if "position" not in z:
            z.setdefault("every", e)
        else:
            assert base_every is None
        z.setdefault("include", True)

    for axis, dict in zip(["x", "y"], [x, y]):
        flesh_out_axis_positions(plan, dict, axis)

    if 'over-axes-outline' in grid:
        oao = grid['over-axes-outline']
        oao.setdefault('include', True)
        oao.setdefault('classname', 'grid-over-axes-outline')

    if 'background-panel' in grid:
        bp = grid['background-panel']
        bp.setdefault('classname', 'polaroid-card-background')


def flesh_out_subgrid(plan):
    if "subgrid" in plan:
        subgrid = plan.setdefault("subgrid", {})
        subgrid.setdefault("include", True)
        subgrid.setdefault("x", {"every": suggested_sep_for_axis(plan, "x") / 10})
        subgrid.setdefault("y", {"every": suggested_sep_for_axis(plan, "y") / 10})

        for axis in ["x", "y"]:
            z = subgrid[axis]
            flesh_out_axis_positions(plan, z, axis)
            classname_setdefault(z, "subgrid")


def flesh_out_annotations(plan):
    an = plan.setdefault("annotations", [])
    for a in an:
        a.setdefault("individual-font-scaling-factor", 1)
        a.setdefault("alternate-glyphs", {})
        a.setdefault("v-alignment", "baseline")
        a.setdefault("h-alignment", "start")
        a.setdefault("include-labelbox", False)
        a.setdefault("angle", 0)


def flesh_out_viewbox(plan):
    vb = plan.setdefault("viewbox", {})
    vb.setdefault("include", False)

    found = {
        "l": False,
        "r": False,
        "b": False,
        "t": False,
    }

    determines_L = {
        "page-cor-width-around-grid-mid",
        "page-cor-width-around-elements-mid",
        "page-cor-margin-left-from-elements",
        "page-cor-margin-left-from-grid",
        "page-cor-margin-left-right-from-elements",
        "page-cor-margin-left-right-from-grid",
        "page-cor-margin-trbl-from-elements",
        "page-cor-margin-trbl-from-grid",
    }

    determines_R = {
        "page-cor-width-around-grid-mid",
        "page-cor-width-around-elements-mid",
        "page-cor-margin-right-from-elements",
        "page-cor-margin-right-from-grid",
        "page-cor-margin-left-right-from-elements",
        "page-cor-margin-left-right-from-grid",
        "page-cor-margin-trbl-from-elements",
        "page-cor-margin-trbl-from-grid",
    }

    determines_T = {
        "page-cor-height-around-grid-mid",
        "page-cor-height-around-elements-mid",
        "page-cor-margin-top-from-elements",
        "page-cor-margin-top-from-grid",
        "page-cor-margin-bottom-top-from-elements",
        "page-cor-margin-bottom-top-from-grid",
        "page-cor-margin-trbl-from-elements",
        "page-cor-margin-trbl-from-grid",
    }

    determines_B = {
        "page-cor-height-around-grid-mid",
        "page-cor-height-around-elements-mid",
        "page-cor-margin-bottom-from-elements",
        "page-cor-margin-bottom-from-grid",
        "page-cor-margin-bottom-top-from-elements",
        "page-cor-margin-bottom-top-from-grid",
        "page-cor-margin-trbl-from-elements",
        "page-cor-margin-trbl-from-grid",
    }

    for letter, keys in zip(
        ["l", "r", "t", "b"], [determines_L, determines_R, determines_T, determines_B]
    ):
        num_keys = len(vb.keys() & keys)
        assert num_keys <= 1
        if num_keys > 0:
            found[letter] = True

    if not found["l"]:
        vb["page-cor-margin-left-from-elements"] = 10

    if not found["r"]:
        vb["page-cor-margin-right-from-elements"] = 10

    if not found["b"]:
        vb["page-cor-margin-bottom-from-elements"] = 10

    if not found["t"]:
        vb["page-cor-margin-top-from-elements"] = 10


def flesh_out_styles(plan):
    plan.setdefault("styles", {})
    styles = load_yaml(yaml_dir + g_default_styles_file)
    update_dict_recursive(styles, plan["styles"])

    s_widths = plan["stroke-widths"]

    for stylename in styles:
        style = styles[stylename]
        if "stroke-width" in style:
            if stylename in s_widths:
                star_number_assimilate(style, "stroke-width", s_widths[stylename])
            style["stroke-width"] *= stroke_scaling(plan)

    for name in plan["stroke-widths"]:
        assert name in styles, name
        assert "stroke-width" in styles[name], name

    plan.pop("stroke-widths")

    # mapping colors...

    for stylename in styles:
        style = styles[stylename]
        for key in [x for x in style if x in ["stroke", "fill"]]:
            if (
                style[key][0] != "#"
                and style[key] != "none"
                and style[key] not in HtmlColorsLowerCase
            ):
                style[key] = g_colors[style[key]]

            if (
                style[key][0] == '#'
                and len(style[key]) not in [1 + 6, 1 + 3]
            ):
                print("")
                print(f"You are punished: please put alpha channel in 'opacity' style field; Inkscape currently doesn't support your advanced ways: {stylename} -> {key} -> {style[key]}")
                print("")
                assert False

    display_attr = plan.setdefault("style-display-attributes", {})

    for stylename in display_attr:
        if stylename not in styles and stylename not in plan["styles"]:
            print("WARNING: adding unknown style '" + stylename + "' to plan['styles']")
            styles[stylename] = {}
        styles[stylename]["display"] = "block" if display_attr[stylename] else "none"

    plan.pop("style-display-attributes")

    plan["styles"] = styles

    assert "yellow-arrows" in styles


def flesh_out_stroke_widths(plan):
    plan.setdefault("stroke-widths", {})


def flesh_out_style_display_attributes(plan):
    sda = plan.setdefault("style-display-attributes", {})
    styles = plan.get("styles", {})
    defaults = {
        "grid": True,
        "subgrid": True,
        "viewbox": True,
        "labelbox": True,
    }

    for stylename in defaults:
        if stylename in sda and stylename in styles and "display" in styles[stylename]:
            assert (
                False
            ), f"!! display value for style '{stylename}' specified in styles and in style-display-attributes"

    for k, v in defaults.items():
        if stylename not in styles or "display" not in styles[stylename]:
            sda.setdefault(k, v)


def flesh_out_mock_display(plan):
    md = plan.setdefault("mock-display", {})
    md.setdefault("margin-top", g_display_margin_top)
    md.setdefault("margin-bottom", g_display_margin_bottom)
    md.setdefault("open-in-browser", True)


def flesh_out_flags(plan):
    flags = plan.setdefault("flags", {})
    flags.setdefault("area-blurbs-on-separate-layers", True)
    flags.setdefault("notch-labels-above-foreground", False)
    flags.setdefault("skip-acid-attack", False)


def flesh_out_scaling_factors(plan):
    plan.setdefault("yellow-arrows-scaling-factor", 1)
    plan.setdefault("yellow-arrows-stroke-scaling-factor", 1)
    plan.setdefault("stroke-scaling-factor", 1)
    plan.setdefault("font-scaling-factor", 1)
    plan.setdefault("axes-labels-font-scaling-factor", 1)
    plan.setdefault("notch-labels-font-scaling-factor", 1)
    plan.setdefault("annotations-font-scaling-factor", 1)
    plan.setdefault("non-font-scaling-factor", 1)
    plan.setdefault("non-font-non-stroke-scaling-factor", 1)
    plan.setdefault("non-stroke-scaling-factor", 1)

    if "global-scaling-factor" in plan:
        g = plan["global-scaling-factor"]
        plan["stroke-scaling-factor"] *= g
        plan["non-stroke-scaling-factor"] *= g
        plan.pop("global-scaling-factor")

    if "inner-graph-width-in-pixels" in plan:
        assert "non-font-non-stroke-scaling-factor-x" not in plan
        plan["non-font-non-stroke-scaling-factor-x"] = 1
        current_width_in_pixels = scaled_x_unit(plan) * lr(plan).length()
        factor = plan["inner-graph-width-in-pixels"] / current_width_in_pixels
        plan["non-font-non-stroke-scaling-factor-x"] = factor
        plan.pop("inner-graph-width-in-pixels")

    if "inner-graph-height-in-pixels" in plan:
        assert "non-font-non-stroke-scaling-factor-y" not in plan
        plan["non-font-non-stroke-scaling-factor-y"] = 1
        current_height_in_pixels = scaled_y_unit(plan) * bt(plan).length()
        factor = plan["inner-graph-height-in-pixels"] / current_height_in_pixels
        plan["non-font-non-stroke-scaling-factor-y"] = factor
        plan.pop("inner-graph-height-in-pixels")

    plan.setdefault("non-font-non-stroke-scaling-factor-x", 1)
    plan.setdefault("non-font-non-stroke-scaling-factor-y", 1)


def flesh_out_ox_oy(plan):
    for key, intv, names in zip(
        ("ox", "oy"),
        (plan["lr"], plan["bt"]),
        (("auto", "left", "right"), ("auto", "bottom", "top")),
    ):
        val = plan.setdefault(key, "auto")

        if val == names[0]:
            plan[key] = 0 if intv.contains(0) else intv.lo

        if val == names[1]:
            plan[key] = intv.lo

        if val == names[2]:
            plan[key] = intv.hi

        assert isinstance(plan[key], Real)


def flesh_out_lrbt(plan):
    def process_3rd_coordinate_as_margin(t):
        if len(t) == 3:
            t[0] -= t[2]
            t[1] += t[2]
            t.pop()

    print(f"graph_no ..., plan['lr']", plan['lr'])
    print(f"graph_no ..., plan['bt']", plan['bt'])

    for key, axis in zip(['lr', 'bt'], ['x', 'y']):
        plan.setdefault(key, 'auto')
        if plan[key] == 'auto':
            box = graph_data_bbox(plan)
            assert box.width() > 0
            plan[key] = box.min_max_along_axis(axis)

        process_3rd_coordinate_as_margin(plan[key])
        extensions = plan.setdefault(key + '-extend', [0, 0])
        plan[key][0] -= extensions[0]
        plan[key][1] += extensions[1]
        plan[key] = Interval(plan[key])


def flesh_out_thingies(plan):
    def_fname = "svg_mk_graph_output.svg"
    if plan["num-graphs"] > 1:
        def_fname = "svg_mk_graph_output_[graphno].svg"
    if "filename" not in plan:
        assert "path" not in plan
        assert "file-dir" not in plan
        plan["file-dir"] = "svg"
    plan.setdefault("filename", def_fname)
    star_number_initialize(plan, "cell", g_default_cell)
    plan.setdefault("x-middle-fudge-%", g_x_middle_fudge)
    plan.setdefault("number-middle-fudge-%", g_middle_number_fudge)
    plan.setdefault("layer-links", [])
    plan.setdefault("svg-type", "inkscape")
    plan.setdefault("pretty-printing", False)


def flesh_out_difference_arrows(plan):
    ha = plan.setdefault("horizontal-difference-arrows", [])
    va = plan.setdefault("vertical-difference-arrows", [])

    if len(ha) == len(va) == 0:
        return

    dac = plan.setdefault("difference-arrows-commonalities", {})  # WE SHALL LEAVE dac ALONE b/c IT'S CONFUSING OTHERWISE!

    standalone_defaults = {
        "include": True,
        "body-classname": "difference-arrows-bodies",
        "bookend-classname": "difference-arrows-bookends",
        "arrow-classname": "difference-arrows-tips",
        "arrow-width-in-exes": 1,
        "arrow-length-as-%-of-width": 0.75,
        "bookend-width-in-arrow-widths": 1,
        "tail-bookend-type": "full",
        "head-bookend-type": "full",
    }

    for a in chain(ha, va):
        for key in standalone_defaults:
            if key in dac:
                setfresh(a, key, dac[key])

            else:
                a.setdefault(key, standalone_defaults[key])

    for a in chain(ha, va):
        if "include-foreground-contours" in dac:
            setfresh(a, "include-foreground-contour", dac["include-foreground-contours"])

        else:
            a.setdefault(
                "include-foreground-contour",
                any(k.startswith("foreground-contour-") for k in a)
                or any(k.startswith("foreground-contour-") for k in dac),
            )

        if a["include-foreground-contour"]:
            if "foreground-contour-offset-in-page-cors" in dac:
                setfresh(a, "foreground-contour-offset-in-page-cors", dac["foreground-contour-offset-in-page-cors"])

            else:
                a.setdefault("foreground-contour-offset-in-page-cors", 1)

        if 'include-brace-labels-labelboxes' in dac:
            setfresh(a, "include-brace-label-labelbox", dac["include-brace-labels-labelboxes"])

        else:
            a.setdefault("include-brace-label-labelbox", False)


    brace_defaults = {
        "brace-label-additional-scaling": 1,
        "brace-sep-in-page-cors": 4,
        "brace-side": "bottom-right",
        "brace-ends": "mid",
        "brace-page-cor-body-thickness": underbrace_body_stroke_width_default,
        "brace-page-cor-mid-thickness": underbrace_tip_stroke_width_default,
        "brace-tip-length-over-body-thickness": underbrace_end_tip_length_over_body_width,
        "brace-mid-length-over-body-thickness": underbrace_midd_tip_length_over_body_width,
        "brace-label-sep-in-exes-for-horizontal-arrows": 2.5,
        "brace-label-sep-in-exes-for-vertical-arrows": 2.6,
    }

    brace_key_names = set([])
    for q in brace_defaults:
        brace_key_names.add("brace-label-sep-in-exes" if q.startswith("brace-label-sep-in-exes") else q)

    def brace_value_lookup(dIcT, key, arrow_type):
        adjusted_key = (
            key + f"-for-{arrow_type}-arrows"
            if key == "brace-label-sep-in-exes"
            else key
        )
        assert adjusted_key in brace_defaults
        return dIcT.get(adjusted_key, None)

    for i, a in enumerate(chain(ha, va)):
        if "include-braces" in dac:
            setfresh(a, "include-brace", dac["include-braces"])

        else:
            a.setdefault(
                "include-brace",
                any(k.startswith("brace-") for k in a)
                or any(k.startswith("brace-") for k in dac),
            )

        if a["include-brace"]:
            arrow_type = "horizontal" if i < len(ha) else "vertical"

            for key in brace_key_names:
                dac_value = brace_value_lookup(dac, key, arrow_type)
                if dac_value is not None:
                    setfresh(a, key, dac_value)

                else:
                    a.setdefault(
                        key, brace_value_lookup(brace_defaults, key, arrow_type)
                    )

            a.setdefault("brace-label", "")

    # finally the stroke widths that depend on classnames...
    for a in chain(ha, va):
        stroke_width = deepget(
            plan, "styles", a["bookend-classname"], "stroke-width", default=None
        )
        assert stroke_width is not None
        assert isinstance(stroke_width, Real)
        setfresh(a, "computed-bookend-stroke-width", stroke_width)

    # change flush-stroke classnames to stroke widths at all levels ("common sink")...
    for d in chain([dac], ha, va):
        for which in ["tail", "head"]:
            classname = d.pop(which + "-flush-stroke-classname", None)
            if classname is not None:
                stroke_width = deepget(
                    plan, "styles", classname, "stroke-width", default=None
                )
                assert stroke_width is not None
                assert isinstance(stroke_width, Real)
                setfresh(d, "computed-" + which + "-flush-stroke-width", stroke_width)

    # ...and transfer the defaults over
    for a in chain(ha, va):
        for key in [
            "computed-tail-flush-stroke-width",
            "computed-head-flush-stroke-width",
        ]:
            if key in dac:
                setfresh(a, key, dac[key])

    # hello, world
    for a in chain(ha, va):
        if a["include-foreground-contour"]:
            assert a["foreground-contour-offset-in-page-cors"] > 0
            for part in ["body", "bookend", "arrow"]:
                classname = a[part + "-classname"]
                stroke_width = deepget(
                    plan, "styles", classname, "stroke-width", default=None
                )
                plan["styles"].setdefault(
                    foreground_contour_classname_from_classname(classname),
                    {
                        "fill": "none",
                        "stroke": "#fdfdfd",
                        "stroke-width": stroke_width
                        + 2 * a["foreground-contour-offset-in-page-cors"],
                        "stroke-linecap": "square",
                    },
                )


def flesh_out_hand_dashes(plan):
    def is_coordinate_pair(thing):
        return (
            isinstance(thing, list)
            and len(thing) == 2
            and all(isinstance(x, Real) for x in thing)
        )

    if "hand-dashes" in plan:
        hd = plan["hand-dashes"]

        # incorporate commonalities
        hdc = plan.get("hand-dashes-commonalities", {})
        for key, value in hdc.items():
            for dashes in hd:
                dashes.setdefault(key, value)
        plan.pop("hand-dashes-commonalities", None)

        # get rid of 'type', compute end, start
        for dashed_line in hd:
            assert all(x in dashed_line for x in ["filename", "start", "end"])
            replace_file_dir_with_path(dashed_line)
            start = dashed_line["start"]
            end = dashed_line["end"]

            if dashed_line.get("type", None) == "vertical":
                if isinstance(start, Real):
                    assert is_coordinate_pair(end)
                    dashed_line["start"] = [end[0], start]

                elif isinstance(end, Real):
                    assert is_coordinate_pair(start)
                    dashed_line["end"] = [start[0], end]

            elif dashed_line.get("type", None) == "horizontal":
                if isinstance(start, Real):
                    assert is_coordinate_pair(end)
                    dashed_line["start"] = [start, end[1]]

                elif isinstance(end, Real):
                    assert is_coordinate_pair(start)
                    dashed_line["end"] = [end, start[1]]

            assert is_coordinate_pair(dashed_line["start"])
            assert is_coordinate_pair(dashed_line["end"])

            dashed_line.pop("type", None)

            dashed_line["start"] = Point(dashed_line["start"])
            dashed_line["end"] = Point(dashed_line["end"])

            dashed_line.setdefault("phase", 0)

    else:
        assert "hand-dashes-commonalities" not in plan or plan["num-graphs"] > 1


def flesh_out_pinholes(plan):
    pinholes = plan.setdefault("pinholes", {})
    holes = pinholes.setdefault("holes", [])
    include = pinholes.setdefault("include", True)

    pinholes.setdefault("cover-opacity", 0.6)
    pinholes.setdefault("hole-background-color", "rgb(237, 237, 233)")
    pinholes.setdefault("r", 0.5)

    assert is_css_color(pinholes["hole-background-color"])

    for hole in holes:
        assert "x" in hole or not include
        assert "y" in hole or not include

    if len(holes) == 0 or not include:
        plan.pop("pinholes")


def flesh_out_squarecap_paths_with_outlines(plan):
    sq = plan.setdefault('squarecap-paths-with-outlines', [])
    for z in sq:
        classname_setdefault(z, 'squarecap-path')
        classname_setdefault(z, 'squarecap-path-outline', key='outline-classname')


def flesh_out(plan):
    assert_matches_template(plan, g_template)
    flesh_out_thingies(plan)
    flesh_out_graphs_part1(plan)
    flesh_out_lrbt(plan)
    flesh_out_ox_oy(plan)
    flesh_out_scaling_factors(plan)
    flesh_out_viewbox(plan)
    flesh_out_flags(plan)
    flesh_out_axes(plan)
    flesh_out_stroke_widths(plan)
    flesh_out_annotations(plan)
    flesh_out_styles(plan)
    flesh_out_subgrid(plan)
    flesh_out_grid(plan)
    flesh_out_notches_and_their_labels(plan)
    flesh_out_style_display_attributes(plan)
    flesh_out_graphs_part2(plan)
    flesh_out_yellow_arrows(plan)
    flesh_out_fat_vector_arrows(plan)
    flesh_out_svg_fragments(plan)
    flesh_out_hand_dashes(plan)
    flesh_out_difference_arrows(plan)
    flesh_out_pinholes(plan)
    flesh_out_squarecap_paths_with_outlines(plan)
    assert_matches_template(plan, g_template)
    assert "mock-display" in plan
    assert "file-dir" in plan


###############################
# svg creation command center #
###############################


def plan2svg(plan):
    flesh_out(plan)
    print('in plan2svg, after flesh_out')

    for i in range(2):
        root = an_svg(plan["svg-type"])
        add_root_style(root, plan)
        add_styles(root, plan)
        root.get_or_create_defs()

        print(f"i = {i} in plan2svg, before add_trbl_clip_path")
        add_trbl_clip_path(root, plan)
        acid_victims = ["grid", "subgrid", "axes", "grid-thin"]
        if plan["flags"]["notch-labels-above-foreground"]:
            acid_victims.append("graph")
            acid_victims.append("graph-thin")

        print(f"i = {i} in plan2svg, before add_areas")
        add_areas(root, plan)
        add_beneath_grid_graphs(root, plan)
        add_subgrid(root, plan)
        add_grid(root, plan)

        print(f"i = {i} in plan2svg, before add_axes")
        add_axes(root, plan)
        add_axes_labels(root, plan)
        add_notches(root, plan)
        add_notch_hairlines(root, plan)
        add_annotations(root, plan)

        print(f"i = {i} in plan2svg, before add_notch_labels")
        add_notch_labels(root, plan, acid_victims)
    
        print(f"i = {i} in plan2svg, before add_above_grid_graphs")
        add_above_grid_graphs(root, plan)
        add_difference_arrows(root, plan)
        add_squarecap_paths_with_outlines(root, plan)
        add_pinholes(root, plan)
        add_hand_dashes(root, plan)
        add_fat_vector_arrows(root, plan)
        add_yellow_arrows(root, plan)
        add_svg_fragments(root, plan)

        print(f"i = {i} in plan2svg, before add_viewbox_attribute")
        add_viewbox_attribute(root, plan)
        add_viewbox_element(root, plan)
        add_width_and_height_attributes_to_root(root, plan)
        add_background_color(root, plan)

        print(f"i = {i} in plan2svg, before close_glyph_requests")
        num_glyph_requests = flush_MathLabel_queue()
        if num_glyph_requests > 0:
            if i > 0:
                print("alas, still had glyph requests at second iteration...")
                
        else:
            break

    return root


#####################################
# writing the file the mock display #
#####################################


def write_out(root, plan):
    def apply_layer_links():
        for link in plan["layer-links"]:
            source = link[0]
            target = link[1]
            assert "style" in source
            target.setdefault("style", {})
            target["style"]["display"] = source["style"]["display"]

    if "inkscape" not in plan["svg-type"]:
        root.remove_attribute("[string-startswith]inkscape", recursive=True)
        root.remove_attribute("[string-startswith]sodipodi", recursive=True)

    base_name = plan["path"] + plan["filename"].replace("[graphno]", str(plan["graph-no"]))

    if "[areano]" in base_name:
        area_layers = area_layers_of(root)
        for l in area_layers:
            l.update_attributes({"style": {"display": "none"}})
        for i in range(len(area_layers) + 1):
            if i > 0:
                area_layers[i - 1].update_attributes({"style": {"display": "block"}})
            final_name = base_name.replace("[areano]", str(i))
            apply_layer_links()
            root.pretty_up().print_to(final_name, announce=True)
            if i > 0:
                area_layers[i - 1].update_attributes({"style": {"display": "none"}})

        for l in area_layers:
            l.update_attributes({"style": {"display": "block"}})

        name = base_name.replace("[areano]", "all")
        apply_layer_links()
        root.pretty_up().print_to(name, pretty_up=plan["pretty-printing"], announce=True)
        return name

    else:
        root.pretty_up().print_to(base_name, pretty_up=plan["pretty-printing"], announce=True)
        return base_name


def replace_file_dir_with_path(d):
    assert "file-dir" in d
    if "file-dir" in d:
        short_dirname = d.pop("file-dir")
        d['path'] = luna_dir + short_dirname + '/'
        if not os.path.isdir(d['path']):
            print(f"\n************************************************")
            print(f"!!! please create directory {short_dirname} in luna/ !!!")
            print(f"************************************************\n")
            assert False
    assert "path" in d
    assert "/" in d["path"]


def dispatcher(plan):
    flesh_out_mock_display(plan)
    all_numbers = find_all_square_bracket_numbers(plan)
    assert all(isinstance(x, int) for x in all_numbers)
    if len(all_numbers) > 0:
        assert all_numbers == set(range(min(all_numbers), max(all_numbers) + 1))

    num_graphs = max(1, len(all_numbers))
    assert "num-graphs" not in plan or plan["num-graphs"] == num_graphs
    plan["num-graphs"] = num_graphs
    plan.setdefault("do-only", list(range(1, plan["num-graphs"] + 1)))
    if isinstance(plan["do-only"], Real):
        plan["do-only"] = [plan["do-only"]]
    if all(x in plan["do-only"] for x in range(1, plan["num-graphs"] + 1)):
        plan.pop("do-only")

    mda = []
    # svgs = []
    for i in range(num_graphs):
        graph_no = i + 1
        if "do-only" in plan and graph_no not in plan["do-only"]:
            print("\n(Skipping graph number %d.)" % graph_no)
            continue

        if num_graphs > 1:
            print("\n******************************")
            print("\nGRAPH NUMBER %d OF %d...\n" % (graph_no, num_graphs))

        pruned_copy = pruned_to_square_bracket_number(plan, graph_no)

        for key in pruned_copy:
            assert not key.endswith("[1]")

        replace_references(pruned_copy)
        assert "graph-no" not in pruned_copy
        pruned_copy["graph-no"] = graph_no
        assert_matches_template(pruned_copy, g_template)
        root = plan2svg(pruned_copy)
        assert "file-dir" in pruned_copy
        root.remove_unused_styles()
        root.remove_unused_definitions()
        root.promote_elements()
        root.remove_empty_elements()
        assert "file-dir" in pruned_copy
        replace_file_dir_with_path(pruned_copy)
        display_filename = write_out(root, pruned_copy)
        if pruned_copy["svg-type"] == "inline":
            display_filename = "inline->" + display_filename

        mt = pruned_copy["mock-display"]["margin-top"]
        mb = pruned_copy["mock-display"]["margin-bottom"]
        w = pruned_copy["mock-display"].get("width", None)
        h = pruned_copy["mock-display"].get("height", None)
        w, h = complete_width_height_pair_from_computed_viewbox(pruned_copy, w, h)

        mda.append((display_filename, mt, mb, w, h))

    print("\n******************************\n")

    print("\nlist of requested display files:")
    for m in mda:
        print("-", m[0])

    mock_multidisplay(mda, open=plan["mock-display"]["open-in-browser"])

# unverified:
DEFAULT_FILE = "yaml_ch2_scientific_grid.yaml"
DEFAULT_FILE = "yaml_x_squared_ch3_exercises_rainbow.yaml"
DEFAULT_FILE = "yaml_toy_maze_velocities.yaml"
DEFAULT_FILE = "yaml_x_squared_4_ch3.yaml"
DEFAULT_FILE = "yaml_curve_sketch_for_total_rise.yaml"
DEFAULT_FILE = "yaml_x_squared_B2A_exercise_ch3.yaml"
DEFAULT_FILE = "yaml_ch3_stretched_x2_factory_roof.yaml"
DEFAULT_FILE = "yaml_ch3_factory_stretched_closeup.yaml"
DEFAULT_FILE = "yaml_ch3_exercises_factory_roof.yaml"
DEFAULT_FILE = "yaml_simple_graph.yaml"
DEFAULT_FILE = "yaml_graphs_for_convex_concave.yaml"
DEFAULT_FILE = "yaml_raw_data_graph.yaml"
DEFAULT_FILE = "yaml_polaroids_first_pair.yaml"

# verified:
DEFAULT_FILE = "yaml_narrow_and_less_narrow_bends.yaml"
DEFAULT_FILE = "yaml_ch3_exercises_factory_roof_with_arrows.yaml"
DEFAULT_FILE = "yaml_chapter4_one_over_x.yaml"
DEFAULT_FILE = "yaml_polaroids_3x_plus_1_pair.yaml"
DEFAULT_FILE = "yaml_slope_one_half_see_saw.yaml"
DEFAULT_FILE = "yaml_ch4_vector_illustration_grid.yaml"
DEFAULT_FILE = "yaml_ch4_putative_function_for_scaling.yaml"
DEFAULT_FILE = "yaml_starfish_curves.yaml"
DEFAULT_FILE = "yaml_for_leibnizian_notation.yaml"
DEFAULT_FILE = "yaml_mosquito.yaml"
DEFAULT_FILE = "yaml_real_pacman_velocities.yaml"
DEFAULT_FILE = "yaml_ch3_f_x_h_illustration.yaml"
DEFAULT_FILE = "yaml_ch4_newton_quotient_illustration.yaml"
DEFAULT_FILE = "yaml_position_tryout.yaml"
DEFAULT_FILE = "yaml_various_units.yaml"
DEFAULT_FILE = "yaml_vx1178A.yaml"
DEFAULT_FILE = "yaml_position_by_time_REDONE.yaml"
DEFAULT_FILE = "yaml_line_with_bumps.yaml"
DEFAULT_FILE = "yaml_second_derivative_simple_bump_height_1.yaml"
DEFAULT_FILE = "yaml_graph_background_17_sinusoids.yaml"
DEFAULT_FILE = "yaml_cosine_curves_2x_ch4.yaml"
DEFAULT_FILE = "yaml_ch4_radius_3_circle.yaml"
DEFAULT_FILE = "yaml_cosine_curves_ch4.yaml"
DEFAULT_FILE = "yaml_exponential_curves_ch4.yaml"
DEFAULT_FILE = "yaml_ch4_circle_illustration_grid.yaml"
DEFAULT_FILE = "yaml_cosine_ch3_ex.yaml"
DEFAULT_FILE = "yaml_ch4_unit_circle.yaml"
DEFAULT_FILE = "yaml_ch5_unit_circle.yaml"
DEFAULT_FILE = "yaml_cosine_ch5_ch.yaml"
DEFAULT_FILE = "yaml_ch5_four_graphs.yaml"
DEFAULT_FILE = "yaml_cosine_ch5_ex.yaml"


g_default_styles_file = "yaml_default_graph_styles.yaml"

DEFAULT_COLOR_FILE = "yaml_color_palette.yaml"

g_yaml_in = sys.argv[1] if len(sys.argv) > 1 else DEFAULT_FILE
g_colors_in = DEFAULT_COLOR_FILE

g_plan = load_yaml(yaml_dir + g_yaml_in)
g_template = load_yaml(yaml_dir + "yaml_template_for_plan.yaml")
g_colors = load_yaml(yaml_dir + g_colors_in)
g_external_yamls = {}
g_dashsets = {}


if "only-in-fucking-template" in g_plan:
    print("attempt to execute template")

else:
    print("mk_graph.py has loaded", g_yaml_in)
    print("mk_graph.py has loaded", g_colors_in)

    dispatcher(g_plan)
    report_all()
