from lib_load_lUnAtIc import load_lUnAtIc_from_string
from lib_dirnames import luna_dir
from math import log

where_to_put_output = luna_dir

mathjax_pixels_roughly = 52.52  # recent measurements:
                                # 53.83788722783404

ex_height_at_20px = 7.9976  # other measurements:
                            # 7.9969
                            # 7.9979

outer_HTML = """
<svg xmlns:xlink="http://www.w3.org/1999/xlink" width="5.165ex" height="1.827ex" viewBox="0 -719.6 2223.9 786.8" role="img" focusable="false" style="vertical-align: -0.156ex;" aria-hidden="true"><defs><path stroke-width="1" id="E77-MJMAIN-35" d="M164 157Q164 133 148 117T109 101H102Q148 22 224 22Q294 22 326 82Q345 115 345 210Q345 313 318 349Q292 382 260 382H254Q176 382 136 314Q132 307 129 306T114 304Q97 304 95 310Q93 314 93 485V614Q93 664 98 664Q100 666 102 666Q103 666 123 658T178 642T253 634Q324 634 389 662Q397 666 402 666Q410 666 410 648V635Q328 538 205 538Q174 538 149 544L139 546V374Q158 388 169 396T205 412T256 420Q337 420 393 355T449 201Q449 109 385 44T229 -22Q148 -22 99 32T50 154Q50 178 61 192T84 210T107 214Q132 214 148 197T164 157Z"></path><path stroke-width="1" id="E77-MJMAIN-D7" d="M630 29Q630 9 609 9Q604 9 587 25T493 118L389 222L284 117Q178 13 175 11Q171 9 168 9Q160 9 154 15T147 29Q147 36 161 51T255 146L359 250L255 354Q174 435 161 449T147 471Q147 480 153 485T168 490Q173 490 175 489Q178 487 284 383L389 278L493 382Q570 459 587 475T609 491Q630 491 630 471Q630 464 620 453T522 355L418 250L522 145Q606 61 618 48T630 29Z"></path><path stroke-width="1" id="E77-MJMAIN-37" d="M55 458Q56 460 72 567L88 674Q88 676 108 676H128V672Q128 662 143 655T195 646T364 644H485V605L417 512Q408 500 387 472T360 435T339 403T319 367T305 330T292 284T284 230T278 162T275 80Q275 66 275 52T274 28V19Q270 2 255 -10T221 -22Q210 -22 200 -19T179 0T168 40Q168 198 265 368Q285 400 349 489L395 552H302Q128 552 119 546Q113 543 108 522T98 479L95 458V455H55V458Z"></path></defs><g stroke="currentColor" fill="currentColor" stroke-width="0" transform="matrix(1 0 0 -1 0 0)"><use xlink:href="#E77-MJMAIN-35" x="0" y="0"></use><use xlink:href="#E77-MJMAIN-D7" x="722" y="0"></use><use xlink:href="#E77-MJMAIN-37" x="1723" y="0"></use></g></svg>
"""

mathjax_scale = 1 / mathjax_pixels_roughly
root = load_lUnAtIc_from_string(outer_HTML)
root.cleanup_misc_mathjax_crap()
root.add_xmlns()
root.pop('style')
root.unfold_viewbox()

width_string = root.pop('width')
height_string = root.pop('height')

if width_string.endswith('ex') and height_string.endswith('ex'):
    width_in_exes_float = float(width_string[:-2])
    height_in_exes_float = float(height_string[:-2])
    final_width_in_outside_pixels = width_in_exes_float * ex_height_at_20px
    final_height_in_outside_pixels = height_in_exes_float * ex_height_at_20px
    # print("final width:", final_width_in_outside_pixels)
    # print("final height:", final_height_in_outside_pixels)
    mathjax_pixels_remeasured_1 = root.viewbox_width() / final_width_in_outside_pixels
    mathjax_pixels_remeasured_2 = root.viewbox_height() / final_height_in_outside_pixels
    print("mathjax_pixels_remeasured_1:", mathjax_pixels_remeasured_1)
    print("mathjax_pixels_remeasured_2:", mathjax_pixels_remeasured_2)

g = root.find('g')
g.unfold_transform_attribute()
g.left_compose_transform('scale', mathjax_scale)
root.scale_viewbox(mathjax_scale)
root.set_width_height_from_viewbox()
root.pretty_up().print_to(where_to_put_output + '1_browser_mathjax_cleaned.svg')
