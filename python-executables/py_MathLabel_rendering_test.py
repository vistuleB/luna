from lib_MathLabel_rendering import (
    MathLabel,
    get_or_queue_MathLabel,
    flush_MathLabel_queue,
    destroy_preexisting_MathLabel_rendering,
    MathLabel_rendering_exists
)

labels = [
    MathLabel("x^6", 20.5, 'default'),
    MathLabel("x^3.5", 20.5, 'default'),
    MathLabel("x^1.5", 20.5, 'default'),
    MathLabel("x^6", 20.5, 'STIX')
]

for label in labels:
    destroy_preexisting_MathLabel_rendering(label)

for label in labels:
    assert not MathLabel_rendering_exists(label)
    get_or_queue_MathLabel(None, label)

flush_MathLabel_queue()

for label in labels:
    assert MathLabel_rendering_exists(label)

print("\nall assertions passed 👌")
